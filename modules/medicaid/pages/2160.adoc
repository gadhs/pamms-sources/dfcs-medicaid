= 2160 Family Medicaid Overview
:chapter-number: 2100
:effective-date: July 2023
:mt: MT-70
:policy-number: 2160
:policy-title: Family Medicaid Overview
:previous-policy-number: MT 68

include::partial$policy-header.adoc[]

== Requirements

Family Medicaid provides Medicaid benefits for low-income families and individuals who are not receiving SSI and may or may not be receiving TANF.
Benefits are provided through a variety of classes of assistance (COA's), each with its own specific eligibility criteria.

Effective January 1, 2014, Family Medicaid will be comprised of two groups; the Modified Adjusted Gross Income (MAGI) COAs and the Non-MAGI COAs.

== Basic Considerations

Family Medicaid Non-MAGI Assistance Units (AUs) include:

* Newborn
* Family Medically Needy
* Pregnant Medically Needy
* Refugee
* Foster Care
* Adoption
* CHAFEE
* Women's Health Medicaid

{blank} +
Family Medicaid MAGI AUs include:

* Parent/Caretaker of child(ren)
* TMA/4Mex
* Pregnant Women
* Children Under 19 years of age
* Pathways
* Planning for Healthy Babies® (P4HB)
* Former Foster Care Medicaid

{blank} +
Family Medicaid AUs must meet specific Basic Eligibility Criteria:

* age
* application for other benefits
* citizenship/immigration status
* cooperation with DCSS
* enumeration
* identity
* residency
* third party liability
* specified relative relationship/tax filer/non-filer status

{blank} +
Each COA has different exceptions to the Basic Eligibility Criteria.
Refer to Section 2200, Basic Eligibility Criteria and to each COA in this chapter.

A pregnant woman who is eligible for and receiving Medicaid under any Medicaid COA (except Q-track, P4HB or a suspended spend down) or SSI on the date the pregnancy terminates is eligible to continue to receive the 12-month extended postpartum period.
The 12-month extended postpartum period count begins the month after the termination of pregnancy.
Medicaid continues through the last day of the 12^th^ month.

Medicaid coverage under any Medicaid COA (except Q-track, P4HB or a suspended spend down) or SSI is continued for a pregnant woman who, after approval becomes financially ineligible solely because of new income or a change in income of any BG member.
The pregnant woman remains eligible for Medicaid for the remainder of the pregnancy, including the 12-month extended postpartum period.
Refer to xref:2720.adoc[].

NOTE: If a pregnant woman meets spend down for at least one month during their pregnancy or in the pregnancy termination month, the pregnant woman is eligible for the 12-month extended postpartum period. Eligibility for any Family Medicaid COA can begin with the month of application and can include up to three months prior to the month of application.
All points of eligibility for that COA must be met in each of the three prior months.
Refer to xref:2053.adoc[].

NOTE: Pathways does not allow for retroactive coverage.

NOTE: Three months retroactive Medicaid does not pertain to January 2014 applications for MAGI COAs.

Under certain conditions, Medicaid may cover services rendered to Medicaid-eligible Georgia residents who are out of state when medical services are provided.
Procedures for qualifying for out-of-state coverage are found on the back of the Medicaid card.

=== Financial Eligibility Criteria

All Family Non-MAGI Medicaid cases are budgeted using prospective income and expenses.

All Family MAGI Medicaid cases are budgeted using prospective income.

[caption=Exception]
NOTE: Eligibility for three months prior Family Medicaid is determined using actual income and expenses.
If available, actual income may be used for intervening months.
Data sources and/or related active programs verification must be accessed for all MAGI Medicaid COAs prior to requesting verification.

For Family Non-MAGI Medicaid COA's, if resources of the BG are within the applicable resource limit at any time during a month, the AU is resource-eligible for that month.

NOTE: There are no resource requirements for MAGI Medicaid COAs.

A Family Medicaid case that is ineligible because of financial reasons for one month only is suspended, not terminated.
Refer to xref:2700.adoc[], xref:2712.adoc[], xref:2714.adoc[], xref:2715.adoc[] and xref:2716.adoc[].

MAGI COAs use the MAGI based income methodology.

== Other Considerations

Family Medicaid applications are accepted at the following sites:

* Federally Facilitated Marketplace (FFM)
* County DFCS offices
* DFCS Project outreach locations
* Department of Public Health offices
* Public medical facilities
* Federally funded health care centers
* Disproportionate-share hospitals
* Gateway

Refer to xref:2050.adoc[].

=== Presumptive Eligibility

Presumptive Eligibility (PE) is determined by Qualified Providers (QPs) and Qualified Hospitals (QHs) certified by the Department of Community Health (DCH).
PE is a temporary eligibility determination and is available to pregnant women, parent/caretakers with children under age 19, children under age 19, former foster care children and women in treatment for breast or cervical cancer.
If an individual is determined eligible for PE, a temporary Medicaid Certification is issued by the QP/QH.
A PE Packet is completed concurrently and routed to DFCS for processing.
Refer to xref:2050.adoc[], xref:2065.adoc[], xref:2067.adoc[] for additional information regarding PE.

== Procedures

The Medicaid application process begins with a request for assistance and ends with notification to the AU of the eligibility decision.
Refer to xref:2050.adoc[] for additional information.

A *Continuing Medicaid Determination* (CMD) is required before denying a Medicaid application or terminating Medicaid under the current COA.
A CMD is the determination of Medicaid eligibility under all COA's.
Refer to xref:2050.adoc[] and xref:2700.adoc[] for additional information.

At application and renewal, contact the A/R to inquire if any AU member is pregnant.
If an AU member is pregnant, Gateway will send a follow-up contact with the pregnant woman during the month prior to the Estimated Delivery Date (EDD).


=== Referrals

Health check services information for all Medicaid recipients under age 21 is referenced on Gateway notices.
Refer to xref:2930.adoc[] for additional information.

Refer the following to the Department of Public Health for services under the *Women, Infant and Children (WIC) Program*:

* pregnant women
* women who are breast feeding through the first twelve months after the birth of a child
* children under age five
* post-partum women for six months from the termination of pregnancy Refer to xref:2985.adoc[] for additional information.
