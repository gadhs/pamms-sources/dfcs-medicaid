= 2198 Women’s Health Medical Assistance
:chapter-number: 2100
:effective-date: January 2021
:mt: MT-63
:policy-number: 2198
:policy-title: Women’s Health Medical Assistance
:previous-policy-number: MT 57

include::partial$policy-header.adoc[]

== Requirements

The Breast and Cervical Cancer Prevention and Treatment Act of 2000 provide Medical Assistance coverage to women diagnosed and who need treatment for breast or cervical cancer and/or precancerous conditions of the breast or cervix.
This coverage is provided under the Women's Health Medical Assistance (WHM) class of assistance.

Presumptive WHM is a determination performed by a Qualified Provider (QP)/Qualified Hospital (QH) which includes the Department of Public Health (DPH), Federally Qualified Health Centers (FQHC), and Rural Health Centers (RHC).
Coverage is available prior to the Division of Family Children Services (DFCS).

== Basic Considerations

Effective July 1, 2001 the Department of Community Health (DCH) began implementation of the Women's Health Medical Assistance (WHM) class of assistance (COA) for women who have been screened under the Centers for Disease Control and Prevention (CDC) breast and cervical cancer early detection program (BCCEDP), established in accordance with the requirements of section 1504 of the https://www.law.cornell.edu/topn/public_health_service_act[Public Health Service Act], and found to need treatment for breast or cervical cancer;
section 1504 of the https://www.law.cornell.edu/topn/public_health_service_act[Public Health Service Act], including pre-cancerous conditions and early stage cancer.
This is a Non-MAGI Class of Assistance.
Refer to xref:2663.adoc[].

Public Health or one of its partner affiliates completes the breast and/or cervical cancer screening in accordance with the Center for Disease Control (CDC) guidelines established under Title XV.

To be eligible under the WHM COA an A/R must meet the following conditions:

* Screened for breast or cervical cancer under the CDC Breast and Cervical Cancer Early Detection Program in accordance with Title XV guidelines and diagnosed and found to need treatment for breast or cervical cancer or a precancerous condition of the breast or cervix.

* Have no creditable health coverage as defined in section 2704(c) of the Public Health Service Act, for treatment of the individual's breast or cervical cancer, including health insurance, Medicare Part A or Part B of Title XVIII of the Social Security Act and Medicaid.
+
[caption=Exception]
NOTE: There may be limited circumstances where the member has creditable coverage but is in a period of exclusion such as a pre-existing condition or where lifetime limits have been exhausted.
In these situations, the member is considered uninsured.

* Is under age 65

* Must be a Georgia resident.

* U.S. citizen or a lawfully admitted immigrant in the U.S.
+
NOTE: An A/R who does not meet the citizenship requirement may qualify for this COA using EMA criteria.
EMA is not completed as part of the PE process.
Refer to xref:2054.adoc[] for clarification on this policy.

* Biologically born a woman or Qualified Transgender

=== Definition-Qualified Transgender

Transgender men (female-to-male) may still receive cancer screenings if they have not had a bilateral mastectomy or total hysterectomy.

Transgender women (male-to-female) are only eligible if on female hormones for transition and should be in the process and/or completed treatment.

=== Income

In order to be considered for the WHM COA, the A/R's income must be at or below 200% of the Federal Poverty Level, as required by the Title XV program.
This screening of income is completed during the Presumptive Eligibility process by the local public health department or qualified providers.

=== Full Medicaid Determination

Applicant/Recipient approved for this COA is entitled to the full range of Medical Assistance covered services.
Eligibility for coverage ends when the A/R's course of treatment is completed or the member no longer meets eligibility requirements (for example, the member has attained the age of 65 or obtained creditable health coverage or the member becomes eligible under another Medical Assistance COA or over the 200% FPL).

Full eligibility determination begins the month of application if the member meets all eligibility criteria.
Retroactive Medical Assistance is available provided the A/R has an affirmative diagnosis of breast or cervical cancer or precancer and meets all other eligibility criteria in the prior month(s) requested.

=== Breast Reconstructions and Prostheses after Mastectomy RHA News Update – December 16, 2016

Women who have had a Mastectomy can receive reconstruction and prosthetic treatment.
Eligibility is based on clinical justification and medical necessity documentation by a physician, surgeon, or licensed medical professional.
The woman can decide at any time after her mastectomy to have reconstruction; there is no time limit to when she can have this done.
She is approved for a one-year post-surgery and will be subject to a yearly redetermination to continue eligibility.
A woman who is no longer in treatment, but later experience complications can reapply.
Eligibility will be determined by a second-level review completed by the State Office Department of Public Health and its designated staff.

== Procedures

=== Process/Implementation

This program involves the Department of Community Health, Division of Public Health, and the Division of Family and Children Services.
The eligibility determination is a two-pronged process consisting of a presumptive eligibility determination and a determination of eligibility for regular categories of Medical Assistance.

=== Presumptive Eligibility Process

Women who have received a diagnosis or suspect they have breast or cervical cancer must apply initially through the local Public Health Department or one of its partner affiliates such as Grady Hospital, Federally Qualified Health Centers (FQHC), and Rural Health Centers (RHC) will complete the breast and/or cervical cancer screening procedures in accordance with CDC guidelines established under Title XV.

If the woman meets all of the guidelines set forth by Title XV, Public Health will take a Presumptive Eligibility application.
This consists of completing an application, interviewing the woman, and determining eligibility in accordance with the basic eligibility criteria.

The PE period begins on the approved application date and ends when the woman is determined eligible or ineligible for regular Medical Assistance by DFCS;
however, no later than the end of the second month of presumptive.
The PE application date is the date that the application was entered into GAMMIS.
Once the application is determined for full medical assistance and is approved, the date will revert to the first date of the month.
If the applicant is denied, the case should closed in GAMMIS the same date the determination is made in Gateway.

As part of the Presumptive Eligibility determination process, health department personnel are required to complete the following forms which complete the application package:

* DMA-632W
* Certificate of Diagnosis
* Form 94 Medical Assistance Application
* Form 216 Citizenship Affidavit,
* DMA-285, Health Insurance Questionnaire on all applicants, if applicable
* DMA-634W, Notice of Action.

=== Encrypted Email

The application package is emailed and encrypted to womenshealth@dhs.ga.gov or fax 912-377-1134.
DFCS will send confirmation of receipt of all WHM packages received.

=== DMA 285

The DMA-285 is required on all applicants that have third-party liability coverage by the health department as part of the PE application.
Any coverage that pays the cost of cancer treatment would make her ineligible.
Limited scope coverage such as vision or dental coverage would not make her ineligible.
Note: The DMA-634W is completed if the application is denied.

=== Notice of Action

If the A/R is determined eligible, she will be given temporary Medical Assistance certification forms.
The member will have immediate access to health care and the full range of Medical Assistance covered services until the plastic Medical Assistance card is received.
The A/R is given a Notice of Action form DMA-634W when the temporary Medical Assistance Certificate does not print for approved PE WHM members advising of approval and a list of cancer specialists in her area.

=== Medicaid Card-CMO Assignment

If the application is approved, Public Health or its affiliate partners will enter the eligibility information directly into the Georgia Medicaid Management Information System (GAMMIS).
The A/R will receive a temporary Medical Assistance card until her ongoing eligibility is determined for full Medical Assistance.
She will receive a plastic Medical Assistance card within seven to ten days.
A/Rs that are eligible under this COA are assigned through passive enrollment to one of the Care Management Organizations (CMOs) serving their area.
The A/R has 90 days to change her CMO after the approval of her application.
If the member requires an immediate CMO change for emergency purposes.
The qualified provider or caseworker must submit a written request to DCH at mailto:lrussell2@dch.ga.gov[pecorrections@dch.ga.gov].
The member must be within her 90 days choice period for the change to take effect.
There are no co-payments in the WHM program.

If the A/R is determined to be ineligible for the program, Public Health gives a Notice of Action advising of ineligibility, an application for the State Cancer Aid Program, and a list of cancer specialists in their area.

=== Eligibility Determination

Public Health will forward to DFCS staff copies of all applications, approved or denied for review.
DFCS will determine the A/R's ongoing eligibility under the WHM COA or any other potential Medical Assistance COA such as Child Under 19 or Parent/Caretaker Medicaid.
If the A/R appears to be potentially eligible for another COA as listed above.

=== Parent/Caretaker and WHM

Women who apply for WHM COA and are approved may sometimes cascade to Parent Caretaker.
The AU must cooperate with DCSS in the attempt to obtain medical support from the absent parent (AP) unless Good Cause is established.
Refer to Policy 2162 – Parent/Caretaker with Child(ren).
An adult who does not cooperate with DCSS, without Good Cause, is penalized.
Refer to Section 2250, Cooperation with Division of Child Support Services.
In this case, when the woman is penalized, she can apply for WHM COA as long as she meets the criteria for eligibility;
there is no penalization for the non-cooperation of DCSS for WHM.

=== Review Process

WHM cases are reviewed for continuing eligibility one year after approval by the date of application.
Gateway sends the A/R a cover letter informing the member of her review.
Since there are some manual components to the WHM review, DFCS sends a review that includes the Physician's Statement of Treatment form and Form 173 for Proof of Income.
The member is given 30 days to return her information to DFCS.
Once the information is returned, DFCS completes the eligibility review.

=== Reassessment

During the WHM review, the Physician's Statement of Treatment form is completed for continued coverage.
However, some members' coverage ends when the doctor states the member is no longer in treatment.
There are times the doctor will indicate on the Physician's Statement of Treatment form that the member is in treatment, but writes on the form that the member is being treated for follow-ups or may have another type of cancer that is not considered as coverage under the Breast and Cervical Cancer Program.
When the doctor writes on the form this will require a second-level review by Public Health to determine if the member is eligible or ineligible.
DFCS must receive a written response from Public Health before action can be taken.
Once there is a decision made, the casework will act on case and document in Gateway of the decision made by Public Health.

=== Denial and Reapplication

If a WHM case has been denied less than 90 days or haven't received presumptive within the last two years, the A/R can reapply with DFCS using the form 94.
In addition to the form 94, the A/R must submit a Physician's Statement of Treatment form or a Certificate of Diagnosis.
Either of these forms must be less than 30 days old.
The RSM worker will screen in GAMMIS or Gateway to verify previous eligibility of WHM.
If the member does not show in GAMMIS or Gateway, a copy of the original presumptive WHM application must be obtained before the reapplication can be completed.

=== Budget Group Composition

In a WHM's budget group the spouse is always included and his or her income (exception: SSI spouse and children, which includes legally adopted and stepchildren living in the home are excluded and their income).
The member can choose to include or exclude her child (ren) from the budget group if income belonging to a child will affect the applicant eligibility.
If she includes her child(ren) or stepchild(ren) living in the home, she must include their income.
If she decides to remove the child(ren) from her BG in an attempt to become eligible; she will be reducing the size of her BG.
The child(ren) must be the biological, stepchild, or a legally adopted child of the member.
No other specified relative meets this budget group description for WHM.

image::2198-9.png[,456,272]

Determining who not to include in the WHM budget group:

* Other Parent's Children
* Other relatives living in the home
** Parents, siblings, nieces, cousins, etc.
** Specified Relative Relationship
* Child (ren) 19 years of age or older
* Child and/or spouse, living in the home who are receiving SSI (Supplemental Security Income).

Gateway will determine if a child(ren) should be included or excluded based on the information provided.
The goal is to try and make the woman eligible at the end of the exclusion or inclusion.

=== Treatment of Income

Please refer to ODIS Policy 2499.
Treatment of Income in Medical Assistance when determining what income should be considered and how it is counted in the case.

=== Hearing Rights

When a A/R is found to be ineligible for Women's Health Medical Assistance, the A/R is sent an appropriate notification.
Appeal rights are applicable when eligibility for continued Medical Assistance is denied.
Appeals and all inquiries pertaining to Women's Health Medical Assistance cases should be filed according to Appendix B-Hearings.
The Department of Community Health (DCH) must be notified of all WHM hearing requests and dispositions at pecorrections@dch.ga.gov.
Department of Public Health (DPH) must be notified in writing of all second-level hearing requests upon receipt of hearing being scheduled.
When there is legal counsel or legal representation of the opposing opponent, DCH legal personnel must be notified to determine if the state will require legal representation.

=== Reports

Presumptive WHM reports are available at https://www.mmis.georgia.gov.
