= 2830 AFDC Deductions
:chapter-number: 2800
:effective-date: November 2020
:mt: MT-62
:policy-number: 2830
:policy-title: AFDC Deductions
:previous-policy-number: MT 45

include::partial$policy-header.adoc[]

== Requirements

Deductions are applied to the AU's income to determine IV-E financial need for AFDC Relatedness eligibility.

== Basic Considerations

A $50.00 deduction is applied to child support income according to the following criteria:

* Prior to the gross income ceiling (GIC) test
* To the AU's total child support income whether received via DCSS or direct from the non-custodial parent (NCP)
* Whether the child support is reported untimely or timely.

Deductions are applied to earned income according to the following criteria:

* After the GIC test
* To the earned income of each employed individual
* Only to income that is reported.

Employed individuals include the following:

* Employed AFDC AU members
* Sanctioned individuals whose earnings are included in the AFDC budget.

== Earned Income Deductions

Earned income deductions include the following:

* $90 standard work expense
* $30 earned income deduction
* 1/3 of the remaining income
* Dependent care expenses.

Do not apply the above listed deductions to the income of individuals whose income is deemed to an AU through the responsibility budgeting process.
The deductions allowed in responsibility and deeming budgeting are unique.
Refer to Chapter 2661 – Responsibility Budgeting for allowable responsibility budgeting deductions.

Refer to xref:2835.adoc[] for instructions on how to apply the deductions.

== Processing

SHINES processes the AFDC Relatedness financial need budgeting.
Revenue Maximization Specialists (RMS) verify and validate the data for the AFDC Relatedness budgeting.
