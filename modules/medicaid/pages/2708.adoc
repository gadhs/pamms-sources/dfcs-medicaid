= 2708 ABD Medicaid Changes
:chapter-number: 2700
:effective-date: September 2024
:mt: MT-73
:policy-number: 2708
:policy-title: ABD Medicaid Changes
:previous-policy-number: MT 69

include::partial$policy-header.adoc[]

== Requirements

A change that occurs in the A/R's circumstances between renewal periods or a change in federal or state policy must be reviewed for its effect on ABD Medicaid eligibility and patient liability/cost share.

== Basic Considerations

Changes in an A/R's circumstances are to be reported to DFCS by the A/R or Personal Representative within 10 calendar days of the change.

Changes may be reported in any of the following ways:

* in person
* by telephone
* by mail
* by email
* by facsimile
* by Gateway “Report MY Change”
* by automatic system update

Action on all changes reported must be initiated by DFCS within 10 days of receipt of the report.
Using appropriate documentation standards, document when the change was received, and the required action completed.

There are two types of ABD Medicaid changes:

* financial
* non-financial

=== Financial Changes

Financial changes are those that affect an individual's or a couple's Medicaid eligibility due to a change in income and/or resources.

=== Non-Financial Changes

Non-financial changes are changes that may or may not affect eligibility but do require DFCS action to insure the continued receipt of correct benefits.

=== Continuous Eligibility
Effective January 1, 2024, children under the age of 19, including those in ABD COAs, will be provided 12 months of continuous eligibility (CE) coverage regardless of change in circumstances with certain exceptions. The exception to CE includes the following:

**	The child reaches age 19.
**	The child is no longer a Georgia resident
**	A voluntary request for closure.
**	The agency determines that eligibility was erroneously granted at the most recent determination, redetermination, or renewal of eligibility because of agency error or fraud, abuse, or perjury attributed to the child or the child’s representative; or
**	The child is deceased.

CE does not apply to:

** Medically Needy,
** Presumptive Eligibility,
** At renewal, children that are only eligible for Transitional Medical Assistance or
** Emergency Medical Assistance.

If a child becomes incarcerated during their CE period, then the child must remain eligible for the remainder of the CE period while incarcerated.

Example:  if a child under 19 is active on CCSP and during the POE an update to resources is discovered that would result in ineligibility for CCSP, the child cannot cascade to CU19/PCK due to this change since it is not at renewal.


=== National Voter Registration Act (NVRA) of 1993

The National Voter Registration Act (NVRA) of 1993 requires that DFCS is to provide a voter registration form to the A/R when an address change is reported in person, electronically, or via telephone, facsimile, or mail and would necessitate a change in the A/R's voting location.
Refer to xref:2980.adoc[Section 2980 - Voter Registration].
