= 2407 Qualified Income Trust
:chapter-number: 2400
:effective-date: September 2024
:mt: MT-73
:policy-number: 2407
:policy-title: Qualified Income Trust
:previous-policy-number: MT 71

include::partial$policy-header.adoc[]

== Requirements

Effective September 1, 2004, Qualified Income Trusts (QIT) become a viable means in Georgia by which an LA-D A/R may remove income from the Medicaid eligibility determination process.

== Basic Considerations

As of September 1, 2004, LA-D A/Rs whose income is equal to or greater than the Medicaid Cap may establish a QIT as an alternative by which they may receive Medicaid benefits by sheltering all or a portion of their income from the eligibility income limit test.

NOTE: Money entered into an LA-D case to increase the amount of the PL/CS due to a partial month penalty is not considered as income for purposes of a QIT.

=== To Qualify as a QIT

The trust must be established for the benefit of the A/R and must meet the following requirements:

* Be composed only of income of the A/R, such as pension, RSDI, VA, accumulated interest, etc.
* Be irrevocable.
A revocable QIT does not meet the criteria of a QIT and will be treated as a resource and other rules pertaining to OBRA '93 trusts.
Refer to xref:2337.adoc[Section 2337 - Trust Property - OBRA '93].
* Contain no resources.
** Money from an existing account cannot be placed in the trust or
** A non-liquid resource may not be placed in the trust or
** A non-liquid resource may not be converted to a liquid resource and placed in the trust

* Provide that at the A/R's death, the remainder of the trust will go to the Department of Community Health (DCH), up to the amount that was spent for the A/R's cost of care by Medicaid.
* The QIT may NOT be backdated.
It is effective beginning the month in which it is completed and signed by all required parties, not before.
* An A/R must establish a QIT in Georgia.
A QIT established in another state cannot be used to qualify for Georgia Medicaid.

=== Who May Establish a QIT?

The following people may establish a QIT:

* The A/R
* His/her court appointed guardian or court appointed conservator
* Attorney-in-fact if Power of Attorney gives that authority

NOTE: Anyone other than the A/R who establishes the QIT must present evidence of a POA, court appointed Guardianship or Conservator.

NOTE: The NH may serve as trustee but cannot establish a trust.

NOTE: A QIT may not be established for a couple, only for an individual.
Therefore, for couples in the same LA-D, if one or both members of the couple are over the individual Cap *and* also together, they are over the Couple Cap, a QIT must be done on the member(s) whose income exceeds the Individual Cap.

=== Treatment of Income

* Income placed in a QIT is NOT counted in determining income eligibility.
* The income cannot be placed into the QIT by direct deposit from the source of income, i.e., Social Security, etc.
However, the current month's income may be transferred from another bank account to the QIT account.

* Income placed in a QIT AND income not placed in the QIT is counted in determining the A/R's patient liability/cost share.
* All of the A/R's income may be placed in the QIT or just the excess over the Medicaid Cap.
It is recommended that all of the A/R's income be placed in the trust, if possible.
* Failure to properly and timely fund the QIT will result in a loss of eligibility for that month or the first month in which timely notice may be given.
Properly funded simply means that you have deposited, at minimum, the difference between the specified state income cap (minus a dollar) and the applicant's total income.

* Payment for the A/R's medical care (i.e., PL/CS) must be paid by the end of the month following the month, the income is received.
If payments are not made, the A/R is not following the provisions of the trust and is not eligible for Medicaid benefits.
Take action to close the Medicaid case for the first month that timely notice permits.
If payment is subsequently made prior to the expiration of the timely notice, Medicaid eligibility may continue without interruption.
* Other payments from the QIT (i.e., PNA, diversion) must be paid by the end of the month following the month in which it is received.
* Income retained by the A/R and not placed in the QIT is income to the A/R and is counted in determining income eligibility and PL/CS determination.
* Income may be diverted from the QIT or from retained income to the community spouse and/or to dependent child(ren) according to spousal impoverishment and diversion rules.
Refer to xref:2554.adoc[Section 2554 - Diversion of Income].

* All the rules pertaining to allowable deductions from the PL/CS determination still apply.
Refer to xref:2552.adoc[Section 2552 - Patient Liability/Cost Share Deductions].
If A/R has self-employment income only the net earnings from self-employment (NESE) should be placed in the QIT since expenses for operating a business are not acceptable disbursements from the QIT.
Refer to xref:2415.adoc[Section 2415 - Self-Employment].
The only income removed from the QIT should be for payment of the PL/CS to the facility, the Personal Needs Allowance (PNA), diversion to the community spouse/dependent child(ren), medical expenses of the community spouse or other medical expenses of the A/R not covered by Medicaid.
See “Rules for Funds Entering, Leaving and Remaining in a QIT” below.
* Any other payments made from the QIT may count as income to the A/R.
Payments made from the QIT for medical purposes are acceptable disbursements as long as such payments do not negatively impact the PL/CS payment.
However, if such income was already used for the PL/CS, it will not be considered income a second time.

=== Rules for Funds Entering, Leaving and Remaining in a QIT

Income placed in a QIT is exempt in determining income eligibility for Medicaid.
However, funds entering and leaving the QIT are not necessarily exempt from treatment for Medicaid.

* Transfer of assets penalties do not apply to income placed in a QIT to the extent that the trust instrument provides that income placed in the trust will be paid out of the trust for medical care provided to the A/R, including nursing home care, CCSP care and institutionalized hospice care, etc.
When such payments are made, the individual is considered to have received fair market value for the income placed in the trust, up to the amount of the PL/CS.

* When income remains in the QIT after the amount paid out of the trust for medical services or other items or services which benefit the A/R, the excess income is subject to the transfer of assets penalty (i.e., remaining income exceeds the Medicaid monthly billing rate of the facility).
Refer to xref:2342.adoc[Section 2342 - Transfer of Assets] and Appendix A1.

* Any income remaining in the trust that is not paid out for the A/R's medical care is held there to be recouped by DCH at the A/R's death.
The remainder income is not counted as a resource.

* Excess income (income above the PL/CS, diversion, and PNA) placed in a QIT may be transferred for the sole benefit of a spouse without incurring a penalty.
Refer to xref:2502.adoc#chart-2502-1[Section 2502 - Deeming (ABD), Chart 2502.1], for a definition of “sole benefit of”.
This may include payments by the QIT for medical care for the community spouse.
There is no transfer of assets penalty for assets transferred to a spouse or to a third party for the sole benefit of the spouse.
To be exempt, the QIT instrument must be drafted to require that this particular property can be used only for the benefit of the A/R's spouse while the QIT exists and that the QIT cannot be terminated and distributed to any other individual or entities for any other purpose.

* Inappropriate payments made from the trust (i.e., lawyer fees, mortgage payments, costs of doing business, etc.) will invalidate the trust.
Consider such payments as income to the A/R.
The A/R thus becomes ineligible because the guidelines of the trust are not being followed.
Such payments could only be made from non-QIT income if any remains after required payments.

* The amount of income deposited into the QIT may change as long as the amount of monthly income that does not go into the QIT does not equal or exceed the Medicaid Cap.

=== Who May/May Not Be the Trustee?

* Spouse may serve as trustee.
* Anyone named by the QIT who is willing and capable to act in that position may serve as trustee.
* NH may serve as trustee.
* The A/R may NOT act as his/her own trustee.

=== QIT Account

The QIT account should be a banking instrument from which funds can be paid out by the trustee, this could include:

* Checking accounts, money market accounts, etc. (preferably non-interest bearing and no fees attached)
* NH patient fund account if such account meets the guidelines of the QIT (composed only of income, irrevocable, no resources and remainder to go to DCH), or
* Banking account specifically for purpose of QIT (no commingled funds).

== Procedures

Follow the steps below to determine the treatment of a QIT:

[horizontal,labelwidth=10]
Step 1:: Give every A/R or authorized representative of an A/R whose income exceeds the Medicaid Cap the following handouts found in xref:appendix-f/index.adoc[Appendix F - TOC]:

* Form 328 - Qualified Income Trust (QIT) – A Guide for Trustees
* Form 412 - Qualified Income Trust (QIT) Worksheet
* Form 936 - Certification of Department of Community Health Approved Qualified Income Trust
* Copy of the QIT template
+
NOTE: It is not a requirement for an attorney to set up or certify a QIT.
But if an attorney sets up the QIT the attorney should certify the QIT with their State Bar Number.

Step 2:: If a QIT is established, obtain a copy of the QIT document, and compare to the QIT template.

* If the QIT is identical to the template, has signature of two witnesses and includes the Form 936 - Certification of DCH Approved QIT, proceed to <<step-3>>.
* If the QIT is identical to the template but does not include the certification form proceed to <<step-4>>.
* If the QIT differs from the template, proceed to <<step-5>>.

[[step-3]]Step 3:: For QITs which are identical to the template QIT and include the certification form, attach a Form 285, include the POA paperwork and submit to:
+
--
[%hardbreaks]
Georgia Department of Community Health
ATTN: Trust Unit
100 Crescent Centre Pkwy, Suite 1000
Tucker, GA 30384
Email: gatrustunit@gainwelltechnologies.com
Fax: 678-564-1169

NOTE: The Georgia Trust Unit is paperless and prefers all documents and information to be emailed. For questions, email using the above address or call 678-564-1168.

Upload a copy into Document Management.
Proceed to <<step-6>>.

--

[[step-4]]Step 4:: For QITs which are identical to the template QIT but do NOT include the certification form contact the A/R or PR to obtain one.
Once received, follow instructions in <<step-3>>.

[[step-5]]Step 5:: For QITs that differ from the template, attach a copy of the xref:attachment$form-947.docx[Form 947 - QIT Approved Format Deviation Form] from xref:appendix-f/index.adoc[Appendix F - TOC] explaining how the QIT differs from the template, include the POA paperwork and send to:
+
--
[%hardbreaks]
Georgia Department of Community Health
Attn: Trust Unit
100 Crescent Centre Pkwy, Suite 1000
Tucker, GA 30384
Email: gatrustunit@gainwelltechnologies.com
Fax: 678-564-1169



NOTE: NOTE: The Georgia Trust Unit is paperless and prefers all documents and information to be emailed. For questions, email using the above address or call 678-564-1168.

Include return mailing instructions.
Upload a copy of the QIT into Document Management.
Do not approve case until approval has been received from DCH Legal.
--
[[step-6]]Step 6:: Verify how much of the A/R's income is going into the QIT and that the income not put into the QIT is under the Medicaid Cap.

Step 7:: Deny the application due to excess income for any benefit month after August 2004 in which a QIT was not established *AND* funded prior to the end of the benefit month and the A/R has income equal to or above the Medicaid Cap.

Step 8:: Proceed with the eligibility determination process, including basic eligibility criteria, income/resource eligibility and PL/CS as applicable to the COA.

Step 9:: Review QIT every six months.
Refer to xref:appendix-f/index.adoc[Appendix F - TOC] for a copy of the xref:attachment$form-937.docx[Form 937 - QIT Review Letter].
One of the reviews may coincide with the annual review.

Step 10:: The six-month review will be an accounting to the Case Worker by the trustee or AREP of the activity of the QIT during this time period.
Obtain bank statements, receipts from nursing facilities or medical providers, documentation of payments to nursing facilities or medical providers etc.
of all activity in the account.

Step 11:: At any point (at the six-month review or any other time) that the Case Worker becomes aware that the trustee is not following the guidelines of the trust, the Case Worker should begin action to terminate the case.
However, the Case Worker or supervisor should contact their Medicaid Program Specialist before taking any such action.

[[step-12]]Step 12:: The QIT terminates only upon the death of the applicant, or upon the express written authorization and approval of DCH TPL unit.
When the A/R dies, leaves LA-D permanently or becomes ineligible (no COA to CMD) resulting in closure of any LA-D COA in which there is a QIT, the QIT trustee should take the following actions:
+
--
* Stop the deposit of funds into the QIT.
* Notify the county DFCS office and DCH for the reason for termination.
Write a check for the balance of the trust fund made payable to “Georgia Department of Community Health.” A copy of the bank statement should be enclosed to confirm that balance.
A cover letter or memo must include a brief explanation that the enclosed check is from a QIT.
The cover letter and check should clearly identify the A/R by name, SSN and/or Medicaid number.
* The above information should be sent to:
+
[%hardbreaks]
Georgia Department of Community Health
Attn: Trust Unit
100 Crescent Centre Pkwy, Suite 1000
Tucker, GA 30384
Email: gatrustunit@gainwelltechnologies.com
Fax: 678-564-1169

NOTE: The Georgia Trust Unit is paperless and prefers all documents and information to be emailed. For questions, email using the above address or call 678-564-1168.

* No other checks should be written from the QIT account after the individual's death.

The Case Worker should complete the following:

* Close the case allowing timely notice and/or accurate notice.
(Timely notice is not required if the closure is due to death.)
* Notify DCH's TPL Unit via a Form 285 of the event.
Print in red ink at the top of the form “QIT”.
Explain on the form the date and reason for the termination.
--

=== Change in Trustee

The provisions of the QIT should be followed to execute a change in the trustee.
The QIT should have provisions for a successor trustee.
The current trustee must do the following:

* Give 30-day written notice of resignation to the Grantor, each beneficiary and DCH Legal Services.
Such notice should clearly identify the A/R/s name, SSN and Medicaid number.

* The resignation may not be effective until 30 days from the date such notice was given.

* The resignation of the old trustee and the appointment of a new trustee must be in writing, witnessed (notarized) and attached to the QIT.

* Depending on the provisions of the trust, it may be necessary to have the old trustee, new trustee and the grantor or their guardian/POA sign the resignation and new appointment document.

* Attach a copy of the new trustee document with signatures, etc.
to the existing QIT and send to the Case Worker.

The Case Worker will complete a new Form DMA 285 to the above documents and send to DCH TPR.
See <<step-3>> for address.
Upload a copy into Document Management.


=== Income Now Under the Medicaid Cap

If an A/R who has had a QIT in place now has income under the Medicaid Cap, complete the following:

* The trustee should cease funding the QIT account.
* The trustee should close the QIT account if no funds remain in the account.
If funds remain in the QIT and the A/R or PR wants the account closed, then follow instructions in <<step-12>>.
* The MES should document the case regarding the change in income, etc.
and change the necessary info in the system to reflect the changes.
