= 2659 Contract Employees
:chapter-number: 2600
:effective-date: June 2020
:mt: MT-60
:policy-number: 2659
:policy-title: Contract Employees
:previous-policy-number: MT 1

include::partial$policy-header.adoc[]

== Requirements

Income received from contractual employment is averaged over a 12-month period if the income is not received on an hourly or piecemeal basis.

== Basic Considerations

Contractual employment is defined as working for a period of time less than a year.

Contract employees include truckers, certain school employees and others who contract to work on a renewable annual basis.

The contract renewal process may involve one of the following:

* signing a new contract each year
* automatic renewal of a contract
* implied renewal precluding the use of a written contract

Contract employees are considered compensated for an entire year, even during predetermined non-work periods such as summer breaks or vacations.

Income received by contract employees is considered compensation for a full year, regardless of the frequency of pay stipulated in the terms of the contract.

== Procedures

Follow the steps below to determine the eligibility of a contract employee:

Step 1:: Determine that the individual is a contract employee.

Step 2:: Determine the frequency of pay to calculate the monthly gross income.

Step 3:: Multiply the monthly gross income by the number of times received to determine the annual gross income.

Step 4:: Divide the annual gross income by twelve to determine the average monthly gross income.

Step 5:: Add the contract income to all other monthly income to determine the total gross monthly income.

Step 6:: Apply income deductions appropriate to the COA under which eligibility is being determined.

[caption=Exceptions]
NOTE: Do not apply the above budgeting procedures in the following situations:

* when payments are not made as specified in the contract
* a labor dispute interrupts the flow of earnings as specified in the contract.
