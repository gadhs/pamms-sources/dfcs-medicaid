= 2655 Family Medicaid Deductions
:chapter-number: 2650
:effective-date: September 2024
:mt: MT-73
:policy-number: 2655
:policy-title: Family Medicaid Deductions
:previous-policy-number: MT 57

include::partial$policy-header.adoc[]

== Requirements

Deductions are applied to the AU's income to determine financial eligibility for Family Medicaid Classes of Assistance (COA).
Deductions are applied to the AU's Modified Adjusted Gross Income (MAGI) for MAGI COAs, and to the total household income for Non-MAGI COAs.

== Basic Considerations

All Family Medicaid deductions are applied whether income is reported timely or untimely.
There is no penalty or loss of deductions for untimely reporting.

=== Pre-Tax Deductions MAGI Medicaid COAs Only

For each wage earner, there are certain deductions that are taken out of the taxable gross pay and considered pre-tax.
These include:

* Health Insurance*
* Dental Insurance*
* Vision Insurance*
* Legal Insurance
* Life Insurance Premiums
* Flexible Spending Accounts
* Deferred Compensation (including retirement plans such as 401(k), 403(b) and 457)

This is not an all-inclusive list.
These deductions can usually be found on an A/R's check stub in the before-tax deduction section.
These amounts are deducted prior to calculating the MAGI income amount.
Not all deductions on taxable income is considered a before-tax deduction.

*Medicare Part A, B, C and D are considered credible health coverage if being paid by the client, not State and should be allowed as a health insurance deduction when calculating budgets that include RSDI income. In addition, if the check stub does NOT specify that Health, Dental, and Vision insurance deductions are pre-tax, allow these deductions in the MAGI financial determination without additional verification.

=== 1040 Deductions MAGI Medicaid COAs Only

For MAGI COAs, deductions that are allowed on the IRS Form 1040 Schedule 1 are allowable deductions.
These include the following:

* Self-Employed Health Insurance
* Health Savings Account Contributions
* Student Loan Interest
* Tuition and Fees *(obsolete as of 01/01/2019)*

NOTE: Due to the Tax Cuts and Jobs Act of 2017, moving expenses are no longer deducted for tax years 2018 through 2025 except for active-duty service members of the Armed Forces and alimony is no longer considered a deduction or income as of 01/01/2019.
Divorces and separations finalized or modified before 01/01/2019, alimony is included as income and can be allowed as a deduction.

=== Verifications
Pre-tax and 1040 Deductions for MAGI Medicaid COA's must be verified to allow as deduction(s).  Pre-tax deductions can be most commonly verified with check stubs but could also be verified with a note from the employer's HR department, especially for new hires.  1040 deductions are verified with 1040 and Schedule 1 to differentiate the specific deductions.

=== MAGI Income Deductions

For MAGI income, a deduction of 5% of the 100% FPL for the budget group is allowed.
This amount is taken off the total Modified Adjusted Gross Income (MAGI limit) for the total budget group.
Please refer to Appendix A-2 for the deduction amounts for the budget group size.

=== Self-Employment

MAGI income deductions are applicable to the earnings of self-employed individuals.
Once the countable gross income is determined by deducting the cost of doing business from gross receipts, the MAGI income deductions can be allowed.
All IRS allowable deductions should be subtracted from the gross income to calculate the MAGI income.
Refer to xref:2415.adoc[Section 2415 - Self-Employment].

[.underline]#*NON-MAGI COAs*#

=== $50 Child Support Deduction

A $50 deduction is applied to any child support income received by the Assistance Unit (AU), whether received through the Division of Child Support Services (DCSS) or directly from the non-custodial parent.

The deduction is applied to the AU's total child support income in all Non-MAGI Family Medicaid COAs.

NOTE: Child support and alimony payments made by a BG member cannot be deducted in determining the net countable income of the BG.

=== Earned Income Deductions

Earned income deductions are applied to the earned income of each employed BG member who is eligible for the deductions.

Potential earned income deductions include the following and are deducted from the gross countable income in the order listed:

* $90 standard work expense for each employed individual
* $30 deduction for each employed individual *(obsolete after 1/1/14)*
* 1/3 of the remaining earned income for each employed individual *(obsolete after 1/1/14)*
* dependent care expenses for each child or incapacitated individual Earned income deductions are applied to the earned income of the following individuals:
** BG Members
** penalized individuals

Earned income deductions are applicable to the earnings of self-employed individuals.
Once the countable gross income is determined by deducting the cost of doing business from gross receipts, the earned income deductions can be allowed.
Refer to xref:2415.adoc[Section 2415 - Self-Employment].

== Procedures

=== $50 Child Support Deduction

Use the following procedures to apply the $50 child support deduction.

* Establish the paternity of the child before allowing the deduction.
Refer to xref:2640.adoc[Section 2640 - Paternity].
+
NOTE: If paternity cannot be established, budget the money paid as a contribution.
The $50 CS deduction does not apply.

* Apply the $50 CS deductions prior to the FPL test for PCT and COAs based on PCT.

* Apply only one $50 CS deduction to the total monthly amount of CS received by the BG, regardless of the number of non-custodial parents paying CS.

* Apply the $50 CS deduction to a lump sum CS arrearage payment only in the month the arrearage is received.

=== $90 Standard Work Expense

Deduct the first $90 of the earnings of each employed individual in the BG, whether employed full or part time.

=== Dependent Care Deductions

Allowable dependent care deductions include the expenses incurred and paid by an AU or BG member for childcare or for care of an incapacitated individual in the home when the care is necessary because of the employment of an AU or BG member.
If there is more than one adult in the AU/BG and only one adult is employed, the A/R must provide verification of why the unemployed adult is unable to care for the child/ren in order to be eligible for the deduction.
Acceptable verification includes a medical statement, proof that adult is attending school, etc.
Failure to provide verification will result in the A/R not receiving the deduction.
Any portion of dependent care expenses that is paid for or subsidized by another agency or individual is not considered an allowable deduction.

Dependent care deductions are allowed as follows:

* for an individual under two years of age, the lesser of the actual cost or $200 monthly
* for an individual age two or older, the lesser of the actual cost or $175 monthly

NOTE: A child is considered to be age two the month following the month of the second birthday.

Verification of dependent care expenses is required only if the information provided by the A/R conflicts with information known to the agency or is otherwise questionable.

Document dependent care expenses as follows:

* the employment status which entitles the BG to a dependent care deduction
* the name of the individual(s) for whom dependent care is paid
* the frequency and date/day of week paid
* the name of the person to whom the expense is paid
