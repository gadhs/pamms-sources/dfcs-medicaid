= 2751 SSA Medicare Savings Programs Applications
:chapter-number: 2700
:effective-date: June 2021
:mt: MT-64
:policy-number: 2751
:policy-title: SSA Medicare Savings Programs Applications
:previous-policy-number: MT 38

include::partial$policy-header.adoc[]

== Requirements

The *Medicare Improvements for Patients and Providers Act of 2008 (MIPPA*) was enacted on July 15, 2008.
MIPPA includes the following provisions that effect policy and procedures for Medicare Savings Programs (MSPs).
MSPs include Qualified Medicare Beneficiary (QMB), Specified Low Income Medicare Beneficiary (SLMB), Qualifying Individual (QI-1) and Qualified Disabled Working Individual (QDWI).

Effective January 1, 2010, MIPPA requires the Social Security Administration (SSA) to electronically transmit data from Low Income Subsidy (LIS) applications, both approved and denied, to the State Medicaid agency for the purpose of determining eligibility for MSPs and *other Medicaid classes of assistance.*

== Basic Considerations

On January 1, 2010, the Department of Community Health (DCH) began accepting LIS application data daily from SSA.
The applicants will be screened on GA Gateway for a matched individual.
When a matched individual is not found in GA Gateway, an application is registered with a task generated to determine Medicaid eligibility.
When a matched individual is found and not already receiving Medicaid in GA Gateway, an application is registered if applicable and a task generated or a program request task generated to determine Medicaid eligibility.
When a matched individual is found and already receiving Medicaid, an application is registered, and the application number withdrawn or denied.
DFCS will not receive the actual LIS applications, but the LIS information provided by SSA interface should be processed according to existing Medicaid policy.

*Refer to Section 2146, Low Income Subsidy, for completing the Low-Income Subsidy Application (LISA), and refer to Section 2931, Medicare Part D and Low-Income Subsidy for more information on Medicare Part D.*

== Procedures

Follow the steps below upon receipt of the Medicare Savings Programs Applications list.

Step 1:: Register the application if applicable.
Do *not* require a separate signed Medicaid application.
The signature on the LIS application filed with SSA will be considered a valid signature to apply for Medicaid.
The application date will be the date the LIS application is filed with SSA.
However, the date SSA transmits the LIS data file to GA Gateway will be regarded as the beginning date for determining whether cases were completed timely.
Cases should be completed within 10 business days of the transmission date for Q-track processing.

Step 2:: Using LIS Interface, LIS Information from SSA PDF, SDX/BENDEX, DOL, Vital Records, related cases, and *any other available information*.
Determine eligibility for Qualified Medicare Beneficiary Track Medicaid or a COA that provides a higher level of coverage if eligible.
In the absence of evidence to the contrary, assume all other eligibility criteria have been met.

*NOTES:*

* The LIS address data will only contain a mailing address with the zip + 4 digit zip code which may not be the same as the residential address.
This may require follow-up with the AU to obtain residential address and additional contact information.

* The income data from the LIS applications is combined for married couples and may not specify individual income.
The income may be the result of self declaration, a direct match from the Internal Revenue Service (IRS), or some other source and may be accepted as verification for all Q-track COAs unless questionable.

* The resources data from the LIS applications is also combined for married couples and may not specify individual resources.
The resource amount may be the result of self-declaration or some other source and may be accepted as verification for all Q-track COAs unless questionable.

* The income and resources data from the LIS application do *not* meet verification standards for other Medicaid classes of assistance and will have to be verified by third party documentation if the A/R appears eligible under another COA (see Section 2051-1, Verification, of the Medicaid Policy Manual).

* TPR – Social Security Administration does not address assignment of third- party rights in the LIS application.
TPR for these applications will have to be addressed in accordance with Section 2230 of the Medicaid policy manual.

Step 3:: Send a Verification Checklist or a DHR 700 form along with the Medicare Savings Programs for Information (MIPPA Cover Letter) to request additional information or verification when required.
The MIPPA cover letter is available as a MS Word form template.
Simply double click the template to open +
it in MS Word.
The date at the top of the form will be set when the form is saved.
After saving the completed form, reopen and print so that the date will print correctly.

Step 4:: Eligibility for SLMB, QI1, QDWI and other Medicaid aid categories are to be made retroactive to the month of the LIS application date and, if appropriate, three months prior.

Step 5:: Eligibility for QMB begins the month *following* the month of the LIS application *unless* the disposition cannot be completed because of *applicant delay*.
If the disposition cannot be completed within the standard of promptness because of applicant delay the eligibility should begin the *month following the month of case disposition*.
If the delay is due to agency or other agency use the QMB override feature to not penalize the A/R for any month(s) of ineligibility (see +
to Section 2143-4 Step 6 in the Medicaid Policy manual).
