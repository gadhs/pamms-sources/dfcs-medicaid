= 2559 Patient Liability / Cost Share Budgeting
:chapter-number: 2550
:effective-date: December 2022
:mt: MT-68
:policy-number: 2559
:policy-title: Patient Liability / Cost Share Budgeting
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

A patient liability/cost share budget is completed on all Medicaid recipients in a nursing home (NH), Institutionalized Hospice, CCSP/EDWP, ICWP or NOW/COMP.

== Basic Considerations

A patient liability/cost share budget is completed at the following times:

* at approval of the application
 ** to calculate the patient liability for the first month of eligibility
 ** to remove the protection of income deduction
* when a change in income occurs
* when a change in incurred medical expenses (IME) occurs
* at the beginning of each new averaging period.

== Procedures

Follow the steps below to complete the patient liability.

[horizontal,labelwidth=10]
Step 1:: Determine the amount of the A/R's income to divert to his/her spouse/dependents at home.

* Refer to xref:2554.adoc[] for the correct maintenance need standard to use.
* Use the ADJUSTED GROSS income of the spouse/dependents, including In- Kind Support and Maintenance (ISM) received by the spouse/dependents.
Refer to xref:2430.adoc[].

Step 2:: If the recipient is Medicaid eligible under the Nursing Home, CCSP/EDWP, Institutionalized Hospice, ICWP or NOW/COMP classes of assistance, use average income and IMEs in the patient liability budget.
Refer to xref:2557.adoc[].
Proceed to Step 3.

Step 3:: Calculate patient liability/cost share.
The PL/CS should never exceed the monthly Medicaid billing rate for the facility in which the A/R resides.
Complete Section C of Form 968 if a manual budget is used to calculate patient liability/cost share.

* Refer to xref:2552.adoc[] for information on the deductions subtracted.
* Refer to xref:2418.adoc[] for information on VA Aid and Attendance payments in the budget.

== Special Considerations

=== CCSP/EDWP to Nursing Home

Calculate a CCSP/EDWP cost share for the month a recipient enters a nursing home from CCSP.
There is no patient liability for the month of nursing home admission.

=== Nursing Home to CCSP/EDWP

Re-calculate the nursing home patient liability for the month an A/R goes into CCSP/EDWP from a nursing home using the FBR as the PNA.
There is no cost share for the month of admission to CCSP.

=== A/R's Income Exceeds the Medicaid Cap

If the A/R's income is equal to or greater than the Medicaid Cap and the A/R has not established a Qualified Income Trust (QIT), the A/R is not eligible for the nursing home COA.
If A/R is eligible under another COA, such as AMN there will be no vendor payment made to the NH nor payments for LTC services (LA-D Providers) on his/her behalf.

A/Rs who establish a QIT may meet the income eligibility requirement based on the income not placed in the QIT.
However, the PL/CS calculation uses income the A/R receives and the income placed in the QIT.
Refer to xref:2407.adoc[] for further instructions.

=== Transfer from out of state Nursing Home to Georgia Nursing Home

When an A/R transfers directly from an out-of-state nursing home to a nursing home in Georgia, calculate the patient liability for the month of admission using the actual payment made to the out-of-state nursing home as an IME.
