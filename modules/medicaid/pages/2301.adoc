= 2301 Family Medicaid Resources Overview
:chapter-number: 2300
:effective-date: July 2023
:mt: MT-70
:policy-number: 2301
:policy-title: Family Medicaid Resources Overview
:previous-policy-number: MT 59

include::partial$policy-header.adoc[]

== Requirements

Resources are assets available to the Assistance Unit (AU) that can be converted to cash to meet daily living expenses.
These assets must be taken into consideration in determining eligibility.

== Basic Considerations

=== Resource Limit

Resource limits are set by federal or state law.

The resource limit for the following Family Medicaid Classes of Assistance (COA) is $1000 per AU:

* Child Welfare Foster Care (CWFC)
* State Adoption Assistance (SAA)

Parent/Caretaker Medicaid, Pregnant Woman Medicaid, Child Under 19 Medicaid, Newborn Medicaid, TMA, Four Months Extended Medicaid (4MEx), PeachCare for Kids®, and Pathways have no resource limit.

The Family Medicaid Medically Needy (FM-MN) resource limit increases based on the number of individuals in the Budget Group (BG), as follows:

.Number of Individuals in FM-MN BG
[cols=8*^]
|===
| 1
| 2
| 3
| 4
| 5
| 6
| 7
| 8

| $2000
| $4000
| $4100
| $4200
| $4300
| $4400
| $4500
| $4600
|===

The FM-MN resource limit increases by $100 for each additional BG member.

=== Consideration of Resources

All countable resources available to the AU are applied to the resource limit of the Family Medicaid COA.

If the total countable resources are less than or equal to the resource limit, the AU is eligible based on resources.

If the countable resources exceed the resource limit for a Family Medicaid COA, the AU is ineligible for that COA.

For Family Medicaid COAs, if resources of the BG are within the applicable limit at any time during a month, the AU is resource eligible for that month.

Eligibility based on resources is determined by resolving the following questions:

* Whose resources are considered?
* Who owns the resource?
* Is the resource available to the AU to meet its needs?
* Is the resource countable?
* What is the value of the resource?
* What is the resource limit in the program for which assistance is requested?

Resources are considered liquid or non-liquid and are described as follows:

* Liquid resources are those such as cash or bank accounts which can be converted to cash and are available for daily living expenses.
* Non-liquid resources are those such as property or vehicles which cannot be easily converted to cash.

Resources available to an AU are used to determine eligibility at the following times:

* application
* review
* when the agency becomes aware of a change.

The countable resources of the following individuals are used to determine eligibility:

* eligible AU members
* ineligible aliens
* penalized individuals
* ineligible parents.

NOTE: A portion of the resources of the sponsor of a sponsored alien is used to determine eligibility.

=== Ownership of Resources

It is assumed that a resource belongs to the individual in whose name it is listed unless the AU can prove otherwise.

The burden of proof in establishing that a resource does not belong to an individual rests with the AU.

Convincing evidence such as the following must be provided to rebut ownership:

* statements from other individuals in a position to substantiate the AU member's claim
* legal documents substantiating the claim.

=== Jointly Owned Resources

A resource that is jointly owned with a non-AU or non-BG member is considered available to the Family Medicaid AU or BG in its entirety if the following conditions apply:

* The AU or BG has the right to dispose of the property.
* The AU or BG can dispose of the property without the consent of the owner.

A resource which is jointly owned with a non-AU or non-BG member is excluded if all of the following apply:

* The resource cannot be practically subdivided.
* Access is dependent on the agreement of the other owner.
* The joint owner states in writing that s/he is unwilling to dispose of the resource.

A portion of a jointly owned resource is included if the AU or BG has access to and may dispose of a portion of the resource.

If a resource is owned by individuals receiving Medicaid in different AUs, the resource is considered available to each owner in equal shares.

The balance of a jointly owned bank account is divided among the individual owners.

[caption=Exception]
NOTE: In the event an AU or BG member is named on a joint bank account with a non-AU or non-BG individual solely for convenience or emergency, the joint account is excluded as a resource to the AU or BG member if the other individual, or someone in a position to know verifies that s/he has deposited all the monies in the account and all withdrawals are used for the non-AU or non-BG individual's benefit.

A resource is considered available when the AU or BG has the legal right to liquidate the resource and to use the proceeds.

=== Accessibility of Resources

Resources that are inaccessible to the AU or BG or which AU or BG cannot legally liquidate are excluded.

Examples of excluded resources include the following:

* security deposits on rental property or utilities
* property in probate
* real estate which the AU or BG is making a good faith effort to sell
* resources jointly owned by women and/or children in shelters for victims of domestic violence and their former AU or BG members if access is dependent on the agreement of the joint owner
* money placed in an account for AUs residing in public housing or receiving Section 8 assistance and participating in the Family Self-Sufficiency Program as long as the AU does not have legal access to the money.

NOTE: This list is not all inclusive.

=== Bankruptcy

Bankruptcy is a condition whereas a debtor, either voluntarily or invoked by a creditor, is judged legally insolvent and the debtor's remaining property is administered and distributed to his/her creditors.

The AU's resources are included or excluded depending on their accessibility and the AU's ability to liquidate the resource and retain the proceeds.

=== Countable Resources

Refer to xref:2399.adoc[].

Excluded income that is retained as a resource the month following the month the income was received is counted as a resource.

Only those resources that are available to the AU at the time that eligibility is determined are counted.

=== Commingled Resources

Excluded resources may be commingled with countable resources.
The excluded resources retain its exclusion for six months from the date the resources were commingled.
Beginning in the seventh month following the commingling of funds, the asset's value is counted in its entirety.

=== Conversion of Resources

In the event an excluded resource is converted to a countable resource, the value of the resource is applied to the appropriate resource limit in the month the resource is converted.

[caption=Exception]
NOTE: Proceeds from the sale of capital goods are considered income.
Refer to Section 2499.1, Treatment of Income by Type.

If a countable resource is converted to cash, the value of the cash is countable toward the appropriate resource limit.

=== Money Received for the Replacement/Repair of a Resource

Money received from a third party, such as an insurance company that is intended to cover the replacement or repair of a resource is excluded based on the guidelines below:

* The amount that is used for the replacement or repair of the resources is excluded.
* The money must be used or contracted to be used for the repair or replacement of the resource within 6 months of receipt.

Any amount not used for the specific replacement or repair is considered income to the AU.
Any unused amount that exceeds the FPL is budgeted as a lump sum in the month received.

=== Determining the Value of a Resource

The most current available information is used to verify the value of a resource in determining eligibility.

Sources which may be used to determine value include the following:

* bank records
* deeds
* property records
* tax records
* appraisals
* tag receipts
* insurance policies
* stock quotes
* statements from individuals in a position to verify the value of a resource.

NOTE: This list is not all inclusive.

=== Determining Appreciation/Depreciation

The appreciation or depreciation of a non-liquid resource is considered in determining the value of the resource.

Appreciation is an increase in the value of a resource due to of any of the following:

* improvements to the property
* normal marketing increases
* interest accrued

Appreciation is determined by obtaining verification from a knowledgeable source.
Depreciation is a decrease in the value of a resource due to any of the following:

* normal use of the resource
* destruction of property in a storm, fire or other casualty
* marketing decreases.

Depreciation is determined by obtaining verification from a reliable source.

=== Resource Value

The value of a resource is determined by using one of the following:

* cash value (CV)
* fair market value (FMV)
* equity value (EV)

==== Cash Value

Cash value is the amount available to the AU if the resource is converted to U.S. funds.
In some cases, a penalty may be applied for early withdrawal of funds.
The amount of the penalty is deducted from the value of the resource to determine the cash value available to the AU.

==== Fair Market Value

Fair market value is the amount that the item can sell for on the open market in the geographic area involved.

==== Equity Value

Equity value is the FMV less legal debts, liens or other encumbrances.

Proof of this legal debt, lien or encumbrance must be in writing and signed by the property owner.
It must specify the location of the property and the amount of the debt.

If the owner has financed the purchase of a resource with a loan, the current payoff of the loan must be verified by the lender to determine indebtedness.

=== Transfer of Resources

A transfer of resources includes selling, swapping, trading, or giving away a countable resource for less than the FMV.

In Family Medicaid Classes of Assistance (COAs), there is no penalty for transferring resources.
Only resources owned by the AU at the time of the eligibility determination are considered.

== Procedures

=== Determining Eligibility on Resources

Follow the steps below to determine whether or not the AU or BG meets the resource limit:

[horizontal,labelwidth=10]
Step 1:: Determine whose resources must be considered.
Step 2:: Determine if the resource is available to the AU/BG.
Step 3:: Determine if the resource must be counted.
Step 4:: Calculate the total countable resources.
+
--
If the total countable resources are less than or equal to the resource limit, the AU/BG meets the resource criteria for that COA.

If the total countable resources exceed the resource limit, deny or terminate benefits.
--

== Verification

Verify the following resources at application, review, or when a change occurs:

* jointly owned property
* real property (excluding homeplace)
* all resources when the total liquid and non liquid value exceeds 75% of the applicable resource limit
* when interest paid from a resource totals $10.00 or more a month
* vehicles (Refer to xref:2308.adoc[].)

For all other countable resources, accept the AU member's statement of type and value unless the information provided conflicts with other information available to the agency.
