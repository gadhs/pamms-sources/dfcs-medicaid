= 2552 Patient Liability/Cost Share Deductions
:chapter-number: 2550
:effective-date: December 2022
:mt: MT-68
:policy-number: 2552
:policy-title: Patient Liability/Cost Share Deductions
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

The Medicaid recipient who is required to contribute toward the cost of care is allowed specific deductions in the patient liability/cost share budget.

== Basic Considerations

The patient liability/cost share is determined by using the recipient's gross income and allowing the following deductions:

* Mandatory Income Deductions (See “Mandatory Deductions” below)
 ** FICA
 ** Federal Withholding Tax
 ** State Withholding Tax
 ** Mandatory Insurance
* Medicare Premium (See “Medicare Premium Deduction” below.)
* Protection of Income
* Personal Needs Allowance
* Diversion of income to the following individuals:
 ** Community Spouse
 ** Dependent Family Member
 ** Non-legal spouse
 ** Couples under CCSP
* Incurred Medical Expenses (IME)
* One third child support payment received by a child A/R

== Procedures

=== Mandatory Deductions

Mandatory deductions that are withheld from earned or unearned income are deducted from the recipient's gross income.
However, if the recipient receives a tax refund from federal or state withholding tax after taxes had been allowed as a deduction, this will be considered as income the month the tax refund check is received.

Allow mandatory deductions from the A/R's gross earned and unearned income in the PL/CS budget if any are *required* to be withheld by the employer or agency issuing the income.
Do *not* allow any deductions from income that are within the control of the individual.
This includes withholdings that are the result of A/R's decisions and/or court-ordered actions (e.g., voluntary income tax withholding and court-ordered deductions for child support, alimony or other garnishments resulting from A/R-induced indebtedness or financial obligations).

Exemption: If a court has entered an order for monthly income for support of the community spouse by the institutionalized spouse, the income allowance for the community spouse *shall not be less than* the amount of the monthly income so ordered.

=== Medicare Premium Deductions

Medicare premiums are deducted in the patient liability budgets for the first month of Nursing Home, IH, CCSP/EDWP, ICWP or NOW/COMP Medicaid eligibility through the month following the month of Medicaid approval.
Do not allow as a deduction in any months in which Medicare premiums are currently being paid or will be paid due to eligibility in another COA, such as Q Track, LIS, SSI, etc.

If an A/R receives Part B and/or Part D Medicare, deduct the Medicare Part B and/or Part D premium(s) as follows:

* Deduct in the patient liability budgets for the first month of nursing home, IH, NOW/COMP, ICWP or CCSP/EDWP Medicaid eligibility through the month following the month of Medicaid approval.
See exceptions above.
* Continue to allow Medicare Part D premium payments if AR reports that they are still being deducted from SS check.
* If AR pays a Medicare Part D premium that is higher than the Base Payment then allow the difference as an IME and document.
+
NOTE: Medicare Part D premium payments are not reimbursed like the Medicare Part B premium payments.

=== Personal Needs Allowance (PNA)

The personal needs allowance (PNA) is the amount the recipient is allowed to retain to pay for incidental personal expenses.

Deduct the PNA after allowing the deduction for protected income.
Refer to xref:appendix-a1/2022-abd-limits.adoc[] for the current amount of appropriate PNA to use in the patient liability budget.

=== Protection of Income

Refer to xref:2553.adoc[] for procedures on allowing the protection of income deduction.

=== Diversion of Income

Refer to xref:2554.adoc[] for procedures on allowing the diversion of income deduction.

=== Incurred Medical Expenses

Refer to xref:2555.adoc[] for procedures on allowing the deduction for IMEs.

=== Child Support

Allow as a deduction to the PL/CS budget a one third deduction from child support income received by a child A/R.
If the A/R is the payer of the child support, there is no deduction allowed.
