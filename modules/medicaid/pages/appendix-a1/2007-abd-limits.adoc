= Appendix A1 ABD Financial Limits 2007
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-27
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual With a Community Spouse | Effective Date

| SSI/LA-D
^| $2000
^| $3000
>| N/A
>| 7-88

| AMN
^| $2000
^| $4000
>| N/A
>| 4-90

| QMB / SLMB / QI-1 / QDWI
^| $4000
^| $6000
>| N/A
>| 1-89

| Spousal Impoverishment
^| N/A
^| N/A
| $101,640 + 2000 = $103,640.00
>| 1-07
|===

.CHART A1.2 - ABD MEDICAID NET INCOME LIMITS (GROSS - $20)
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
^| $317
^| $375
| 10-90

.4+^| *FBR (SSI Limit)*
^| A
^| $623
^| $934
.4+| 1-07

^| B
^| $415.33
^| $623

^| C
^| $623
^| N/A

^| D
^| $30
^| N/A

^| *Medicaid CAP*
^| D
^| $1869
^| $3738
| 1-07

.3+^| *QDWI*
^| A
^| $3489
^| $4649
.3+a| 3-07

NOTE: Effective 3-98, ISM no longer applies to this COA eliminating LA-B.

^| C
^| $3489
^| N/A

^| D
^| $3489
^| N/A

^| *QMB*
^| A
^| $851
^| $1141
| 4-07

^| *SLMB*
^| A
^| $1021
^| $1369
| 4-07

^| *QI-1*
^| A
^| $1149
^| $1541
| 3-07
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
^| $4358.57
^| 4-07
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
| $227
^| $331.34
>| $311.34
>| 1-07

^| *FBR*
| $227
^| $331.34
>| $311.34
>| 1-07

^| *QMB*
| N/A
^| N/A
>| $387.00
>| 4-07

^| *SLMB*
| N/A
^| N/A
>| $463.00
>| 4-07

^| *QI-1*
| N/A
^| N/A
>| $520.34
>| 3-07
|===

.CHART A1.5 - SUBSTANTIAL GAINFUL ACTIVITY
|===
| Category ^| Income Limit ^| Effective Date

| Non-Blind individuals
^| $860
.2+^| 1-06

| Blind individuals
^| $1450
|===

.CHART A1.6 – BREAK-EVEN POINTS
[cols=6*]
|===
.2+h| Living Arrangement
2+^h| Earned Income
2+^h| Unearned Income
.2+h| Effective Date

^h| Individual
^h| Couple
^h| Individual
^h| Couple

s| A
^| $1271
^| $1873
^| $603
^| $904
.2+^| 1-06

s| B
^| $869
^| $1271
^| $402
^| $603

s| D
^| $145
^| $205
^| $50
^| $80
^| 7-88
|===

.CHART A1.7 – MONTHLY AVERAGED MEDICAID RATES FOR KATIE BECKETT
|===
| Level of Care ^| Monthly Amount ^| Effective Date

| Skilled Nursing Facility
^| $3645
.2+^| 11/04

| ICF/MR
^| $6667
|===

*A1.8 – MEDICARE EXPENSES*

Medicare Part B Premium rate: $93.50 (effective 1-07).

.CHART A1.9 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the Patient Liability / Cost Share Budget:

| an individual in a nursing home or Institutionalized Hospice
^| $50
^| Effective 7-06

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
^| $50
^| Effective 7-06

a| a VA pensioner or his/her surviving spouse in a nursing home who has no dependents

NOTE: The VA check for these individuals is reduced to the amount of the PNA, regardless of other income.
^| $90
^| Effective 1-92 +
(Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard

| an individual in MRWP
2+| the current Medicaid Cap
|===

.CHART A1.10 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard ^| Amount ^| Effective Date

| Community Spouse Maintenance Need Standard
^| $2541
^| 1-07

| Dependent Family Member Need Standard
^| $1712
^| 4-07
|===

|===
| HOUSEHOLD SIZE | 100% | 135% | 150% | EFF. DATE

^| 1
| $10,210.00
| $13,783.50
| $15,315.00
.5+^| 2007

^| 2
| 13,690.00
| 18,481.50
| 20,535.00

^| 3
| 17,170.00
| 23,179.50
| 25,755.00

^| 4
| 20,650.00
| 27,877.50
| 30,975.00

^| 5
| 24,130.00
| 32,575.50
| 36,195.00
|===

The FPL (100% level) is increased by $3,480 for each additional person in the household.

.CHART A1.12 – COSTS AND GUIDELINES FOR RECEIPT OF MEDICARE PART D - LOW INCOME SUBSIDY
|===
| ^| Group 1 ^| Group 2 ^| Group 3 ^| Eff. Date

^| Resource Limit
^| None
^| Non Q Track Individual - $7,620

Non Q Track Couple - $12,190
^| Individual - $11,710

Couple - $23,410
.6+^| 2007

s| Income Limit
^| 100% of FPL or full Medicaid
^| Less than 135% of FPL
^| Less than 150% of FPL

s| Monthly Premium
^| $0
^| $0
^| Sliding Scale

s| Deductible Per Year
^| $0
^| Up to $53.00
^| Up to $53.00

s| Coinsurance up to $3600 Out of Pocket
^| $1 - $3.10 Copay
^| $2.15 - $5.35 Copay
^| 15% Coinsurance

s| Catastrophic 5% or $2/$5 Copay
^| $0
^| $0
^| $2.15 - $5.35 Copay
|===

*A1.13 – Medically Needy Mileage Reimbursement Rate*

[%hardbreaks]
*48.5 cents per mile – 9/10/05 – 12/31/05*
44.5 cents per mile – 1/1/06 – 1/31/07
48.5 cents per mile – effective 2/1/07
