= Appendix A1 ABD Financial Limits 2003
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-7
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual With a Community  Spouse | Effective Date

| SSI
^| $2000
^| $3000
>| N/A
>| 7-88

| AMN
^| $2000
^| $4000
>| N/A
>| 4-90

| QMB/SLMB/ QI-1/QDWI
^| $4000
^| $6000
>| N/A
>| 1-89

| Spousal Impoverishment
^| N/A
^| N/A
| $90,660 + 2000 =

$92,660.00
>| 1-03
|===

.CHART A1.2 - ABD MEDICAID INCOME LIMITS
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
>| $317
^| $375
| 10-90

.4+^| *FBR*

*(SSI Limit)*
^| A
>| $552
^| $829
.4+^| 1-03

^| B
>| $368
^| $552

^| C
>| $552
^| N/A

^| D
>| $30
^| N/A

^| *Medicaid CAP*
^| D
>| $1656
^| $3312
| 1-03

.3+| *QDWI*
^| A
>| $3059
^| $4105
.3+a|
3-03

NOTE: Effective 3-98, ISM no +
longer applies to this COA +
eliminating LA-B.

^| C
>| $3059
^| N/A

^| D
>| $3059
^| N/A

^| *QMB*
^| A
>| $749
^| $1010
| 4-03

^| *SLMB*
^| A
>| $898
^| $1212
| 4-03

^| *QI-1*
^| A
>| $1011
^| $1364
| 3-03
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
^| $3673
^| 4-03
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
| $204
^| $296.33
^| $277
>| 1-03

^| *FBR*
| $204
^| $296.33
^| $277
>| 1-03

^| *QDWI*
| N/A
^| N/A
^| $680
>| 3-03

^| *QMB*
| N/A
^| N/A
^| $343.33
>| 4-03

^| *SLMB*
| N/A
^| N/A
^| $410.67
>| 4-03

^| *QI-1*
| N/A
^| N/A
^| $461.33
>| 3-03
|===

.CHART A1.5 - SUBSTANTIAL GAINFUL ACTIVITY
|===
| Category | Income Limit | Effective Date

^| Non-Blind individuals
^| $800
.2+^| 1-03

^| Blind individuals
^| $1330
|===

.CHART A1.6 – BREAK-EVEN POINTS
[cols=6*]
|===
.2+h| Living Arrangement
2+h| Earned Income
2+h| Unearned Income
.2+h| Effective Date

^h| Individual
^h| Couple
^h| Individual
^h| Couple

| *A*
^| $1,169
^| $1,723
^| $552
^| $829
.2+^| 1-03

| *B*
^| $801
^| $1,170.34
^| $368
^| $552.67

| *D*
^| $145
^| $205
^| $50
^| $80
^| 7-88
|===

Medicare Part B Premium rate: $58.70 (effective 1-03).

.CHART A1.7 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the  Patient Liability/Cost Share Budget:

| an individual in a nursing home or Institutionalized Hospice
^| $30
| Effective 01-92

Effective 04-03

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
^| $30
| Effective 1-92

a|
a VA pensioner or his/her surviving spouse in a nursing home who has no dependents

NOTE: The VA check for these individuals is reduced to the amount of the PNA, regardless of other income.
^| $90
^| Effective 1-92

(Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard

| an individual in MRWP
2+| the current Medicaid Cap
|===

.CHART A1.8 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard | Amount | Effective  Date

| Community Spouse Maintenance Need Standard
^| $2266.50
^| 1-03

| Dependent Family Member Need Standard
^| $1535
^| 4-03
|===
