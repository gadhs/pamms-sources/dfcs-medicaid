= Appendix A1 ABD Financial Limits 2015
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-49
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual With a Community  Spouse | Effective Date

| SSI/LA-D
>| $2000
^| $3000
>| N/A
>| 7-88

| AMN
>| $2000
^| $4000
>| N/A
>| 4-90

| QMB/SLMB/ QI-1
>| $7280
^| $10,930
>| N/A
>| 1-15

| QDWI
>| $4000
^| $6000
>| N/A
>| 1-89

| Spousal Impoverishment
^| N/A
^| N/A
| $119,220 + 2000 =

$121,220.00
>| 1-15
|===

.CHART A1.2 - ABD MEDICAID NET INCOME LIMITS (GROSS - $20)
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
^| $317
^| $375
| 10-90

.4+^| *FBR*

*(SSI Limit)*
^| A
^| $733
^| $1100
.4+^| 1-15

^| B
^| $488.67
^| $733.33

^| C
^| $733
^| N/A

^| D
^| $30
^| N/A

^| *Medicaid CAP*
^| D
^| $2199
^| $4398
| 1-15

.3+| *QDWI*
^| A
^| $3989
^| $5375
.3+a|
3-15

NOTE: Effective 3-98, ISM no +
longer applies to this COA +
eliminating LA-B.

^| C
^| $3989
^| N/A

^| D
^| $3989
^| N/A

^| *QMB*
^| A
^| $981
^| $1328
| 4-15

^| *SLMB*
^| A
^| $1177
^| $1593
| 4-15

^| *QI-1*
^| A
^| $1325
^| $1793
| 3-15
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
| $5931.00
^| 4-15
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
^| $264.33
^| $386.67
>| $366.67
>| 1-15

^| *FBR*
^| $264.33
^| $386.67
>| $366.67
>| 1-15

^| *QMB*
^| N/A
^| N/A
>| $437.00
>| 4-14

^| *SLMB*
^| N/A
^| N/A
>| $525.00
>| 4-14

^| *QI-1*
^| N/A
^| N/A
>| $590.00
>| 3-14
|===

4

.CHART A1.5 - SUBSTANTIAL GAINFUL ACTIVITY
|===
| Category | Income Limit | Effective Date

^| Non-Blind individuals
^| $1090
.2+^| 1-15

^| Blind individuals
^| $1820
|===

.CHART A1.6 – BREAK-EVEN POINTS
[cols=6*]
|===
.2+h| Living Arrangement
2+h| Earned Income
2+h| Unearned Income
.2+h| Effective Date

^h| Individual
^h| Couple
^h| Individual
^h| Couple

^| *A*
^| $1271
^| $1873
^| $603
^| $904
.2+^| 1-06

^| *B*
^| $869
^| $1271
^| $402
^| $603

^| *D*
^| $145
^| $205
^| $50
^| $80
^| 7-88
|===

.CHART A1.7 – MONTHLY AVERAGED MEDICAID RATES FOR KATIE BECKETT
|===
| Level of Care | Monthly Amount | Effective  Date

^| Skilled Nursing Facility
^| $5164.60
.2+| 04/15

^| ICF/MR
^| $14729.96

^| Hospital
^| $5432.06
| 4/15
|===

[.text-center]
*A1.8 – MEDICARE EXPENSES*

Medicare Part B Premium rate: $104.90 (effective 1-14).

.CHART A1.9 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the Patient Liability/Cost Share Budget:

| an individual in a nursing home or Institutionalized Hospice
>| $50
>| Effective 7-06

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
>| $50
>| Effective 7-06

a|
a VA pensioner or his/her surviving spouse in a nursing home who has no dependents

NOTE: The VA check for these individuals is reduced to the amount of the PNA, regardless of other income.
>| $90
^| Effective 1-92

(Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard

| an individual in NOW/COMP
2+| the current Medicaid Cap
|===

.CHART A1.10 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard | Amount | Effective Date

| Community Spouse Maintenance Need Standard
| $2980.00
^| 1-15

| Dependent Family Member Need Standard
| $1992.00
^| 4-15
|===

|===
| HOUSEHOLD SIZE | 100% | 135% | 150% | EFF.
DATE

^| 1
| $11,770.00
^| $15,889.00
^| $17,655.00
.5+^| 2015

^| 2
| 15,930.00
^| 21,505.00
^| 23,985.00

^| 3
| 20,090.00
^| 27,121.00
^| 30,135.00

^| 4
| 24,250.00
^| 32,737.00
^| 36,375.00

^| 5
| 28,410.00
^| 38,353.00
^| 42,615.00
|===

The FPL (100% level) is increased by $4,060 for each additional person in the household.

.CHART A1.12 – COSTS AND GUIDELINES FOR RECEIPT OF MEDICARE PART D - LOW INCOME SUBSIDY
|===
| | Group 1 | Group 2 | Group 3 | Eff.  Date

^| *Resource Limit*
^| None
^| Non Q Track Individual -

$8,780

Non Q Track Couple -

$13,930
| Individual - $13,640 Couple - $27,250
.6+| 2015

^| *Income Limit*
^| Full

Medicaid
^| Q Track

or

Less than 135% of FPL
^| Less than 150% of FPL

| *Monthly*

*Premium*
^| $0
^| $0
^| Sliding Scale

| *Deductible*

*Per Year*
^| $0
^| Up to $66.00
^| Up to $66.00

| *Coinsurance*

*up to $3600 Out of Pocket*
| $1.20 -

$3.60

Copay
^| $2.65 - $6.60 Copay
^| 15% Coinsurance

| *Catastrophic*

*5% or $2/$5 Copay*
^| $0
^| $0
^| $2.65 - $6.60 Copay
|===

[.text-center]
*Low-Income Part D Premium Subsidy Amount* +
2010 – 29.62 +
2011 – 32.83 +
2012 – 31.18 +
2013 – 34.22 +
2014 – 29.32 +
2015 – 26.47

[.text-center]
*A1.13 – Medically Needy Mileage Re-imbursement Rate* +
48.5 cents per mile – 9/10/05 – 12/31/05 +
44.5 cents per mile – 1/1/06 – 1/31/07 +
48.5 cents per mile – 2/1/07 – 03/31/08 +
50.5 cents per mile – 4/1/08 – 7/31/08 +
58.5 cents per mile – 8/1/08 – 12/31/08 +
55 cents per mile – 1/1/09 – 12/31/09 +
50 cents per mile – 1/1/10 – 12/31/2010 +
51 cents per mile – 01/01/11 – 04/16/2012 +
55.5 cents per mile – 04/17/2012 – 12/31/2012 +
56.5 cents per mile – 01/01/2013 – 12/31/2013 +
56 cents per mile -- 01/01/2014 – 12/31/2014 +
57.5 cents per mile – 01/01/2015 - Present
