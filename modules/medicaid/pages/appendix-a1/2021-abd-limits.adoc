= Appendix A1 ABD Financial Limits 2021
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-64
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual With a Community  Spouse | Effective Date

^| SSI/LA-D
^| $2000
^| $3000
^| N/A
^| 7-88

^| AMN
^| $2000
^| $4000
^| N/A
^| 4-90

| QMB/SLMB/ QI-1
^| $7970
^| $11,960
^| N/A
^| 1-21

^| QDWI
^| $4000
^| $6000
^| N/A
^| 1-89

| Spousal Impoverishment
^| N/A
^| N/A
^| $130,380+ 2000

=

$132,380.00
^| 1-21
|===

.CHART A1.2 - ABD MEDICAID NET INCOME LIMITS (GROSS - $20)
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
^| $317
>| $375
| 10-90

.4+^| *FBR*

*(SSI Limit)*
^| A
^| $794
>| $1191
.4+^| 1-21

^| B
^| $529.34
>| $794

^| C
^| $794
^| N/A

^| D
^| $30
^| N/A

^| *Medicaid CAP*
^| D
^| $2382
>| $4764
| 1-21

.3+^| *QDWI*
^| A
^| $4379
>| $5893
.3+^a|
[.text-center]
3-21

NOTE: Effective 3-98, ISM no +
longer applies to +
this COA eliminating LA-B.

^| C
^| $4379
^| N/A

^| D
^| $4379
^| N/A

^| *QMB*
^| A
^| $1074
>| $1452
| 4-21

^| *SLMB*
^| A
^| $1288
>| $1742
| 4-21

^| *QI-1*
^| A
^| $1449
>| $1960
| 3-21
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
| $8821.00
^| 4-21
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
^| $284.66
^| $417.00
^| $397.00
^| 1-21

^| *FBR*
^| $284.66
^| $417.00
^| $397.00
^| 1-21

^| *QMB*
^| N/A
^| N/A
^| $490.66
^| 4-21

^| *SLMB*
^| N/A
^| N/A
^| $587.33
^| 4-21

^| *QI-1*
^| N/A
^| N/A
^| $660.00
^| 3-21
|===

.CHART A1.5 - SUBSTANTIAL GAINFUL ACTIVITY
|===
| Category | Income Limit | Effective Date

^| Non-Blind individuals
^| $1310
.2+^| 1-21

^| Blind individuals
^| $2190
|===

.CHART A1.6 – BREAK-EVEN POINTS
[cols=6*]
|===
.2+h| Living Arrangement
2+h| Earned Income
2+h| Unearned Income
.2+h| Effective Date

^h| Individual
^h| Couple
^h| Individual
^h| Couple

^| *A*
^| $1673
^| $2467
^| $814
^| $1211
.2+^| 1-21

^| *B*
^| $1143.68
^| $1673
^| $549.34
^| $814

^| *D*
^| $145
^| $205
^| $50
^| $80
^| 7-88
|===

.CHART A1.7 – MONTHLY AVERAGED MEDICAID RATES FOR KATIE BECKETT
|===
| Level of Care | Monthly Amount | Effective Date

^| Skilled Nursing Facility
| $6344.46 (31 days)
.2+^| 04/21

^| ICF/MR
| $14,846.21 (31 days)
|===

[.text-center]
*A1.8 – MEDICARE EXPENSES*

Medicare Part B Premium rate: $104.90 (effective 1-14) +
$121.80 (effective 1-16) +
$134.00 (effective 2017 and 2018) +
$135.50 (effective 2019) +
$144.60 (effective 2020) +
$148.50 (effective 2021) +
Effective 01/2016 Medicare Part B Premium rates may vary check BENDEX for applicable rate.

.CHART A1.9 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the Patient Liability/Cost Share Budget:

| an individual in a nursing home or Institutionalized Hospice
^| $70
^| Effective 7-19

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
^| $70
^| Effective 7-19

a|
a VA pensioner or his/her surviving spouse in a nursing home who has no dependents

NOTE: The VA check for these individuals is reduced to the amount of the PNA, regardless +
of other income.
^| $90
^| Effective 1-92

(Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard

| an individual in NOW/COMP
2+| the current Medicaid Cap
|===

.CHART A1.10 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard | Amount | Effective Date

| Community Spouse Maintenance Need Standard
^| $3259.50
^| 1-21
|===

[cols=7*]
|===
4+| Dependent Family Member Need Standard
| $2178.00
2+^| 4-21

7+^| *CHART A1.11- FEDERAL POVERTY LIMITS*

| *HOUSEHOLD SIZE*
^| *100%*
^| *135%*
3+^| *150%*
| *EFF.
DATE*

^| 1
^| $12,880.00
^| $17,388.00
3+| $19,320.00
.5+^| 2021

^| 2
^| 17,420.00
^| 23,517.00
3+| 26,130.00

^| 3
^| 21,960.00
^| 29,646.00
3+| 32,940.00

^| 4
^| 26,500.00
^| 35,775.00
3+| 39,750.00

^| 5
^| 31,040.00
^| 41,904.00
3+| 46,560.00
|===

The FPL (100% level) is increased by $4,540 for each additional person in the household.

.CHART A1.12 – COSTS AND GUIDELINES FOR RECEIPT OF MEDICARE PART D - LOW INCOME SUBSIDY
|===
| | Group 1 | Group 2 | Group 3 | Eff.
Date

^| *Resource Limit*
^| None
^| Non-Q Track Individual -

$9,470

Non-Q Track Couple -

$14,960
| Individual - $14,790 Couple - $29,520
.6+| 2021

^| *Income Limit*
| Full Medicaid
^| Q Track or

Less than 135% of FPL
^| Less than 150% of FPL

| *Monthly*

*Premium*
^| $0
^| $0
^| Sliding Scale

| *Deductible*

*Per Year*
^| $0
^| Up to $92.00
^| Up to $92.00

| *Coinsurance*

*up to $3600 Out of Pocket*
^| $1.30 -

$4.00Copay
^| $3.70 - $9.20Copay
^| 15% Coinsurance

| *Catastrophic*

*5% or $2/$5 Copay*
^| $0
^| $0
^| $3.70 - $9.20Copay
|===

[.text-center]
*Low-Income Part D Premium Subsidy Amount* +
2010 – 29.62 +
2011 – 32.83 +
2012 – 31.18 +
2013 – 34.22 +
2014 – 29.32 +
2015 – 26.47 +
2016 – 25.78 +
2017 – 26.43 +
2018 – 24.53 +
2019 - 25.68 +
2020 – 25.34 +
2021 - 29.80

[.text-center]
*A1.13 – Medically Needy Mileage Re-Imbursement Rate* +
48.5 cents per mile – 09/10/05 – 12/31/05 +
44.5 cents per mile – 01/01/06 – 01/31/07 +
48.5 cents per mile – 02/01/07 – 03/31/08 +
50.5 cents per mile – 04/01/08 – 07/31/08 +
58.5 cents per mile – 08/01/08 – 12/31/08 +
55.0 cents per mile – 01/01/09 – 12/31/09 +
50.0 cents per mile – 01/01/10 – 12/31/10 +
51.0 cents per mile – 01/01/11 – 04/16/12 +
55.5 cents per mile – 04/17/12 – 12/31/12 +
56.5 cents per mile – 01/01/13 – 12/31/13 +
56.0 cents per mile - 01/01/14 – 12/31/14 +
57.5 cents per mile – 01/01/15 – 12/31/15 +
54.0 cents per mile – 01/01/16 – 12/31/16 +
53.5 cents per mile – 01/01/17 - 12/31/17 +
54.5 cents per mile – 01/01/18 – 12/31/18 +
58.0 cents per mile – 01/01/19 - 12/31/19 +
57.5 cents per mile - 01/01/20 - 12/31/20 +
56.0 cents per mile - 01/01/21 - present
