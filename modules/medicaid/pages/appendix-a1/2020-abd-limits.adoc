= Appendix A1 ABD Financial Limits 2020
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-59
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual With a Community  Spouse | Effective Date

^| SSI/LA-D
^| $2000
^| $3000
^| N/A
^| 7-88

^| AMN
^| $2000
^| $4000
^| N/A
^| 4-90

| QMB/SLMB/ QI-1
^| $7860
^| $11,800
^| N/A
^| 1-20

^| QDWI
^| $4000
^| $6000
^| N/A
^| 1-89

| Spousal Impoverishment
^| N/A
^| N/A
^| $128,640 + 2000

=

$130,640.00
^| 1-20
|===

.CHART A1.2 - ABD MEDICAID NET INCOME LIMITS (GROSS - $20)
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
>| $317
^| $375
| 10-90

.4+^| *FBR*

*(SSI Limit)*
^| A
>| $783
^| $1175
.4+^| 1-20

^| B
>| $522
^| $783.34

^| C
>| $783
^| N/A

^| D
^| $30
^| N/A

^| *Medicaid CAP*
^| D
>| $2349
^| $4698
| 1-20

.3+^| *QDWI*
^| A
>| $4339
^| $5833
.3+^a|
[.text-center]
3-20

NOTE: Effective 3-98, ISM no +
longer applies to +
this COA eliminating LA-B.

^| C
>| $4339
^| N/A

^| D
>| $4339
^| N/A

^| *QMB*
^| A
>| $1064
^| $1437
| 4-20

^| *SLMB*
^| A
>| $1276
^| $1724
| 4-20

^| *QI-1*
^| A
>| $1436
^| $1940
| 3-20
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
| $8517.00
^| 4-20
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
^| $281.00
^| $411.66
^| $391.66
^| 1-20

^| *FBR*
^| $281.00
^| $411.66
^| $391.66
^| 1-20

^| *QMB*
^| N/A
^| N/A
^| $485.66
^| 4-20

^| *SLMB*
^| N/A
^| N/A
^| $581.33
^| 4-20

^| *QI-1*
^| N/A
^| N/A
^| $653.33
^| 3-20
|===

.CHART A1.5 - SUBSTANTIAL GAINFUL ACTIVITY
|===
| Category | Income Limit | Effective Date

^| Non-Blind individuals
^| $1260
.2+^| 1-20

^| Blind individuals
^| $2110
|===

.CHART A1.6 – BREAK-EVEN POINTS
[cols=6*]
|===
.2+h| Living Arrangement
2+h| Earned Income
2+h| Unearned Income
.2+h| Effective Date

^h| Individual
^h| Couple
^h| Individual
^h| Couple

^| *A*
^| $1651
^| $2435
^| $803
^| $1195
.2+^| 1-20

^| *B*
^| $1129
^| $1651.68
^| $542
^| $803.34

^| *D*
^| $145
^| $205
^| $50
^| $80
^| 7-88
|===

.CHART A1.7 – MONTHLY AVERAGED MEDICAID RATES FOR KATIE BECKETT
|===
| Level of Care | Monthly Amount | Effective Date

^| Skilled Nursing Facility
>| $6,111.96 (31 days)
.2+^| 04/20

^| ICF/MR
>| $14,846.21 (31 days)
|===

[.text-center]
*A1.8 – MEDICARE EXPENSES*

Medicare Part B Premium rate: $104.90 (effective 1-14) +
$121.80 (effective 1-16) +
$134.00 (effective 2017 and 2018) +
$135.50 (effective 2019) +
$144.60 (effective 2020) +
Effective 01/2016 Medicare Part B Premium rates may vary check BENDEX for applicable rate.

.CHART A1.9 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the Patient Liability/Cost Share Budget:

| an individual in a nursing home or Institutionalized Hospice
^| $70
^| Effective 7-19

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
^| $70
^| Effective 7-19

a|
a VA pensioner or his/her surviving spouse in a nursing home who has no dependents

NOTE: The VA check for these individuals is reduced to the amount of the PNA, regardless of other income.
^| $90
^| Effective 1-92

(Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard

| an individual in NOW/COMP
2+| the current Medicaid Cap
|===

.CHART A1.10 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard | Amount | Effective Date

| Community Spouse Maintenance Need Standard
^| $3216.00
^| 1-20

| Dependent Family Member Need Standard
^| $2155.00
^| 4-20
|===

.CHART A1.11- FEDERAL PROVERTY LIMITS
|===
| HOUSEHOLD SIZE | 100% | 135% | 150% | EFF.
DATE

^| 1
^| $12,760.00
^| $17,226.00
^| $19,140.00
.5+^| 2020

^| 2
^| 17,240.00
^| 23,274.00
^| 25,860.00

^| 3
^| 21,720.00
^| 29,322.00
^| 32,580.00

^| 4
^| 26,200.00
^| 35,370.00
^| 39,300.00

^| 5
^| 30,680.00
^| 41,418.00
^| 46,020.00
|===

The FPL (100% level) is increased by $4,480 for each additional person in the household.

.CHART A1.12 – COSTS AND GUIDELINES FOR RECEIPT OF MEDICARE PART D - LOW INCOME SUBSIDY
|===
| | Group 1 | Group 2 | Group 3 | Eff.
Date

^| *Resource Limit*
^| None
^| Non-Q Track Individual -

$9,360

Non-Q Track Couple -

$14,800
| Individual - $14,610 Couple - $29,160
.6+| 2020

^| *Income Limit*
^| Full

Medicaid
^| Q Track

or

Less than 135% of FPL
^| Less than 150% of FPL

| *Monthly*

*Premium*
^| $0
^| $0
^| Sliding Scale

| *Deductible*

*Per Year*
^| $0
^| Up to $89.00
^| Up to $89.00

| *Coinsurance up to $3600 Out of Pocket*
| $1.30 -

$3.90

Copay
^| $3.60 - $8.95 Copay
^| 15% Coinsurance

| *Catastrophic*

*5% or $2/$5 Copay*
^| $0
^| $0
^| $3.60 - $8.95 Copay
|===

[.text-center]
*Low-Income Part D Premium Subsidy Amount* +
2010 – 29.62 +
2011 – 32.83 +
2012 – 31.18 +
2013 – 34.22 +
2014 – 29.32 +
2015 – 26.47 +
2016 – 25.78 +
2017 – 26.43 +
2018 – 24.53 +
2019 - 25.68 +
2020 – 25.34

[.text-center]
*A1.13 – Medically Needy Mileage Re-imbursement Rate* +
48.5 cents per mile – 09/10/05 – 12/31/05 +
44.5 cents per mile – 01/01/06 – 01/31/07 +
48.5 cents per mile – 02/01/07 – 03/31/08 +
50.5 cents per mile – 04/01/08 – 07/31/08 +
58.5 cents per mile – 08/01/08 – 12/31/08 +
55.0 cents per mile – 01/01/09 – 12/31/09 +
50.0 cents per mile – 01/01/10 – 12/31/10 +
51.0 cents per mile – 01/01/11 – 04/16/12 +
55.5 cents per mile – 04/17/12 – 12/31/12 +
56.5 cents per mile – 01/01/13 – 12/31/13 +
56.0 cents per mile - 01/01/14 – 12/31/14 +
57.5 cents per mile – 01/01/15 – 12/31/15 +
54.0 cents per mile – 01/01/16 – 12/31/16 +
53.5 cents per mile – 01/01/17 - 12/31/17 +
54.5 cents per mile – 01/01/18 – 12/31/18 +
58.0 cents per mile – 01/01/19 - 12/31/19 +
57.5 cents per mile - 01/01/20 - present
