= 2201 Basic Eligibility Criteria Overview
:chapter-number: 2200
:effective-date: July 2023
:mt: MT-70
:policy-number: 2201
:policy-title: Basic Eligibility Criteria Overview
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

Basic Eligibility Criteria are non-financial requirements the Assistance Unit (AU) members must meet in order to qualify for Medicaid.

== Basic Considerations

=== ABD Medicaid

The following Basic Eligibility Criteria are required when determining eligibility under any ABD Medicaid Class of Assistance (COA).

* Aged, Blind or Disabled
* Application for Other Benefits
* Citizenship/Immigration Status/Identity
* Enumeration
* Residency
* Third Party Liability (TPL) assignment

*EXCEPTION:*

* Application for Other Benefits is NOT a requirement for Q Track COAs.
* Citizenship/Immigration/Identity and Enumeration are NOT requirements for Emergency Medical Assistance (EMA).
* Third Party assignment is NOT a requirement for SLMB and QI-1 COAs.

The following Basic Eligibility Criteria are requirements when determining eligibility under any Medicaid CAP COA:

* Length of Stay
* Level of Care

[caption=Exception]
NOTE: Length of Stay is *NOT* required for Deeming Waiver.
Refer to Section 2101, ABD COA Overview.

=== Family Medicaid

The following Basic Eligibility Criteria are requirements when determining eligibility under a Family Medicaid COA.
Refer to the specific COA to determine if the criterion applies.

* Age
* Application for Other Benefits
* Citizenship/Immigration Status/Identity
* Cooperation with the Division of Child Support Services (DCSS)
* Deprivation
* Enumeration
* Residency
* Third Party Liability (TPL)
* Living with a Specified Relative/Tax Filer/Non Tax Filer Status

[caption=Exception]
NOTE: Citizenship/Immigration/Identity and Enumeration are *NOT* requirements for Emergency Medical Assistance (EMA).

== Procedures

Establish and verify, if required, that the A/R meets all Basic Eligibility Criteria required for the COA under which Medicaid eligibility is being determined or continued.

If all Basic Eligibility Criteria requirements are met, proceed with the financial eligibility determination.

=== Failure to Comply with Basic Eligibility Criteria

If a member of the AU fails to comply with one or more of the required Basic Eligibility Criteria, deny or terminate Medicaid or apply a penalty, depending on the COA under which Medicaid is being determined.

If the Basic Eligibility Criterion the AU member has failed to meet is not required under another COA, complete a CMD to determine eligibility under the other COA before denying or terminating Medicaid.

=== Failure to Comply - ABD Medicaid

If the A/R fails to meet one or more of the Basic Eligibility Criteria, deny or terminate ABD Medicaid.

=== Failure to Comply - Family Medicaid

Do not include an adult in the AU if s/he fails to comply with Basic Eligibility Requirements.
A non-compliant adult *is* included in the BG if s/he is a parent or spouse of an AU member.

[caption=Exception]
NOTE: If an adult fails to apply for potential benefits, exclude the parent and everyone for whom s/he is financially responsible, unless the adult is included in a Pregnant Woman AU.

Remove from the Family Medicaid AU, but not the BG the adult who fails to comply with any of the following requirements:

* cooperation with his/her own Enumeration
* when Reasonable Opportunity Period (ROP) has expired
* when the AU contains at least one qualifying child
* identification and provision of information regarding any TPL available to the AU
* referral to and cooperation with DCSS

*EXCEPTION:*

* Pregnant women in any COA are not referred to and are not required to cooperate with DCSS for an unborn child.

* Pregnant women are not referred to, and are not required to cooperate with DCSS for an existing child

* A referral to DCSS is not required for a Medicaid-eligible individual age 19 years or older.

* A referral to DCSS is *NOT* made for child-only Medicaid cases.

* A child-only Medicaid case is defined as a Medicaid AU in which no adults are receiving Medicaid under the same case as the child or under any related case.
An AU which contains a penalized adult is *NOT* considered a child-only case.

Refer to specific Basic Eligibility Requirements in this Chapter.

Remove the child from the AU if the responsible adult in the AU or BG fails to enumerate the child; and/or fails to apply for benefits to which the child may be entitled; and/or ROP has expired for the child.
For FM-MN, the child may be included or excluded in the BG at the request of the A/R.

[caption=Exception]
NOTE: These requirements do not apply to NB Medicaid, or Medicaid applications for deceased individuals.
For EMA only, citizenship/immigration status/identity and enumeration do not apply.

== Verification

=== ABD Medicaid

Basic Eligibility Criteria must be established when determining eligibility under all ABD COAs.

[caption=Exception]
NOTE: It is not necessary to verify Enumeration or Residency, unless questionable.

=== Family Medicaid

The statement of the A/R may be accepted as verification for all Basic Eligibility Criteria unless there is information known to the agency that conflicts with the statement of the A/R or if the statement is otherwise questionable.

*EXCEPTION:*

* Citizenship/Immigration Status/Identity
* Any information that conflicts with information known to the agency, or that is otherwise questionable must be verified.

The following charts list Basic Eligibility Criteria and the COAs to which each applies.

[#chart-2201-1]
.CHART 2201.1 ABD MEDICAID BASIC ELIGIBILITY CRITERIA
[cols="2,^,^,^,^,^,^,^,^"]
|===
^| ABD MEDICAID CLASS OF ASSISTANCE | AGED / BLIND / DISABLED | APPLICATION FOR OTHER BENEFITS | CITIZENSHIP / IMMIGRATION STATUS / IDENTITY | ENUMERATION | LENGTH OF STAY | LEVEL OF CARE | RESIDENCY | THIRD PARTY LIABILITY

| SSI Medicaid (SSI)
| X
| X
| X
| X
|
|
| X
| X

| Pickle (PL 94-566)
| X
| X
| X
| X
|
|
| X
| X

| Disabled Adult Child (PL 99-643)
| X
| X
| X
| X
|
|
| X
| X

| Former SSI-Disabled Child
| X
| X
| X
| X
|
|
| X
| X

| Disabled Widow(er)
| X
| X
| X
| X
|
|
| X
| X

| Widow(er) Age 60-64 (PL 100-203)
| X
| X
| X
| X
|
|
| X
| X

| 1984 Widow(er) (PL 99-272)
| X
| X
| X
| X
|
|
| X
| X

| 1972 COLA (PL 92-603)
| X
| X
| X
| X
|
|
| X
| X

| Community Care Services Program
| X
| X
| X
| X
| X
| X
| X
| X

| NOW/COMP
| X
| X
| X
| X
| X
| X
| X
| X

| Deeming Waiver
| X
| X
| X
| X
|
| X
| X
| X

| Hospice (at home or institutionalized)
| X
| X
| X
| X
| X
| X
| X
| X

| 30 Day Hospital
| X
| X
| X
| X
| X
| X
| X
| X

| Independent Care Waiver Program
| X
| X
| X
| X
| X
| X
| X
| X

| Nursing Home
| X
| X
| X
| X
| X
| X
| X
| X

| QMB
| X
|
| X
| X
|
|
| X
| X

| SLMB
| X
|
| X
| X
|
|
| X
|

| QI-1
| X
|
| X
| X
|
|
| X
|

| QDWI
| X
| X
| X
| X
|
|
| X
| X

| ABD Medically Needy (AMN)
| X
| X
| X
| X
|
|
| X
| X
|===

[#chart-2201-2]
.CHART 2201.2 FAMILY MEDICAID BASIC ELIGIBILITY CRITERIA
[cols="2,^,^,^,^,^,^,^,^"]
|===
^| FAMILY MEDICAID CLASS OF ASSISTANCE | AGE | APPLICATION FOR OTHER BENEFITS | CITIZENSHIP / IMMIGRATION STATUS / IDENTITY | COOPERATION WITH DIVISION OF CHILD SUPPORT SERVICES * | ENUMERATION | LIVING WITH A SPECIFIED RELATIVE / TAX FILER / NON TAX FILER | RESIDENCY | THIRD PARTY LIABILITY

| Parent/Caretaker with Child(ren) Medicaid
| X
| X
| X
| X
^| X
| X
| X
| X

| Transitional Medical Assistance (TMA)
| X
|
| X
|
^| X
| X
| X
| X

| Four Months Extended (4MEx)
| X
|
| X
|
^| X
| X
| X
| X

| Newborn
| X
|
| X
|
|
|
| X
| X

| Children Under 19 Years of Age
| X
| X
| X
| *
^| X
| X
| X
| X

| Pregnant Woman (PgW)
|
|
| X
|
^| X
|
| X
| X

| State Adoption Assistance (SAA) Medicaid
| X
| X
| X
| X
^| X
| X
| X
| X

| Child Welfare Foster Care (CWFC) Medicaid
| X
| X
| X
| X
^| X
|
| X
| X

| Former FosterCare Medicaid
| X
|
| X
|
^| X
|
| X
| X

| Women's Health Medicaid
| X
| X
| X
|
^| X
|
| X
| X

| Pathways
| X
|
| X
|
^| X
| X
| X
| X

| Planning For Healthy Babies (P4HB)
| X
| X
| X
|
^| X
|
| X
| X

| PeachCare for Kids®
| X
|
| X
|
^| X
| X
| X
| X

| Family Medicaid Medically Needy
| X
| X
| X
| X
^| X
| X
| X
| X
|===

{asterisk}No referral to Child Support Services is made for child-only Medicaid cases.
Refer to xref:2250.adoc[] for definition of a child-only Medicaid AU.
