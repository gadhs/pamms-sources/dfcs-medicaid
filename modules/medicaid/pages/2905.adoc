= 2905 Cancer State Aid Program
:chapter-number: 2900
:effective-date: November 2023
:mt: MT-71
:policy-number: 2905
:policy-title: Cancer State Aid Program
:previous-policy-number: MT 60

include::partial$policy-header.adoc[]

== Requirements

The Cancer State Aid Program was established in 1937 by the Georgia legislature at the request of Georgia physicians to provide cancer treatment to uninsured and under-insured low-income cancer patients.
The program funds diagnosis and treatment for medically needy cancer patients in Georgia.

== Basic Considerations

To be eligible for the program, patients must meet the following *financial, citizenship, residency, and medical* criteria:

* Family income must be at or below 250 percent of federal poverty income guidelines
* Must be uninsured or underinsured and not be eligible for full coverage Medicaid
* Must be a U.S. citizen or lawful permanent resident
* Must be a resident of Georgia
* Must receive active medical treatment for cancer *and* be likely to benefit from active medical treatment
* Must receive treatment from facilities that participate in the Cancer State Aid Program
* Must be accepted for treatment by a physician affiliated with a Cancer State Aid participating facility.
* Due to funding restrictions, a limited number of applicants are accepted each fiscal year.
Applications will be considered based on the date they are received and the availability of funding.

Participating provider types include:

* Hospitals (25)
* Free standing radiation therapy treatment centers (5 providers across multiple locations)
* Pharmacies (Number varies depending on location of need)

Covered services include:

* Inpatient and outpatient cancer related diagnostic and treatment services
* Prescription drugs related to treatment of cancer.
Prior approval is required from the Cancer State Aid Program.
* Limited outpatient cancer related home services may be considered.
Prior approval is required from the Cancer State Aid Program.

== Procedures

Cancer State Aid Program information may be obtained by calling the Georgia Department of Public Health (DPH), Division of Health Protection, *Cancer State Aid Program at 404-463-5111*.
Information may also be obtained from:

* Social services personnel and financial counselors at participating facilities

Applications are submitted directly from participating providers to the Cancer State Aid Program for eligibility determination.
Eligibility determination will take approximately 5 working days from receipt of all required information.

Mailing address:

[%hardbreaks]
Cancer State Aid Program
Georgia Department of Public Health Division of Health Protection
200 Piedmont Avenue SE
Atlanta, GA 30334
Phone: 404-463-5111
Fax: 404-657-6316

The Cancer State Aid Program web site is https://dph.georgia.gov/cancer-aid.
