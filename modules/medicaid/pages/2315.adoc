= 2315 Dividends, Accrued
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2315
:policy-title: Dividends, Accrued
:previous-policy-number: MT 26

include::partial$policy-header.adoc[]

== Requirements

Accrued dividends earned on financial investments, such as stocks, are countable resources separate and apart from the investment resource.
This applies to all ABD Medicaid classes of assistance (COA).
They are excluded in Family Medicaid COAs.

Whether or not accrued dividends on life insurance policies are countable resources is dependent upon whether the life insurance policy is excluded for burial purposes and is for a Non-FBR A/R.

== Basic Considerations

A dividend is a share of surplus company earnings paid on some financial investments and life insurance policies.

Accrued dividends are dividends that an A/R has constructively received but left in the custody of the company.

=== Non-FBR COAs

For Non-FBR COA, dividends left to accrue on Excluded life insurance policies are excluded.
Dividends left to accrue on countable life insurance are a resource.
Dividends actually received are income in the month received.

=== FBR COAs

For FBR COAs, dividends left to accrue on all life insurance policies (including excluded policies) are countable resources, separate and apart from any CSVs.

If a policy states non-participating or does not pay dividends no further development of dividends is required.
Otherwise, verify from the insurance company whether a policy earns dividends and the amount of accrued dividends as of the first day of the month of verification.

== Procedures

Verify from the source the value of any accrued dividends as of the first moment of the first day of the month of verification.
