= Appendix F - Forms Table of Contents
:chapter-number: Appendix F
:effective-date: N/A
:mt: MT-73
:policy-number: Appendix F
:policy-title: Table of Contents
:previous-policy-number:

include::partial$policy-header.adoc[]

== Policy Statement

Only State Office approved forms may be used.

== Voter Registration Application Form Information

For a copy of the Voter Registration Application Form and information on how to apply to register to vote, visit: https://sos.ga.gov/sites/default/files/forms/GA_VR_APP_2019.pdf Also, refer to Form # VRA-95.

== Medicaid Forms Table of Contents

[%header,cols="^.^,.^3,^.^,^.^,^.^"]
,===
Form Number,Form Title,Revision Date,Order Info,Owner

1,xref:attachment$form-1.docx[OSAH-1-Medicaid (Hearing Request)],07/23, Screen Print, OSAH
1,PeachCare for Kids Flyer (English),,Gainwell,DCH
6,LOC Approval/NH,,Gainwell, DCH
6A,xref:attachment$form-6a.docx[Physician’s Recommendation for Pediatric Care],08/24, Screen Print, DCH
21,PeachCare for Kids Handbook,, Gainwell, DCH
41,PeachCare for Kids Handbook (Spanish),, Gainwell, DCH
59,Authorization for NH Facility Reimbursement/Vendor Payment,, Gainwell, DCH
71,Medicaid Disability Determination Inquiry (Obsolete as of 09/2013),02/11, Screen Print, DHS
89,xref:attachment$form-89.docx[Medicare Savings Programs Request for Information],08/24, Screen Print, DHS
94,xref:attachment$form-94.docx[Medicaid Application] ,10/22, SO, DHS
94 LP,xref:attachment$form-94-lp.docx[Medicaid Application (Large Print)], 01/22, SO, DHS
94 SP,xref:attachment$form-94-es.docx[Medicaid Application (Spanish)],10/22, SO, DHS
94 LP SP,xref:attachment$form-94-es-lp.docx[Medicaid Application (Large Print Spanish)],01/22, SO, DHS
94A,xref:attachment$form-94a.docx[Medicaid Streamlined Application],07/23, SO, DHS
94A LP,xref:attachment$form-94a-lp.docx[Medicaid Streamlined Application (Large Print)],01/22, SO, DHS
94A SP,xref:attachment$form-94a-es.docx[Medicaid Streamlined Application (Spanish)],10/22,SO,DHS
94A LP SP,xref:attachment$form-94a-es-lp.docx[Medicaid Streamlined Application (Large Print Spanish)],01/22,SO,DHS
94A Appendix A,xref:attachment$form-appendix-a.docx[Streamlined Application Appendix A], 07/23,SO,DHS
94A Appendix A SP, xref:attachment$form-appendix-a-es.docx[Streamlined Application Appendix A (Spanish)],09/17,SO,DHS
94A Appendix A LP, xref:attachment$form-appendix-a-lp.docx[Streamlined Application Appendix A (Large Print)],09/17,SO,DHS
94A Appendix A SP LP, xref:attachment$form-appendix-a-es-lp.docx[Streamlined Application Appendix A (Spanish Large Print)],09/17,SO,DHS
94A Appendix B, xref:attachment$form-appendix-b.docx[Streamlined Application Appendix B],07/23,S0,DHS
94A Appendix B SP, xref:attachment$form-appendix-b-es.docx[Streamlined Application Appendix B (Spanish)],09/17,SO,DHS
94A Appendix B LP, xref:attachment$form-appendix-b-lp.docx[Streamlined Application Appendix B (Large Print)],09/17,SO,DHS
94A Appendix B SP LP, xref:attachment$form-appendix-b-es-lp.docx[Streamlined Application Appendix B (Spanish Large Print)],09/17,SO,DHS
94A Appendix C, xref:attachment$form-appendix-c.docx[Streamlined Application Appendix C],07/23,SO,DHS
94A Appendix C SP, xref:attachment$form-appendix-c-es.docx[Streamlined Application Appendix C (Spanish)],04/22,SO,DHS
94A Appendix C LP, xref:attachment$form-appendix-c-lp.docx[Streamlined Application Appendix C (Large Print)],09/17,SO,DHS
94A Appendix C SP LP, xref:attachment$form-appendix-c-es-lp.docx[Streamlined Application Appendix C (Spanish Large Print)],09/17,SO,DHS
94A Appendix D, xref:attachment$form-appendix-d.docx[Streamlined Application Appendix D],07/23,SO,DHS
106,xref:attachment$form-106.docx[Insurance Clearance] ,06/24,Screen Print, DHS
107,xref:attachment$form-107.docx[SSI Status Change],06/24,Screen Print, DHS
109,xref:attachment$form-109.docx[SSI Cont Med Determination Notice (Ex Parte Cover Letter)],06/24,Screen Print, DHS
109 SP,xref:attachment$form-109-es.docx[SSI Cont Med Determination Notice (Ex Parte Cover Letter) (Spanish)],06/24, Screen Print, DHS
118,xref:attachment$form-118.docx[Request for Hearing],01/22, Screen Print, DHS
118 SP,xref:attachment$form-118-es.docx[Request for Hearing (Spanish)],02/10, Screen Print, DHS
123,Interagency/Interoffice Update and Follow-Up,, Forms OL, DHS
124,xref:attachment$form-124.pdf[Application for Health Insurance Premium Payments],11/22,Screen print, DCH
124I,xref:appendix-f/form-124i.adoc[Instructions: Application for Health Insurance Premium Payments], 11/21, Screen Print, DCH
125,xref:attachment$form-125.pdf[CHIPPRA Application],05/23,Screen Print, DCH
129,xref:attachment$form-129.docx[Recipient Notice for Spousal Impoverishment],06/24,Screen Print, DHS
130,xref:attachment$form-130.docx[TANF and Family Medicaid Child and Medical Support Letter],06/16,Screen Print, DHS
130 SP,xref:attachment$form-130-es.docx[TANF and Family Medicaid Child and Medical Support Letter (Spanish)],06/16,Screen Print, DHS
136,xref:attachment$form-136.docx[County Request for Final Appeal],02/10,Screen Print, DHS
138,xref:attachment$form-138.docx[Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate with DCSS],6/16,Screen Print  ,DHS
138 SP,xref:attachment$form-138-es.docx[Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate with DCSS (Spanish)],6/16,Screen Print,DHS
139,xref:attachment$form-139.docx[Contribution Statement],02/10,Screen Print,DHS
139 SP,xref:attachment$form-139-es.docx[Contribution Statement (Spanish)],02/10,Screen Print,DHS
171,xref:attachment$form-171.docx[Parent to Child Deeming Worksheet],10/12,Screen Print, DHS
172,xref:attachment$form-172.docx[ABD MAO Individual/Couple/Spouse to Spouse Deeming],10/12, SO,DHS
172I,xref:appendix-f/form-172i.adoc[Instructions: ABD MAO Individual/Couple/Spouse to Spouse Deeming],,Screen Print, DHS
173,xref:attachment$form-173.docx[Verification Checklist],06/10,Screen Print, DHS
173 SP,xref:attachment$form-173-es.docx[Verification Checklist (Spanish)],06/10,Screen Print, DHS
173I,xref:appendix-f/form-173i.adoc[Instructions: Verification Checklist],,Screen Print, DHS
174,xref:attachment$form-174.docx[SMEU Medical Records Cover Letter],06/24, Screen Print, DHS
184,SMEU Data Report,06/24,SO,DHS
185,xref:attachment$form-185.docx[Affidavit of Paternity],10/12,SO,DHS
214,xref:attachment$form-214.docx[Medicaid Notification Form],08/24,SO,DHS
214 SP,xref:attachment$form-214-es.docx[Medicaid Notification Form (Spanish)],11/07,SO,DHS
216,xref:attachment$form-216.docx[Declaration of Citizenship],06/24,Screen Print, DHS
216 SP,xref:attachment$form-216-es.docx[Declaration of Citizenship (Spanish)],06/24,Screen Print, DHS
217,xref:attachment$form-217.docx[Affidavit to Establish Identity for Medicaid Applicant/Recipients < 16],06/24,SO, DHS
217 SP,xref:attachment$form-217-es.docx[Affidavit to Establish Identity for Medicaid Applicant/Recipients < 16 (Spanish)],06/24,SO,DHS
218,xref:attachment$form-218.docx[Citizenship/Identity Verification Checklist],06/24,Screen Print, DHS
218 SP,xref:appendix-f/form-218-es.adoc[Citizenship/Identity Verification Checklist (Spanish)],01/14,Screen Print, DHS
219,xref:attachment$form-219.docx[Affidavit of Facts Concerning Citizenship],06/24,Screen Print, DHS
219I,Instructions: Affidavit of Facts Concerning Citizenship,,,DHS
219 SP,xref:attachment$form-219-es.docx[Affidavit of Facts Concerning Citizenship (Spanish)],06/24,Screen Print, DHS
223,xref:attachment$form-223.docx[Medicaid and IV-E Application for Foster Care],10/12,Screen Print,DHS
223I,xref:appendix-f/form-223i.adoc[Instructions:Medicaid and IV-E Application for Foster Care], ,Screen Print, DHS
224,xref:attachment$form-224.docx[Removal Home Income and Asset Checklist],10/12,Screen Print,DHS
224I,xref:appendix-f/form-224i.adoc[Instructions:Removal Home Income and Asset Checklist],,Screen Print, DHS]
225,xref:attachment$form-225.docx[IV-E Eligibility Documentation Sheet],10/12,Screen Print,DHS
226,xref:attachment$form-226.docx[Medicaid and IV-E Redetermination Form],10/12,Screen Print,DHS
226I,xref:appendix-f/form-226i.adoc[Instructions:Medicaid and IV-E Redetermination Form],10/12,Screen Print,DHS
227,xref:attachment$form-227.docx[Notification of Change in Foster Care or Adoption Assistance],10/12,Screen Print,DHS
227I,xref:appendix-f/form-227i.adoc[Instructions:Notification of Change in Foster Care or Adoption Assistance],10/12,Screen Print,DHS
238,xref:attachment$form-238.docx[Medically Needy Budget Sheet],08/11,SO,DHS
239M,xref:attachment$form-239m.docx[MAGI Budget Sheet],04/23,Screen Print, DHS
243,xref:attachment$form-243.docx[Providing Verification of Citizenship for Medicaid],05/08,Screen Print, DHS
243 SP,xref:attachment$form-243-es.docx[Providing Verification of Citizenship for Medicaid (Spanish)],05/08,Screen Print, DHS
245,SMEU Request Form,06/24,SO,DHS
256,Interview Guide for TANF/FS/Medicaid,,SO,DHS
285,Third Party Liability,01/06,Gainwell,DCH
297,xref:attachment$form-297.docx[Application for TANF Food Stamps or Medical Assistance]. For voter registration information refer to <<Voter Registration Application Form Information>>,07/23,SO,DHS
297,Application for TANF Food Stamps or Medical Assistance (Arabic Chinese Farsi Hmong Italian Portuguese Russian or Vietnamese),,Hard Copy Only,DHS
297 SP,xref:attachment$form-297-es.docx[Application for TANF Food Stamps or Medical Assistance (Spanish)] For voter registration information refer to <<Voter Registration Application Form Information>> ,10/22,SO,DHS
297 LP,xref:attachment$form-297-lp.docx[Application for TANF Food Stamps or Medical Assistance (Large Print)]. For voter registration information refer to <<Voter Registration Application Form Information>>,12/21,SO,DHS
297 SPLP,xref:attachment$form-297-es-lp.docx[Application for TANF Food Stamps or Medical Assistance (Spanish Large Print)]. For voter registration information refer to <<Voter Registration Application Form Information>>,12/21,SO,DHS
297A,xref:appendix-f/form-297a.adoc[Rights and Responsibilities],10/22,SO,DHS
297A,Rights and Responsibilities,,Hard Copy Only,DHS
297A SP,xref:appendix-f/form-297a-es.adoc[Rights and Responsibilities (Spanish)],10/22 (Spanish)],SO,DHS
297A LP,xref:attachment$form-297a-lp.docx[Rights and Responsibilities (Large Print)],12/21 ,SO,DHS
297A SPLP,xref:attachment$form-297a-es-lp.docx[Rights and Responsibilities (Spanish Large Print)],12/21,SO,DHS
297M,Medicaid Addendum to Form 297 (Obsolete as of 12/2021),01/14,SO,DHS
297M SP,Medicaid Addendum to Form 297 (Spanish) (Obsolete as of 12/2021) ,01/14,SO,DHS
306,xref:attachment$form-306.docx[Annuity Issuer Notification],08/24,Screen Print, DHS
315,xref:attachment$form-315.docx[Official Notice of Georgia Medicaid Estate Recovery Program],10/21,Screen Print, DCH
315 SP,xref:attachment$form-315-es.docx[Official Notice of Georgia Medicaid Estate Recovery Program (Spanish)],10/21,Screen Print, DCH
315 LP,xref:attachment$form-315-lp.docx[Official Notice of Georgia Medicaid Estate Recovery Program (Large Print)],10/21,Screen Print, DCH
315 SPLP,xref:attachment$form-315-es-lp.docx[Official Notice of Georgia Medicaid Estate Recovery Program (Spanish Large Print)],10/21,Screen Print, DCH
327,xref:attachment$form-327.docx[Estate Recovery Notification Form],07/22,Screen Print,DCH
328,xref:attachment$form-328.docx[Quarterly Report Form],06/24,Screen Print, DHS
328 SP,xref:attachment$form-328-es.docx[Quarterly Report Form (Spanish)],06/24,Screen Print, DHS
400,Medically Needy First Day Liability Authorization for Reimbursement,4/93,Hard Copy Only,DCH
403,xref:attachment$form-403.docx[Adoption Assistance Benefits Memorandum],05/11,Screen Print, Adoptions
411,xref:attachment$form-411.docx[Undue Hardship Waiver Application],06/24,Screen Print, DHS
411 SP,xref:attachment$form-411-es.docx[Undue Hardship Waiver Application (Spanish)],06/24,Screen Print, DHS
508,xref:attachment$form-508.docx[Food Stamp TANF Medicaid Renewal Form].For voter registration information refer <<Voter Registration Application Form Information>>,10/22,SO,DHS
508 SP,xref:attachment$form-508-es.docx[Food Stamp TANF Medicaid Renewal Form]. For voter registration information refer to <<Voter Registration Application Form Information>>,10/22 (Spanish),SO,DHS
508 LP,xref:attachment$form-508-lp.docx[Food Stamp TANF Medicaid Renewal Form (Large Print)]. For voter registration information refer to <<Voter Registration Application Form Information>>,12/21,SO,DHS
508 SPLP,xref:attachment$form-508-es-lp.docx[Food Stamp TANF Medicaid Renewal Form (Spanish Large Print)]. For voter registration information refer to <<Voter Registration Application Form Information>>,12/21,SO,DHS
512,xref:attachment$form-512.docx[Notification of Eligibility-EMA],06/24,Screen Print,DHS
512 SP,xref:attachment$form-512-es.docx[Notification of Eligibility-EMA (Spanish)],06/24,Screen Print, DHS
526,xref:attachment$form-526.docx[Physician’s Statement for EMA],08/24,Screen Print, DCH
700,xref:attachment$form-700.docx[Application for Medicaid & Medicare Savings for Qualified Beneficiaries],10/22,SO,DHS
700 SP,xref:attachment$form-700-es.docx[Application for Medicaid & Medicare Savings for Qualified Beneficiaries (Spanish)],10/22,SO,DHS
700 LP,xref:attachment$form-700-lp.docx[Application for Medicaid & Medicare Savings for Qualified Beneficiaries (Large Print)],01/22,SO,DHS
700 SPLP,xref:attachment$form-700-es-lp.docx[Application for Medicaid & Medicare Savings for Qualified Beneficiaries (Spanish Large Print)],01/22,SO,DHS
701,xref:attachment$form-701.docx[Q-Track Brochure],08/24,SO,DHS
703,xref:attachment$form-703.docx[Medicare Buy-In Problem Template],06/24,Screen Print, DHS
704,xref:attachment$form-704.docx[TEFRA/Katie Beckett Cost Effectiveness Form],10/04,Screen Print, DCH
705,xref:attachment$form-705.docx[TEFRA/Katie Beckett LOC Determination Routing Form],05/12,Screen Print, DCH
706,xref:attachment$form-706.docx[TEFRA/Katie Beckett Medical Necessity LOC Statement],01/18,Screen Print, DCH
713,xref:attachment$form-713.docx[Interagency Interoffice referral/ Follow Up],11/10,SO,DHS
809,xref:attachment$form-809.docx[Verification of Earned Income],06/16,SO, DHS
809 SP,xref:attachment$form-809-es.docx[Verification of Earned Income (Spanish)],06/16,SO,DHS
936,xref:attachment$form-936.docx[QIT Certification],06/24,Screen Print, DCH
937,xref:attachment$form-937.docx[QIT Review Letter],06/24,Screen Print, DHS
937 SP,xref:attachment$form-937-es.docx[QIT Review Letter (Spanish)],06/24,Screen Print, DHS
938,Understanding Medicaid (Spanish),,Gainwell,DCH
939, Understanding Medicaid,,Gainwell,DCH
942, xref:attachment$form-942.docx[IME Verification Form],08/24,Screen Print, DHS
943, xref:attachment$form-943.docx[Notification of Deduction of Medical Expense],06/24,Screen Print, DHS
944, xref:attachment$form-944.docx[IME Query Form],06/24,Screen Print, DCH
945, xref:attachment$form-945.docx[QIT Trustee Guide],06/24,Screen Print, DCH
945 SP, xref:attachment$form-945-es.docx[QIT Trustee Guide (Spanish)],06/24,Screen Print, DCH
946, xref:attachment$form-946.docx[QIT Frequently Asked Questions and Worksheet],06/24,Screen Print, DCH
946 SP, xref:attachment$form-946-es.docx[QIT Frequently Asked Questions and Worksheet (Spanish)],06/24,Screen Print, DCH
947, xref:attachment$form-947.docx[QIT Approved Format Deviation],08/24,Screen Print, DHS
948, xref:attachment$form-948.docx[QIT Approved Template 1],08/24,Screen Print, DCH
949, xref:attachment$form-949.docx[QIT Checklist],08/24,Screen Print, DCH
950, xref:attachment$form-950.docx[Facility Action Request],10/12,Screen Print, DHS
954, xref:attachment$form-954.docx[OptumRx Prescription Update Template],06/24,Screen Print, DHS
955, xref:attachment$form-955.docx[Notice of Review of Promissory Note Loan or Property Agreement],06/24,Screen Print, DHS
955 SP, xref:attachment$form-955-es.docx[Notice of Review of Promissory Note Loan or Property Agreement (Spanish)],06/24,Screen Print, DHS
956, xref:attachment$form-956.docx[Special Needs Trust Routing Form],08/24,Screen Print, DHS
958, xref:attachment$form-958.docx[Nursing Facility Information Request],06/24,Screen Print, DHS
960, xref:attachment$form-960.docx[IME Pricing Document],08/24,Screen Print, DCH
962, Certification of Medicaid Eligibility,07/23,SO, DHS
963, Medicaid Notification Form,01/07,SO,DHS
963I, Instructions: Medicaid Notification Form,,,DHS
966, xref:attachment$form-966.docx[Absent Parent Information Form],08/24,Screen Print, DHS
967,xref:attachment$form-967.docx[Non-Emergency Medical Transportation Information Sheet (NEMT)],08/24,Screen Print, DCH
967 SP,Non-Emergency Transportation Broker Sheet (Spanish),,Screen Print, DCH
968, xref:attachment$form-968.docx[MN PL Budget Sheet],10/12,Screen Print, DHS
969, xref:attachment$form-969.docx[Living Arrangement Determination - LA/ISM Guide],10/12,Screen Print, DHS
970, xref:attachment$form-970.docx[VA Communication Form],08/24,SO,DHS
984, xref:attachment$form-984.docx[Burial Contract Verification],07/24,Screen Print, DHS
985,xref:attachment$form-985.docx[Burial Exclusion and Designation],08/24,Screen Print, DHS
986,xref:attachment$form-986.docx[MAO Cemetery Lot Verification],08/24,Screen Print, DHS
987, xref:attachment$form-987.docx[Designation of Cemetery Lot],08/24,Screen Print, DHS
988, xref:attachment$form-988.docx[Notice of Review of Annuity],06/24,Screen Print, DCH
988 SP, xref:attachment$form-988-es.docx[Notice of Review of Annuity (Spanish)],06/24,Screen Print, DCH
991, xref:attachment$form-991.docx[MAO Property Search Record],08/24,SO,DHS
995, xref:attachment$form-995.docx[Pathways Qualifying Activities Report Form],07/23,Screen Print, DHS
996, xref:attachment$form-996.docx[Pathways Good Cause RM and RA Form],07/23,Screen Print, DHS
998, xref:attachment$form-998.docx[Notice of Termination of Medicaid Benefits Due to Contract(s)],08/24,Screen Print, DHS
1610-U2, Public Assistance Agency Information,02/82,SSA,Social Security
3327, xref:attachment$form-3327.docx[Health Check Brochure],10/22,Gainwell,DCH
3328, Health Check Brochure (Spanish),,Gainwell,DCH
3329, Health Check Brochure (Braille),,Gainwell,DCH
5459, xref:attachment$form-5459.docx[Authorization for Release of Information],07/16,SO,DHS
5459 SP, xref:attachment$form-5459-es.docx[Authorization for Release of Information (Spanish)],07/16,SO,DHS
5460, xref:attachment$form-5460.docx[Notice of Privacy Practices],12/23,Screen Print, DHS
5460 SP,xref:attachment$form-5460-es.docx[Notice of Privacy Practices (Spanish)],12/23,Screen Print, DHS
, Notice of Privacy Practices (Arabic Chinese Farsi Hmong Italian Portuguese Russian Vietnamese),,Hard Copy Only, DHS
G-845-S, INS SAVE Document Verification,, DHS,INS
SS-5, Application for a Social Security Card,,SSA,Social Security
,xref:attachment$form-foster-care-worker-card.docx[Foster Care Worker Card],04/04,Screen Print, DHS
,xref:attachment$form-gmwd-fact-sheet.docx[GMWD Fact Sheet],09/17,Screen Print, DHS
,xref:appendix-f/form-icama-member-contact-list.adoc[ICAMA Member Contact List],,NA,DHS
,xref:appendix-f/form-icama-non-member-contact-list.adoc[ICAMA Non-Member Contact List],,,DHS
,xref:attachment$form-iv-e-budget-sheet.docx[IV-E Budget Sheet],10/12,Screen Print, DHS
,xref:attachment$form-dcss-noncooperation-letter.docx[Letter of Non-Cooperation with DCSS],10/12,Screen Print, DHS
,xref:attachment$form-level-of-care-agreement.docx[Level of Care Agreement],,NA,DBHDD
,xref:attachment$form-medicaid-review-repsonse-form.docx[Medicaid Review Response Form],05/16,Screen Print, DHS
,xref:attachment$form-medically-needy-option-statement.docx[Medically Needy Option Statement],05/15,Screen Print, DHS
,xref:attachment$form-medicare-part-d-complaint-checklist.docx[(Medicare) Part D Complaint Checklist],,Screen Print, CMS
,xref:attachment$form-peachcare-special-request-form.docx[PeachCare Special Request Form],,Screen Print, DCH
,xref:attachment$form-record-of-life-ins-policies.docx[Record of Life Insurance Policies],01/07,Screen Print, DHS
,xref:attachment$form-tefra-kb-cover-letter.docx[TEFRA/Katie Beckett Cover Letter],05/12,Screen Print, DHS
,xref:attachment$form-tefra-kb-cover-letter-es.docx[TEFRA/Katie Beckett Cover Letter (Spanish)],04/05,Screen Print, DHS
,xref:attachment$form-tefra-kb-worksheet.docx[TEFRA/Katie Beckett Worksheet], 08/11,Screen Print, DHS
,xref:attachment$form-undue-hardship-letter.docx[Undue Hardship Waiver Letter],02/07,Screen Print, DHS
,xref:attachment$form-whm-pst.pdf[Women's Health Medicaid Physician's Statement of Treatment],09/23,Screen Print, DHS
,Women's Health Medicaid Physician's Statement of Treatment (Spanish),04/23,Screen Print, DHS
,Women's Health Medicaid Review Form (Obsolete as of 21/2022),01/14, Screen Print, DHS
,===
