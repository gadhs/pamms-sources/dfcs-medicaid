= 223i Instructions for Form 223: Medicaid and IV-E Application for Foster Care and Adoption Assistance
:form-number: 223i

== Purpose

Form 223 is to be used by the SSCM/JPPS to apply for Medicaid and to request a IV-E determination on behalf of a foster care or adoption assistance child.
An application should be completed and filed for *each* child who comes into foster care within five (5) working days of the child's placement regardless of the length of stay.
Provide information for all questions to the extent possible.

=== Instructions

*APPLICANT CHILD INFORMATION:* The child's name should be listed as it appears on the Social Security Identification card.
Registration of an application using a nickname or incomplete name may cause problems with the payment of Medicaid claims.
The screening process on SUCCESS should be thorough.
It is important that a new identification number not be given if the child already has a client number assigned in the system.

The fields for SSN, date of birth, gender, race, and citizenship are self-explanatory.
Provide all information known on child's mother and father including address, SSN, race, DOB, legal relationship, paternity and court ordered child support.

*MEDICAID INFORMATION SECTION:*

NOTE: If the child has returned home prior to making application, the entire form is completed and faxed to the appropriate Revenue Maximization Regional Office and not to the Revenue Maximization Intake Unit.

*County:* enter custody county

*Removal Date:* enter removal date

*Prior Months MAO?* Is Medicaid needed for any of the three months prior to the application month?
List month(s).

If Medicaid is being requested for any of the three months prior to the application month, complete a separate Form 224 for the application month and for each month of retroactive MAO requested.

*Questions 1* and *2* are self-explanatory and are addressed with Form 224, Removal Home – Income and Asset Checklist, which should accompany the IV-E Information Section.
However, if only the form 223 is used, income should be entered as gross income.

*Question 3* is self-explanatory.
A copy of verification of pregnancy and estimated date of delivery must be faxed with this form if available.
If not, notate medical provider and telephone number for verification.

*Question 4* is self-explanatory.
A copy of the insurance card, if available, should be attached to the Form 223 and faxed with the form.

*The form should be signed and dated by the SSCM/JPPS with printed name and the fax number*.

*Fax form and required documentation to the Revenue Maximization Intake Unit at 770-473-2620.*

*IV-E INFORMATION SECTION:*

*Initial court orders faxed: Revenue Maximization records are now required to have copies of all pertinent court orders.
Have the initial court orders been faxed to the appropriate Rev Max MES?*

*Question 4a.* List the name of the person the child was physically living with at the time of the removal.

*Question 4b*.
Indicate if the person named in 4a.
is a parent, specified relative within the degree of relationship, or other.
If specified relative or other is checked, explain the relationship to the child.

*Question 4c*.
List the individual from whom legal custody was removed in the court order removing the child from the home.

*Question 4d*.
If this is the same person listed in 4a, indicate “Yes”.
If the answer is no, determine if the child lived with the person listed in 4c within the 6 months prior to the removal from the home.
If the child lived with the individual within the 6 months, list the month(s) that the child lived with the person.
List all individuals living in the home at the time the child was removed.

*Question 5* is self-explanatory.
If disability/incapacity or unemployed parent is indicated, additional information may be needed by the Rev Max MES to determine eligibility.

*Question 6* is self-explanatory.

*Question 7* is self-explanatory.
The SSCM/JPPS's statement is accepted if in writing, signed and dated by the SSCM/JPPS.

This section of the form must be signed and dated by the SSCM/JPPS.
Please print name and provide a contact phone number with area code.

== Distribution

File the original form in the case record and fax a copy to Revenue Maximization Intake Unit at 770-473-2620 for Medicaid application.
Fax the completed IV-E Information to the appropriate Revenue Maximization Regional Office.