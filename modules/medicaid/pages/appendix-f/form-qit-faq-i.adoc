= Qualified Income Trust FAQ and Work Sheet Instructions

== Purpose:

This form must be given to all applicants (or their representatives) for ABD long-term care classes of assistance whose income places them over the Medicaid CAP.
The ABD Medicaid Eligibility Specialist completes the form.

== Instructions/Work Sheet:

Income Source – insert the gross monthly amount in appropriate blanks.
Refer to Chapter 2400 for income sources that are countable in the eligibility budget.

Total Monthly Income – insert the sum of the individual income amounts.

Current Medicaid Cap - $1.00 – insert the current Medicaid Cap limit minus a dollar.

Amount over Cap – insert the difference between the gross monthly income and the Medicaid Cap minus one dollar.

Allowable QIT Disbursements – insert the appropriate amounts in the space provided.
This information is drawn from the Patient Liability/Cost Share (PL/CS) Budget.
Refer to Chapter 2550 for PL/CS budget policy.

File a copy of the QIT Worksheet in the case record.

== Instructions/FAQ page:

Insert the “Amount Over Cap” from the work sheet in the blank provided in the second paragraph from the bottom.

QIT FAQ/Work Sheet Instructions 04/05
