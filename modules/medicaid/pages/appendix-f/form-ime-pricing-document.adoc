= IME Pricing Document

Use the Pricing Document only if you receive a Form 942 (Incurred Medical Expense) on an A/R who has to pay a cost share or patient liability amount.
The Document is a list of medical services not covered by Medicaid.
The cost share or patient liability amount may be adjusted to allow for non-covered services only.

Only adjust the cost share or patient liability for a non-covered service that is prescribed by a doctor or dentist.
Refer to the Form 942 for verification.

Do not adjust the cost share or patient liability for a service that would have been paid by Medicaid had the A/R used a Medicaid-participating provider.
Medicaid does not pay non-participating providers for a covered service.
Refer to the Form 942 for verification.

When you receive a Form 942 on an A/R, use the Pricing Document to determine if the service is non-covered.
If you think that the Pricing Document does not provide you the information that you need for a particular Form 942, submit the Form 942 to the Department of Community Health for review.
Attach the IME Query form to the Form 942 and mail to the address on the IME Query form.

Medical services *not* covered by Medicaid for adults, age 21 and older are:

* routine dental services (tooth extractions are covered),
* complete and partial dentures,
* prescription eyeglasses, and
* hearing aids.

Refer to the listing of these services for the maximum amount to allow as an income deduction.
*If the medical care provider charges less than the maximum amount listed in the Document, allow the lesser of the two amounts*.

== Eyeglasses

|===
| Item | Maximum Allowed

| eye exam/office visit (determine refractive state)
^| $41

| eyeglass frames
^| $35

| bifocal lenses
^| $45

| monofocal lenses
^| $25

| fitting of bifocals (dispensing of)
^| $34

| fitting of monofocals
^| $28
|===

== Hearing Aids

|===
| Item | Maximum Allowed

|Hearing Aid
^|$833

|Hearing Aid, Programmable
^|$1193

|Earmold, Hearing Aid, Not Disposable, (custom filled, per ear mold, 12/yr.)
^|$45

|Repairs/Labor
^|$45

|Batteries, Hearing Aid (1 pkg.
per month)
^|$5

|Hearing Aid Supplies/Accessories: garment - 2/yr.
^|$23

|Hearing Aid Supplies/Accessories: cord - 6/yr.
^|$10
|===

== Home Health Care and Supplies (for the CCSP COA's only)

|===
| Item | Maximum Allowed

|Incontinence Care (diapers, pads and briefs)
|allow actual cost

|Liquid Nutritional Supplements (i.e. Ensure)
|allow actual cost

|Over-the-Counter Medical Supplies
|allow actual cost
|===

== Prescription Drugs

Most categories of prescription (legend) drugs are covered services through the Medicaid pharmacy program.
There are very few categories of non-covered drugs.
Only non-covered drugs are allowed as an income deduction.

Below are the categories of drugs not covered for adults, thus, can be allowed as an income deduction from liability or cost share:

* prescription (legend) cough and cold medications,
* over-the-counter drugs, if prescribed by doctor (note: this applies only to the CCSP COA) and
* vitamin and mineral supplements, if prescribed by doctor (note: this applies only to the CCSP COA).

NOTE: Vitamin/mineral supplements and OTC drugs are not allowed as income deductions for A/R's in nursing homes or institutional hospice as the nursing home or hospice provider is to provide these drugs at no cost to the A/R.

=== Prescription Cough and Cold Drugs

Allow the full cost of these non-covered drugs as given on the Form 942.
The Form 942 must be completed in full and signed by the pharmacist dispensing the drug.
An incomplete Form 942 must be returned for proper completion.

=== Vitamin/Mineral Supplements and OTC Drugs (CCSP A/R's Only)

Allow the full cost of these non-covered drugs as given on the Form 942.
The Form 942 must be completed in full and signed by the pharmacist in the store selling the OTC's and the vitamin/mineral supplements.
The Form 942 is needed to confirm a doctor's prescription for the items, as well as all other information needed.

=== Important Information on Medicaid Drug Coverage

Some Medicaid-covered drugs require prior approval before Medicaid pays.
Drugs denied for prior approval may be reconsidered for payment if the doctor appeals the denial.
Also, the doctor usually can prescribe a different drug with the same therapeutic effect, which can be paid through the Medicaid pharmacy program.

. *If the pharmacist's response to item #3 on the Form 942 is “Yes,” do not allow the cost of the drug as an income deduction for liability or cost share.*
. *If the pharmacist's response to item #4 on the Form 942 is “Yes,” do not allow the cost of the drug as an income deduction for liability or cost share.*

== Nursing Home Services

Do not allow the cost of items or services listed below as an income deduction from patient liability.
These services are paid to the nursing home through the daily Medicaid reimbursement rate (per diem).
Some of the services include but are not limited to:

* liquid nutritional supplements,
* over-the-counter drugs (OTC's), such as antidiarrheals, antacids, analgesics (i.e. aspirin, ibuprophen, acetaminophen), artificial tears, skin ointments, bandages and other such items,
* over-the-counter laxatives and stool softeners,
* incontinency care items, such as pads, diapers, special mattresses, and
* durable medical equipment, such as wheelchairs, walkers, lifts, beds.

An A/R or the A/R's family may choose to pay for certain medical items or services out of personal preference rather than medical necessity.
Examples include a private duty nurse, a private room or bed-hold days.
Do not allow the cost of personal preference items or services as an income deduction from patient liability.

== Physician and Psychiatric Services

Submit the completed Form 942 for a physician service to the Department of Community Health for a decision regarding allowing the cost of the service.

== Podiatry and Orthopedic Services

Submit the completed Form 942 for a podiatry or orthopedic service to the Department of Community Health for a decision regarding allowing the cost of the service.

== Psychological Services

|===
| Service | Maximum Allowed

|Psychological Diagnostic Interview / Evaluation / Testing
|$62 per 1 hour unit

|Individual or Family Psychotherapy
|$53 per ½ hour unit

|Group Psychotherapy
|$29 per ½ hour unit
|===

== Dental Services

[cols=4*]
|===
4+| *Full-Mouth Radiographs*
|D0210
|Full-Mouth Series
^|(EPSDT ONLY)
>|$72.45

2+| *Individual Periapical Radiographs*
^| "
|

|D0220
|Periapical, One Film
^| "
>| $13.45

| D0230
|Periapical, each additional film
^| "
>|$10.35

2+| *Occlusal Radiographs*
^| (EPSDT ONLY)
|

| D0240
|Occlusal Film, one film
^| "
>| $19.66

2+| *Individual Bitewing Radiographs*
^| "
|

| D0270
|Bitewing, One Film
^| "
>| $14.49

| D0272
|Bitewing, Two Films
^| "
>| $21.73

| D0274
|Bitewing, Four Films
^| "
>| $33.12

4+| *Diagnostic and Clinical Examinations and Services*

| D0150
| Comprehensive Oral Evaluation
^| (EPSDT ONLY)
>| $39.33

| D0120
| Periodic Oral Evaluation
|
>| $ 22.77

4+| *Amalgam Restorations (Including local anesthesia, base and polishing)*
| D2140
| Permanent, One Surface
^| "
>| $60.03

|
| Primary
^| "
>| $53.82

| D2150
| Permanent, Two Surfaces
^| "
>| $77.62

|
| Primary
|
>| $69.34

| D2160
| Permanent, Three Surfaces
^| "
>| $94.18

|
| Primary
|
>| $82.80

| D2951
| Pin Retention (exclusive of restoration)
^| "
>| $28.98

| D2999
| Endo Post
^| "
>| $54.22

4+| *Acrylic and Composite Restorations*

| D2330
| Composite - One Surface-Anterior
^| "
>| $71.41

| D2331
| Composite - Two Surfaces-Anterior
^| "
>| $91.08

| D2332
| Composite - Three or More Surfaces - Anterior
^| "
>| $110.74

| D2951
| Composite, Pin Retained
^|(EPSDT ONLY)
>| $28.98

| D2391
| Composite - One Surface Posterior - Primary
^| "
>| $80.73

| D2391
| Permanent
^| "
>| $88.80

| D2392
| Composite - Two Surface Posterior - Primary
^| "
>| $95.22

| D2392
| Permanent
|
>| $110.74

| D2394
| Composite – Four More Surfaces - Primary
^| "
>| $126.37

| D2394
| Permanent
|
>| $151.42

4+| *Crowns*

| D2932
| Plastic, Acrylic, Performed, or Composite Crown
^| "
>| $176.98

| D2930
| Stainless Steel, primary tooth open-face stainless steel crown with composite or acrylic facing
^| "
>| $143.86

| D2931
| Stainless Steel, permanent open-face stainless steel crown with composite or acrylic facing
^| "
>| $162.49

4+| *Other Restorative Services*

| D2970
| Fracture of Tooth - Composite Build-up
^| "
>| $154.21

| D2920
| Re-cement Crowns
^| "
>| $41.40

| D2940
| Sedative Fillings
^| "
>| $54.85

2+| *Endodontic Services*
^|(EPSDT ONLY)
|

| D3220
|Pulpotomy
^| "
>| $90.04

4+| *Root Canal Therapy*

| D3310
| Deciduous (per tooth)
^| "
>| $77.64

| *D3310
| One Canal - Permanent
^| "
>| $379.84

| D3320
| Two Canals - Permanent
^| "
>| $463.68

| D3999
| Emergency - Open Pulp Chamber to Establish Drainage
^| "
>| $91.08

2+| *Periapical Services*
^| "
|

| D3410
| Apicoectomy - performed as separate surgical procedure
^| "
>| $229.81

| D3426
| Apicoectomy - any and all additional roots
^| "
>| $38.06

4+| *Periodontal Services*

| *D4341
| Periodontal Scaling and Root Planning, per quadrant
^| "
>| $140.76

| *D4210
| Gingivectomy or Gingivoplasty, per quadrant
^| (EPSDT ONLY)
>| $157.38

| *D4220
| Gingival Curettage, per quadrant
^| "
>| $129.37

| *D4260
| Osseous Surgery, per quadrant
^| "
>| $341.00

| D4271
| Autogenous Graft
^| "
>| $259.84

| D4270
| Pedicle Graft
^| "
>| $272.14

4+| *Prosthodontic Services, Removable Complete Dentures*

| D5110
| Complete Upper
^| "
>| $673.78

| D5120
| Complete Lower
^| "
>| $673.78

| D5130
| Immediate Upper
^| "
>| $554.12

| D5140
| Immediate Lower
^| "
>| $554.12

2+| *Partial Dentures*
^| "
|

| D5201
| Upper - Acrylic base w/wrought wire clasps
^| "
>| $276.64

| D5202
| Lower - Acrylic base w/wrought wire clasps
^| "
>| $276.64

| D5211
| Upper Partial-Resin Base (Including Any Conventional Clasps, Rests and Teeth)
^| "
>| $569.25

| D5212
| Lower Partial-Resin Base (Including Any Conventional Clasps, Rests and Teeth)
^| "
>| $661.36

| *D5899
| Upper - Acrylic base w/o clasps
^| "
>| $184.46

| *D5899
| Lower - Acrylic base w/o clasps
^| "
>| $184.46

4+| *Repairs to Dentures*

| D5410
| Adjustment - Complete Denture Upper
^| "
>| $23.77

| D5411
| Adjustment - Complete Denture Lower
^| "
>| $23.77

| D5421
| Adjustment - Partial Denture Upper
^| "
>| $11.76

| D5422
| Adjustment - Partial Denture Lower
^| "
>| $11.76

| D5510
| Repair broken complete or partial denture - no teeth broken
^| "
>| $73.48

| D5640
| Repair broken complete or partial denture - replace one or more broken teeth
^| "
>| $92.17

| D5650
| Adding tooth to partial denture to replace extracted tooth
^| "
>| $92.17

| D5660
| Adding clasp to existing partial denture
^| "
>| $110.74

| D5750
| Laboratory Relining Upper
^| "
>| $156.56

| D5751
| Laboratory Relining Lower
^| "
>| $156.56

| D5850
| Tissue Conditioning/upper
^| "
>| $47.54

| D5851
| Tissue Conditioning/lower
^| "
>| $47.54

4+| *Alveoplasty (Surgical preparation of ridge for dentures)

| *D7310
| Alveoplasty in conjunction with extractions/quad
^| (EPSDT ONLY)
>| $150.07

| D7310
| Alveoplasty, less than a quadrant In conjunction with extractions
|
>| $54.22

4+| *Alveoplasty without extractions*

| *D7320
| Alveoplasty without extractions, (quadrant)
^| (EPSDT ONLY)
>| $669.64

| D7320
| Alveoplasty less than a quadrant without extractions
|
>| $63.86
|===

=== Surgical Excision

*Codes D7971, D7440, D7450, D7451, D7460, D7461, D7471 and D7480 are non-covered for Adults.*

[cols=4*]
|===
| D7440
| Excision of malignant tumor Lesion diameter up to 1.25cm
^| "
>| $843.52

| D7450
| Removal of benign odontogenic cyst or tumor up to 1.25cm
^| "
>| $477.13

| D7451
| Removal of benign odontogenic cyst or cyst or tumor 1.25cm or larger
^| "
>| $750.37

| D7460
| Removal of benign nonodontogenic cyst or tumor - up to 1.25cm
^| "
>| $477.13

| D7461
| Removal of benign nonodontogenic cyst or tumor – over 1.25cm
^| "
>| $769.00

| D7471
| Removal of exostosis lateral - maxilla
^| "
>| $230.55

| D7471
| Removal of exostosis lateral - mandible
^| "
>| $230.55
|===

=== Other Repair Procedures

[cols=4*]
|===
| *D7960
| Frenulectomy (Frenectomy)
^| (EPSDT ONLY)
>| $315.67

| D7970
| Excision of Hyperplasic Tissue (per arch)
^| "
>| $324.99

| D7971
| Excision of Pericoronal Tissue
|
>| $85.90
|===

=== Adjunctive General Services

|===
4+| *Unclassified Treatment*
| D9110
| Palliative (emergency) treatment of dental pain, minor procedure
^| (EPSDT ONLY)
>| $51.75

4+| *Drugs*

| D9610
| Chemotherapy - Therapeutic Drug Injection
^| (EPSDT ONLY)
>| $53.82

| D9630
| Other Drugs and/or Medications
^| (EPSDT ONLY)
>| $38.29
|===
