= 963i Instructions for Medicaid Notification Form
:form-number: 963i
:revision-date: 01/07

== Purpose

This form is to be used ONLY when the system cannot be updated with correct information and therefore no system-generated notice will be produced *OR* when the system's closure notice is not appropriate.
Example: Update is to a month more that 13 months in the past, or a change to AMN FDL is needed.

== Instructions

*Header* – required.

Insert county name in the space provided in the header.

Client's complete name and mailing address must be entered on the left hand side.
Complete case and worker information must be entered in spaces provided.

*Prior Medicaid Approval/Denial/Closure* – eligibility for months too old for the system or closure when the system generated closure is not acceptable.

Information on each person for whom Medicaid was requested must be entered.
If any A/R is approved for any month, a completed Form 962 must be attached.

*ABD Patient Liability/Cost Share Change* – Information for each month PL/CS is *decreased* and cannot be processed by the system.

*Medically Needy Begin Authorization Date/First Day Liability Change* – Information for each month the BAD is *earlier* than in pervious notices of FDL has changed.

*Distribution:* Original to the client.
Copy filed in case record.
Mail or fax copy to GHP.

_Revision Date: {revision-date}_

