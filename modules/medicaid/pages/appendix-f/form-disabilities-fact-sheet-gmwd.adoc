= Disabilities Fact Sheet

== What is GMWD?

Georgia Medicaid for Workers with Disabilities (GMWD) provides new options for people with disabilities who are working, or are interested in returning to the workforce, the opportunity to buy health care through Georgia Medicaid.
GMWD offers State Medicaid Plan coverage to working age people who have a disability that is permanent but stable and have a desire to return to work.

== Why a GMWD?

GMWD establishes a public policy that encourages work.
Many people with disabilities can work at least part time, earn money and pay taxes.
Generally, people are healthier - both physically and mentally – when they have meaningful, productive lives.
An equally important but often overlooked benefit is that a GMWD program can allow people to save money.
GMWD will exempt Medical Savings Accounts and approved Independence Accounts to enable an individual to save for adaptive equipment such as a life-equipped vehicle or overhead lift system.

== What are the eligibility requirements?

* Be a Georgia Resident and meet the Citizenship requirements for Medicaid.
* You are at least 16 years of age and under age 65.
* You are disabled based on the SSA definition of disability.
* You have earned income from employment or self-employment.
* You have disability income between $600 and $699/mo.
* You have countable income less than 300% of the Federal Poverty Level (FPL) based on your family size.
* Your resources or assets are less than $4000 for an individual or $6000 for a couple.

== How much does GMWD cost?

Depending on an individual's age and income, a premium payment may be required for this health care coverage.
GMWD premiums are based on a three tier step.
Individuals under age 18 and individuals with countable income less then 150% of the FPL are not required to pay a premium.
The minimum monthly premium for individuals with countable income of 150% of the FPL or greater is $35.

== What services does GMWD cover?

GMWD provides the same services as other “full Medicaid” categories of assistance.
Family coverage is not available.

*For more information*: Contact the Department of Community Health at 404-651-9982.
