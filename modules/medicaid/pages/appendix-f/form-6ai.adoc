= 6Ai Instructions for Form DMA-6(A): Physician’s Recommendation for Pediatric Care
:form-number: 6Ai

== Instructions

It is important that EVERY item on the DMA-6(A) is answered, even if it is answered as N/A (not applicable).
Make sure that the physician or nurse who completes some of the sections is aware of this requirement.
The form is only valid for 90 days from the date of the physician's signature.
The form should be completed as follows:

=== Section A - Identifying Information

Section A of the form should be completed by the parent or the legal representative of the Katie Beckett child unless otherwise noted.
All reference to “the applicant” means the child for whom Medicaid is being applied for.

==== Item 1: Applicant's Name/Address

Enter the complete name and address of the applicant including the city and ZIP code.
For DFCS County enter the applicant's county of residence.

==== Item 2: Medicaid Number

To be completed by county staff.

==== Item 3: Social Security Number

Enter the applicant's nine-digit Social Security number.

==== Item 4 & 4A: Sex, Age and Birthdate

Enter the applicant's sex, age, and date of birth.

==== Item 5: Primary Care Physician

Enter the entire name of the applicant's Primary Care Physician.

==== Item 6: Applicant's Telephone Number

Enter the telephone number, including area code, of the applicant's parent or the legal representative.

==== Item 7: Does guardian think the applicant should be institutionalized?

If the Katie Beckett applicant were not eligible under this category of Medicaid, would s/he be appropriate for placement in a nursing facility or institution for the intellectually disabled.

Check the appropriate box.

==== Item 8: Does the child attend school?

Check the appropriate box.

==== Item 9: Date of Medicaid Application

To be completed by county staff.

==== Fields below Item 9:

Please enter the name of the primary caregiver for the applicant.
If a secondary caregiver is available to care for the applicant, include the name of the caregiver.

==== Item 10: Signature

Read the statement below the name(s) of the caregiver(s), and then, the parent or legal representative for the applicant should sign the DMA-6(A) legibly.

==== Item 11: Date

Please record the date the DMA-6 (A) was signed by the parent or the legal representative.

=== Section B - Physician's Examination Report and Recommendation

This section must be completed in its entirety by the Katie Beckett child's Primary Care Physician.
No item should be left blank unless indicated below.

==== Item 12: History

Attach additional sheet(s) if needed.

Describe the applicant's medical history (Hospital records may be attached).

==== Item 13: Diagnosis

Add attachment(s) for additional diagnoses.

Describe the primary, secondary, and any third diagnoses relevant to the applicant's condition on the appropriate lines.
Please note the ICD codes.
Depending on the diagnosis, a psychological evaluation may be required.
If you have an evaluation conducted within the past three years, include a copy with this packet.

==== Item 13A: ICD-10 Diagnosis Code

Add attachment(s) for additional diagnoses.

Describe the primary, secondary, and any third ICD-10 diagnoses relevant to the applicant's condition on the appropriate lines.

==== Item 14: Medications

Add attachment(s) for additional medication(s).

The name of all medications the applicant is to receive must be listed.
Include name of drugs with dosages, routes, and frequencies of administration.

==== Item 15: Diagnostic and Treatment Procedures

Include all diagnostic or treatment procedures and frequencies.

==== Item 16: Treatment Plan

Attach copy of order sheet if more convenient or other pertinent documentation.

List previous hospitalization dates, as well as rehabilitative and other health care services the applicant has received or is currently receiving.
The hospital admitting diagnoses (primary, secondary, and other diagnoses) and dates of admission and discharge must be recorded.
The treatment plan may also include other pertinent documents to assist with the evaluation of the applicant.

==== Item 17: Anticipated Dates of Hospitalization

List any anticipated dates of hospitalization for the applicant.
Enter N/A if not applicable.

==== Item 18: Level of Care Recommended

Check the correct box for the recommended level of care;
nursing facility or intermediate care facility for the intellectually disabled.
If left blank or N/A is entered, it is assumed that the physician does not deem this applicant appropriate for institutional care.

==== Item 19: Type of Recommendation

Indicate if this is an initial recommendation for services, a change in the member's level of care, or a continued placement review for the member.

==== Item: 20: Patient Transferred From

Check one.

Indicate if the applicant was transferred from a hospital, private pay, another nursing facility or lives at home.

==== Item 21: Length of Time Care Needed

Enter the length of time the applicant will require care and services from the Medicaid program.
Check the appropriate box for permanent or temporary.
If temporary, please provide an estimate of the length of time care will be needed.

==== Item 22: Is Patient Free of Communicable Diseases?

Check the appropriate box.

==== Item 23: Alternatives to Nursing Facility Placement

The admitting or attending physician must indicate whether the applicant's condition could be managed by provision of the Community Care or Home Health Care Services Programs.
Check either/both the box(es) corresponding to Community Care and/or Home Health Services if either/or both is appropriate.

==== Item 24: Physician's Name and Address

Print the admitting or attending physician's name and address in the spaces provided.

==== Item 25: Certification Statement of the Physician and Signature

The admitting or attending physician must certify that the applicant requires the level of care provided by a nursing facility or an intermediate care facility for the intellectually disabled.

*This must be an original signature;  signature stamps are not acceptable.*

If the physician does not deem this applicant appropriate for institutional care, enter N/A and sign.

==== Item 26: Date Signed by the Physician

Enter the date the physician signs the form.

==== Item 27: Physician's Licensure Number

Enter the attending or admitting physician's license number.

==== Item 28: Physician's Telephone Number

Enter the attending or admitting physician's telephone number including area code.

=== Section C - Evaluation of Nursing Care Needed

*Check appropriate boxes only.*

This section may be completed by the Katie Beckett child's Primary Care Physician or a registered nurse who is well aware of the child's condition.

==== Items 29 - 38

Check each appropriate box.

==== Item 39: Other Therapy Visits

If applicable, check the appropriate box for the number of treatment or therapy sessions per week the applicant receives or needs.
Enter N/A, if not applicable.

==== Item 40: Remarks

Enter additional remarks if needed or “None”.

==== Item 41: Pre-admission Certification Number

Leave this item blank.

==== Item 42: Date Signed

Enter the date this section of the form is completed.

==== Item 43: Print Name of MD or RN/Signature of MD or RN

The individual completing Section C should print their name legibly and sign the DMA-6(A).

*This must be an original signature; signature stamps are not acceptable.*

==== Items 44 - 52

*Do Not Write Below This Line*.

Items 44 through 52 are completed by Contractor staff only.
