= Form 218 SP: Citizenship/Identity Verification Checklist (Spanish)
:form-number: 218 SP
:lang: es
:revision-date: 01/14

NOMBRE DE UNIDAD DE ASISTENCIA (AU): pass:[____________________]

NÚMERO DE AU: pass:[_________________]

== LISTA DE CONTROL PARA LA VERIFICACIÓN DE CIUDADANÍA/IDENTIDAD

*SE DEBE VERIFICAR LA CIUDADANÍA/IDENTIDAD DE TODAS LAS PERSONAS QUE SOLICITAN/RENUEVAN LOS BENEFICIOS DE MEDICAID*

*Si ya proporcionó verificación aceptable de su ciudadanía/identidad como se indica a continuación, o si recibe Ingresos de Seguro Suplementario (SSI) o Medicare no es necesario presentar otro documento para la verificación.
Si necesita alguna aclaración, consulte por teléfono a la línea de Servicio al Cliente de DFCS o con la oficina DFCS de su condado local.*

*Recuerde presentar uno de los siguientes documentos y devuelva usando la información de contacto que figura en la lista de control para la verificación.*

*No es necesario comprobar la identidad en los siguientes documentos para verificar la ciudadanía:*

* Pasaporte de EE. UU. (no se permiten pasaportes con limitaciones)
* Certificado de Naturalización (N-550 o N-570)
* Certificado de Ciudadanía (N-560 o N-561)

*Es necesario comprobar la identidad en los siguientes documentos para verificar la ciudadanía:*

* Registro público de nacimiento en EE. UU. que indique el nacimiento en uno de los 50 estados, Distrito de Columbia, Territorios de EE. UU. o Guam
* Certificado de nacimiento en EE. UU. o información que concuerde con la Agencia Estatal de Estadísticas Vitales
* Certificado de Informe de Nacimiento (DS-1350)
* Informe Consular de Nacimiento en el Extranjero de un Ciudadano de EE. UU. (FS-240)
* Certificado de Nacimiento en el Extranjero (FS-545)
* Tarjeta de Identificación de Ciudadano de los Estados Unidos (I-197 o la versión anterior I-179)
* Tarjeta de Indígena de EE. UU. (I-872) con la clasificación “KIC” (Emitida por DHS para identificar ciudadanos de EE. UU. miembros de la Banda de Kickapoos de Texas que viven cerca de la frontera entre EE. UU. y México)
* Documento de Naturalización Colectiva/Tarjeta de Identificación de las Islas Marianas del Norte (I-873)
* Decreto Final de Adopción
* Comprobante de empleo en el servicio civil por parte del gobierno de EE. UU.
* Registro oficial militar
* Registro del censo federal o estatal donde se establezca ciudadanía de EE. UU y se indique lugar de nacimiento en EE. UU.
* Registro de censo tribal de la tribu Seneca o de la Oficina de Asuntos Indígenas
* Declaración firmada por un médico o partera que estuvo presente en el momento del nacimiento
* Uno de los siguientes documentos creados al menos 5 años antes de solicitar Medicaid que indiquen un lugar de nacimiento en EE. UU.:
** Extracto del registro del hospital en papel con membrete del hospital emitido en el momento del nacimiento de la persona
** Registro de seguro de vida, de salud u otro tipo de cobertura
** Registro público de nacimiento en EE. UU. que haya sido enmendado
** Registro de clínica médica (no del Departamento de Salud), registro médico o registro de hospital que indique un lugar de nacimiento en EE. UU.
** Documentos de admisión institucional a una casa de salud, centro de atención de la salud u otra institución

*Si no tiene ninguno de los documentos previamente enumerados, comuníquese con la línea del Servicio al Cliente de DFCS o con la oficina DFCS de su condado local para completar una declaración jurada de ciudadanía o identidad.*

*Verificación aceptable de identidad:*

* Licencia de conducir emitida por el estado con la fotografía de la persona *o* Tarjeta de Identificación del Estado de Georgia
* Certificado de Sangre Indígena, documento emitido por tribu indígena de EE.
UU./Alaska o documento emitido por tribus indígenas de EE. UU.
* Tarjeta militar de EE. UU. o registro de servicio militar, tarjeta de identificación con fotografía de dependientes de personal militar, tarjeta de marino mercante de la Guardia Costera de EE. UU.
* Tarjeta de identificación emitida por agencias o entidades del gobierno a nivel federal, estatal o local con fotografía o información de identificación personal
* Tarjeta de identificación escolar con fotografía
* Pasaporte de EE. UU. emitido con limitaciones
* Datos obtenidos o documentos de agencias del orden público o establecimientos penitenciarios, como el departamento de policía o del sheriff, oficina de libertad condicional, Departamento de Justicia para Menores (DJJ) y Centros de Detención Juvenil

En el caso de personas menores de 16 años que no pueden presentar ninguno de los documentos enumerados previamente, se aceptan los siguientes documentos como comprobantes de identidad solamente:

* Registro escolar, incluido el la tarjeta de notas, registro del centro de cuidado infantil o del programa preescolar.
(Se debe verificar el registro con la escuela que emite el documento)
* Registro de la clínica, del médico o del hospital que indica la fecha de nacimiento.
Se acepta el registro de vacunas (formulario 3231) emitido por el Departamento de Salud Pública (DPH) si se registró una fecha de vacunación en el formulario antes de que la persona cumpliera 16 años de edad.
* Declaración jurada bajo pena de perjurio por el padre/madre/tutor legal.
(*Comuníquese con la línea de Servicio al Cliente de DFCS o con la oficina DFCS de su condado local*.)
* Formulario firmado de la Declaración de Ciudadanía que incluya la fecha y el lugar de nacimiento del menor.
(*Comuníquese con la línea de Servicio al Cliente de DFCS o con la oficina DFCS de su condado local*.)
* Todos los documentos presentados para verificar la ciudadanía/identidad deben ser ORIGINALES o copias CERTFICADAS emitidas por la agencia correspondiente.

_Revision Date: {revision-date}_
