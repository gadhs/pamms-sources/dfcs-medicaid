= 2321 Japanese American and Aleutian Restitution Payments
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2321
:policy-title: Japanese American and Aleutian Restitution Payments
:previous-policy-number: MT 1

include::partial$policy-header.adoc[]

== Requirements

Permanently exclude from resources unspent portions of Japanese American and Aleutian Restitution Payments.

== Basic Considerations

These payments are restitution payments made by the U.S. Government to Japanese Americans and Aleutians or their survivors who were interned or relocated during World War II.

NOTE: Interest earned on unspent portions is treated as countable income for the month available and as a resource if retained in subsequent months.

== Procedures

If an individual alleges that his or her resources include restitution payments, obtain a statement as to the following:

* The date(s) and amount(s) of such payment(s)
* The date(s) and amount(s) of any corresponding account deposits.

Accept the individual's allegation in the absence of evidence to the contrary.
