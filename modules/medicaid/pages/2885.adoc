= 2885 Transition from Foster Care to Adoption Assistance
:chapter-number: 2800
:effective-date: January 2021
:mt: MT-63
:policy-number: 2885
:policy-title: Transition from Foster Care to Adoption Assistance
:previous-policy-number: MT 45

include::partial$policy-header.adoc[]

== Requirements

For Children who receive an Adoption Assistance Payment, Foster Care Medicaid case will be closed and an Adoption Assistance Medicaid case opened only after the adoption has been finalized.

== Basic Considerations

When children move from foster care to adoptive status with the signing of the placement agreement, their Foster Care Medicaid case will remain open under their birth name.
After the adoption is finalized, the Foster Care Medicaid case will be closed and an Adoption Assistance case will be opened under the adoptive name.
The name change is not legal until the adoption is finalized.

Reference SHINES Job Aids details for SSCM and RMS processing of funding source determination at adoptive placement and at adoption finalization.

The Social Security Administration cannot issue a new SSN until the adoption is finalized.
Once SSA issues a new SSN, all references to the old SSN are lost.
They do not cross-reference SSNs in adoption situations.

SSA will not issue a new SSN in the following situations:

* The child is receiving Title II auxiliary benefits or Title XVI benefits, and the child will continue to receive payments;
* The child knows the previously assigned SSN and/or the child knows that he/she is adopted;
* The adopting parent is a stepparent;
* The adopting parent is a grandparent;
* The child has worked.

== Procedures

When the adoption is finalized, the SSCM will upload Form 403, Adoption Assistance Benefits Memorandum, and the final adoption decree to SHINES External Documentation and send notification to the assigned RMS.
Form 403 will have the date the adoption was finalized and the name that should now appear on the child's Medicaid card.

The RMS will close the Foster Care Medicaid Case and open an Adoption Assistance Case, if the child receives an Adoption Assistance payment.
RMS will end date the appropriate funding summary in SHINES to allow a stage progression to finalized adoption case.
Reference SHINES Job Aids.

* Using the child's new legal name
* Using the new SSN

For confidentiality, only the new name and new SSN will be entered into GA Gateway when opening an Adoption Assistance case after finalization of the adoption.
If the new SSN is unknown, contact the SSCM for verification of the new number or the SSA documentation of denial to issue a new SSN and leave GA Gateway SSN field blank.
New AU and Client ID numbers are to be used for opening the new case in GA Gateway.

For a relative adoption where the child's name is not legally changed and a new SSN is not issued, the foster care case may remain open with the Class of Assistance changed to the appropriate Adoption Assistance GA Gateway code and documentation added in GA Gateway.

Revenue Maximization staff enter the new Medicaid number issued by GAMMIS into SHINES Person Detail Page for all new adoption cases.

The RMS must copy and file mandatory, specific foster care documentation and verification in the Adoption Assistance case as a permanent record to verify the funding determination for payment of Adoption Assistance:

* All initial court orders
* All funding determination, funding notification, budgets and payment authorization forms
* GA Gateway documentation screen print
* SSI award letter, if applicable
* Birth certificate
* Social Security Card
* Screen print of SHINES Eligibility Summary Page, if applicable
* Screen print of SHINES Initial Application Page, if applicable

The above documents are to be filed in the Adoption Assistance case record as permanent funding verification for audit purposes.
The foster care record will never be merged with the Adoption Assistance record and will be maintained separate and apart to retain the above original documentation as a permanent record for funding determination verification for foster care as it is also subject to audit.

Revenue Maximization Adoption Assistance case records must be retained for a period of five years from month of receipt of last Adoption Assistance payment.
Reference Section 2760 – Case Record Maintenance.

When a Foster Care case is closed and a CMD is completed for Medicaid, any materials, forms, collateral contacts, or other documentation that pertain to a child's IV-E eligibility determination and placement in custody must remain in the closed Revenue Maximization Foster Care record.
Foster Care Services policy is followed regarding retention of this material.
A case record of a child who has spent more than six months of his life in care is retained and safeguarded at least until the child is 23 years old.
Reference Foster Care Services: Needs of the child, Record Retention 1011.18.
