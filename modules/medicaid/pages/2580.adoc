= 2580 SSI Recipients Temporarily in a Nursing Home
:chapter-number: 2575
:effective-date: February 2020
:mt: MT-58
:policy-number: 2580
:policy-title: SSI Recipients Temporarily in a Nursing Home
:previous-policy-number: MT 1

include::partial$policy-header.adoc[]

== Requirements

Section 9115 of the Omnibus Budget Reconciliation Act amended the Social Security Act to entitle certain SSI individuals to retain their income for the month of admission to a nursing home (NH) and the following three full months.

== Basic Considerations

The SSI recipient must provide SSA with physician certification that the NH confinement should last no more than 90 days.
S/he must also show a need to pay outside expenses to maintain their private living arrangements until s/he returns home.

The NH and SSI recipients are responsible for providing the physician certification and documentation of living expenses to the Social Security Administration (SSA).

The SSI recipient's income is not considered in determining patient liability for the month of admission and the following three full months in the NH.

== Procedures

Treat all SSI recipients entering a NH according to standard policy UNLESS written documentation from SSA identifies the individual as temporarily in an institution (LA-D).

Follow the steps below to authorize a NH vendor payment if documentation is provided in writing by SSA to verify that the SSI recipient is temporarily in the NH.

[horizontal,labelwidth=10]
Step 1:: Authorize the vendor payment to the NH.
There is no patient liability for the month of admission and the following three months.
Refer to xref:2576.adoc[].

Step 2:: Generate an alert to review the case toward the end of the third full month of the SSI recipient's NH confinement.

Step 3:: If the individual remains in the NH in the fourth month after the month of admission, determine patient liability according to regular policy and procedures.
