= 2501 Marital Relationship
:chapter-number: 2500
:effective-date: December 2022
:mt: MT-68
:policy-number: 2501
:policy-title: Marital Relationship
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

The A/R's marital relationship must be established to determine whether or not to consider the income and resources of a person other than the A/R.

== Basic Considerations

=== Marital Relationship (Prior to 1/1/97)

A marital relationship exists for a man and a woman who are legally married or living together, free to marry and holding out (common-law) to the community as married.
The common–law marriage must have existed prior to 1/1/97 to be recognized as a marital relationship.

=== Marital Relationship (On or after 1/1/97)

A marital relationship exists for a man and a woman who are legally married.
A common-law marriage established on or after 1/1/97 is not considered a marital relationship in Georgia.

=== Marital Relationships Established Prior to 1/1/97 Begins

A marital relationship begins to exist the month following the month two people marry or begin living together and holding out to the community as husband and wife.

[caption=Exception]
NOTE: Use of the Spousal Impoverishment resource limit is limited to situations where an A/R in LA-D has a legal spouse in the community.
See Section 2502 for definitions of legal and non-legal spouses.

=== Marital Relationship Ends

A marital relationship ceases the month following the month of separation of spouses, regardless of whether or not either or both is Medicaid eligible.

The admission of one or both spouses into LA-D is considered separation.

[caption=Exception]
NOTE: Admission of one spouse to a hospital is not always considered separation.

Refer to Special Considerations in xref:2503.adoc[].

An A/R is considered an individual only if a marital relationship has ended, by definition above, or has never existed.

== Procedures

A marital relationship has ceased for an individual who is legally married but is living separately from his/her spouse due to estrangement (alienation, loss of affection, indifference).
The spouse, including spouse who is incarcerated, is not considered a community spouse for purposes of determining resource eligibility and patient liability.

Identify an adult A/R as one of the following:

* a Medicaid individual
* a Medicaid individual married to and living with a Medicaid individual (Medicaid couple)
* a Medicaid individual living with an ineligible spouse.

=== Medicaid Individual

Consider an SSI or ABD Medicaid A/R who is not currently in a marital relationship to be a Medicaid individual.

Consider only the income and resources of the Medicaid individual when determining his/her financial eligibility for ABD Medicaid.
Use individual income and resource limits.

=== Medicaid Couple

Consider an SSI or ABD Medicaid A/R who is married and living with another SSI or ABD Medicaid A/R to be a Medicaid couple.

Refer to xref:2503.adoc[] for information on considering the income and resources of a Medicaid couple.

=== Medicaid Individual with Ineligible Spouse

Consider an SSI or ABD Medicaid A/R who is married to and living with a spouse who is not an SSI or ABD Medicaid A/R to be a Medicaid individual with an ineligible spouse.

Refer to xref:2502.adoc[] for information on deeming the income and resources of an ineligible spouse.
