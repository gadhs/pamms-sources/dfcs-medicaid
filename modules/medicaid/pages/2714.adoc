= 2714 Family Medicaid AU/BG Composition Changes
:chapter-number: 2700
:effective-date: June 2020
:mt: MT-60
:policy-number: 2714
:policy-title: Family Medicaid AU/BG Composition Changes
:previous-policy-number: MT 48

include::partial$policy-header.adoc[]

== Requirements

Medicaid eligibility must be established upon any change in assistance unit (AU) or budget group (BG) composition.

== Basic Considerations

Individuals who are added to an AU must meet all eligibility requirements.

A Declaration of Citizenship/Immigration Status is required if there is no previous Declaration or Application for the person being added to the AU.
This is *not* required if a person is being added to the BG only.

== Procedures

=== New AU Member

Complete the following procedures when an individual is added to the AU.

[horizontal,labelwidth=10]
Step 1:: Obtain a signed application and accept client statement, unless questionable, of the new AU member and their tax filer/non-tax filer status.
Verify his/her citizenship/immigration status/identity income and resources, if applicable for the COA.
Refer to xref:2051.adoc[].

Step 2:: Complete a trial budget.
+
If the income and/or resources (if applicable) of a new AU member are required to be included but cannot to be established, allow timely notice and terminate Medicaid for the AU.

Step 3:: If the AU is eligible based on the trial budget, complete the following procedures.

* establish all points of eligibility
* request any required verification
* complete any mandatory forms

Step 4:: If the AU is *eligible* based on the addition of the new AU member, add the individual and his/her income and resources (if required) to the AU effective the month s/he began living with the AU.
+
--
If the AU is *ineligible* based on the trial budget, complete a CMD and terminate Medicaid the month following the expiration of timely notice.

[caption=Exception]
NOTE: Do not terminate Medicaid for a pregnant woman if eligible under Continuous Coverage.
Refer to xref:2720.adoc[].
--

Step 5:: Determine eligibility for the new AU member for any prior months requested.

Step 6:: Notify the AU.

=== New BG Member

Complete the following procedures when an individual is added to the BG.

[horizontal,labelwidth=10]
Step 1:: Accept client statement, unless questionable, of the new BG member and their tax filer/non-tax filer status.
A new application is not required to add a new BG member.
Verify his/her income and resources, if applicable for the COA.
Refer to Section 2051, Verification.
+
If the income and/or resources (if applicable) of a new BG member are required to be included but cannot be established, allow timely notice and terminate Medicaid for the AU.

Step 2:: Complete a trial budget.

Step 3:: If the AU is eligible based on the trial budget, complete the following procedures:

* establish all points of eligibility
* request any required verification
* complete any mandatory forms

Step 4:: If the AU is *eligible* based on the addition of the new BG member, add the individual and his/her income and resources (if required) to the BG the month following expiration of timely notice.
+
--
If the AU is *ineligible* based on the trial budget, complete a CMD and terminate Medicaid the month following the expiration of timely notice.

* complete a CMD
* terminate Medicaid
* notify the AU
+
[caption=Exception]
NOTE: Do not terminate Medicaid if for a pregnant woman eligible under Continuous Coverage.
Refer to xref:2720.adoc[].
--

Step 5:: Provide notification to the AU.

=== Birth of a Child

[horizontal,labelwidth=10]
Step 1:: Document the following:

* the child's name and date of birth
* the date the change is reported to the agency
* the name of the individual reporting the birth

Step 2:: Establish that the child continues to live in Georgia.
The parent or guardian's statement is acceptable verification.

Step 3:: Approve Parent/Caretaker with Child(ren), if eligible.
+
If ineligible for Parent/Caretaker with Child(ren), approve Deemed Newborn Medicaid effective the month of the child's birth and ongoing.
Refer to xref:2174.adoc[].

=== Non-Custodial Parent (NCP) Returns Home

Follow procedures, in this Section, for adding a new AU or BG member.

Notify Division of Child Support Services (DCSS) via Form 713 that the NCP has returned home.

=== AU Member is Penalized

Apply the penalty the month following the expiration of timely notice.

Notify the AU of the penalty and the effect on Medicaid eligibility.

Budget the income and resources of the penalized individual according to the guidelines in xref:2657.adoc[].

Remove the penalized individual effective the month following the month the agency determines the penalty.
Allow timely notice.

The penalized adult remains in the BG but not the AU unless she is a pregnant woman.
Reinstate Medicaid beginning with the month of compliance.

=== AU Member Becomes SSI Eligible

Document the following:

* the name of the individual receiving SSI
* the date SSI was approved
* the date the change was reported to the agency
* the amount of the SSI benefit and whether RSDI is received
* method of verification (client statement or other verification)

If RSDI is received, determine if dependents of the SSI individual also receive or are potentially eligible to receive RSDI.

Provide adequate notice and make the SSI recipient ineligible effective the month after the change is reported.

NOTE: Do *NOT* consider the income and resources of the SSI individual in the Family Medicaid budget.
For MAGI Medicaid include the SSI individual in the BG but exclude all income received by the SSI individual (earned or unearned) from the budget.

=== AU Member Reports A Marriage

Document the following:

* the name of the AU member who married
* the name of the individual s/he married
* the date of the marriage
* the date the marriage was reported to the agency

Determine the relationship of all AU members to the new spouse and his/her tax filer/non-tax filer status.
Refer to xref:2245.adoc[].

If the new spouse is the parent of an AU child, follow the procedures in “NCP Returns Home”, in this section.

Complete a trial budget to determine ongoing eligibility.

If the new spouse is eligible based on the trial budget, add him/her to the AU effective the month of the marriage.

If the AU is ineligible based on the trial budget, complete a CMD and terminate Medicaid the month following the expiration of timely notice.

Notify the AU.

=== AU or BG Member Moves out of the Home

Document the following:

* the name of the individual who left the home
* the date the individual left the home
* the date the change was reported to the agency Complete a trial budget based on the new AU or BG composition.

If removing the individual causes ineligibility of the entire AU, complete a CMD.
If no COA exists under which the remaining AU members are eligible, terminate Medicaid the month following the expiration of timely notice.
Otherwise, remove the individual from the AU effective the month after the change is reported.

Notify the AU.
