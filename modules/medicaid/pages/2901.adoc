= 2901 Division of Aging Services (DHS)
:chapter-number: 2900
:effective-date: June 2020
:mt: MT-60
:policy-number: 2901
:policy-title: Division of Aging Services (DHS)
:previous-policy-number: MT 37

include::partial$policy-header.adoc[]

== Requirements

The Division of Aging Services (DAS) administers a statewide system of services for older Georgians and adults with disabilities.
These programs seek to secure maximum independence and dignity for individuals, especially the vulnerable elderly.

== Basic Considerations

Services are administered through Area Agencies on Aging (AAAs), each coordinating service delivery in a designated geographic area of the state.
The following are services offered:

* Aging and Disability Resource Connection (ADRC) provides accurate information about publicly and privately financed long-term supports and services and offers a consumer-oriented approach to learning about the availability of services in the home and community.
The ADRC also provides preliminary screening and referral for the Medicaid Waiver Programs administered by the Department of Community Health and those operationally managed by the Department of Behavioral Health and Developmental Disabilities using a “no wrong door” approach to inquiries for long term care assistance.

* In-Home Services, such as homemaker/chore and personal care services, are provided to older citizens.
Home delivered meals are distributed as part of the statewide nutrition program.
Other in-home services include respite care, friendly visiting, telephone reassurance and home management.

* Community Services include congregate nutrition services, which provide meals, nutrition education and counseling and other supportive services to older persons in centers throughout the state.
The services may also include adult day care, legal assistance, elder abuse preventions programs, health promotion/disease prevention programs and health insurance counseling.

* Long-Term Care Ombudsmen investigate and work to resolve complaints regarding the quality of care and protection of the rights of long-term care residents.
See Section 2937, Ombudsman, for additional information.

* Employment opportunities for economically disadvantaged persons 55 and over are available through the Senior Community Service Employment Program (SCSEP).
These opportunities include training, part-time community service employment and placement in unsubsidized employment.

* The Alzheimer's Disease and Related Disorders Program provides support to people with dementia disorders and their caregivers.
Projects serve clients with in-home respite care, day care center services and referrals.

* The National Family Caregiver Support Program (NFCSP) provides support services to functionally impaired elders and their caregivers such as in-home respite care and day care, information and training, and assistance with access to services.
This program also may assist seniors who are caring for grandchildren.

* GeorgiaCares is a statewide coalition to assist low income Georgians in applying for the various drug assistance programs sponsored by pharmaceutical companies.
This program also provides health insurance information, counseling and community education to Georgia's citizens.
Customers may call 1-866-552-4464, option 4, or their local Area Agency on Aging for assistance in this program.
See Section 2926, GeorgiaCares, for additional information.

* The Nursing Home Transitions program transitions eligible individuals from long-term inpatient facilities back into community settings.

* The Georgia Elderly Legal Assistance Program (ELAP) serves people age 60 and older by providing legal representation, information and education in civil legal matters throughout the State of Georgia

Services administered directly by Division of Aging staff:

* Adult Protective Services provides a mechanism to report abuse, neglect or exploitation of disabled adults or elder persons who are not residents of nursing homes or personal care homes.
Calls that are for emergency situations should be directed to call “911”.
Non-emergency reports should be directed to:
+
[%hardbreaks]
APS Central Intake Unit
Toll-Free 1-866-552-4464, option 3

* The Public Guardianship Office (PGO) of the Division of Aging Services is assigned oversight and delivery of guardianship case management services on behalf of the Department of Human Services.
The Department of Human Services is the appointed guardian of last resort when there is no willing or suitable person to act as the guardian for an adult whom the probate court has determined lacks enough capacity to make or communicate significant responsible decisions concerning health or safety

The Area Agency on Aging (AAA) coordinates a variety of services and information for the elderly and their caregivers.
The AAA serves as an entry point for all programs and services, determining both client eligibility and the type of services needed.
To find the nearest AAA for your area, call 1-866-552-4464, and press 2.

Listed below are Georgia's 12 AAA Service Areas:

[cols=2*]
|===
| *ATLANTA REGIONAL COMMISSION*

Area Agency on Aging – (404) 463-3333 (Atlanta)

Cherokee, Clayton, Cobb, DeKalb, Douglas, Fayette, Fulton, Gwinnett, Rockdale, and Henry.

| *NORTHEAST GEORGIA*

Area Agency on Aging – (800) 474-7540 (Athens)

Barrow, Clarke, Elbert, Greene, Jackson, Jasper, Madison, Morgan, Newton, Oconee, Oglethorpe and Walton.

| *CENTRAL SAVANNAH RIVER*

Area Agency on Aging (Augusta) (888) 922-4464

Burke, Columbia, Glascock, Hancock, Jefferson, Jenkins, Lincoln, McDuffie, Richmond, Screven, Taliaferro, Warren, Washington and Wilkes.

| *NORTHWEST GEORGIA (formerly Coosa Valley/North Georgia)*

Area Agency on Aging – (800)759-2963 (Rome)

Bartow, Catoosa, Chattooga, Dade, Fannin, Floyd, Gilmer, Gordon, Haralson, Murray, Paulding, Pickens, Polk, Walker and Whitfield.

| *COASTAL*

Area Agency on Aging – (800) 580-6860 (Darien)

Bryan, Bullock, Camden, Chatham, Effingham, Glynn, Liberty, Long and McIntosh.

| *SOUTHERN GEORGIA*

Area Agency on Aging – (912) 285-6097 (Waycross)

Atkinson, Bacon, Ben Hill, Berrien, Brantley, Brooks, Charlton, Clinch, Coffee, Cook, Echols, Irwin, Lanier, Lowndes, Pierce, Tift, Turner and Ware.

| *LEGACY LINK, INC*.

(Georgia Mountains) Area Agency on Aging – (855) 266-4283(Oakwood)

Banks, Dawson, Forsyth, Franklin, Habersham, Hall, Hart, Lumpkin, Rabun, Stephens, Towns, Union and White.

| *THREE RIVERS*

Area Agency on Aging – (866) 854-5652 (Franklin)

Butts, Carroll, Coweta, Heard, Lamar, Meriwether, Pike, Spalding, Troup and Upson.

| *HEART OF GEORGIA/ ALTAMAHA*

Area Agency on Aging – (912) 367-3648 (Baxley) or (888) 367-9913

Appling, Bleckley, Candler, Dodge, Emanuel, Evans, Jeff Davis, Johnson, Laurens, Montgomery, Tattnall, Telfair, Toombs, Treutlen, Wayne, Wheeler and Wilcox.

| *SOUTHWEST GEORGIA (SOWEGA)*

Area Agency on Aging – (800) 282-6612 or 229)

Baker, Calhoun, Colquitt, Decatur, Dougherty, Early, Grady, Lee, Miller, Mitchell, Seminole, Terrell, Thomas and Worth.

| *MIDDLE GEORGIA*

Area Agency on Aging – (888) 548-1456 (Macon)

Baldwin, Bibb, Crawford, Houston, Jones, Monroe, Peach, Pulaski, Putnam, Twiggs and Wilkinson.

| *RIVER VALLEY*

Area Agency on Aging – (800) 615-4379 (Columbus)

Chattahoochee, Clay, Crisp, Dooly, Harris, Macon, Marion, Muscogee, Quitman, Randolph, Schley, Stewart, Sumter, Talbot, Taylor and Webster.
|===
