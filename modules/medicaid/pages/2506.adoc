= 2506 Medicaid Individual Budgeting
:chapter-number: 2500
:effective-date: February 2020
:mt: MT-58
:policy-number: 2506
:policy-title: Medicaid Individual Budgeting
:previous-policy-number: MT 6

include::partial$policy-header.adoc[]

== Requirements

Individual budgeting is completed when an individual residing in LA-A or B applies for or receives ABD Medicaid as a Medicaid Individual with no spouse.

== Basic Considerations

An individual budget is completed for a Medicaid Individual residing in LA-A or B without a spouse whose eligibility is determined under the following classes of assistance (COAs):

* SSI (3 months prior or intervening months)
* Public Law Classes, including Disabled Widow(er) and Disabled Adult Child (DAC)
* ABD Medically Needy (AMN)
* QMB
* SLMB
* QI-1
* QDWI

== Procedures

Enter the appropriate information in the computer system to allow the system to budget correctly,

*OR*

Follow the procedures below to manually complete an Individual budget on Form 172:

Step 1:: Complete Section A of Form 172.

* Include the income of the Medicaid individual in Section A.
* Use the Individual income limit for the COA under which the Medicaid individual is applying.

Step 2:: If there is a deficit on Line 13 of Section A, the Medicaid individual is eligible under this COA based on income.

Step 3:: If there is a surplus or zero on Line 13 of Section A, the Medicaid individual is ineligible under this COA.
Complete a CMD.
Refer to xref:2052.adoc[].

[caption=Exception]
NOTE: If the individual is applying for QMB and there is a zero on Line 13, the individual is eligible for QMB based on income.

[caption=Exception]
NOTE: If the individual is being budgeted under AMN and there is a surplus on Line 13, use the amount from Line 13 as the AMN spenddown.
