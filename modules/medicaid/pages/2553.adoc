= 2553 Protection of Income
:chapter-number: 2550
:effective-date: December 2022
:mt: MT-68
:policy-number: 2553
:policy-title: Protection of Income
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

All or part of the recipient's income is protected for the month of admission to or discharge from a nursing home (NH), Institutionalized Hospice, CCSP/EDWP, ICWP or NOW/COMP.

== Basic Considerations

Income is protected for the month in which any of the following situations occurs:

* The recipient enters a NH, Institutionalized Hospice, CCSP/EDWP, ICWP or NOW/COMP from LA-A or B.
* The recipient leaves a NH, Institutionalized Hospice, CCSP/EDWP, ICWP or NOW/COMP and enters LA-A or B.
* The recipient has CCSP/EDWP, ICWP or NOW/COMP waivered services and case management is terminated.
* The recipient is admitted to and leaves a NH, Institutionalized Hospice, CCSP/EDWP, ICWP or NOW/COMP in the same month.
* The recipient is admitted to a NH, Institutionalized Hospice, CCSP/EDWP, ICWP or NOW/COMP and dies in the same month.
* The recipient enters a NH, Institutionalized Hospice, CCSP/EDWP, ICWP or NOW/COMP in the same month in which s/he was admitted to a hospital or other LA-D from LA-A or B.
* Goes directly from NH to CCSP/EDWP or CCSP/EDWP to NH.

NOTE: In the above situation, use the date of admission to the first LA-D as the admission date for determining the amount of protected income.

* For Institutionalized Hospice use the date of election of Hospice services.
* For CCSP/EDWP, ICWP, or NOW/COMP cases, use the date case management begins.
Third party vendor payment supplements are not protected.
VA Aid and Attendance are not counted in the Patient Liability/Cost Share (PL/CS) budget.
Refer to xref:2418.adoc[].

Allow the protected income deduction when calculating the PL/CS.

Use the following chart to determine the amount of the recipient's income to protect based on when the recipient entered and/or left LA-D.
If an A/R enters and leaves a facility/facilities more than once during the same month, or if the A/R dies during the month of admission, the total number of days spent in the facility/facilities for the entire month is considered in determining protected income.
Refer to the following chart.

* Count the day of admission towards the total stay.
* Do not count the day of discharge or death towards the total stay.

.CHART 2553.1 – PROTECTION OF INCOME
[#chart-2553-1]
|===
^| IF the applicant/recipient ^| THEN Protect

| enters the 1^st^ through the 10^th^ day of the month from LA-A, B, or C
| one half income

| enters the 11^th^ through 31^st^ day of the month from LA-A, B, or C
| ALL income

| enters/leaves (leaves/enters) or dies in the same month

*AND*

the total stay is 10 days or less
| ALL income

| enters/leaves (leaves/enters) or dies in the same month

*AND*

the total stay is 11 days or more
| one half income

| leaves the 1^st^ through 10^th^ day of the month to LA-A, B, or C
| ALL income

| leaves the 11^th^ through 31^st^ day of the month to LA-A, B, or C
| one half income

| dies in a NH, Institutionalized Hospice, CCSP/EDWP, ICWP, or NOW/COMP in any month after the month of admission
| NO income

| Leaves CCSP/EDWP and enters a NH on the same day
| ALL income in the month of admission to NH

| Leaves NH and enters CCSP/EDWP Case Management on the same day
| ALL income for the CCSP/EDWP CS for month of entry.

NH PL will have a PNA of the FBR in month of discharge to CCSP/EDWP.

| is admitted to an LA-D directly from another LA-D and has been continuously residing in the first LA-D since prior to the first day of the month of entry to the second LA-D (except for NH to CCSP/EDWP *or* CCSP/EDWP to NH)
| NO income
|===
