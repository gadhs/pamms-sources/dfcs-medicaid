= 2939 Money Follows The Person
:chapter-number: 2900
:effective-date: June 2020
:mt: MT-60
:policy-number: 2939
:policy-title: Money Follows The Person
:previous-policy-number: MT 42

include::partial$policy-header.adoc[]

== Requirements

The Money Follows the person Initiative (MFP) is a grant awarded to shift Medicaid Long Term Care from its emphasis on institutional care to Home and Community-based services.
The MFP grant opportunity was made available as part of the Federal Deficit Reduction Act of FY 2006.
Georgia's grant of over $34 million in federal funds will operate from May 1, 2007 until the grant funds have been exhausted.

== Basic Considerations

The Money Follows the Person Initiative (MFP) is a collaboration between the Georgia Department of Community Health (DCH) and the Department of Human Services (DHS) to transition over 1000 Georgians from institutional settings to the community through the Medicaid waiver programs: Elderly and Disabled waiver programs operating as the Community Care Services Program (CCSP) and Service Options Using Resources in Community Environment (SOURCE), NOW/COMP, and Independent Care Waiver Program (ICWP).
Under the grant, the goal is for states to develop alternative long-term care opportunities which will enable the elderly and people with disabilities to fully participate in their communities.

MFP will provide necessary transitional supports for eligible members who choose to leave the nursing home and receive care at home or community-based settings.
Barriers will be removed to allow for payments of deposits for utilities/rent, adaptive equipment, and for transportation needs such as dialysis treatment.

* Persons eligible for MFP include those who have resided in an institution for a minimum of six months and whose care has been covered by Medicaid in the month preceding their transition to home and community-based services.
* The MFP individual must continue to meet institutional level of care criteria after transitioning to the community.

To inquire more about Community options or designing a transition plan from an institution, the A/R, or a representative can contact the Area Agency on Aging 1-866-552-4464, Georgia Department of Community Health, Medicaid Division, Money Follows the Person project, at 404-651-9961; or email gamfp@dch.ga.gov; or visit our website at https://dch.georgia.gov/programs/hcbs/money-follows-person.
