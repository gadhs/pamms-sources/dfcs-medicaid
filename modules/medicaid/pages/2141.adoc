= 2141 Nursing Home
:chapter-number: 2100
:effective-date: February 2020
:mt: MT-58
:policy-number: 2141
:policy-title: Nursing Home
:previous-policy-number: MT 49

include::partial$policy-header.adoc[]

== Requirements

Nursing Home is a class of assistance (COA) that provides Medicaid to individuals residing in a Medicaid participating nursing home.

== Basic Considerations

To be eligible under nursing home COA, an A/R must meet the following conditions:

* The A/R is in a Medicaid participating nursing home, swing bed, facility for mentally retarded or Tertiary Care Unit (TCU) (See Section 2581 for Swing Bed.)
* The A/R meets the Length of Stay (LOS) and Level of Care (LOC) basic eligibility criteria.
* The A/R meets all other basic and financial eligibility criteria.

NOTE: Please see Section 2143 regarding Medicare Skilled Nursing Facility (SNF) for QMB

== Procedures

Follow the steps below to determine ABD Medicaid eligibility under the Nursing Home COA.

[horizontal,labelwidth=10]
Step 1:: Accept the A/R's Medicaid application.

Step 2:: Verify from the receipt of a Form DMA-59 from the nursing home that the A/R resides in a nursing home or TCU.
Verify from the receipt of an approved LOC instrument that the A/R resides in a swing bed or that the A/R is in a facility for the mentally retarded (may be a NH).

Step 3:: Conduct an interview.
A telephone interview is acceptable.

Step 4:: Determine basic eligibility, including Length of Stay (LOS) and Level of Care (LOC).
LOC verification may vary depending on whether A/R is in NH, TCU, swing bed or has a LOC of IC-MR.
Refer to Chapter 2200, Basic Eligibility Criteria.

Step 5:: Determine financial eligibility.

* Refer to Chapter 2500, ABD Financial Responsibility and Budgeting for procedures on whose resources to consider and the resource limit to use in determining resource eligibility.
* Complete a Medicaid CAP budget to determine income eligibility.
Refer to Section 2510, Medicaid CAP Budgeting

Step 6:: Approve Medicaid using the Nursing home COA if the A/R meets all the above eligibility criteria.
The system will determine the A/R's patient liability based on information entered.
+
NOTE: Do not approve Medicaid under the Nursing Home COA for any month in which the A/R was not confined to a nursing home for at least one day of the month.

Step 7:: Notify the A/R and/or PR and the nursing home of the case disposition and patient liability.

== Special Considerations

Effective September 1, 2004, AMN-NH will be eliminated due to a change in the State Plan.
Complete a CMD to another COA, such as AMN (Section 2150).

=== Sanctions

If the NH is under a Medicaid sanction resulting in a “ban on admissions”, no DMA-59 should be sent to DFCS until such time as the ban is lifted.
Until the “ban on admissions” is lifted, no A/R should be approved for the NH COA if the A/R is admitted to the NH on or after the effective date of the ban on admissions.
A ban on admissions has no effect on A/Rs who are already Medicaid recipients in the NH or who were admitted to the NH prior to the imposition of the ban.

If an application is received on an A/R who was admitted during the time the “ban on admissions” is in place, hold the case until either the ban is lifted and the case can be approved under the NH COA or the standard of promptness (SOP) is reached.
If the case cannot be approved under the NH COA by the SOP date, determine eligibility under another COA such as regular AMN.
Do not determine eligibility under any LA-D COA.
If the “ban on admissions” is subsequently lifted, historically close the other Medicaid case and approve under the NH COA back to the first month of eligibility.

The “Denial of Payment” notification letters will come to the Medicaid Policy Unit from DCH's Member Services.
The Medicaid Policy Unit will forward the letters to the appropriate Field Program Specialist who will then forward to the county DFCS office.
When the Denial of Payment ban is lifted this same process will take place.
