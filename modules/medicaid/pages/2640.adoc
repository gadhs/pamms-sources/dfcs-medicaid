= 2640 Paternity
:chapter-number: 2600
:effective-date: June 2020
:mt: MT-60
:policy-number: 2640
:policy-title: Paternity
:previous-policy-number: MT 21

include::partial$policy-header.adoc[]

== Requirements

The paternity of a dependent child included in a Family Medicaid AU must be established in order to determine relationship, to determine financial responsibility, to determine whether child support is being received from a non-custodial parent and to appropriately make Child Support Services (CSS) referrals.

== Basic Considerations

Paternity is established for each child at application and when a child is added to an AU.
Paternity is reestablished if a change occurs as a result of one of the following:

* the mother names someone else as the father
* CSS determines the man who is named as the father is not the biological father
* a judicial determination alters paternity, e.g., adoption.

The following chart lists situations and the procedures to follow to establish paternity.

[#chart-2640-1]
.CHART 2640.1 - ESTABLISHING PATERNITY
|===
| SITUATION | TREATMENT

| Mother is unmarried at the time of the child's birth.
| Accept the person she names to be the child's father.

| Mother is married at the time of the child's birth.
| The spouse is the legal father.

| Mother is married at the time of the child's birth and states a man other than her husband is the biological father.
a| The husband is the legal father unless one of the following occurs:

* the reputed father legitimates the child
+
OR
* the child's paternity is determined by a judicial proceeding
+
OR
* the reputed father, living in the home with the child, signs Form 185, Affidavit of Paternity
+
OR
* an affidavit of paternity is returned by CSS.

| Mother is unavailable, e.g., deceased or whereabouts unknown, and an application is filed by a non-parent.
a| Establish paternity by one of the following:

* the child's birth certificate
* the reputed father's written statement acknowledging paternity
* a document showing that the reputed parent has legitimated the child
* written evidence that paternity has been proven in a judicial proceeding
* the subsequent marriage of the reputed father to the mother and his acknowledgement that he is the father of the child
* prior case record documentation of the mother's statement of paternity
* SSA records showing that the child receives benefits from the reputed father's account
* records of an employer showing that the child is a dependent of the reputed father for tax or insurance purposes
* court records showing that the mother has, under oath, asserted the father's identity.

NOTE: This would not apply when the court has determined the man not to be the father.

| CSS provides paternity test results that show the alleged father is not the father of the child

AND

the mother insists there is no other man who could be the father

AND

there is no other evidence to the contrary.
| Notify CSS that a penalty will not be imposed.

| CSS provides paternity test results that show the alleged father is not the father of the child

AND

the mother refuses to name another man as the father

AND

there is supporting evidence to the contrary.
| Penalize the Medicaid AU member who failed to cooperate.
Refer to xref:2657.adoc[].
|===
