= 2510 Medicaid Cap Budgeting
:chapter-number: 2500
:effective-date: June 2021
:mt: MT-64
:policy-number: 2510
:policy-title: Medicaid Cap Budgeting
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

Medicaid CAP budgeting is completed when an individual resides in LA-D and applies for ABD Medicaid.

[caption=Exception]
NOTE: If the A/R's income exceeds the Medicaid CAP, ABD Medicaid eligibility is determined under ABD Medically Needy (AMN) using the ABD Medically Needy Income Level (AMNIL).
See Section 2150.
Medicaid will not cover the vendor payment to the nursing home.
However, if the A/R has a Qualified Income Trust (QIT), then only the income that the A/R actually receives is counted in the Medicaid Cap budget.

The AMN NH/IH COA is no longer an option as of September 1, 2004.

== Basic Considerations

A Medicaid CAP budget is completed for an A/R who is residing in LA-D with or without his/her spouse and whose ABD Medicaid eligibility is determined under the following classes of assistance (COAs):

* Elderly Disabled Waiver Program (EDWP), formerly Community Care Services Program (CCSP)
* Deeming Waiver (Katie Beckett)
* Hospice Care
* Institutionalized Hospice
* Hospital
* Independent Care Waiver Program (ICWP)
* Nursing Home
* NOW/COMP

== Procedures

Follow the steps below to complete a Medicaid CAP budget.
Document all calculations:

Step 1:: Calculate the GROSS countable monthly income of the Medicaid A/R residing in LA-D.

NOTE: Do *Not* count any income that goes into a QIT.
Do *Not* include the income of the A/R's spouse, even if the A/R lives at home in LA-D with his/her spouse.

Step 2:: Compare the GROSS countable monthly income to the Individual Medicaid CAP.

NOTE: Do not allow the income deductions discussed in Section 2505, Income Deductions.

If the A/R's gross income is less than the Individual Medicaid CAP, the A/R is income eligible under the LA-D COA.

Effective September 1, 2004, if the A/R's gross income is greater than or equal to the Individual Medicaid CAP and the A/R has not established a QIT, the A/R is income ineligible under any LA-D COA unless/until a QIT is established.
Budget the A/R as Medically Needy.
See Section 2150, AMN and Section 2407, QIT.

== Special Considerations

=== Couple Medicaid CAP

Use the Couple Medicaid CAP only when the following conditions are met:

* Both spouses of a Medicaid couple reside together in LA-D at home or in the same nursing home, hospice in the nursing home (Institutionalized Hospice) or hospital.
+
*AND*
* Using the Individual Medicaid CAP results in Medicaid ineligibility for one of the spouses.

If the Couple Medicaid CAP is used, complete the Medicaid CAP budget as follows:

* Combine the gross countable monthly income of the Medicaid couple.
Do not include any income placed in a QIT.
* Compare the combined income to the Couple Medicaid CAP.
