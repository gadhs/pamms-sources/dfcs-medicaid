= 2706 Medicaid Renewals
:chapter-number: 2700
:effective-date: September 2024
:mt: MT-73
:policy-number: 2706
:policy-title: Medicaid Renewals
:previous-policy-number: MT 72

include::partial$policy-header.adoc[]

== Requirements

Medical Assistance Units (AUs) must comply with periodic renewals of continued eligibility.

== Basic Considerations

Medical Assistance renewals must be completed:

* Annually for ABD Classes of Assistance (COA)
+
[caption=Exception]
NOTE: ABD Medically Needy renewals must be completed semi-annually.

* Annually for Chafee Independence Program Medicaid
* Annually for Former Foster Care Medicaid
* Annually for Adoption Assistance Medicaid
* Semi-annually for Foster Care Medicaid
* Annually for Non-MAGI Women's Health Medicaid
* Semi-annually, by the end of the sixth month following the month in which the application is approved and every six months thereafter, for ABD and Family Medically Needy
* Annually for Family Medicaid MAGI COA, including:
** Parent/Caretaker with Children
** Children under 19 years of age
** PeachCare for Kids®
** Planning for Healthy Babies®
** Pathways

*EXCEPTION:* Annual renewals are not required for the following Family Medicaid COAs:

** Pregnant Woman Medicaid
** Newborn Medicaid
** Transitional Medicaid Assistance (TMA)
** 4 Months Extended Medicaid (4MEx)


Refer to <<chart-2706-1,Chart 2706.1, Family Medicaid Renewals>> to determine which Family COAs require renewals.

Renewals are also to be completed due to changes reported by or affecting the AU that allow for the completion of the renewal based on the reported change, and renewals initiated in other, related, programs.

For MAGI Medical Assistance, if verification is required for the reported change, only verification for the reported change can be requested.
If the reported change alone cannot allow enough information to complete a renewal, then continue with the remainder of the period of eligibility (POE).
If the reported change allows for a renewal to be completed use the system function to allow a new 12-month POE to be issued to the AU.

When a Family Medicaid renewal is finalized with a related active program(s), the POE for the Family Medicaid case will match that of the related active program(s).

NOTE: If the related active Medicaid case involves a Foster Care and/or Adoption COAs (this includes CHAFEE and Former Foster Care Medicaid) contact REV MAX before beginning the renewal.

The renewal process must be completed by the last day of the month the renewal is due.

NOTE: If an AR does not submit the MAGI renewal form or return the requested verification but does respond within 90 days (last day of the third month following termination), eligibility can be reconsidered without a new application.
For MAGI COA, the AU must submit a signed renewal form.

NOTE: Any AU, BG or Authorized Representative (AREP) may sign a renewal form for Medicaid.

The following points of eligibility must be reviewed, if applicable:

* resources
* income
* dependent care expenses
* third party liability
* application for other benefits
* living arrangements
* possibility of transfer of assets by A/R or spouse transferring annuity or home place.
* pre-tax deduction(s)
* 1040 deduction(s)
* tax filing status
* Qualifying Activities (Pathways)
* any other points of eligibility subject to change

Renewals are completed by one of the following methods:

* administrative renewal (MAGI Medicaid only)
* alternate renewal
* standard renewal

NOTE: A face-to-face (FTF) renewal cannot be required for any Medicaid COA.
At the Case Worker's discretion or the request of the A/R or AREP, a FTF renewal may be scheduled;
however, a Medicaid case may *not* be closed for failure to appear for a FTF renewal.

=== Administrative Renewal

An administrative renewal can be completed at any time, even after the renewal notice has been generated, and also at any time when A/R is reporting changes, applying for or renewing attached programs.

NOTE: Administrative renewals should not be completed for Medicaid cases whose Medicaid eligibility has been determined using Express Lane Eligibility (ELE) .

An attempt must be made to renew eligibility for MAGI COA without requesting or requiring information from the household.
The annual renewal should be conducted using electronic data sources and any other information already available to DFCS (e.g., in a related active case).
If the household's eligibility is renewed based on available information, the household must be notified of the results.
Do not send a renewal form when an administrative renewal is completed.
A signed renewal form and DMA 285 are not required to be signed and returned.

If the AU cannot be renewed based on the available information, the AU falls into the regular process for alternate renewals and a notice will be sent the following month.
Upon request MAGI AUs will be provided with a pre-populated renewal form (web services) with information currently used to determine eligibility.

=== Alternate Renewal

An alternate renewal is completed by mail, telephone, fax, email, or through the Gateway online renewal process.

An alternate renewal notice must contain the following information:

* that a renewal is necessary to continue eligibility
* an alternate renewal form (system or manually issued)
* the date the alternate renewal form is due
* the consequences of failing to comply with the renewal
* the AU's responsibility to provide all required verification
* the AU's right to request a fair hearing
* address to return the renewal form
* telephone # for contacting the agency regarding the renewal or to request a prepopulated renewal form (for MAGI COA only)

The AU may respond to the renewal via:

* Online with Gateway
* Telephone
* Mail
* Other electronic sources (e.g., fax, email)
* In person or authorized representative (AREP)

The AU must be provided at least 30 calendar days in which to respond.

MAGI AUs that respond to the renewal request by means other than online with Gateway or submitting a signed renewal form MUST submit a signed form, either by mail or other electronic sources (e.g., fax, email).

Non-MAGI Medicaid AUs that fail to return the alternate renewal form, or that return an incomplete form, may be contacted by phone to complete the renewal requirements.
If missing information is obtained by telephone or other contact, the renewal is considered complete.
Document case to this effect.

NOTE: AUs should be reminded that sending personal information via email is not a secure mode of transmission.

=== Standard Renewal

A standard renewal is an in-depth FTF interview in which all points of eligibility are examined with an appropriate AU or BG member or a authorized representative (AREP).

A standard renewal is not required for any Medicaid COA.
A Medicaid case may not be terminated for failure to appear for a standard renewal even if the AU requested a FTF and failed to show.

A standard renewal appointment notice must include the following:

* that a renewal is necessary to continue eligibility
* that a FTF renewal is not required for continued eligibility and that an alternate renewal may substitute for a FTF renewal
* the date, time, and location of the interview
* the AU's responsibility to provide all required information
* the AU's right to request a fair hearing
* the name and telephone number of the worker

=== Unearned Income Verification Requirements

At renewal, the A/R's statement of unearned income will be accepted as verification if the source and amount is stated to have remained the same or changed less than $50 since last verified from the source.
Income types include but are not limited to direct child support, extended Unemployment Compensation Benefits that are not on DOL, RSDI and SSI that are not updated or not on BENDEX/SDX files, contributions, Veteran's Assistance (VA) benefits, Workmen's Compensation, Alimony, Pensions and Retirement and In-Kind Support and Maintenance (ISM).
MAGI Medicaid only includes taxable income.

The amount should be verified by a third-party source when the A/R's statement is questionable.
All electronic methods of verification (Clearinghouse, $TARS, etc.) will be utilized prior to accepting the client's statement of income.
For MAGI Medicaid COAs all data sources and any active related case information is to be used prior to requesting any verification.

=== Continuous Eligibility

Effective January 1, 2024, eligibility for Medicaid and PeachCare for Kids® recipients should be renewed every 12 months and no more frequently than once every 12 months. Children under the age of 19 will be provided 12 months of continuous eligibility (CE) coverage regardless of change in circumstances with certain exceptions. The exceptions to CE include the following:

•	The child reaches age 19.
•	The child is no longer a Georgia resident
•	A voluntary request for closure.
•	The agency determines that eligibility was erroneously granted at the most recent determination, redetermination, or renewal of eligibility because of agency error or fraud, abuse, or perjury attributed to the child or the child’s representative; or
•	The child is deceased.

CE does not apply to:

•	Medically Needy,
•	Presumptive Eligibility,
•	At renewal, children that are only eligible for Transitional Medical Assistance or
•	Emergency Medical Assistance.

If a child becomes incarcerated during their CE period, then the child must remain eligible for the remainder of the CE period while incarcerated.


== Procedures

=== Administrative Renewal

Follow the steps below to process an Administrative Renewal for a MAGI case

[horizontal,labelwidth=10]
Step 1:: In the second month prior to the renewal month, identify the MAGI COA case for Administrative Renewal

Step 2:: Using electronic data sources (DOL, UCB, SDX, BENDEX, Truv, etc.), review eligibility

Step 3:: If the electronic data sources are sufficient to validate eligibility and no additional contact with the AU is required, complete the renewal.
The system will extend the renewal period by 12 months.

Step 4:: Complete a Continuing Medicaid Determination (CMD) for any case determined no longer eligible under its current COA at renewal.

Step 5:: Document case notes that an administrative renewal was completed

Step 6:: Notify the AU of renewal by allowing the system to send the disposition notice.
If a manual notification is needed, mail xref:attachment$form-214.docx[Form 214 - Medicaid Notification Form] found in xref:appendix-f/index.adoc[Appendix F - TOC].

=== Alternate Renewal

Follow the steps below to process an Alternate Renewal.

[horizontal,labelwidth=10]
Step 1:: Mail the alternate renewal form to the AU no less than 10 days prior to the date the completed form is due to be returned. If the renewal form is not submitted, send a second request.
+
NOTE: AR has the ability to request a renewal form by emailing PaperRenewalForm@dhs.ga.gov.

[horizontal,labelwidth=10]
Step 2:: Mail any other required forms.
+
*For MAGI COA*
+
Review the returned renewal form (508 or 508M) or Customer Portal (CP) renewal for all points of eligibility. Contact the AU if the renewal form is not returned, if it is incomplete, or if additional information or verification is required. Contact may be made by telephone or by mail. A system-generated notice that a renewal form was not returned is considered sufficient contact.
+
If a renewal form is not completed on Gateway Customer Portal, a 297A, and DMA 285 must be sent to the A/R. The signed DMA 285 must be returned or any adult receiving in a Non-MAGI Family Medicaid case will be penalized.

+
*For Non-MAGI COA:*
+
Review the returned renewal form (508 or 508M) or Customer Portal (CP) renewal for all points of eligibility. Contact the AU if the renewal form is not returned, if it is incomplete, or if additional information or verification is required. Contact may be made by telephone or by mail. A system-generated notice that a renewal form was not returned is considered sufficient contact.
+
For Non-MAGI COA, the renewal may be processed without a signature or completed renewal form if all other required information is obtained by other measures. If a renewal form is not received or if the renewal is not completed on Gateway Customer Portal, then a 297A, and DMA 285 must be sent to the A/R The signed DMA 285 must be returned or the case will be closed. *(EXCEPTION:* SLMB, QI1, and QDWI does not require a DMA 285 be sent).

[horizontal,labelwidth=10]
Step 3:: Complete Clearinghouse requirements.

Step 4:: Document the information obtained during the renewal process.

Step 5:: Upon completion of the renewal and, if applicable, the receipt of any additional information or verification requested, finalize the renewal.

Step 6:: Complete a continuing Medicaid determination (CMD) for any case determined no longer eligible under its current COA at renewal

Step 7:: Notify the AU of the renewal disposition by allowing the system to send the disposition notice.
If a manual notification is needed, mail xref:attachment$form-214.docx[Form 214 - Medicaid Notification Form] found in xref:appendix-f/index.adoc[Appendix F - TOC].

=== Standard Renewal

Follow the steps below to process a Standard Renewal:
[horizontal,labelwidth=10]

NOTE: A standard renewal is not required for ANY Medicaid COA.
A Medicaid case may not be terminated for failure to appear for a standard renewal.

[horizontal,labelwidth=10]
Step 1:: Mail the AU an appointment notice to schedule the standard renewal.
The interview must be scheduled for a date that allows sufficient processing time of the renewal by the due date.
An appointment notice must be mailed to the AU no less than 10 days prior to the scheduled appointment.
+
[caption=Exception]
NOTE: The 10-day requirement does not apply to appointments scheduled verbally, either in person or by telephone;
however, the appointment notice must be mailed.

Step 2:: Conduct a FTF interview with the appropriate AU/BG member or AREP.
Review all points of eligibility.

Step 3:: Request additional information or verification, if appropriate.

Step 4:: Complete any forms necessary.

Step 5:: Complete Clearinghouse requirements.

Step 6:: Document the information obtained during the renewal process.

Step 7:: Upon completion of the interview and, if applicable, the receipt of any additional information or verification requested, finalize the renewal.

Step 8:: Complete a continuing Medicaid determination (CMD) for any case determined no longer eligible under its current COA at renewal

Step 9:: Notify the AU of the renewal disposition by allowing the system to send the disposition notice.
If a manual notification is needed, mail xref:attachment$form-214.docx[Form 214 - Medicaid Notification Form] found in xref:appendix-f/index.adoc[Appendix F - TOC].

Use the following chart to determine which Family Medicaid COAs require renewals.

.Chart 2706.1 – Family Medicaid Renewals
[#chart-2706-1,cols="1,^,^"]
|===
^| CLASS OF ASSISTANCE | SPECIAL REVIEWS | RENEWAL PERIOD

| Parent/Caretaker with Child(ren)
| as needed
| Annual (as of 1/1/14)

| TMA
| quarterly renewals
| At the end of the TMA eligibility period.

| 4MEx
| as needed
| At the end of 4MEx eligibility period

| Deemed Newborn
| No
| Month the child turns 1

| Children Under Age 19
| as needed
| Annual (as of 1/1/14)

| Pregnant Women
| month prior to the expected date of delivery and each month thereafter until termination of pregnancy
| None

| FM-MN
| as needed
| Every six months

| CWFC
| as needed
| Every six months

| Adoption Assistance
| yearly renewals
| Annual

| Women's Health Medicaid (WHM)
| yearly renewals
| Annual

| Planning for Healthy Babies® (P4HB)
| yearly renewals
| Annual

| PeachCare for Kids®
| yearly renewals
| Annual

| Pathways
| yearly renewals
| Annual

| Chafee Independence Medicaid
| as needed
| Annual

| Former Foster Care Medicaid
| as needed
| Annual
|===

Use the following chart to process a Medicaid Renewal.
Refer to <<chart-2706-1,Chart 2706.1, Family Medicaid Renewals>> for COAs that do not require renewal.

[#chart-2706-2]
.Chart 2706.2 - Procedures for Disposition of the Medicaid Renewal
|===
^| IF ^| THEN

| the AU complies with all requirements
| Continue eligibility, if appropriate.

| the AU misses a scheduled appointment
a| contact the AU to obtain required information.
This contact may be made by mail and/or by telephone.

NOTE: A standard (FTF) renewal is not required for *ANY* Medicaid COA.
A Medicaid case may *not* be terminated for failure to appear for a standard renewal.

| the agency did not provide written notice of the appointment 10 days prior to the appointment date and the appointment is missed
a| contact the AU to obtain required information.
This contact may be made by mail and/or by telephone.

NOTE: A standard (FTF) renewal is not required for *ANY* Medicaid COA.
A Medicaid case may *not* be terminated for failure to appear for a standard renewal.

| the AU fails to provide any requested verification
| determine if Medicaid eligibility for any other COA can be established without the requested verification.
If so, continue eligibility under the new COA.
If not, send timely notice and close the Medicaid case following expiration of the timely notice period.
If incomplete and/or incorrect verification is returned, a new checklist must be sent allowing for additional time to return the remainder and/or correct verification.
Refer to xref:2051.adoc[Section 2051 - Verifications].

| the AU fails to return the Alternate Renewal Form or complete the renewal on Gateway
a| contact the AU to obtain required information.
A system-generated notice that a renewal was not returned is considered sufficient contact.

NOTE: The Non-MAGI Medicaid renewal may be processed without a signature or completed renewal form if all other required information is obtained by other measures.
For MAGI Medicaid renewals a signature is required on the renewal form.
When received without a signature, and during the renewal period, the renewal form must be returned to the AU and signature requested.
Document the case.

| The MAGI and Non-MAGI Medicaid AU provides the renewal form or requested verification within 90 days
a| Process as renewal and reinstate the case back to the first day of closure month, and ongoing, if all information is received with the signed renewal form.
The system will generate a new 12-month POE.

For Non-MAGI cases process as renewal and reinstate the case back to the first day of closure month, and ongoing, if all renewal requirements are met.
The system will generate a new 12-month POE.

NOTE: QMB and P4HB COAs are excluded and do not have the 90-day reinstatement grace period.
|===
