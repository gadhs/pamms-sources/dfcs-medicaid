= 2335 Stocks and Mutual Fund Shares
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2335
:policy-title: Stocks and Mutual Fund Shares
:previous-policy-number: MT 1

include::partial$policy-header.adoc[]

== Requirements

The value of shares of stock and mutual funds is a countable resource.

[caption=Exception]
NOTE: Shares of stock in an Alaskan native regional or village corporation are excluded as resources.

== Basic Considerations

Shares of stock represent ownership in a business corporation.
Their value shifts with demand and may fluctuate widely.
The following guidelines apply to all types of stock, including preferred stock, warrants and rights, and options to purchase stock.

A _mutual fund_ is a company whose primary business is buying and selling securities and other investments.
Shares in a mutual fund represent ownership in the investment held by the fund.

== Procedures

Verify and document the following:

* Ownership Interest: If the shares are owned jointly, assume that each owner owns an equal share.
* Number of Shares Owned: Ask the individual to submit the stock certificate or most recent statement of account (including dividend account) from the firm that issued or is holding the stock.
Document the system and case with a photocopy.
If the individual does not have this documentation, have him/her obtain a statement from the firm.
Provide assistance as needed.

Determine the countable resource value of shares of stock or mutual funds by multiplying the number of shares owned by the CMV of each share.

* Current Market Value (CMV) of Each Share: The CMV of a stock as of the first moment of a given month is its closing price on the last business day of the preceding month.
The value of over-the counter stock is shown on a bid and asked basis.
Use the bid price as the CMV.
The par value or stated value shown on some stock certificates is not the market value of the stock.
* Consider the CMV of a mutual fund share to be the selling price (sell).

The closing price of a stock on a given day can usually be found in the next day's newspaper.

If the value of a stock does not appear in a newspaper, contact a local securities firm.
Provide the firm with the following information:

* name of stock, bond, or mutual fund
* type of stock, such as preferred or common
* months for which values are needed.
