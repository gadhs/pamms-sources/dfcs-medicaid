= 2123 Protected Medicaid 1972 (Public Law 92-603)
:chapter-number: 2100
:effective-date: February 2020
:mt: MT-58
:policy-number: 2123
:policy-title: Protected Medicaid 1972 (Public Law 92-603)
:previous-policy-number: MT 10

include::partial$policy-header.adoc[]

== Requirements

Protected Medicaid 1972 (PL 92-603) is a class of assistance (COA) that provides Medicaid for an individual/couple who received AABD or AFDC and RSDI concurrently in 1972 and became ineligible for AABD or AFDC because of the 20% 1972 COLA increase in RSDI.

== Basic Considerations

To be eligible under the Protected Medicaid 1972 COA an A/R must meet the following conditions:

* The A/R is currently receiving RSDI.
* The A/R received AABD or AFDC and RSDI in 8/72.
* The A/R is currently eligible for SSI if the 1972 RSDI COLA is disregarded.
* The A/R meets all basic and financial eligibility criteria.

NOTE: Length of Stay (LOS) and Level of Care (LOC) are *NOT* requirements for this COA.

NOTE: Prior receipt of SSI is *NOT* a requirement for this COA.

NOTE: Applications are no longer approved under this COA.

== Procedures

Follow the steps below to determine Medicaid eligibility under the Protected Medicaid 1972 COA:

[horizontal,labelwidth=10]
Step 1:: Accept the A/R's Medicaid application.

Step 2:: Obtain information required to complete the eligibility determination.

Step 3:: Using county and state records, determine whether the A/R received AABD or AFDC in 8/72.

Step 4:: Obtain the following verification from the Social Security Administration:

* The current amount of the A/R's RSDI benefit.
* The amount of the A/R's 8/72 RSDI COLA.

Step 5:: Determine all basic eligibility criteria except LOS and LOC.
Refer to the Chapter 2200, Basic Eligibility Criteria.

Step 6:: Determine financial eligibility using the current SSI income and resource limits.
Refer to the Chapter 2500, ABD Financial Responsibility and Budgeting, to determine the following:

* Whose income and resources to consider
* Which SSI income and resource limit (individual or couple) to use
* Which eligibility budget to complete

Step 7:: Approved Medicaid under the Protected Medicaid 1972 COA if the A/R meets all the above eligibility criteria, including retroactive months if needed.
+
NOTE: Do not approve Medicaid under the Protected Medicaid 1972 COA for any month for which the A/R was eligible for and received an SSI payment.
