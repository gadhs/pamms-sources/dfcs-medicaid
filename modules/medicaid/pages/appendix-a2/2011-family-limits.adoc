= Appendix A2 Family Medicaid Financial Limits 2011
:chapter-number: Appendix A2
:effective-date: 02/01/2011
:mt: MT-42
:policy-number: Appendix A2
:policy-title: Family Medicaid Financial Limits 2011 (effective 02/01/2011)
:previous-policy-number:

include::partial$policy-header.adoc[]

== 2011 Income Limits

[cols="9*^"]
|===
| h| LIM h| LIM h| PCK h| RSM PgW, NB, WHM, P4HB, PE  h| RSM CHILD 0-1, TMA, WIC h| RSM CHILD 1-5 h| RSM CHILD 6-19 h| FM-MNIL

h| BUDGET GROUP (BG) SIZE h| GROSS INCOME CEILING (GIC) h| STANDARD OF NEED (SON) h| 235% FEDERAL POVERTY LEVEL h| 200% FEDERAL POVERTY LEVEL (FPL) h| 185% FEDERAL POVERTY LEVEL (FPL) h| 133% FEDERAL POVERTY LEVEL (FPL) h| 100% FEDERAL POVERTY LEVEL (FPL) h| FAMILY MEDICAID MNIL

| 1
| $435
| 235
| 2134
| 1815
| 1679
| 1207
| 908
| 208

| 2
| 659
| 356
| 2881
| 2452
| 2268
| 1631
| 1226
| 317

| 3
| 784
| 424
| 3628
| 3089
| 2857
| 2054
| 1545
| 375

| 4
| 925
| 500
| 4378
| 3725
| 3446
| 2478
| 1863
| 442

| 5
| 1060
| 573
| 5125
| 4362
| 4035
| 2901
| 2181
| 508

| 6
| 1149
| 621
| 5873
| 4999
| 4624
| 3324
| 2500
| 550

| 7
| 1243
| 672
| 6622
| 5635
| 5213
| 3748
| 2818
| 600

| 8
| 1319
| 713
| 7370
| 6272
| 5802
| 4171
| 3136
| 633

| 9
| 1389
| 751
| 8120
| 6910
| 6393
| 4596
| 3455
| 667

| 10
| 1487
| 804
| 8870
| 7548
| 6984
| 5021
| 3774
| 708

| 11
| 1591
| 860
| 9620
| 8186
| 7575
| 5446
| 4093
| 758

| 12
| 1635
| 884
| 10370
| 8824
| 8166
| 5871
| 4412
| 808

| (+) PER ADDITIONAL BG MEMBER
| 44
| 24
| 750
| 638
| 591
| 425
| 319
| 50
|===

== 2011 Resource Limits

|===
h|LIM RESOURCE LIMIT |$1000
h|FM-MN ALLOWABLE MILEAGE REIMBURSEMENT| 51 CENTS PER MILE
|===

.FAMILY MEDICAID MEDICALLY NEEDY (FM-MN) RESOURCE LIMIT
[cols=12*^]
|===
12+h|NUMBER OF INDIVIDUALS IN FM-MN BG
h| 1 h| 2 h| 3 h| 4 h| 5 h| 6 h| 7 h| 8 h| 9 h| 10 h| 11 h| 12

| $2000
| 4000
| 4100
| 4200
| 4300
| 4400
| 4500
| 4600
| 4700
| 4800
| 4900
| 5000
|===
