= Appendix A2 Family Medicaid Financial Limits 2013
:chapter-number: Appendix A2
:effective-date: 02/01/2013
:mt: MT-46
:policy-number: Appendix A2
:policy-title: Family Medicaid Financial Limits 2013 (effective 02/01/2013)
:previous-policy-number:

include::partial$policy-header.adoc[]

== 2013 Income Limits

[cols="9*^"]
|===
| h| LIM h| LIM h| PCK h| RSM PgW, NB, WHM, P4HB, PE  h| RSM CHILD 0-1, TMA, WIC h| RSM CHILD 1-5 h| RSM CHILD 6-19 h| FM-MNIL

h| BUDGET GROUP (BG) SIZE h| GROSS INCOME CEILING (GIC) h| STANDARD OF NEED (SON) h| 235% FEDERAL POVERTY LEVEL h| 200% FEDERAL POVERTY LEVEL (FPL) h| 185% FEDERAL POVERTY LEVEL (FPL) h| 133% FEDERAL POVERTY LEVEL (FPL) h| 100% FEDERAL POVERTY LEVEL (FPL) h| FAMILY MEDICAID MNIL

| 1
| $435
| 235
| 2252
| 1916
| 1773
| 1275
| 958
| 208

| 2
| 659
| 356
| 3039
| 2586
| 2393
| 1720
| 1293
| 317

| 3
| 784
| 424
| 3826
| 3256
| 3012
| 2166
| 1628
| 375

| 4
| 925
| 500
| 4614
| 3926
| 3632
| 2611
| 1963
| 442

| 5
| 1060
| 573
| 5401
| 4596
| 4252
| 3057
| 2298
| 508

| 6
| 1149
| 621
| 6188
| 5266
| 4872
| 3502
| 2633
| 550

| 7
| 1243
| 672
| 6975
| 5936
| 5491
| 3948
| 2968
| 600

| 8
| 1319
| 713
| 7763
| 6606
| 6111
| 4393
| 3303
| 633

| 9
| 1389
| 751
| 8550
| 7276
| 6731
| 4838
| 3638
| 667

| 10
| 1487
| 804
| 9337
| 7946
| 7351
| 5283
| 3973
| 708

| 11
| 1591
| 860
| 10124
| 8616
| 7971
| 5728
| 4308
| 758

| 12
| 1635
| 884
| 10911
| 9286
| 8591
| 6173
| 4643
| 808

| (+) PER ADDITIONAL BG MEMBER
| 44
| 24
| 787
| 670
| 620
| 445
| 335
| 50
|===

== 2013 Resource Limits

|===
h|LIM RESOURCE LIMIT |$1000
h|FM-MN ALLOWABLE MILEAGE REIMBURSEMENT| 56.5 CENTS PER MILE
|===

.FAMILY MEDICAID MEDICALLY NEEDY (FM-MN) RESOURCE LIMIT
[cols=12*^]
|===
12+h|NUMBER OF INDIVIDUALS IN FM-MN BG
h| 1 h| 2 h| 3 h| 4 h| 5 h| 6 h| 7 h| 8 h| 9 h| 10 h| 11 h| 12

| $2000
| 4000
| 4100
| 4200
| 4300
| 4400
| 4500
| 4600
| 4700
| 4800
| 4900
| 5000
|===
