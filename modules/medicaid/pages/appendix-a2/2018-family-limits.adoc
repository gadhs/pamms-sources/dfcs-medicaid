= Appendix A2 Family Medicaid Financial Limits 2018
:chapter-number: Appendix A2
:effective-date: 03/01/2018
:mt: MT-52
:policy-number: Appendix A2
:policy-title: Family Medicaid Financial Limits 2018 (effective 03/01/2018)
:previous-policy-number:

include::partial$policy-header.adoc[]

== 2018 Income Limits

.Percentage of the Federal Poverty Level (FPL)
[cols="13*^"]
|===
| Family Size | Parent / Caretaker with Children | Plus 5% | 247% PCK | Plus 5% | 205% Child 0-1 TMA | Plus 5% | 211% P4HB | Plus 5% | 149% Child 1-5 | Plus 5% | 133% Child 6-19 | Plus 5%
| 1 | 310 | 361 | 2499 | 2550 | 2074 | 2125 | 2135 | 2186 | 1508 | 1559 | 1346 | 1397
| 2 | 457 | 526 | 3388 | 3457 | 2812 | 2881 | 2895 | 2964 | 2044 | 2113 | 1825 | 1894
| 3 | 551 | 638 | 4278 | 4365 | 3550 | 3637 | 3654 | 3741 | 2581 | 2668 | 2304 | 2391
| 4 | 653 | 758 | 5167 | 5272 | 4288 | 4393 | 4414 | 4519 | 3117 | 3222 | 2782 | 2887
| 5 | 752 | 875 | 6056 | 6179 | 5026 | 5149 | 5173 | 5296 | 3653 | 3776 | 3261 | 3384
| 6 | 826 | 967 | 6945 | 7086 | 5764 | 5905 | 5933 | 6074 | 4190 | 4331 | 3740 | 3881
| 7 | 903 | 1062 | 7834 | 7993 | 6502 | 6661 | 6693 | 6852 | 4726 | 4885 | 4219 | 4378
| 8 | 970 | 1147 | 8724 | 8901 | 7240 | 7417 | 7452 | 7629 | 5263 | 5440 | 4698 | 4875
| 9 | 1034 | 1229 | 9613 | 9808 | 7978 | 8173 | 8212 | 8407 | 5799 | 5994 | 5176 | 5371
| 10 | 1113 | 1326 | 10502 | 10715 | 8716 | 8929 | 8971 | 9184 | 6335 | 6548 | 5655 | 5868
| 11 | 1194 | 1425 | 11391 | 11622 | 9454 | 9685 | 9731 | 9962 | 6872 | 7103 | 6134 | 6365
| 12 | 1244 | 1493 | 12280 | 12529 | 10192 | 10441 | 10491 | 10740 | 7408 | 7657 | 6613 | 6862
| For each additional member, add:  |  |  | 890 |  | 738 |  | 760 |  | 537 |  | 479 |

|===

NOTE: A Budget Group of One does not exist for Parent/Caretaker with Child(ren) Medicaid or Pregnant Woman Medicaid.


.Percentage of the Federal Poverty Level (FPL)
[cols="5*^"]
|===
| Family Size | 220% PGW Newborn | Plus 5% | 200% WHM P4HB  | Family Medicaid MNIL
| 1 | 2226 | 2277 | 2024 | 208
| 2 | 3018 | 3087 | 2744 | 317
| 3 | 3810 | 3897 | 3464 | 375
| 4 | 4602 | 4707 | 4184 | 442
| 5 | 5394 | 5517 | 4904 | 508
| 6 | 6186 | 6327 | 5624 | 550
| 7 | 6978 | 7137 | 6344 | 600
| 8 | 7770 | 7947 | 7064 | 633
| 9 | 8562 | 8757 | 7784 | 667
| 10 | 9354 | 9567 | 8504 | 708
| 11 | 10146 | 10377 | 9224 | 758
| 12 | 10938 | 11187 | 9944 | 808
| For each additional member, add:  | 792 |  | 720 | (+) PER
ADDITIONAL
BG MEMBER
50
|===

NOTE: A Budget Group of One does not exist for Parent/Caretaker with Child(ren) Medicaid or Pregnant Woman Medicaid.

== 2018 Resource Limits

.FAMILY MEDICAID MEDICALLY NEEDY (FM-MN) RESOURCE LIMIT
[cols=12*^]
|===
12+h|NUMBER OF INDIVIDUALS IN FM-MN BG
h| 1 h| 2 h| 3 h| 4 h| 5 h| 6 h| 7 h| 8 h| 9 h| 10 h| 11 h| 12

| $2000
| 4000
| 4100
| 4200
| 4300
| 4400
| 4500
| 4600
| 4700
| 4800
| 4900
| 5000
|===

FM-MN Allowable Mileage Reimbursement: 54.5 Cents Per Mile
