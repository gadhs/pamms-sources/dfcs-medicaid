= Appendix A2 Family Medicaid Financial Limits 2023
:chapter-number: Appendix A2
:effective-date: 07/01/2023
:mt: MT-70
:policy-number: Appendix A2
:policy-title: Family Medicaid Financial Limits 2023 (effective 07/01/2023)
:previous-policy-number:
:!example-caption:

include::partial$policy-header.adoc[]

== 2023 Income Limits

.Percentage of the Federal Poverty Level (FPL)
[cols="15*^"]
|===
| Family Size | Parent / Caretaker with Children | Plus 5% | 247% PCK | Plus 5% | 205% Child 0-1 TMA | Plus 5% | 211% P4HB | Plus 5% | 149% Child 1-5 | Plus 5% | 133% Child 6-19 | Plus 5% | 95% Pathways | Plus 5% Pathways

| 1
| $310
| 371
| 3002
| 3063
| 2491
| 2552
| 2564
| 2625
| 1811
| 1872
| 1616
| 1677
| 1155
| 1215

| 2
| 457
| 540
| 4060
| 4143
| 3369
| 3452
| 3468
| 3551
| 2449
| 2532
| 2186
| 2269
| 1562
| 1644

| 3
| 551
| 655
| 5118
| 5222
| 4247
| 4351
| 4372
| 4476
| 3087
| 3191
| 2756
| 2860
| 1969
| 2072

| 4
| 653
| 778
| 6175
| 6300
| 5125
| 5250
| 5275
| 5400
| 3725
| 3850
| 3325
| 3450
| 2375
| 2500

| 5
| 752
| 899
| 7233
| 7380
| 6004
| 6151
| 6179
| 6326
| 4364
| 4511
| 3895
| 4042
| 2782
| 2929

| 6
| 826
| 994
| 8291
| 8459
| 6882
| 7050
| 7083
| 7251
| 5002
| 5170
| 4465
| 4633
| 3189
| 3357

| 7
| 903
| 1093
| 9349
| 9539
| 7760
| 7950
| 7987
| 8177
| 5640
| 5830
| 5035
| 5225
| 3596
| 3785

| 8
| 970
| 1181
| 10407
| 10618
| 8638
| 8849
| 8891
| 9102
| 6278
| 6489
| 5604
| 5815
| 4003
| 4214

| 9
| 1034
| 1267
| 11465
| 11698
| 9516
| 9749
| 9794
| 10027
| 6917
| 7150
| 6174
| 6407
| 4410
| 4642

| 10
| 1113
| 1367
| 12523
| 12777
| 10394
| 10648
| 10698
| 10952
| 7555
| 7809
| 6744
| 6998
| 4817
| 5070

| 11
| 1194
| 1469
| 13581
| 13856
| 11271
| 11546
| 11601
| 11876
| 8193
| 8468
| 7313
| 7588
| 5224
| 5498

| 12
| 1244
| 1541
| 14639
| 14936
| 12150
| 12447
| 12506
| 12803
| 8831
| 9128
| 7883
| 8180
| 5631
| 5927

| For each additional member, add:
| $150
|
| $1058
|
| $879
|
| $904
|
| $639
|
| $570
|
| $407
|
|===

NOTE: A Budget Group of One does not exist for Parent/Caretaker with Child(ren) Medicaid or Pregnant Woman Medicaid.

.Percentage of the Federal Poverty Level (FPL) (continued)
[cols="6*^"]
|===
| Family Size | 220% PGW Newborn | Plus 5% | 200% WHM | 235% ELE/CU19 (see <<ele,NOTE>>) | FAMILY MEDICAID MNIL

| 1
| 2673
| 2734
| 2430
| 2856
| 208

| 2
| 3616
| 3699
| 3287
| 3862
| 317

| 3
| 4558
| 4662
| 4144
| 4869
| 375

| 4
| 5500
| 5625
| 5000
| 5875
| 442

| 5
| 6443
| 6590
| 5857
| 6882
| 508

| 6
| 7385
| 7553
| 6714
| 7889
| 550

| 7
| 8327
| 8517
| 7570
| 8895
| 600

| 8
| 9270
| 9481
| 8427
| 9902
| 633

| 9
| 10212
| 10445
| 9284
| 10908
| 667

| 10
| 11154
| 11408
| 10140
| 11915
| 708

| 11
| 12096
| 12371
| 10996
| 12921
| 758

| 12
| 13039
| 13336
| 11854
| 13928
| 808

| For each additional member, add:
| $943
|
| $857
| $1007
| (+) PER ADDITIONAL BG MEMBER

50
|===

NOTE: A Budget Group of One does not exist for Parent/Caretaker with Child(ren) Medicaid or Pregnant Woman Medicaid.

[#ele]
NOTE: Regarding Express Lane Eligibility, if child is in an active SNAP or TANF case, and they are over the 235%, but under 247% FPL (PCK Limits), the child ELE PCK.

== 2023 Resource Limits

.FAMILY MEDICAID MEDICALLY NEEDY (FM-MN) RESOURCE LIMIT
[cols=12*^]
|===
12+h|NUMBER OF INDIVIDUALS IN FM-MN BG
h| 1 h| 2 h| 3 h| 4 h| 5 h| 6 h| 7 h| 8 h| 9 h| 10 h| 11 h| 12

| $2000
| 4000
| 4100
| 4200
| 4300
| 4400
| 4500
| 4600
| 4700
| 4800
| 4900
| 5000
|===

.FM-MN Allowable Mileage Reimbursement
====
65.5 CENTS PER MILE EFFECTIVE 01/01/2023
====
