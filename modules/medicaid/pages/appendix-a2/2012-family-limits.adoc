= Appendix A2 Family Medicaid Financial Limits 2012
:chapter-number: Appendix A2
:effective-date: 02/01/2012
:mt: MT-44
:policy-number: Appendix A2
:policy-title: Family Medicaid Financial Limits 2012 (effective 02/01/2012)
:previous-policy-number:

include::partial$policy-header.adoc[]

== 2012 INCOME LIMITS

[cols="9*^"]
|===
| h| LIM h| LIM h| PCK h| RSM PgW, NB, WHM, P4HB, PE  h| RSM CHILD 0-1, TMA, WIC h| RSM CHILD 1-5 h| RSM CHILD 6-19 h| FM-MNIL

h| BUDGET GROUP (BG) SIZE h| GROSS INCOME CEILING (GIC) h| STANDARD OF NEED (SON) h| 235% FEDERAL POVERTY LEVEL h| 200% FEDERAL POVERTY LEVEL (FPL) h| 185% FEDERAL POVERTY LEVEL (FPL) h| 133% FEDERAL POVERTY LEVEL (FPL) h| 100% FEDERAL POVERTY LEVEL (FPL) h| FAMILY MEDICAID MNIL

| 1
| $435
| 235
| 2188
| 1862
| 1723
| 1239
| 931
| 208

| 2
| 659
| 356
| 2964
| 2522
| 2333
| 1678
| 1261
| 317

| 3
| 784
| 424
| 3739
| 3182
| 2944
| 2117
| 1591
| 375

| 4
| 925
| 500
| 4515
| 3842
| 3554
| 2555
| 1921
| 442

| 5
| 1060
| 573
| 5290
| 4502
| 4165
| 2994
| 2251
| 508

| 6
| 1149
| 621
| 6066
| 5162
| 4775
| 3433
| 2581
| 550

| 7
| 1243
| 672
| 6841
| 5822
| 5386
| 3872
| 2911
| 600

| 8
| 1319
| 713
| 7617
| 6482
| 5996
| 4311
| 3241
| 633

| 9
| 1389
| 751
| 8392
| 7142
| 6606
| 4750
| 3571
| 667

| 10
| 1487
| 804
| 9168
| 7802
| 7216
| 5189
| 3901
| 708

| 11
| 1591
| 860
| 9944
| 8462
| 7826
| 5628
| 4231
| 758

| 12
| 1635
| 884
| 10720
| 9122
| 8436
| 6067
| 4561
| 808

| (+) PER ADDITIONAL BG MEMBER
| 44
| 24
| 776
| 660
| 610
| 439
| 330
| 50
|===

== 2012 Resource Limits

|===
h|LIM RESOURCE LIMIT |$1000
h|FM-MN ALLOWABLE MILEAGE REIMBURSEMENT| 51 CENTS PER MILE
|===

.FAMILY MEDICAID MEDICALLY NEEDY (FM-MN) RESOURCE LIMIT
[cols=12*^]
|===
12+h|NUMBER OF INDIVIDUALS IN FM-MN BG
h| 1 h| 2 h| 3 h| 4 h| 5 h| 6 h| 7 h| 8 h| 9 h| 10 h| 11 h| 12

| $2000
| 4000
| 4100
| 4200
| 4300
| 4400
| 4500
| 4600
| 4700
| 4800
| 4900
| 5000
|===
