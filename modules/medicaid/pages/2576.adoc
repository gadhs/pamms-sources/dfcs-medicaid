= 2576 Vendor Payment Authorization
:chapter-number: 2575
:effective-date: February 2020
:mt: MT-58
:policy-number: 2576
:policy-title: Vendor Payment Authorization
:previous-policy-number: MT 44

include::partial$policy-header.adoc[]

== Requirements

A payment to the nursing home (NH) to defray the cost of care may be authorized when Medicaid eligibility has been approved.
This Medicaid payment for NH care is called a vendor payment.

Nursing home A/Rs whose income is equal to or above the Medicaid Cap and who do not establish a Qualified Income Trust (QIT) may have their Medicaid eligibility determined under AMN (S99) Medicaid.
Vendor payments are not authorized unless the A/R is approved under the NH COA.

== Basic Considerations

The vendor payment is authorized by DFCS by entering pertinent data into the computer system.

At application, Form DMA-59 is prepared by the NH and sent to DFCS with the following information completed:

* The recipient's identifying information in Section I
* The *Patient Admitted From* field in Section II.

Form DMA-59 must be signed by the NH administrator (initial application only) and dated.
A signature by the NH staff is not necessary on the DMA-59 for subsequent actions reflected on the DMA-59.

Prepayments and deposits may be required by a NH for an individual not already receiving Medicaid on the date of admission.

=== Refunds of Prepayments or Deposits Made to Nursing Homes by Medicaid Applicants

The facility must return such deposits to the individual or his/her family after eligibility for Medicaid is established.
The refund, if made to the patient, is not counted as income to the individual in determining eligibility or patient liability but is considered a resource to the patient at the time of application.

== Procedures

Authorize the NH vendor payment via the current eligibility system.
This information will be passed to DCH via the interface.

DFCS is not required to return a copy of the initial DMA-59 to the nursing home when the case is approved, and vendor payment is authorized.

=== Parent/Caretaker with Child(ren) (PCT)/BCCP/PeachCare for Kids® Temporarily in the NH

If a PCT/BCCP/PeachCare for Kids® A/R is temporarily in a NH, authorize the vendor payment by faxing a copy of a DMA-59 to DCH at 404-463-2538.
Send it to the attention of the DCH Member Services and Policy unit and annotate the DMA-59 by stating it is “LIM in the NH”, “BCCP in the NH” or “PeachCare for Kids® in the NH”.

Use the following table to determine the Initial “Payment Authorization Date” to enter in the system:

NOTE: The Initial “Payment Authorization Date” entered in the system *CANNOT* pre-date the effective date of Medicaid eligibility.

[#chart-2576-1]
.Chart 2576.1 - Determining the Initial “Payment Authorization Date of a NH Vendor Payment
|===
^| IF the A/R is ^| THEN the initial payment Authorization Date entered in the system is

| a Medicaid recipient under any class of assistance whose income is under the Medicaid Cap (except Q Track) upon admission to the NH

*AND*

has no VA third party contracts
a| the date indicated on Form DMA-59 as the Admission Date or the Effective Date, whichever is later.

NOTE: A/Rs who enter the NH who are Q Track eligible should be approved for NH in the system as a separate NH Assistance Unit.
A/Rs who are approved under the NH COA may also be approved as QMB eligible under a different AU.

| not a Medicaid recipient* upon admission to the NH

*AND*

has no VA third party contracts

*AND*

Meets all eligibility criteria

*This includes Q Track recipients.
a| the latest of the following dates:

* the date indicated on Form DMA-59 as the Admission Date or Effective Date, whichever is later.
* the beginning date of Medicaid eligibility.
* the first Medicaid eligible day of any month for which the facility has not been paid in full or agrees to make a refund to the patient or family.

| a Medicaid recipient in an NH

*AND*

a VA third party contract is paying the cost of care
a| the first day after the VA third party contract benefits are exhausted or terminated.

NOTE: A/R can be Medicaid eligible, but no VP is authorized to the NH while the A/R is under a VA contract.
The NH will notify DFCS when the contract expires.
|===

Refer to xref:2577.adoc[Section 2577 - Limited Stays] for more information on Limited Stays.

Code income from VA Aid and Attendance appropriately in the system since it is not included in patient liability or eligibility determinations.

=== Terminations

Terminate the vendor payment by completing the appropriate fields in the system.
It is not necessary to submit a DMA-59 to DCH or the nursing home.

=== Notification

Notify the recipient and personal representative of the effective date of the vendor payment and the patient liability amounts via the system generated notice.

Process changes in the system to allow timely notice:

* 14 days before the end of the month for an increase in patient liability to be effective the same month.
If timely notice cannot be given within 14 days prior to the end of the month, the PL increase will not be effective until the following month.
* 14 days before the end of the month for termination of the vendor payment to be effective the first day of the following month.
* A decrease in patient liability may be processed at any time.

NOTE: It is not necessary for the nursing home to send a discharge DMA-59 to DFCS if the A/R is discharged to the hospital and readmitted to the same nursing home, even if the hospital stay exceeds seven days.

The nursing home should notify DFCS via DMA-59 if the A/R discharges to another nursing home, home, dies, or becomes otherwise ineligible.
However, should DFCS become aware of such a discharge, a DMA-59 is not mandatory to process the change as long as known information is validated.
