= 2504 Determining Countable Income
:chapter-number: 2500
:effective-date: February 2020
:mt: MT-58
:policy-number: 2504
:policy-title: Determining Countable Income
:previous-policy-number: MT 23

include::partial$policy-header.adoc[]

== Requirements

Countable income for determining ABD Medicaid eligibility may include more or less income than is actually received.
Countable income is determined for each month separately based on income received each month.

== Basic Considerations

The following are examples of situations where more or less income than the A/R actually receives is included in the eligibility and patient liability/cost share budgets:

* Expenses of obtaining income (less)
* Expenses of converting payments in foreign currency to U.S. dollars (less)
* Garnishment (more)
* Gross earnings, before any deductions (more)
* Infrequent or irregular income (less).

NOTE: A portion of the A/R's income may be excluded based on the type of income received, such as child support.
Refer to xref:2499.adoc#chart-2499-1[Chart 2499.1, Treatment of Income in Medicaid].

=== Expenses of Obtaining Income

An expense that is essential to obtain a particular payment(s) is deducted from the gross income.

A fee to acquire documentation to establish that an individual has a right to certain income (e.g., a fee for a birth certificate, legal fee, or medical examination) is an essential expense.

A guardianship fee is an essential expense if the presence of a guardian is a requirement for receiving the income.
In cases where SSA requires that payments be made to a representative payee, the appointment of a legal guardian is unnecessary, and guardianship fees are not allowable.

== Procedures

=== Expenses of Obtaining Income

Deduct that part of a payment that is an essential expense incurred in obtaining the payment.

* Subtract legal, medical and other expenses connected with an accident from the payment for damages received in the accident.
* Subtract legal fees connected with that claim if an individual receives a retroactive payment from a benefit program other than SSI.

Deduct expenses from the first and any subsequent payments of the same type of income until all expenses are eliminated.

* Deduct those verified expenses which the recipient has previously paid (e.g., a partial payment to an attorney made from the individual's savings account) as long as the expenses are essential.
* Use bills, receipts, contact with the provider, etc., to verify all essential expenses.
* Consider the remainder as unearned income subject to the general rules pertaining to income and income exclusions.

=== Expenses in Converting Payments in Foreign Currency to U.S. Dollars

An individual may receive income tendered to him/her in a monetary unit other than U.S. dollars.

Consider the U.S. dollar value of a payment made in foreign currency, less expenses, as income.

Count foreign currency payments received.
If the individual alleges and can establish that the payment was received too late in the month for conversion, count the payment as income for the following month.

Use a check or documents in the individual's possession to verify receipt of a foreign payment and the amount in foreign currency.
If the payment is made directly to a bank, the bank may provide a statement of the amount received.

If the evidence is not readily available, or if translation of the documents would require a delay beyond the receipt of the next payment, complete the following procedures:

* Accept the individual's signed statement.
* Ask the individual to present his next check before cashing it.

Verify the exchange rate for conversion to the foreign currency into U.S. dollars using one of the following:

* a receipt for the individual's last exchange
* a telephone call to a local bank or currency exchange.

Presume that an established exchange rate remains constant until the next review.
If, at the next review, the exchange rate has changed, presume the change occurred in the month of verification.

[caption=Exception]
NOTE: If the individual reports that the exchange rate has changed, verify the change and adjust the income charged to reflect the new rate.

=== Garnishment

A garnishment is withholding of an amount from earned or unearned income in order to satisfy a debt or legal obligation.

Include a garnishment from earned or unearned income as income for Medicaid eligibility and patient liability budgeting purposes.

Include garnishments based on any of the following as income:

* voluntary agreement
* repayment of a debt
* satisfying a legal obligation.

NOTE: This policy does not apply to amounts withheld to pay the expense of obtaining the income, since such amounts are not income.

=== Gross Income Withholding

Do not allow the following items as deductions from income for ABD Medicaid eligibility budgeting purposes:

* alimony payments, court ordered or voluntary
* child support payments, court ordered or voluntary
* federal, state, or local income taxes
* garnishments
* guardianship fees if the presence of a guardian is not a requirement for receiving the income
* health or life insurance premiums
* inheritance taxes
* loan payments
* penalty deductions for failure to report changes
* service fees charged on interest-bearing checking accounts
* SMI (Medicare Part B) premiums
* Medicare Part D premiums
* union dues.

Use a document in the individual's possession or contact the source of the payment to verify the amount withheld.
Add the amount withheld to the amount received and consider the total as the earned income from that source.

NOTE: Refer to Chapter 2552, Patient Liability Budgeting, for a list of deductions allowed in the patient liability/cost share budget.

=== Estimated Income

Estimate current and future monthly income.

If the amount is the same each payday, multiply the amount by the number of paydays in the month to obtain the monthly amount.

When income fluctuates within a month, use representative income to represent the amount of income for each period of receipt.

* Determine representative income by using an average of the amounts from the last 4 periods or by using the amount indicated by the A/R as most representative.
* Multiply the representative income by the number of periods of receipt in the month being budgeted to obtain the anticipated monthly income.

=== Income Expected Less Than Once a Month

Determine the specific month(s) the income is expected to be received and budget the amount for the appropriate month(s).

=== Infrequent or Irregular Income

Exclude income received either infrequently or irregularly, provided the total of such income does not exceed the following amounts:

* $30 per quarter of earned income
+
*AND/OR*
* $60 per quarter of unearned income.

In order for this exclusion to apply, income need only be one of the following:

* Infrequent – income received no more than once in a calendar quarter from a single source.
* Irregular – income that the A/R cannot reasonably expect to receive on a regular basis.

=== Total Exceeds the Limit

Apply this exclusion to both earned and unearned income in the same month, provided the total of each does not exceed the limits above.
It is possible to exclude as much as $30 a month under this provision.

Include all income received on an infrequent or irregular basis in the eligibility budget if the total of such income received in a particular month exceeds the limits stated above.

=== Limit as It Applies to Medicaid Couples or a Medicaid Individual with an Ineligible Spouse

Apply only one exclusion each month to income received infrequently or irregularly by a Medicaid individual, Medicaid couple, ineligible spouse, ineligible parent and ineligible child.

=== Determining Frequency of Receipt

Determine the frequency of the income by reviewing the receipts of the same type of income from a single source.

=== Interest Income of $20 or Less per Month

Refer to Interest in xref:2499.adoc#chart-2499-1[Chart 2499.1, Treatment of Income in Medicaid], for information on the exclusion of interest income of $20 per month or less.

=== Interest Income Earned on Excluded Resources

Exclude from the eligibility determination and PL/CS process ANY interest/dividends earned on countable resources.
Include such interest/dividends in the resource determination the month following the month of receipt.
Refer to Interest in xref:2499.adoc#chart-2499-1[Chart 2499.1, Treatment of Income in Medicaid].
