= 2068 Special Considerations
:chapter-number: 2050
:effective-date: December 2019
:mt: MT-57
:policy-number: 2068
:policy-title: Special Considerations
:previous-policy-number: MT 06

include::partial$policy-header.adoc[]

== Applications at a Non-DFCS Site

DFCS workers are outstationed at certain public health centers, disproportionate share or public hospitals and federally funded health centers to expedite the processing of applications for Medicaid.
These workers process the Medicaid applications as if taken at the DFCS office.

Medicaid applications are taken at sites other than DFCS by the facility's personnel.
The facility workers are trained by DFCS to accept applications and conduct interviews.

During the interview at a non-DFCS site, the facility interviewer documents the information, signs and dates the form and forwards it to DFCS.

The DFCS worker reviews the application and interview information and conducts follow-up contact with the applicant, if necessary, either by telephone or mail.

== Procedures

Follow the steps below for applications received at approved facilities:

[horizontal,labelwidth=10]
Step 1:: Review the application for all points of eligibility.
If additional information is required, contact the A/R by telephone or mail.
+
NOTE: Follow-up is generally not required for Presumptive Eligibility applications, which contain all information required to determine eligibility.

Step 2:: Mail required Forms DMA-285, 297A and 138.

Step 3:: Determine eligibility and notify the A/R of the decision.
The application date is the date the non-DFCS facility received the signed application from the A/R.
+
NOTE: The application date for applications received from private medical facilities is the date the application is received by DFCS.

=== Application Received for an Individual Currently Receiving Medicaid in Another State

An individual who is currently receiving Medicaid in another state but has moved to Georgia may file an application.
Notify the previous state that the applicant has moved to Georgia, and determine eligibility for Medicaid.
Do not delay the disposition of the Medicaid application while waiting for the previous state to terminate benefits.

An individual may receive Medicaid in both states the month that s/he moves to Georgia.

=== Out-of-State Application for Three months Prior Medicaid for a Former Georgia Resident

An application may be filed for medical services received as a Georgia resident even if the individual subsequently moved out of state.

Complete all necessary forms.
Forms can be mailed to the applicant.

Determine eligibility using the appropriate Class of Assistance (COA).
Refer to xref:2053.adoc[].

If eligible, send notification of the decision and certification of Medicaid eligibility.

=== Current Georgia Resident Requests Three Months Prior Medicaid from Another State

Refer the A/R to the previous state for application and eligibility processing.

Offer assistance in contacting the other state's agency if the A/R is unable to do so.

=== Application for a Deceased Individual

Accept an application on behalf of a deceased individual by a relative or other responsible party who can provide sufficient information for an eligibility determination.

Determine eligibility based on circumstances that existed in the month(s) prior to the individual's death that Medicaid coverage is requested.
Refer to xref:2053.adoc[].

NOTE: The months are limited to the application month and three months prior to the application.

=== Application for a Pregnant Woman After Termination of the Pregnancy

Accept an application and determine eligibility if the A/R meets all eligibility requirements.

The eligibility determination is based on actual circumstances of the month of pregnancy termination and any of the three months prior to the application month.
Determine eligibility under Family Medicaid Medically Needy if the BG is over the RSM income level.

NOTE: If eligibility is determined for any month of the pregnancy, eligibility continues through the remainder of the pregnancy and through the 60-day postpartum transition period.
Refer to xref:2184.adoc[].

=== Out-of-State Application

Accept applications from individuals outside the state who express the intent to move to Georgia.
Deny the application because the state residency is not met.
Instruct the applicant to re-apply when s/he moves to Georgia.

=== A/R Moves to Another County While in Application Atatus

Complete the application process in the county in which the application was received if an applicant moves to another county prior to approval or denial of the application.

Transfer an approved case to the appropriate county after eligibility is determined and the A/R is notified of the decision.

=== A/R Is Not a Resident of the County in Which S/he Is Filing an Application

Inform the applicant of his/her right to file the application and explain that the application will be faxed or mailed to the county of residence

*AND*

Inform the applicant of his/her option to take the application to the county of residence him/herself.

The application must be faxed or mailed by the agency the same day if the applicant requests the agency send the application.
The date of application is the date the application was first filed by the applicant in any county.

If the applicant chooses to take the application to the county of residence, the date of application is the date the applicant presents the application to the county of residence.

=== Applicant Mails an Application to a County in Which S/he Is Not a Resident

Forward the application to the county of residence.

The date of application is the date the application is first received by any county in the state.

=== Changes While in Applicant Status

Inform the applicant that s/he is required to report within 10 days any change that occurs during the application process.

Take action on the reported change during the application process, allowing the applicant sufficient time to provide any information and/or verification that may be required.
