= 2955 Service Options Using Resources In A Community Environment (SOURCE)
:chapter-number: 2900
:effective-date: June 2020
:mt: MT-60
:policy-number: 2955
:policy-title: Service Options Using Resources In A Community Environment (SOURCE)
:previous-policy-number: MT 16

include::partial$policy-header.adoc[]

== Requirements

Service Options Using Resources in a Community Environment (SOURCE) is available to elderly and disabled people who meet Supplemental Security Income (SSI) Medicaid eligibility criteria.
Individuals under the age of 65 are eligible if they have a significant disability.

== Basic Considerations

A comprehensive assessment is made by SOURCE staff to identify the participant's needs.
Each participant has a care plan designed based on the need for medical monitoring and assistance with functional tasks.

SOURCE's goal is to link primary medical care with long-term health services in an individual's home or community to avoid preventable hospital or nursing home care or to return to the community from a nursing home or hospital.

SOURCE services include:

* Home Delivered Meals
* Home Delivered Services
* Adult Day Health
* Personal Support Services/Extended Personal Support
* 24 hour Medical Access
* Alternative Living Services
* Emergency Response System

== Procedures

SOURCE is only available in certain areas of the state.
A participant must be a resident of one of the counties served by this program.
To apply for SOURCE, the customer must call the number listed for the program serving their county:

Listed below are the SOURCE providers in Georgia:

[%hardbreaks]
*Albany ARC*
Albany: 229-883-2710
Counties: Baker, Calhoun, Clay, Colquitt, Decatur, Dougherty, Early, Grady, Lee, Miller, Mitchell, Seminole, Terrell, Thomas, Worth

[%hardbreaks]
*Blue Ridge Source/UHS-Pruitt*
Blue Ridge: 706-632-9263 or toll-free 800-632-2101
Atlanta: 770-925-1143 or toll-free 866-864-4325
Counties: Butts, Cherokee, Clayton, Cobb, Douglas, Fannin, Fayette, Gilmer, Gwinnett, Henry, Lumpkin, Murray, Paulding, Pickens, Rockdale, Towns, Union, Walker

[%hardbreaks]
*Columbus Regional Healthcare System*
Columbus: 706-571-1946
Counties: Chattahoochee, Marion, Muscogee

[%hardbreaks]
*Diversified Resources, Inc.*
Waycross: 912-285-0367
Counties: Atkinson, Ben Hill, Berrien, Brantley, Brooks, Camden, Charlton, Clinch, Coffee, Cook, Echols, Glynn, Irwin, Lanier, Lowndes, Pierce, Tift, Turner, Ware, Wilcox

[%hardbreaks]
*Legacy Links, Inc.*
Gainesville: 770-538-2650
Counties: Banks, Dawson, Franklin, Habersham, Hall, Hart, Stephens, White

[%hardbreaks]
*Source Care Management*
Butler: 478-862-5886 or toll-free 888-762-2420
Augusta: 706-737-0705
Macon: 478-741-0782
Counties: Bibb, Bleckley, Burke, Columbia Crawford, Crisp, Dodge, Dooly, Glascock, Greene, Hancock, Harris, Houston, Jefferson, Johnson, Laurens, Lincoln, Macon, Marion, McDuffie, Peach, Pulaski, Quitman, Randolph, Richmond, Schley, Stewart, Sumter, Talbot, Taliaferro, Taylor, Twiggs, Upson, Warren, Washington, Webster, Wilkes, Wilkinson

[%hardbreaks]
*St. Joseph/Candler Health System*
Savannah: 912-819-1520
Counties: Appling, Bacon, Bryan, Bulloch, Candler, Chatham, Effingham, Evans, Jeff Davis, Liberty, Long, McIntosh, Montgomery, Tattnall, Toombs, Wayne

[%hardbreaks]
*Wesley Woods (Atlanta Source)*
Atlanta: 404-728-6555
Counties: DeKalb, Fulton

For more information, please call 404-651-6889.
