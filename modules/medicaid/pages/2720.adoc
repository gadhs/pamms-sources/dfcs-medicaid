= 2720 Continuous Coverage For Pregnant Women
:chapter-number: 2700
:effective-date: December 2022
:mt: MT-68
:policy-number: 2720
:policy-title: Continuous Coverage For Pregnant Women
:previous-policy-number: MT 65

include::partial$policy-header.adoc[]

== Requirements

A pregnant woman, adult or minor, who becomes or would otherwise become ineligible for any Medicaid Class of Assistance (COA) because of a change of an Assistance Unit (AU) or Budget Group (BG) member remains eligible for Medicaid for the remainder of her pregnancy and through the 12- month extended postpartum period.

NOTE: See Section 2184 Pregnant Women for the eligibility criteria for the 12-month extended postpartum period.

== Basic Considerations

Continuous coverage for a pregnant woman applies in the following situations:

* a pregnant woman who becomes ineligible for SSI because of an increase in income or resources
* a pregnant woman who becomes ineligible for any Medicaid COA because of a change such as an increase in net taxable income, sanctioned for failing to cooperate with DCSS, etc.

NOTE: For Women's Health Medicaid if she becomes pregnant and is eligible for Pregnant Woman Medicaid, a CMD to Pregnant Woman Medicaid must be completed.
After her continuous coverage period expires, complete a CMD to Parent/Caretaker with Child(ren) Medicaid.
If ineligible for Parent/Caretaker with Child(ren), CMD back to WHM if she is still eligible.
Her child(ren) will be a deemed newborn(s).
If a PeachCare for Kids_®_ enrollee becomes pregnant, a CMD to Pregnant Woman Medicaid must be completed.

For continuous coverage purposes, an increase in net taxable income includes any one of the following:

* an increase in the AU's or BG's taxable income

* a decrease or loss of MAGI deductions

* a decrease in the number of individuals included in the AU and/or BG per stated tax status

* the addition to the AU and/or BG of an individual with taxable income per stated tax status

* expiration of the MN budget period if the pregnant woman was Medicaid eligible or would have been if her pregnancy was known

* any other change that results in excess net taxable income.

Continuous coverage for a pregnant woman includes reinstatement of Medicaid if a voluntary closure or other termination has occurred, whether or not the pregnancy was known at the time of termination.

A pregnant woman who is approved for EMA is not automatically eligible for the 12- month extended postpartum period.
She may, however, qualify for additional days of EMA *during* the 12-month extended postpartum period if she receives pregnancy-related emergency treatment.
Refer to Section xref:2184.adoc[2184], Pregnant Women.

== Procedures

Use the following procedures to establish continuous coverage eligibility for a pregnant woman:

Step 1:: Determine that the pregnant woman would otherwise be ineligible to continue Medicaid under the current COA because of an increase in AU/BG net taxable income or other change.

[.text-center]
*or*

Determine that a pregnant woman is ineligible for SSI because of an increase in income or resources.
The following sources may be used to verify SSI ineligibility:

* SSI notification letter
* State Data Exchange (SDX)
* other verification from the Social Security Administration
* GAMMIS

Step 2:: Establish that the woman was pregnant during the last month of Medicaid eligibility and that her pregnancy has terminated.

Step 3:: Determine that the pregnant woman met non-financial eligibility requirements during the last month of eligibility for the COA under which Medicaid is being or would be terminated.

Step 4:: Continue Pregnant Woman coverage or approve Pregnant Woman if the pregnant woman is/was not actively receiving Medicaid under another COA.

== Special Considerations

A pregnant woman who is correctly determined Medicaid eligible remains financially eligible from the effective month of approval through the end of the 12-month extended postpartum period, regardless of changes in the BG income.

Pregnant individuals (including Individuals in their postpartum period) can be terminated for the following reasons:

* Voluntary termination
* Moves out of state
* Invalidly enrolled
* Death
+
NOTE: Gaining SSI is not an allowable reason for termination during the 12-month extended postpartum period.

If a pregnant individual is actively enrolled in PeachCare for Kids_®_ at delivery/termination of pregnancy, the individual must remain eligible in PeachCare for Kids_®_ through the last day of the month in which the 12-month extended postpartum period ends regardless of any changes in circumstances that may affect eligibility (aging out, income, household composition, non-payment of premium or becoming Medicaid/SSI eligible).
