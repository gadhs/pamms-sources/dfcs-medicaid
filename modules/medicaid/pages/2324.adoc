= 2324 Lump Sums
:chapter-number: 2300
:effective-date: July 2022
:mt: MT-65
:policy-number: 2324
:policy-title: Lump Sums
:previous-policy-number: MT 57

include::partial$policy-header.adoc[]

== Requirements

Money received in the form of a lump sum that is not expected to recur, i.e., rebates, retroactive or corrective payments for prior months, insurance settlements, federal or state tax refunds.

== Basic Considerations

Lump Sums are counted as income the month of receipt.
Any remainder counts as a resource beginning the month after the month of receipt.

[caption=Exceptions]
NOTE: Unspent RSDI or SSI lump sums are excluded resources for 9 full calendar months after receipt.
Federal and State tax refunds do not count as income, but any remaining amount is counted as a resource in months following the month of receipt.

Interest earned on unspent RSDI or SSI lump sums is not excluded as income.
See xref:2499.adoc[] for exceptions.

Refer to xref:2305.adoc[] if unspent RSDI or SSI lump sums are commingled with other funds.

Refer to xref:2405.adoc[] for instructions on how to treat lottery and gambling lump sum winnings which are received in a single payment.

== Procedures

For all lump sums, verify the amount and date of receipt from the source of the payment.
