= 2010 Confidentiality
:chapter-number: 2000
:effective-date: May 2023
:mt: MT-69
:policy-number: 2010
:policy-title: Confidentiality
:previous-policy-number: MT 57

include::partial$policy-header.adoc[]

== Requirements

Any information related to individual applicants and recipients of programs administered by DFCS is confidential and is governed by regulations, which specifically forbid the release of this information to unauthorized persons or agency representatives.

== Basic Considerations

All case record material is confidential, including the names and addresses of applicants and recipients, as well as the types and amount of benefits provided.

Refer to xref:2011.adoc[] for additional information regarding privacy of health information.

Case record information may be released to the following:

* the applicant/recipient (A/R)
* the personal representative (PR)
* an individual or agency if the A/R signs Form 5459, Authorization of Release of Information

NOTE: The county office may charge for photostatic copies of case record material.

The following information may *not* be released to *anyone*, including the A/R:

* medical or psychiatric information marked _confidential_ if the information in the record could be harmful to the A/R
* information provided by an individual who has requested confidentiality
* confidential information regarding pending criminal prosecution
* information obtained from the IRS

Penalties for release of IRS information include:

* fine not exceeding $5,000 or imprisonment of not more than 5 years, or both, including costs of prosecution for felony disclosure
* dismissal from employment
* discharge from employment upon conviction for such offense
* civil action for damages against the taxpayer brought by that taxpayer in a district court of the United States.

If the A/R requests any of the above information, refer the A/R to the source of the information.

== Procedures

Release the requested information pertinent to service delivery to the following agencies or their representatives without obtaining the A/R's permission:

* the Department of Community Health (DCH)
* individuals directly connected with the administration of the DFCS, Child Support Program, Office of Investigation Services, Department of Behavioral Health and Developmental Disabilities (DBHDD), Division of Aging, Health and Human Services and SSA employees as necessary to assist in establishing or verifying eligibility benefits under Titles II and XVI of the Social Security Act
* individuals directly connected with other federal assistance programs and federally assisted state programs providing assistance on a means tested basis to low-income individuals
* persons directly connected with the administration of the Income and Eligibility Verification System (IEVS)
* employees of the Comptroller General's Office of the United States for an audit examination
* the current address of any fugitive felon or assistance unit (AU) member who has knowledge of the fugitive felon's whereabouts may be given to a law enforcement officer if the name and social security number (SSN) of the felon is known by the officer.
* name(s) and address(es) only of participating AUs to persons directly connected with nutrition education,
* researchers authorized in writing by the agency,
* public officials who request information in writing and the request originated from an inquiry by the AU,
* emergency situations in which the County Director, Regional Director, or Division Director judges that the release is necessary to prevent loss of or risk of life or health upon approval from the State Office.
Notify A/R immediately of this release.

=== Employee Cases

An employee may be eligible for certain public assistance benefits.
Employees who apply for or are receiving benefits as a member of a household must notify their supervisor.
All employees' cases must be handled by a supervisor or supervisor designee.
Each county should develop a plan for limited access to these cases by other staff members and the employee himself.
The Regional Manager must approve the plan for handling employee cases and is to be notified of any employee receiving benefits.

Employees should not handle relatives or personal friend's applications or cases and should notify his/her supervisor when such situations occur.
Employees should not be authorized representatives unless approved by the Regional Manager.

The employee/client must comply with all requirements of the program in which he or she is participating including the prompt and accurate reporting of all changes in income or circumstances.
If an employee takes action on or views the case of another employee, a relative or an individual of personal interest, he may be subject to disciplinary action and prosecution for a second- or third-degree felony.
