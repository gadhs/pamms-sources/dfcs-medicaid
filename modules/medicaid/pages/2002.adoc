= 2002 Income And Eligibility Verification System
:chapter-number: 2000
:effective-date: May 2023
:mt: MT-69
:policy-number: 2002
:policy-title: Income And Eligibility Verification System
:previous-policy-number: MT 57

include::partial$policy-header.adoc[]

== Requirements

The Income and Eligibility Verification System (IEVS) is a federally operated system through which agencies request data, wage and benefit information on program applicants and recipients from other state and federal agencies.

== Basic Considerations

IEVS computer matches are performed by Gateway with the following files from other agencies:

* SSA Beneficiary Earnings Exchange Record (BEER)
* United States Internal Revenue Service (IRS)
* Interstate Files
* SSA Prisoner Verification Inquiry
* SSA Death Verification Inquiry
* SSA Bendex
* SDX

A wage and benefit match is completed to compare the information in Gateway and other computer files.
If a match is found and the information in a computer file differs from the information in Gateway, a system-generated alert is sent to inform the worker of the discrepancy.
The worker takes action to resolve the discrepancy and documents those actions in the A/R's case notes.
Refer to Gateway Documentation Standards for documentation requirements.

The SSA Prisoner Verification Inquiry and SSA Death Verification Inquiry match the files of the Social Security Administration with Gateway files to determine if A/Rs are incarcerated or deceased.

NOTE: Information received by the IRS and BEERS systems is no longer received by the Medicaid program and is not updated in the Gateway system for Medicaid purposes.
This information is no longer subject to the special security considerations.
Refer to xref:2003.adoc[Section 2003], IRS/BEERS Security for more information.

== Procedures

=== Processing Match Data

Follow the procedures below to process data received from the computer matches.
Use information received from the matches to determine eligibility and benefit level.

Complete case actions to resolve discrepancies within 45 days of receipt of the information.

Completion of case actions may be postponed to the next review if the actions cannot be completed due to non-receipt of verification already requested from a collateral contact.

=== Verifications

Consider the following information as a lead and verify the income information when a match is received:

* RSDI benefits,
+
NOTE: Verify gross RSDI income on Clearinghouse.
* DOL earned income matches
* IRS earned income and pension matches
* questionable IEVS information
* prisoner verification data.

=== Documentation

Record the following information:

* results of the case record screening
* the reason a discrepancy does not exist, if applicable
* date verification was requested and from whom verification was requested
* date action has been taken to correct ongoing benefits
* date of completion of the case action to resolve the discrepancy
