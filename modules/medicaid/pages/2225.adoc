= 2225 Residency
:chapter-number: 2200
:effective-date: February 2020
:mt: MT-58
:policy-number: 2225
:policy-title: Residency
:previous-policy-number: MT 47

include::partial$policy-header.adoc[]

== Requirements

The A/R member(s) must be a resident of Georgia in order to be eligible for Medicaid.

== Basic Considerations

The A/R must live or intend to live in Georgia indefinitely.

There is no specific durational requirement but the A/R may not be in Georgia for a visit.

The A/R does not have to live in Georgia on the first day of the month or live in Georgia for any certain number of days during the month in order to be considered a Georgia resident.

An A/R may still be considered a resident of Georgia if s/he is temporarily out of state and intends to return to Georgia once the purpose of the absence has been accomplished.
However, if the A/R is receiving Medicaid benefits from another state, s/he is no longer considered a resident of Georgia, and Georgia Medicaid benefits should be terminated until/unless A/R returns.

If the A/R has been out of the country for any period of time, s/he can begin receiving Medicaid benefits immediately upon return if all eligibility criteria are met.

If an A/R receives a Medicaid card in another state for a particular month and applies for Medicaid in Georgia later in the same month, the A/R is considered a Georgia resident that same month if s/he intends to remain in Georgia indefinitely.
S/he is potentially eligible for Medicaid in Georgia for that month.

The place of residence need not be a fixed dwelling.

== Procedures

Use the following guidelines to determine the state of residence:

For MAGI Family Medicaid COAs, a tax dependent is considered to be a resident of the state in which the tax filer expects to file their tax return.

For Non MAGI Family Medicaid COAs, a child is considered to be a resident of the state in which the parent or caretaker of the child resides.
This includes MAGI Family Medicaid COAs that do not expect to file a tax return (non tax filer budget groups).

For ABD Medicaid COAs, a child applicant who is not in LA-D is considered to be a resident of the state in which s/he lives.

If a child applicant is in LA-D, base the child's residency on one of the following:

* the state in which the parent(s) or guardian lives at the time of placement
+
*OR*

* the current state of residence of the parent(s) or guardian if the child resides in LA-D in the same state
+
*OR*

* the residence of the person who makes the application for the child if the child is abandoned (without a guardian) and lives in the same state.

NOTE: Use an Interstate Residency Agreement (IRA) if possible to waive residency when it is determined that a LA-D child is a resident of a state other than Georgia.
Refer to <<special-considerations>> in this section.

Consider an adult applicant who is mentally capable to be a resident of the state, in which s/he lives and intends to remain indefinitely.

=== Adult Applicant who Became Mentally Incapable After Age 18

If an adult applicant became mentally incapable after age 18, consider the adult to be a resident of the state in which s/he is physically present.

=== Adult Applicant who Became Mentally Incapable Before Age 18

If an adult applicant who became mentally incapable before age 18 is *not* in LA-D, consider the adult to be a resident of the state in which s/he lives.

If an adult applicant who became mentally incapable before age 18 is in LA-D, use the rules for a LA-D child applicant to determine the state of residency, including use of an IRA.

NOTE: An applicant is never a Georgia resident if s/he is placed in a Georgia institution by an out-of-state state agency.

== Verification

Establish Georgia residency at initial application.

Accept and document A/R's statement of residency unless information known to the agency conflicts with the A/R's statement.

Verify when questionable with one of the following:

* lease, rent or utility company receipts
* school records
* written statement of responsible reference
* any other document proving residency

Document the A/R's statement of residency and the source of verification , if required.

For ABD Medicaid, if the A/R is mentally incapable of stating residency, verify and document mental incapability using any of the following:

* personal observation
* documentation on Form DMA-6
* a statement from a physician
* legal documentation of incompetency

For ABD Medicaid, also document a mentally capable adult A/R's statement of intent to remain in Georgia indefinitely.

[#special-considerations]
== Special Considerations

=== Interstate Residency Agreement (IRA) For ABD

Waive the Georgia residency requirement if the A/R who is determined to be a resident of another state meets the following conditions:

* the applicant is placed in LA-D in Georgia
+
AND

* the applicant is under age 18
+
OR

* the applicant is age 18 or older but became mentally incapable prior to age 18
+
AND

* Georgia has an IRA with the individual's state of residence.
** Georgia has an IRA with the following states:
WV AL CA MS FL MD LA TN MN KY NM NJ OH PA WI NC* NY

NOTE: Some of the other states with which Georgia has an IRA make the effective dates retroactive.
Consequently, some individuals who applied for Medicaid who were determined ineligible due to residency may now be eligible for that period.

*The agreement with North Carolina covers only incapable individuals who reside in non-border institutions.
Incapable individuals who enter a border institution which serves communities in both Georgia and North Carolina are the responsibility of the state in which they are determined to be a resident.
A border institution is one located within forty miles of the Georgia/North Carolina state line.
