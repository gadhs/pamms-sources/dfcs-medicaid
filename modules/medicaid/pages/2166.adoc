= 2166 Transitional Medical Assistance (TMA)
:chapter-number: 2100
:effective-date: September 2024
:mt: MT-73
:policy-number: 2166
:policy-title: Transitional Medical Assistance (TMA)
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

Transitional Medical Assistance (TMA) provides continued Medicaid coverage for up to 12 months for Parent/Caretaker with Child(ren) AUs that become ineligible because of changes related to earned income.

== Basic Considerations

To be eligible for continued Medicaid coverage under TMA, the AU must have correctly received Parent/Caretaker with Child(ren) in three of the six months preceding the first month of Parent/Caretaker with Child(ren) ineligibility.
An AU is potentially eligible to receive TMA for 12 months beginning with the first month following the last month of Parent/Caretaker with Child(ren) Medicaid.

To be eligible for continued Medicaid coverage under TMA, Medicaid ineligibility must result exclusively from new or increased Modified Adjusted Gross Income (MAGI) earnings of an adult AU member or child AU Member with earnings above the allowable IRS dependent exemption, referred herein as a "TMA-qualifying event."

TMA-qualifying event includes any of the following:

* new employment
* increase in earnings as a result of an increase in hours worked
* increase in salary or hourly wage
* earnings of an eligible member added to the AU
* decrease in pre-tax or 1040 deductions

NOTE: Ineligibility may be caused by new or increased earnings *and* a *concurrent* change.
If the concurrent change *alone* caused ineligibility, the AU is *ineligible* for TMA.

Cooperation with Third Party Liability (TPL) is required at approval for TMA as well as during both 6-month review periods.
Refer to xref:2230.adoc[Section 2230 - Third Party Liability].

Referral to Child Support Services is not required.

Effective 5/17/2024, the first month of Parent/Caretaker ineligibility and the first month of the TMA period will **No Longer Be The Same Month**. The first month of Parent/Caretaker ineligibility is based on when the AU's income actually exceeds Parent/Caretaker FPL. The first month of TMA period is the month after the case worker (CW) takes action based on the change or renewal (including administrative renewals) that an AU member has experienced a TMA-qualifying event and the expiration of timely notice.

The AU still must report a TMA-qualifying event within 10 days of the change.
If the AU fails to report the change within 10 days, the CW will no longer be required to look back to determine the effective date of change for TMA. Rather, the effective date now will only be prospective (after expiration of timely notice).

Any individual who moves into the home during the TMA eligibility period is ineligible for TMA, however s/he may qualify for another Medical Assistance COA.

[caption=Exception]
NOTE: If the individual was previously a member of the TMA AU, the individual may be added.

=== Eligibility Period

The TMA period of eligibility consists of the following:

* the initial 6-month extension
* an additional 6-month extension

Each of the 6-month periods has specific and distinct eligibility requirements.

=== Financial Eligibility

There is no MAGI income requirement for the first 6 months of TMA.
To remain eligible for the second 6 months of TMA, the AU's MAGI income must be below 205% of the Federal Poverty Level (FPL).

There is no resource requirement for TMA.

=== Reporting

To remain eligible for TMA, the AU must report MAGI income on a quarterly basis.
The xref:attachment$form-328.docx[Form 328 - Quarterly Report Form (QRF)] is mailed by the agency to the AU.

NOTE: All MAGI income reported on the QRF must be verified.
Refer to xref:2051.adoc[Section 2051 - Verification].

[.text-center]
=== Summary of TMA Quarterly Reporting Requirements
[format="csv"]
[options="header", cols="^.^,^.^,^.^"]
|==========================================
Quarterly Report,Reporting Period,Due Date

1^st^ Quarterly Report, Months 1-3 of the initial TMA period, 21^st^ day of Month 4

2^nd^ Quarterly Report, Months 4-6 of the initial TMA period, 21^st^ day of Month 7

3^rd^ Quarterly Report, Months 7-9 of the second TMA period, 21^st^ day of Month 10
|==========================================
[.text-left]
During the initial 6-month TMA eligibility period, if the AU does not comply with QRF reporting requirements for the QRF due in the 4^th^ month, TMA eligibility terminates effective the first month after the initial extension (7^th^ month).
[.text-left]
If the AU does not comply with QRF requirements for the QRF due in the 7^th^ month, eligibility terminates effective the 8^th^ month.
[.text-left]
If the AU does not comply with QRF requirements for the QRF due in the 10^th^ month of TMA, eligibility terminates effective the 11^th^ month.

=== Initial Six Months Extension

To be eligible to *begin* the *initial* six months of TMA, the AU must meet *ALL* of the following requirements:

* must be financially ineligible for Medicaid based exclusively from new or increased Modified Adjusted Gross Income (MAGI)
* earnings of an AU member must have correctly received Medicaid during three of the six months preceding the first month of Medicaid ineligibility
* must include a child under 19 years of age

=== Additional Six Months Extension

To be eligible to *begin* the *additional* six-month extension the AU must meet *ALL* of the following requirements:

* must have received TMA for each month of the initial six-month extension
* must have met the QRF reporting requirement in the 4th month of TMA
* must include a child under 19 years of age

To *remain* TMA eligible for the additional six-month extension, the AU must meet *ALL* of the following requirements:

* must comply with the 7^th^ and 10^th^ month QRF reporting requirements by the 5^th^ day of the 7^th^ and 10^th^ months of TMA
* must meet TMA income eligibility requirements
* must include the caretaker or other eligible adult who was employed for at least part of each of the months included in the 7^th^ and 10^th^ months QRFs.
+
NOTE: Any eligible member in the AU can meet the employment criteria, even if s/he was not employed when the AU became eligible for TMA.
Employment, for TMA purposes, is defined as working during the month.
Receipt of a remaining paycheck from previous employment does not meet this criterion.
* must include a child under 19 years of age

[caption=Exception]
NOTE: If the only child in the TMA AU becomes eligible for SSI, the other AU members may continue to receive TMA until the child is 19 years of age, or until the end of the TMA period if all of the above is met, whichever occurs first.

NOTE: If the income in the 7^th^ or 10^th^ month QRF exceeds the TMA FPL limit but is less than the PCK FPL limit, then the child will remain TMA eligible and **NOT** cascade to PCK due to continuous eligibility.  However, if the income in the 7^th^ or 10^th^ month QRF exceeds the TMA and PCK FPL limits, then the child would terminate from TMA and not cascade to another COA.

Good Cause may be claimed for unemployment during one or more of the specified months.
Refer to Special Considerations in this section.

If the TMA case is terminated because of unemployment of the caretaker or other eligible adult, without Good Cause, TMA cannot be reinstated, even if employment is subsequently obtained.

== Procedures

=== Initial Six-Month Extension

Follow the steps below to establish the initial six-month TMA extension.
[horizontal,labelwidth=10]
Step 1:: Establish that the AU is financially ineligible for Medicaid based exclusively from a TMA-qualifying event.
All income must be verified.
Refer to xref:2051.adoc[Section 2051 - Verification].

Step 2:: Establish that the AU correctly received Medicaid in three of the six months preceding the first month of Medicaid ineligibility.
Refer to xref:2162.adoc[Section 2162 - Parent/Caretaker with Child(ren) Medicaid].

Step 3:: Determine the last month of Parent/Caretaker with Child(ren) Medicaid, based on the date of the change, date the caseworker took action and the expiration of timely notice.
All MAGI income must be verified.
Refer to xref:2051.adoc[Section 2051 - Verification].

Step 4:: Notify the AU of the following:
** termination of Parent/Caretaker with Child(ren) Medicaid eligibility
** approval of the initial six months of TMA
** reporting requirements of continued TMA eligibility

Step 5:: Mail xref:attachment$form-328.docx[Form 328 - Quarterly Report Form (QRF)] by the 15^th^ of the third month of TMA.
The QRF must request the AU's actual MAGI income for the first three months of TMA.
Refer to xref:2051.adoc[Section 2051 - Verification].

Step 6:: Use <<chart-2166-1,Chart 2166.1, Processing QRF Due in the Fourth Month of TMA>>, to process the QRF or information received, or to determine the appropriate action to be taken if QRF reporting requirements are not met.

NOTE: All MAGI income reported on the QRF must be verified.
Refer to xref:2051.adoc[Section 2051 - Verification].

=== Additional Six-Month Extension

Follow the steps below to continue eligibility for the additional six-month extension of TMA.
[horizontal, labelwidth=10]
Step 1:: Mail xref:attachment$form-328.docx[Form 328 - Quarterly Report Form (QRF)] by the 15^th^ day of the sixth month of TMA if the recipient complied with fourth month reporting and received all six months of TMA during the initial extension.
Request MAGI income for the fourth, fifth and sixth months of TMA.
+
NOTE: All MAGI income reported on the QRF must be verified.
Refer to xref:2051.adoc[Section 2051 - Verification].
The QRF is due by the 5^th^ day of the seventh month.

Step 2:: Complete TMA budgeting procedures in the seventh month after the QRF is returned by the AU.
Refer to xref:2667.adoc[Section 2667 - Transitional Medical Assistance Budgeting] and <<chart-2166-2,Chart 2166-2, TMA QRF Processing Procedures for the Seventh and Tenth Months>>.
+
If the AU remains eligible based on the TMA budget, continue TMA.
+
If the AU is TMA ineligible because of MAGI income reported on the QRF, or if ineligible for any other reason, complete a Continuing Medicaid Determination (CMD) prior to termination of TMA and notify the AU.
Refer to xref:2052.adoc[Section 2052 - Continuing Medicaid Determination].

Step 3:: Mail xref:attachment$form-328.docx[Form 328 - Quarterly Report Form (QRF)] by the 15^th^ day of the ninth month of TMA if the recipient complied with the seventh month QRF.
Request MAGI income for the seventh, eighth and ninth months of TMA.
Refer to xref:2051.adoc[Section 2051 - Verification].
+
NOTE: All MAGI income reported on the QRF must be verified.
Refer to xref:2051.adoc[Section 2051 - Verification].
The QRF is due by the 5^th^ day of the tenth month.

Step 4:: Complete a TMA budget in the tenth month after the QRF is returned by the AU.
Refer to xref:2667.adoc[Section 2667 - Transitional Medical Assistance Budgeting] and <<chart-2166-2,Chart 2166.2 - TMA QRF Processing for the Seventh and Tenth Months>>.
+
If the AU remains eligible based on the TMA budget, continue TMA.
+
If the AU is TMA ineligible because of MAGI income reported on the QRF, or if ineligible for any other reason, complete a CMD prior to termination of TMA and notify the AU.
Refer to xref:2052.adoc[Section 2052 - Continuing Medicaid Determination].

Step 5:: Complete a CMD during the 12th (final) month of TMA eligibility and notify the AU prior to termination of TMA.
Refer to xref:2052.adoc[Section 2052 - Continuing Medicaid Determination].

[#tma-special-considerations]
== TMA Special Considerations

=== Procedures to Determine if a QRF is Complete

Use the following criteria to determine if a QRF is complete:

* The QRF is signed by the recipient and dated on or after the last day of the last month for which information is being reported.
* All items (except Question No. 3) are completed.
All *yes/no* blocks are checked.
+
NOTE: Question No. 3 is used only for CMD purposes if the family is ineligible for TMA.
Refer to xref:2052.adoc[Section 2052 - Continuing Medicaid Determination].

All MAGI income reported on the QRF, due in the fourth, seventh and tenth months, must be verified.
Refer to xref:2051.adoc[Section 2051 - Verification].

If the QRF is incomplete, request the information in writing.
The A/R is not required to send back the actual QRF form.
A written statement of the total MAGI income for the months listed on the QRF along with the appropriate verification is sufficient.
Refer to xref:2051.adoc[Section 2051 - Verification].

=== Procedures to Determine Good Cause for Failure to Meet Work Requirements

Use the following information to determine if Good Cause exists for failure to meet the work requirements because of the termination of employment of a caretaker or other eligible adult:

* Explore the reason for termination of employment with the A/R.
* Use the following list as examples in determining if Good Cause exists.
This list is not inclusive.
** involuntary loss of employment, e.g., layoff
** illness of the recipient or an immediate family member
** family emergency
** childcare not available
** transportation not available

NOTE: If Good Cause exists, the reporting requirement is met.
Obtain the information needed to determine continued eligibility.

Document the case decision.

=== Procedures to Determine Good Cause for Failure to Comply with QRF Requirements

Good Cause for untimely or incomplete submission of QRF or QRF information may be granted.

The following are examples of Good Cause.
This list is not all inclusive:

* The recipient did not receive the QRF or received it untimely.
* The recipient or an immediate family member was ill or in the hospital.
* The recipient is illiterate.
* There was a serious family crisis such as death.
* There was a natural disaster.
* The recipient was out of town.
* The return envelope was postmarked in time to reach the county department but did not.
The QRF is considered timely if postmarked at least one day prior to the deadline.
* The AU was ineligible for TMA when the report was due but the reason for ineligibility no longer exists.
This is applicable only to AUs who were ineligible for TMA because of any of the following reasons:
** the AU moved out of state
** the only child cease to live with the family
** the individual who qualified the AU for TMA ceased to live with AU.

NOTE: If Good Cause exists, the reporting requirement is met.
Obtain the information needed to determine continued eligibility.

Document the decision.

[#chart-2166-1]


Use the following chart to process the QRFs due in the fourth month of TMA.

**CHART 2166.1 - PROCESSING QRF DUE IN FOURTH MONTH OF TMA**
|===


^|IF ^|THEN
.^| The completed QRF or QRF information is received by the 5^th^ calendar day of the report month .^| Begin the additional six-month extension of TMA in the 7^th^ month.
.^|The QRF or QRF information is not received by the 5^th^ calendar day of the 4^th^ month of TMA (or by the following workday if the 5^th^ is a weekend or holiday) .^| Send TMA Quarterly Report Follow-up Notice (GA Gateway sends this automatically), giving the AU until the 21^st^ to provide the completed QRF or QRF information.
.^| The completed QRF or QRF information is received by the 21^st^  .^| Begin the additional six-month extension of TMA in the 7^th^ month.
.^| The QRF or QRF information is not received by the 21^st^  .^| Determine if Good Cause exists.  Refer to <<tma-special-considerations>> in this Section.
.^| The completed QRF or QRF information is not received by the 21^st^ and Good Cause does not exist .^| Complete a CMD and terminate TMA effective the 7^th^ month of eligibility.  Provide adequate notice.  Refer to xref:2052.adoc[Section 2052 - CMD].
.^| The QRF or QRF information is received by the 21^st^ but is not complete. .^| Send a verification checklist requesting the missing information within 5 calendar days.  Allow an additional 10 days or until the 21^st^, whichever is later, to provide the information.  Refer to xref:2051.adoc[Section 2051 -  Verification].
.^| The completed QRF or QRF information is received by the second deadline. .^| Begin the additional six-month extension of TMA in the 7^th^ month.
.^| The QRF or QRF information is not received by the second deadline. .^| Complete a CMD and terminate TMA effective the 7^th^ month of eligibility.  Allow adequate notice.  Refer to xref:2052.adoc[Section 2052 - CMD].
.^| The QRF or QRF information is received by the second deadline but is not complete. .^a| Send another verification checklist.  Allow an additional 10 days for response.

•	If the completed QRF or QRF information is received by the extended deadline, the report requirement is met.

•	If the completed QRF or QRF information is not received by the extended deadline, complete a CMD and terminate TMA effective the 7^th^ month.  Allow adequate notice.  Refer to xref:2052.adoc[Section 2052 - CMD].
|===

[#chart-2166-2]

Use the following chart to process the QRFs due in the seventh and tenth months of TMA.

**CHART 2166.2 - TMA QRF PROCESSING PROCEDURES FOR THE SEVENTH AND TENTH MONTHS**
|===
^|IF ^| THEN

.^| The completed QRF or QRF information is received by the 5^th^ calendar day of the report month
.^| Continue TMA eligibility.

.^| The QRF or QRF information is not received by the 5^th^ calendar day of the report month (or by the following workday if the 5^th^ is a weekend or holiday
.^| Send TMA Quarterly Report Follow-up Notice (GA Gateway does this automatically), giving the AU until the 21^st^ to provide the completed QRF or QRF information.

.^| The completed QRF or QRF information is received by the 21^st^
.^| Continue TMA eligibility.

.^| The QRF or QRF information is not received by the 21^st^
.^| Determine if Good Cause exists.
Refer to <<tma-special-considerations>> in this Section.

.^| The completed QRF or QRF information is not received by the 21^st^ and Good Cause does not exist
.^| Complete a CMD and terminate TMA effective the 8^th^ or 11^th^ month of TMA.
Provide adequate notice.
Refer to xref:2052.adoc[Section 2052 - Continuing Medicaid Determination].

.^| The QRF or QRF information is received by the 21^st^ but is not complete.
.^| Send a verification checklist requesting the missing information.
Allow an additional 10 days or until the 21^st^, whichever is later, for the AU to provide the information.
Refer to xref:2051.adoc[Section 2051 - Verification].

.^| The completed QRF or QRF information is received by the second deadline.
.^| Continue TMA eligibility.

.^| The QRF or QRF information is not received by the second deadline.
.^| Complete a CMD and terminate TMA effective the 8^th^ or 11^th^ month.
Provide adequate notice.
Refer to xref:2052.adoc[Section 2052 - Continuing Medicaid Determination].

.^| The QRF or QRF information is received by the second deadline but is not complete.
.^a| Send another verification checklist.
Allow an additional 10 days for response.

* If the completed QRF is received by the extended deadline, the reporting requirement is met.
Continue TMA eligibility.
* If the completed QRF is not received by the extended deadline, complete a CMD and terminate TMA effective the 8^th^ or 11^th^ month.
Provide adequate notice.
Refer to xref:2052.adoc[Section 2052 - Continuing Medicaid Determination].
|===

NOTE: Refer to <<tma-special-considerations>> for information on determining if a QRF is complete and determining good cause for not complying with reporting requirements.
