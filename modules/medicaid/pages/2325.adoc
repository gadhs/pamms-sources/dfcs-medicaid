= 2325 Patient Fund Account (NH)
:chapter-number: 2300
:effective-date: July 2022
:mt: MT-65
:policy-number: 2325
:policy-title: Patient Fund Account (NH)
:previous-policy-number: MT 59

include::partial$policy-header.adoc[]

== Requirements

A nursing home patient fund account is treated as any other financial account.
The balance of the account as of the first moment of the first day of the month is a countable resource.
This is not applicable to Family Medicaid with the exception of a Parent/Caretaker with Child(ren) individual in a nursing home.

== Basic Considerations

A nursing home patient fund account is a bank account set up by the nursing home for the convenience of the patient.

A nursing home holding a patient fund account with a balance of $50 or less is not required to pay interest on the account.
Individual patient fund accounts may not be pooled with other resident's patient fund accounts.
Any interest earned on the account belongs to the resident.

Refer to Interest in xref:2499.adoc#chart-2499-1[Section 2499, Chart 2499.1, Treatment of Income in ABD Medicaid], for the income treatment of any interest earned on a patient fund account containing more than $50.

== Procedures

On every application and at redetermination (except for SSI A/Rs), verify first moment of the first day of month balance by telephone, printed patient fund account statement, or xref:attachment$form-958.docx[Form 958] whether a NH A/R has an account and the account balance and interest earned as of the first moment of the first day of the month of application or review.
