= 2947 Healthcare Facility Regulation (DCH)
:chapter-number: 2900
:effective-date: November 2023
:mt: MT-71
:policy-number: 2947
:policy-title: Healthcare Facility Regulation (DCH)
:previous-policy-number: MT 60

include::partial$policy-header.adoc[]

== Requirements

The Healthcare Facility Regulation Division (HFRD), a division of the Department of Community Health (DCH), is responsible for health care planning, licensing, certification and oversight of various health care facilities and services in Georgia.
This is accomplished through periodic inspection and the investigation of complaints.
The division was created in 2009 as a result of the passage of two laws, HB 228 (2009) and SB 433 (2008).
These laws transferred functions previously performed by the Office of Regulatory Services to DCH in 2009.
It works to ensure that facilities and programs operate at acceptable levels, as mandated either by the state statutes and rules and regulations adopted by the Board of Human Resources.
HFRD also certifies various health care facilities to receive Medicaid and Medicare funds through contracts with the Center for Medicare and Medicaid Services of the U.S. Department of Health and Human Services.

== Basic Considerations

=== Health Care Facilities

HFRD regulates both long-term and primary care facilities.
Long-term care facilities include skilled nursing homes and intermediate care nursing homes and personal care homes.
Primary care facilities and programs include general and specialized hospitals, clinical laboratories, home health agencies, rehabilitation centers, end-stage renal centers, drug abuse treatment facilities, hospices, ambulatory surgical treatment centers, x-ray machines, and several other types of facilities such as rural health clinics.
Many of the regulated health care facilities are certified by HFRD for reimbursement under the Medicare and Medicaid programs.

More detailed information about specific programs, including a link to applicable rules and regulations can be found at https://dch.georgia.gov/divisionsoffices/hfrd.
A listing of all regulated providers is also available at that site.

Complaints about regulated programs can be submitted by telephone, online or in writing.
Complaints regarding health care facilities can be filed by calling 404-657-5726 or 1-800-878-6442.
Written complaints for all programs can be sent to:

[%hardbreaks]
Healthcare Facility Regulation Division
2 Martin Luther King Jr. Drive SE
East Tower, 17^th^ Floor
Atlanta, Georgia 30334
