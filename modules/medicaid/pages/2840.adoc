= 2840 IV-E Budgeting
:chapter-number: 2800
:effective-date: September 2024
:mt: MT-73
:policy-number: 2840
:policy-title: IV-E Budgeting
:previous-policy-number: MT 62

include::partial$policy-header.adoc[]

== Requirements

IV-E Budgeting procedures are used as the financial eligibility step when determining IV-E Foster Care reimbursability at renewal.

== Basic Considerations

This budget is used at the six-month review of IV-E Foster Care reimbursability.
The AU size used in IV-E budgeting is always one, as the child is in placement.

A unique payment standard is used at IV-E redeterminations.
AFDC policy is used in determining net countable income.

Reference xref:2860.adoc[Section 2860 - IV-E Reimbursability] for additional information.

== Procedures

=== IV-E Budget

SHINES calculates the IV-E reimbursability budget based on existing data in the child's case.
The steps below determine the financial eligibility for an IV-E child at renewal for reimbursability.

Apply all of the child's countable resources to the $10,000 resource limit.
If the child is under the resource limit, proceed to net countable income.
If the child is over the resource limit, the child is non-reimbursable for IV-E.
Complete the re-rate process to change the case from IV- E to IV-B funding.

Allow the $50.00 child support disregard in determining the child's net countable income.

When siblings or half siblings receive child support from the same absent parent, each child is entitled to the $50.00 child support disregard in his/her own AU.

NOTE: Exclude SSI benefits received by a IV-E child in determining eligibility.

Apply all net countable income of the IV-E child to the Foster Care Payment Standard.
Reference xref:2499.adoc[Section 2499 - Treatment of Income in Medical Assistance].

If the child's net income is less than the Foster Care Payment Standard, approve the child for IV-E Foster Care reimbursability.
Report net countable income to the Social Services Case Manager (SSCM).

If the net income is greater than the Foster Care Payment Standard, complete an Eligibility Summary Page indicating nonreimbursability and complete the re-rate process with regional accounting.
Report reimbursability determination to the SSCM.

Use the chart below for determining the appropriate Foster Care Payment Standard for budgeting based on the child's age at redetermination.

== Processing

The IV-E budget is calculated by SHINES as a function of the redetermination and reimbursability of each case.

[cols=4*]
|===
.4+h| FOSTER CARE PAYMENT STANDARD Effective 7/1/2022
h| Child's Age
h| Daily Rate X 30
h| SON

| Birth through 5
| $27.80 X 30 =
| $834.00

| 6 through 12
| $29.99 X 30 =
| $899.70

| 13 and older
| $32.62 X 30 =
| $978.60
|===
