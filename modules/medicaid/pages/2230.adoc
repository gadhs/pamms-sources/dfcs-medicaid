= 2230 Third Party Liability
:chapter-number: 2200
:effective-date: September 2024
:mt: MT-73
:policy-number: 2230
:policy-title: Third Party Liability
:previous-policy-number: MT 71

include::partial$policy-header.adoc[]

== Requirements

Medical Assistance applicants and recipients (A/Rs) are required to provide information regarding third party liability (TPLs) available to AU members.
Rights to TPLs must be assigned to the Department of Community Health (DCH).

== Basic Considerations

TPLs are medical benefits and include, but are not limited to, the following:

* TriCare (formerly known as CHAMPUS) - active-duty insurance
* TriCare (formerly known as CHAMPVA) or TriCare for Life for veterans enrolled in Medicare Part A & Part B - disabled veteran insurance
* Court ordered payments of medical costs by a non-custodial parent (NCP)
* Court awards or trusts which provide for payment of medical expenses
* Commercial Health insurance policies (including a NCP's policy)
** private
** indemnity
** group
** liability
** Long Term Care
** Medicare Supplement Plans
** Managed Care Plans (i.e., HMO, PPO, etc.)
* Health Reimbursement Accounts
* Medicare (does not have to be reported as a TPL)
* Worker's Compensation
* Any trust
* Any legal document that specifies monies are due to the State (including lawsuit settlements, workers' compensation benefits, etc.)

Casualty Unit Contact numbers: Phone: 678-564-1163; Fax: 855-467-3970 or email gacasualty@gainwelltechnologies.com

Payments from TPLs are assigned to DCH when the A/R signs the application/renewal forms or the DMA 285 form for Medicaid.

=== Form DMA 285

If a TPL exists a Form DMA 285, Health Insurance Information Questionnaire, must be signed and placed in the case record or scanned into DIS.
A copy of the front and back of the Medical Insurance card, if available, should also be scanned into DIS.
The reported TPL must be entered into Gateway on corresponding page as this is electronically transmitted to HMS.
Do not submit Form DMA 285 if Medicare is the only TPL.

Form DMA 285 is not needed if the AU reported their TPL while using one of the following:

* Form 94A Medicaid Streamlined application
* Federal Single Streamlined application
* Gateway Online Medicaid application or renewal

NOTE: The pre-paid dental services do not need a DMA 285 completed.
For example: Onsite Dental Services does not require a DMA 285.

If an AU has no TPL, completion of the DMA 285 is *not* required if application for Medicaid is made with the following forms that include the assignment of TPL rights:

* Form 94 (rev. 12/03 or later)
* Form 508
* Form 297M (rev. 11/07 or later) This must be completed with the Form 297 *(Obsolete as of 12/2021)*
* Form 700 (rev. 11/07 or later)
* Gateway Online Medicaid Application and Renewal
* PeachCare for Kids® Application *(Obsolete as of 09/2017)*
* Form 94A Medicaid Streamlined Application
* Form 632 Presumptive Eligible (PE) Pregnancy Medicaid (rev. 4/1/10 or later)
* Form 632W Presumptive Eligible (PE) Women's Health Medicaid (rev. 1/1/13 or later)

NOTE: Form DMA 285 can be accessed by the below link: https://www.mmis.georgia.gov/portal/Portals/0/StaticContent/Public/ALL/FORMS/10232006_285_Rev-Jan06_RVSD%2013-02-2012%20212123.pdf

The assignment of TPL rights must be done at each application and renewal.
If a renewal is completed by phone, a renewal form must be sent to the A/R for signature, and the form must be returned to prevent penalization/loss of coverage.

For MAGI Family Medicaid COAs a separate DMA 285 is not required if the Form 94A Medicaid streamlined application, or pre-populated renewal (web services) has the TPL information included, and the form is signed.

For Q-Track applications, a DMA 285 is not required to be completed even when the AU has TPL.
A copy of the application may be submitted in lieu of the DMA 285 with a copy of the insurance card attached, if possible.

Mail, fax, or email Form DMA 285 to:

[%hardbreaks]
Health Management Systems
100 Crescent Centre Pkwy, Suite 1000
Tucker, Georgia 30084
Phone 678-564-1162, Press 3
Email: gatpl@gainwelltechnologies.com

or

[%hardbreaks]
fax # 770-937-0180


NOTE: It is not necessary to mail or fax Form DMA 285 if the Case Worker was able to input the required information on the assigned Gateway page.
Prior to sending form DMA 285 to HMS please consult with your local Medicaid Field Program Specialist.

=== Trusts and Other Legal Documents

Any trust, such as a Special Needs Trust, Qualified Income Trust (QIT), Pooled Trust or other similar legal document is considered a TPL and is to be reported to DCH.
Annotate Form 285 to indicate there is a trust document.
Attach a copy of the trust or legal document and mail or email to the TPL Trust Unit.

If mailed, send the trust document (including QITs that adhere to a DCH approved format along with the QIT Certification form) *and* Form DMA 285, Health Insurance Information Questionnaire, to:

[%hardbreaks]
Georgia Department of Community Health
ATTN: Trust Unit
100 Crescent Centre Pkwy, Suite 1000
Tucker, GA 30084
Email: gatrustunit@gainwelltechnologies.com
Fax: 678-564-1169

Mail QITs that do not adhere to one of the DCH approved formats to:

[%hardbreaks]
Georgia Department of Community Health
ATTN: Trust Unit
100 Crescent Centre Pkwy, Suite 1000
Tucker, GA 30084
Email: gatrustunit@gainwelltechnologies.com
Fax: 678-564-1169

NOTE: The Georgia Trust Unit is paperless and prefers all documents and information be emailed. For questions, email using the above address or call 678-564-1168.

Include xref:attachment$form-947.docx[Form 947 - QIT Approved Format Deviation Form], found in xref:appendix-f/index.adoc[Appendix F - TOC]; explaining how the trust differs from the DCH approved QIT format and proof that a QIT account has been opened.

When the A/R with a trust/QIT dies or becomes ineligible, send the TPL Unit a copy of the original DMA 285.
Annotate in red at the top of the form that the A/R is deceased/ineligible, the date of death/ineligibility, and whether the TPL is a trust or QIT.

[%hardbreaks]
*Qualified Income Trust*
Fax: 678-564-1169
QIT email: gatrustunit@gainwelltechnologies.com

[%hardbreaks]
*Special Needs Trusts*
Contact numbers: 678-564-1168;
Fax: 678-564-1169
Special Needs Trust email: gatrustunit@gainwelltechnologies.com

=== ABD and SSI Medicaid TPL Requirements

ABD and SSI Medicaid recipients who refuse to cooperate with the TPL process, are ineligible for Medicaid effective the month following the expiration of timely notice.
ABD and SSI applicants who refuse to assign TPLs during the SSI application process are ineligible for Medicaid until TPL is assigned at the county DFCS office.

=== Family Medicaid TPL Requirements

Family Medicaid A/Rs must provide information regarding a TPL held by a non-custodial parent unless good cause is asserted and upheld.
Good cause for refusing to cooperate is based on Division of Child Support Services good cause and non-cooperation standards.

Complete xref:attachment$form-138.docx[Form 138 - Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate with DCSS] to document waiver of the TPL requirement because of Good Cause for non-cooperation with DCSS.

An adult A/R is penalized for failure to cooperate with the TPL process.
If there are two adults in the AU, both would be penalized if there is no TPL cooperation.

Recipients of Newborn Medicaid are not required to provide information regarding TPLs.

A child is never penalized or excluded from the AU because of an adult's failure to cooperate with TPL.

Refer to xref:2657.adoc[Section 2657 - Penalized Individuals] for information regarding Family Medicaid failure to comply with TPL requirements.

Children covered by other public or private health insurance (except vision or dental insurance) are ineligible to receive PeachCare for Kids®.
PeachCare for Kids® enrollees may only be denied/terminated for TPL at application or renewal due to continuous eligibility restrictions.

NOTE: For children in DFCS custody, refer to <<Problem Resolutions with Medicaid Billing and TPL>> and <<Children in Placement>>.

=== Nursing Home Insurance

For A/Rs who have insurance that pays for care in a nursing facility, determine if the insurance payment can be assigned to the facility.
If the payments *can* be assigned, complete Form DMA 285 notifying DCH that the nursing facility will be paid directly from the insurance carrier.
If payments *cannot* be assigned to the facility, treat the payments made to the A/R as income in the month received and include in the patient liability budget.

=== Health Insurance Premium Payment Program

The xref:attachment$form-124.pdf[Form 124 - Health Insurance Premium Payment (HIPP) Referral] is used to notify DCH via HMS of the potential purchase of an A/R's health insurance.
HIPP referrals may come from DFCS, a hospital, or other medical providers.

When DCH/HMS receives a referral for a “priority” applicant (person has cancer, diabetes, etc.), a decision is normally pended for 30 days awaiting the outcome of the Medicaid determination.
If the applicant is not approved for Medicaid within the 30 days, the HIPP request is denied, and the applicant is sent a denial letter.

If the referral is for a “non-priority” applicant, then a survey letter is sent to the applicant requesting the name of the employer, insurance company, etc.
The applicant has 30 days from the date of the survey letter to return the letter to HMS.
If the applicant is not made eligible for Medicaid by the time the survey letter is received by HMS, the HIPP referral is denied, and a denial letter is mailed to the applicant.
If the applicant is made eligible for Medicaid by the time the survey letter is returned, the approval process for HIPP begins.
If the applicant fails to return the survey letter within the 30 days from the date on the letter, the HIPP referral is denied, and a denial letter is mailed to the applicant.
The earliest HIPP payments will begin is the first month of Medicaid eligibility.
Payments are not made for any month(s) in which the A/R is not Medicaid eligible.
HIPP payments are not retroactive.

Do *NOT* make HIPP referrals for an A/R:

* with no health insurance or no access to health insurance
* whose only insurance is a Medicare Supplement
* whose only insurance is a per-diem (a policy that reimburses the policyholder a contractual amount per day for specified medical services or procedures) or cancer policy
* who is eligible only for Q-Track
* who does not have ongoing Medicaid coverage (for example, approved for three months prior only)
* who is Medically Needy spenddown eligible, and spenddown is met at or near the end of a budget period
+
NOTE: A referral *should* be made if the A/R is de facto eligible or spenddown eligible for multiple budget periods.

* whose coverage is through a non-custodial parent
* who is a refugee
* whose employer information is unavailable
* when the name of the policy holder is not known
* when there is no known person to contact for referral.

Refer only the primary policy to HIPP if an A/R has multiple health insurance policies.

Complete a xref:attachment$form-124.pdf[Form 124 - Health Insurance Premium Payment (HIPP) Referral], if appropriate, and forward the original to Health Management Systems or document why a HIPP referral was not made.

Mail or fax the HIPP Referral Form along with Form DMA 285, Health Insurance Information Questionnaire, to:

[%hardbreaks]
Health Management Systems
100 Crescent Centre Pkwy, Suite 1000
Tucker, Georgia 30084
Phone 678-564-1162, Press 1
Email: hippga@gainwelltechnologies.com

or

[%hardbreaks]
fax # 800-817-1769


NOTE: If a TPL pays DCH more than the amount DCH paid for all other services, including HIPP expenses, DCH will issue a refund to the A/R and notify the county via the state office of the refund.
Refer to xref:2499.adoc[Section 2499 - Treatment of Income for Medical Assistance] for treatment of refunds from DCH.

=== Disability Insurance Payments

If the A/R receives payments based on disability from an insurance policy, treat the payments as follows:

* If the payments are designated by the policy owner to cover medical expenses only, consider the payments to be a TPL.
Report the payments to Health Management Systems on Form DMA 285.
* If the payments are designated to cover lost wages or to be used at the discretion of the policyholder (A/R), consider the payments to be unearned income if the payments cannot be assigned.

=== Problem Resolutions with Medicaid Billing and TPL

Pharmacies should never deny filling an A/R's prescription because of an insurance issue.
However, follow the instructions below if the insurance continues to be a barrier to getting prescriptions filled or Medicaid claims paid:

. Worker is notified by A/R, pharmacy, or provider that a claim cannot be processed because of a TPL.
. Worker checks with A/R to validate if TPL exists.
Also check case record/Gateway for information regarding either the existence of TPL or cancellation of TPL.
. If the TPL is valid, inform A/R that the TPL is the primary payer of prescription.
No further action needed.
. If the TPL is valid, but the benefits have been exhausted for that particular service, that TPL may not be deleted from GAMMIS.
The pharmacy provider may process the claim Point of Sale (POS) using an Other Coverage Code = 4 Other coverage exists - payment not collected.
If the pharmacy system does not allow for POS processing the pharmacy provider may submit the claim manually on a Universal Claim Form (UCF).
The pharmacy provider should also include an explanation of benefits (EOB) from the primary carrier or a pharmacy screen print/profile with primary carrier detail that should include: the primary (copay/deductible), and remaining amount due that is being billed to DCH.
The provider must maintain documentation in their records concerning the denial in case of an audit.
Mail pharmacy paper claims (Universal Claim Form (UCF)) to:
+
[%hardbreaks]
OptumRx
PO Box 968021
Schaumburg IL 60196-8021

. If evidence is that the TPL is no longer valid, complete a Form DMA 285 and put a note on the top of the form that the insurance is not valid and attach a copy of the GAMMIS screen showing the invalid TPL.
Fax to HMS at 770-937-0180.
HMS has 30 days to act on the cancellation.
. If the TPL was cancelled many months ago or has never been a valid TPL for the A/R, complete a Form DMA 285 and put a note on the top of the form that the insurance is not valid and attach a copy of the GAMMIS screen showing the invalid TPL.
Fax to HMS at 770-937-0180.
. However, if this is an emergency and the prescription needs to be processed immediately, you may need to contact HMS by phone at 678-564-1162 option 3 or by fax at 770-937-0180.
. The pharmacy should immediately fill the prescription, but in the event that the pharmacist, at the “Point of Sale”, is unsure of what COB override code to use, the table below provides the appropriate designation for their use.
If necessary, direct them to the OptumRx Pharmacy Services Helpdesk at 1-866-525-5826.
+
[cols="1,2,4"]
|===
^| FIELD ^| NAME OF FIELD ^| VALUES/DEFINITIONS OF FIELDS

.4+^| 308-C8
.4+^| Other Coverage Code
| 2 = Other coverage exists – payment collected

| 3 = Other coverage exists – claim not covered

| 4 = Other coverage exists – payment not collected

| 8 = Claim is billing for patient financial responsibility only(co-pay/coinsurance)
|===

. Paper processing is allowed if the pharmacy system does not allow for online processing of Medicaid secondary or tertiary claims.
If the carrier returns payment for the claim, the pharmacy provider must include EOB from primary carrier or a pharmacy screen print/profile with primary carrier detail that paid, recipient amount paid (co-pay/deductible), and remaining amount due that is being billed to DCH.
If the other carrier denied the claim, attach the denial statement from the other insurance carrier to your claim form for processing.
If no response is received from the insurance carrier, attach the coordination of benefits confirmation statement to the back of your claim for payment.
+
Please mail pharmacy paper claims (Universal Claim Form (UCF)) to:
+
[%hardbreaks]
OptumRx
PO Box 968021
Schaumburg IL 60196-8021

=== Children in Placement

For children in placement, when there are difficulties in verifying a child's insurance coverage or termination of insurance coverage with an insurance carrier due to HIPAA and custody issues, RevMax RMS should submit form DMA 285 with all known information, including: RMS name and contact number;
contact date(s) and name for insurance carrier and issue details.
Submit form to:

[%hardbreaks]
Health Management Systems
100 Crescent Centre Pkwy, Suite 1000
Tucker, Georgia 30084

[%hardbreaks]
or fax # 770-937-0180

