= 2750 DCH Reports-Ex Parte Lists
:chapter-number: 2700
:effective-date: December 2022
:mt: MT-68
:policy-number: 2750
:policy-title: DCH Reports-Ex Parte Lists
:previous-policy-number: MT 60

include::partial$policy-header.adoc[]

== Requirements

Monthly reports are generated by the Department of Community Health (DCH) or DCH's contract entity and are transmitted by Gainwell Technologies to Gateway.
Eligibility determinations are completed by local Department of Family and Children Services (DFCS) offices and/ Right from the Start (RSM) Outreach Project offices for required action.

== Basic Considerations

As part of the Continuing Medicaid Determination (CMD) process for selected A/Rs whose SSI benefits are terminated or denied, DCH makes temporary determinations of continued eligibility under a new Ex Parte Medicaid Class of Assistance.
Four reports listing these individuals are generated and are accessible via Gateway.
Gateway generates the following reports for tracking Ex Parte individuals received monthly from the GAMMIS file and the disposition of these cases.
The reports are as follows:

* *[.underline]#SSI Ex Parte Determination List#*

Monthly report generated on the 2^nd^ business day of the month.
Report lists individuals received in the Inbound Ex Parte file for the month.

* *[.underline]#Dispositioned Ex Parte Report#*

Monthly report generated on the 2^nd^ business day of the month.
Report includes all clients that were processed and authorized as part of the Ex Parte automation and will include all clients approved or denied by eligibility worker the prior month.

* *[.underline]#Pending Ex Parte Report#*

Daily report that is ran Monday-Friday.
Report includes all clients that were received in Inbound Ex Parte file from GAMMIS that have not been disposed.
This includes clients that a verification checklist has been sent requesting more information.
The report may show same client multiple times if received during different months.
The client received date will be different.

* *[.underline]#SSI Ex Parte Exception#*

Monthly report generated on the 2^nd^ business day of the month.
Report includes all individuals received on the Inbound Ex Parte file from GAMMIS, but Gateway was unable to automate the application registration for the client.
This report is reviewed by designated staff.

== Procedures

Effective 09/01/2022, enhancements to Gateway and GAMMIS were made to automate the Ex Parte eligibility determination process

=== Automated Process

The steps below detail the automated process and the procedures for eligibility worker to complete upon receipt the generated tasks.

Step 1:: Gateway receives a monthly file from GAMMIS.
This file includes all new SSI Ex Parte determinations and made by DCH for the prior month resulting in eligibility for Aged, Blind, and Disabled (ABD) COAs or MAGI Family Medicaid COA.
This includes terminations from SSI.

Step 2:: Gateway will automate the application registration.
If A/R is known to +
Gateway system, it will associate the application to the established case number, if one exists.
During case application registration process if the A/R is receiving Ex Parte Medicaid based on an ABD COA, the application will be flagged as ABD to ensure correct routing.

Step 3:: Gateway will generate either an Ex Parte Intake or Change task and route to the appropriate worker based on their Gateway role.

Step 4:: Worker receives Ex Parte Intake or Change task and using SDX/BENDEX, DOL, Vital Records, Ex Parte interface, related cases, and any other available information, determine eligibility for an appropriate COA, either the COA specified by the report or, if appropriate, a COA that provides a higher level of coverage.
If necessary, contact the client to clarify any missing or unclear information.
Citizenship/Immigration must be established.
If there is not enough information to make a determination, send Form 508, 94A or 700 with an SSI Continuing Medicaid Determination Notice and checklist to the client with an appropriate due date.

In the absence of evidence to the contrary, assume all other eligibility criteria have been met and that SSA has determined there has been no transfer of assets.
Ex Parte Interface can be accessed on the Person Detail screen by selecting the “EX” icon or through Interface by entering at minimum the A/R's SSN to review the information received on the file.
For Ex Parte ABD tasks created do not assume that the A/R is Medicare eligible.

*NOTES:*

* For Waiver COAs Aid Category 449 or Category code “Waivered Services”, use Ex Parte Interface which provides PA begin and PA end dates needed.

Additional LOC and Communicator are not required.
Process as EDWP (CCSP), NOW/COMP or ICWP based on PA information on report.

* Assume SSA has forwarded TPR information to DCH

* Prior receipt of SSI is prima facie evidence of disability for 12 months from the SSI termination date unless SSI was terminated for failure to meet disability criteria.

* For Public Law COAs, determine COLA and entitlement to or increases in RSDI based on SDX/BENDEX, using the best estimate possible.

* For Ex Parte ABD tasks, do not assume that the A/R is Medicare eligible.

Step 5:: Approve or deny the case and document the case record within 10- days of receipt of the file (this should be equal to the task generation date).

*NOTE For Spend Down Cases:* The system generated notice, which includes Medicaid eligibility information, replaces Form 962.
Complete a Form 962 only if eligibility cannot be entered in the system, such as a three-month prior application that is greater than thirteen months old.

NOTE: A complete redetermination of eligibility must be completed on all cases when a change is reported, or within 12 months after the SSI termination, whichever comes first.
Contact with the individual may be required to complete this process.

[.text-center]
Step 6:: Notify the A/R of the eligibility decision.

Step 7:: Upon completion of case and task disposition, Gateway will notify GAMMIS of A/R's approval or denial through daily interface files sent from Gateway to GAMMIS

Step 8:: GAMMIS updates the A/R's eligibility in their system and removes them from the non- confirmation reports.

NOTE: If Gateway is unable to automate the registration of an Ex Parte A/R received on the monthly file then the A/R will be placed on an Exceptions report and reviewed by designated staff.

== Other Considerations

=== Continued SSI Eligibility

For individuals that are currently receiving SSI, no action is required.

=== Documentation

Follow current documentation standards

NOTE: All actions taken on an Ex Parte case must be documented in case note.

=== Document Management

All forms including cover letter are available in Gateway correspondence.

=== Eligible in GAMMIS, But Not on List

The CMD process must be documented.

At times an individual may become known to DFCS who is showing eligible on GAMMIS but has never appeared on an Ex Parte list and is not eligible on Gateway, SDX or PeachCare for Kids®.
Treat these individuals as Ex Parte individuals by either registering an application or by updating an existing case.
Gateway will create a task when an application is registered.
Worker will need to create a manual task if updating an existing case is necessary.
For both applications and changes, the Ex Parte indicator *MUST* be selected on the Program Request screen when processing the case.
Continue by following steps on page 3 beginning with Step 4.

If the A/R is eligible for full Medicaid, approve on Gateway as soon as possible.

If the A/R is not eligible for full Medicaid, such as AMN or Q Track, approve on Gateway beginning with the first month following the end date reflected from DCH.
