= 2715 Family Medicaid Changes In Income
:chapter-number: 2700
:effective-date: September 2024
:mt: MT-73
:policy-number: 2715
:policy-title: Family Medicaid Changes In Income
:previous-policy-number: MT 60

include::partial$policy-header.adoc[]

== Requirements

When a change in the AU's or BG's financial circumstances occurs, ongoing eligibility must be determined.

== Basic Considerations

A change in income includes changes in income and/or expenses that affect the ongoing benefit amount.
A change in income includes the following.
This list is not all-inclusive.

* Income begins or ends
* Change in employer or obtaining new or additional employment
* Increase or decrease in the rate of pay
* Increase or decrease in dependent care expenses due to a change in provider, number of hours of care, number of individuals for whom care is given, or amount charged (Non-MAGI Medicaid only)
* Change in type of self-employment activity
* Change in self-employment income or expenses
* Change in pre-tax deductions (MAGI COAs only)
* Change in 1040 deductions (MAGI COAs only)
* Change in 5% FPL deduction (MAGI COAs only)

Changes in income must be verified.
Self-attestation verified by electronic data sources or other information known to the agency must be used to the maximum extent possible.
Refer to xref:2051.adoc[Section 2051 - Verification], and xref:2405.adoc[Section 2405 - Treatment of Income].
Client statement is acceptable verification of income for Pregnant Woman Medicaid and Newborn Medicaid, unless questionable.

A change in financial circumstances requires a recalculation of representative income amount and a calculation of the best estimate of income based on the AU's past, current, and anticipated circumstances.

Representative income is the amount of income that best represents what the AU is most likely to receive in each pay period and is used to calculate the AU's monthly income.
Refer to xref:2653.adoc[Section 2653 - Prospective Budgeting].

Normal fluctuations in the income amounts are not considered a change in circumstances and do not require a recalculation of representative income.
Normal fluctuations include the following:

* overtime not expected to last for more than one calendar month
* a fifth or periodic paycheck
* vacation/sick pay received within a calendar month.

This list is not all inclusive.
Refer to xref:2653.adoc[Section 2653 - Prospective Budgeting].

=== Continuous Eligibility
Effective January 1, 2024, children under the age of 19 will be provided 12 months of continuous eligibility (CE) coverage regardless of change in circumstances with certain exceptions. The exceptions to CE include the following:

*	The child reaches age 19.
*	The child is no longer a Georgia resident
*	A voluntary request for closure.
*	The agency determines that eligibility was erroneously granted at the most recent determination, redetermination, or renewal of eligibility because of agency error or fraud, abuse, or perjury attributed to the child or the child’s representative; or
*	The child is deceased.

CE does not apply to:

*	Medically Needy,
*	Presumptive Eligibility,
*	At renewal, children that are only eligible for Transitional Medical Assistance or
*	Emergency Medical Assistance.

If a child becomes incarcerated during their CE period, then the child must remain eligible for the remainder of the CE period while incarcerated.


== Procedures

=== New Earnings

Document the following:

* date the change is reported to the agency
* who is employed and where s/he is employed
* when employment began and date of the first paycheck
* termination date of previous employment, if applicable
* the estimated number of hours per week of employment and hourly wages
* the frequency of pay and pay dates
* the source of verification
* dependent care expenses (non-MAGI COAs only)
* third party liability, if applicable
* change in pre-tax deductions (MAGI COAs only)
* change in 1040 deductions (MAGI COAs only)

If the AU is ineligible based on the trial budget, complete a CMD and terminate eligibility.
Notify the AU.

If the Family Medicaid AU is eligible based on the trial budget, using anticipated income and expenses, refer to xref:2653.adoc[Section 2653 - Prospective Budgeting], and complete the following procedures:

* establish representative pay
* budget the income effective the month following the expiration of timely notice and after verification is received, if required

Continue Medicaid for the AU members in Family Medicaid other than FM-MN.
In Family Medicaid-Medically Needy (FM-MN) cases:

* Eligibility for all MAGI and Non-MAGI Medicaid COAs and PeachCare for Kids ® must be ruled out prior to determining eligibility under FM-MN
* Recalculate all income received and calculate prospective income for each month remaining in the budget period.
* If the budgeted income places the case in spenddown status or increases the spenddown amount, change the case status in the system and notify the AU

Do not deny a case as over income if it is cascading to FM-MN.
Allow the case to cascade and close appropriately if client reports no medical expense or bills.
Ensure the A/R receives the correct FM-MN disposition notice as well as a referral to the FFM.

If client reports medical bills/expenses, request the BG to submit any medical bills not covered by Medicaid to apply to the spenddown.
If budgeted income does not change the eligibility status, document the record.

=== Loss of Income or Decrease in Income

Document the following:

* the type of change
* the effective date of the change
* the date the change is reported to the agency
* method of verification
* changes in pre-tax deductions (MAGI COAs only)
* changes in 1040 deductions (MAGI COAs only)
* change in dependent care expenses (non-MAGI COAs only)

Remove or decrease the income the month after the change occurs and was reported.
If the AU is in the MN spenddown, recalculate the spenddown.

Late reporting requires calculation and documentation of when the change should have been budgeted.

NOTE: Explore all benefits to which the AU may be entitled.

=== Increase in Income

Document the following:

* the effective date of the change
* the date the change is reported to the agency
* the type of increase (number of hours, rate of pay)
* the amount of the increase
* method of verification
* change in pre-tax deductions (MAGI COAs only)
* change in 1040 deductions (MAGI COAs only)
* change in dependent care expenses (non-MAGI COAs only)

Complete a trial budget to determine ongoing eligibility.

If the AU is ineligible based on the trial budget, complete a CMD and terminate eligibility the month following timely notice.
Notify the AU.

If the AU is eligible based on the trial budget, refer to xref:2653.adoc[Section 2653 - Prospective Budgeting], and complete the following procedures:

* budget income effective the month following the expiration of timely notice
* if the AU is MN, recalculate the income for the budget period.
If the increased income affects spenddown, notify the AU.

Late reporting requires calculation and documentation of when the change should have been budgeted.

=== Change in the Source of Income

Document the following:

* the date the change in income is reported to the agency
* the date that the new or changed income is first received
* who receives the income
* the source and type of the new income
* the frequency of the income and day of the week received
* the amount of the income
* method of verification
* change in pre-tax deductions (MAGI COAs only)
* change in 1040 deductions (MAGI COAs only)
* change in dependent care expenses (non-MAGI COAs only) Complete a trial budget to determine ongoing eligibility.

If the AU is ineligible based on the trial budget, complete a CMD and terminate Medicaid the month following the expiration of timely notice.
Notify the AU.

If the AU is eligible based on the trial budget, complete the following procedures:

* determine ongoing eligibility by establishing representative pay, and, if appropriate, converting this income using the correct conversion factor for the ongoing benefit month.
Refer to xref:2653.adoc[Section 2653 - Prospective Budgeting].
* notify the AU
* if the AU is FM-MN, recalculate the income for the budget period.
If the change in income affects spenddown, notify AU and make the necessary changes to the case.

Late reporting requires calculation and documentation of when the change should have been budgeted.

MAGI Medicaid includes taxable income only in the BG.

Non-MAGI Medicaid includes or excludes income differently than MAGI Medicaid in the BG.
Refer to xref:2499.adoc[Section 2499 - Treatment of Income in Medical Assistance].

=== Unearned Income: Child or Spousal Support Income

Document the following information if child or spousal support is reported as a new source of income or a change in child or spousal support is reported:

* the date the change is reported to the agency
* the date the new child or spousal support or change in child or spousal support was first received by the AU
* the frequency of receipt of the income
* the day of the week it is received
* the amount of the income
* who pays the child or spousal support and for which child
* method of verification

Calculate a trial budget to determine ongoing eligibility.
MAGI Medicaid does not include child support in the budget.

Non-MAGI Medicaid includes child support and spousal support in the budget.

If the AU is ineligible based on the trial budget, complete the following procedures:

* if receiving Parent/Caretaker with Child(ren), change the COA to Four Months Extended Medicaid (4MEx) if all 4MEx requirements are met.
+
NOTE: If the increase in spousal support occurs concurrently with an increase in earned income, TMA may be approved if the earned income change is what caused the case to be ineligible for Parent/Caretaker with Child(ren).
Refer to xref:2166.adoc[Section 2166 - Transitional Medical Assistance].

* complete a CMD
* if ineligibility will only last for one month, suspend benefits.
Offer MN before suspending benefits.
* notify the AU and allow timely notice.

If the AU is eligible based on the trial budget, complete the following procedures:

* determine the amount of child or spousal support and add the child or spousal support to the budget.
Refer to xref:2653.adoc[Section 2653 - Prospective Budgeting].
* allow timely notice
* notify the AU.

=== Unearned Income: Loss of Child or Spousal Support

Document the following:

* the date the AU or BG last received child or spousal support
* the date the loss of child or spousal support is reported to the agency
* the reason for the loss of child or spousal support, if applicable
* method of verification

Delete the child or spousal support income for the ongoing benefit month and notify the AU.

NOTE: For FM-MN AUs, consider the effect of the loss of child or spousal support on spenddown status.

MAGI Medicaid does not include child support in the budget.

Non-MAGI Medicaid includes child support and spousal support in the budget.

Non-MAGI Medicaid earned income of a child is not included in the BG.
Certain unearned income of a child is included in the BG.
Refer to xref:2499.adoc[Section 2499 - Treatment of Income in Medical Assistance].

=== Income of a Child

MAGI Medicaid does not include taxable income of a child in the BG when the total taxable amount is below the allowable IRS dependent exemption amount regardless if the child is required to file a tax return and/or if they do file a tax return.
The dependent exemption amount is established by IRS yearly and is set each January for the previous tax year.
The dependent exemption will be used for the current MAGI Medicaid year. Refer to xref:2610.adoc[Section 2610 - MAGI Budget Groups/Assistance Units] for each tax year threshold amount.

Filing a tax return and filing for a tax refund are not the same.

=== Changes in Deductions to Income

Consider the effect of the following on eligibility:

* Pre-Tax Deductions (MAGI Medicaid only)
* 1040 Deductions (MAGI Medicaid only)
* 5% FPL Deduction (MAGI Medicaid only)
* a change in dependent care expenses (non-MAGI only)
* a change of medical expenses (FM-MN only)
* expiration of the $30 plus 1/3 deduction because of time limitations. *(obsolete as of 1/1/14)*
* $90 work expense (Non-MAGI Medicaid only)

Document the following:

* the type of deduction that changed
* the date the change occurred
* the date the change is reported to the agency
* how the deduction changed
* method of verification (client statement or other verification).
Document reason verification was requested.

Complete a CMD, if necessary.
Recalculate the budget, including the new deduction amount.
Provide timely notice of any change(s) to the AU.
