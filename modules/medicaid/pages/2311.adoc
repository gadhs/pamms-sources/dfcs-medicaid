= 2311 Burial Contracts (Pre-Paid or Pre-Need) and Burial Space Items
:chapter-number: 2300
:effective-date: July 2022
:mt: MT-65
:policy-number: 2311
:policy-title: Burial Contracts (Pre-Paid or Pre-Need) and Burial Space Items
:previous-policy-number: MT 64

include::partial$policy-header.adoc[]

== Requirements

The treatment of burial contracts and burial space items is dependent upon whether an A/R's class of assistance (COA) is FBR, Non-FBR or Family Medicaid.

== Basic Considerations

=== Contracts

A prepaid (or pre-need) burial contract is an agreement whereby a buyer pays in advance for a burial that the seller agrees to furnish upon death of the buyer or other designated individual.
A burial contract is usually with a funeral home and may include coffin, vault, flowers, embalming, cremation, etc.
A cemetery contract is with owners of a cemetery and may include opening/closing of the grave, maintenance of the gravesite, mausoleum, headstone, etc.
At times a burial contract may include items pertaining to the gravesite.

All burial contracts purchased in Georgia are, by state law revocable.
Assume that all other contracts are revocable unless the A/R provides proof to the contrary.
Treat any burial contracts that are irrevocable the same as revocable contracts.

A non-itemized contract does not indicate the cost of each item.

Only one burial contract and one cemetery contract designated on a particular individual may be considered for exclusion from resources.

NOTE: For Non-FBR ABD Medicaid COAs, treat a life insurance policy that is purchased to fund a prepaid burial contract in the following way:

* if the contract is itemized, treat as a burial contract.
See Procedures in this section.
* if the contract is not itemized, treat as a life insurance policy.
See xref:2323.adoc[].

In either case, the face value should be equal to the purchase price of the burial contract at the time of purchase.
Any appreciation of the excluded funds after the date of designation may also be excluded.
If the life insurance policy has not been irrevocably assigned to the funeral home, then the contract is not considered as paid in full.
Notice of the irrevocable assignment must be received.

=== Burial Space Items

Burial space items may be part of a burial contract or owned outright.
The following are burial space items:

* burial plot
* grave site
* crypt
* mausoleum
* casket
* urn
* niche
* other repository customarily and traditionally used for the deceased's bodily remains.

The term burial space item also includes necessary and reasonable improvement for additions to such spaces, including but not limited to the following:

* vaults
* headstones, markers, or plaques
* other burial containers for caskets
* arrangements for the opening and closing of the gravesite
* contracts for the care and maintenance of the gravesite, sometimes referred to as endowment or perpetual care.

Only the value of burial space items, which are paid in full, may be exempt from resources and the burial exclusion policy.
If the A/R and deemor own burial assets in excess of the burial exclusion allowance, the excess is considered as a countable resource to be applied toward the resource limit of the appropriate COA.

NOTE: Examples of Non-Burial space items include: Services, Embalming, Cremation, Flowers, Cards, Newspaper, Death Certificates, Service Vehicle, etc.
This list is not all inclusive.

=== Immediate Family

Immediate family includes the Medicaid individual's spouse;
minor and adult natural, adopted and stepchildren and their spouses; natural and adoptive parents and their spouses, siblings and their spouses.
If the relationship to the A/R is by marriage only, the marriage must be in effect in order for the burial space exclusion to continue to apply.
Immediate family does not include members of an ineligible spouse's family unless they are also within the appropriate degree of relationship(s) to the Medicaid individual.

== Procedures

=== Family Medicaid

Exclude up to $1500 of the combined equity value (EV) of all burial contracts and one burial plot per each AU or BG member.
Count the EV of any additional plot(s) toward the resource limit.
Changes in the burial contract policy are effective July 1, 2005.

=== Burial Contracts Non-FBR COAs

However, implement the policy for applications (including the three prior months) and reviews beginning January 1, 2007.
Non-FBR A/Rs may exclude up to a total of $10,000 for burial purposes.
This includes the FV of life insurance policies xref:2323.adoc[], funds set aside for burial xref:2312.adoc[] and the purchase price of burial contracts less paid in full burial space items.

=== Burial Contracts FBR COAs

FBR A/Rs may exclude up to a total of $1500 for burial purposes.
This includes the FV of life insurance policies xref:2323.adoc[], funds set aside for burial xref:2312.adoc[] and the purchase price of burial contracts less paid in full burial space items.

=== Burial/Cemetery Contracts and Burial Space Items ABD COAs

Obtain an original copy of each burial/cemetery contract to verify the following:

* for whom the contract is designated
* that the contract is with a business that conducts funeral services or operates a cemetery
* if the contract is itemized
* the value of items at time of purchase
* whether the contract is paid in full at time of purchase

The purchase price of a contract is the amount paid for contract less any sales tax.

Count as a resource burial space items owned by an A/R or deemor if they are designated for anyone other than a member of the A/R's immediate family or they are not designated for use by a specific individual.

Exclude only one burial space item per person that serves the same purpose, such as a casket or an urn.

==== Treatment of Itemized Burial Contracts

If the contract is paid in full, the value of the contract is the purchase price less any burial space items.

NOTE: Sales tax is not part of the purchase price.

If the contract is not paid in full, the value of the contract is the amount paid to date less any paid in full burial space items.

If the contract includes no burial space items, the value is the purchase price or the amount paid to date.

==== Treatment of Non-Itemized Burial Contracts

If the contract is not itemized, the value of the contract is the full purchase price or amount paid to date.

NOTE: Allow reasonable time for the A/R to have the contract itemized if possible.

=== Determining the Current Value of a Burial Contract

*If the contract is paid in full and includes the cost of burial space items, follow the steps below to determine the current value.*

Refer to Appendix F, for the “Burial Exclusion” xref:attachment$form-985.docx[form *985*].

Step 1:: Determine the purchase price of the contract less any sales tax.

Step 2:: If the contract is itemized, subtract from the purchase price any burial space items included in the contract.

Step 3:: The remainder is the value of the burial contract.

NOTE: If the contract is not itemized the value is the purchase price less sales tax.

*If the contract is not paid in full, follow the steps below to determine the value:*

Step 1:: Determine the amount paid to date on the contract.

Step 2:: If the contract is itemized, subtract from the purchase price only the paid in full burial space items (if any have been paid in full).

Step 3:: The remainder is the value of the burial contract.

NOTE: If the contract is not itemized, the value is the amount paid to date.

The value of the burial contract may be excluded under the burial exclusion allowance or counted as a resource.

=== Significant Hardship

Consult the Field Program Specialist for instructions if the A/R claims that selling or cashing in of a burial contract will cause significant hardship.

=== Documentation and Verification of Burial Space Items

If an A/R alleges owning only one of a particular burial space item, or an A/R and spouse allege owning no more than two, assume that the items are designated for the A/R and spouse.
Document the allegation in the case record.

If an A/R or A/R and spouse allege owning more than one (or two for the A/R and spouse) of a particular burial space item, obtain a signed statement xref:attachment$form-987.docx[Form 987] showing the name and relationship of the person for whose burial each item is designated.

Verify the CMV and EV of all non-excluded burial space items using xref:attachment$form-986.docx[Form 986], found in Appendix F.

Document the case appropriately.

=== Burial Plots ABD Medicaid COAs

Exclude from resources only the burial plots owned by an ABD A/R or deemor that are designated for immediate family members.
Count as a resource those owned for others.

Document the A/R's statement as to the number of burial plots owned.

=== Computing Burial Assets

Refer to the “Burial Exclusion” xref:attachment$form-985.docx[form 985] in Appendix F and compute the $1500/$10,000 burial funds exclusion by the value of any of the following assets owned by the A/R and deemor:

* the face value of burial insurance policies
* the face value of any life insurance policy (whole or term) on the A/R or A/R's spouse.
For Non-FBR A/Rs:
** If the FV was not used to reduce the burial exclusion allowance, then the CSV of the life insurance is a countable resource.
** All or part of the FV of an A/R's life insurance may not be designated for his/her spouse.
See xref:2323.adoc[].
* The current value of a burial contract.
* Funds set aside for burial (less any interest/dividends left to accrue).
Funds designated for burial must be owned by the individual or jointly owned between the A/R and spouse.
See xref:2312.adoc[].

If the A/R or deemor owns burial assets in excess of the burial exclusion allowance, determine which assets to exclude.
Guidelines for this determination:

. FV of term life insurance must be applied to the burial exclusion allowance first.
. Other assets should be applied in the most advantageous way for the AR or deemor.
. Whole life policies may not be partially excluded.
If the full FV cannot be excluded, the CSV must be counted as a resource.

If a burial fund or the countable value of a burial contract is included in the burial asset exclusion, the xref:attachment$form-985.docx[Form 985] *must* be signed to specifically designate those items for burial.
