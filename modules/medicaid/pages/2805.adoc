= 2805 Funding Sources
:chapter-number: 2800
:effective-date: November 2020
:mt: MT-62
:policy-number: 2805
:policy-title: Funding Sources
:previous-policy-number: MT 45

include::partial$policy-header.adoc[]

== Requirements

Maintenance and administrative costs for children in foster care or receiving Adoption Assistance are paid from federal and state funding sources.

== Basic Considerations

The funding for Georgia's Foster Care and Adoption Assistance Program is available from federal and state sources including IV-E Foster Care, IV-B Foster Care, IV-E Adoption Assistance, State Funded Adoption Assistance, Supplemental Security Income (SSI), Medicaid and state funds.
Federal funding sources are pursued because they share in the cost, therefore conserving state funds.

Another funding source available is child support.
The parents of children in care are routinely referred to the Division of Child Support Services (DCSS) and may be obligated by court order to contribute to their child's care and medical support.

=== IV-E Foster Care Funds

Title IV-E is the federal funding source designated for certain children who are under the care and supervision of the State Child Welfare Agency.
IV-E provides reimbursement for costs associated with the care and maintenance of children in placement and for administrative cost related to the State's Child Welfare Program.
The IV-E Foster Care Program authorized by the Social Security Act, provides funds to states for the following activities:

* Maintenance of children in foster care placements
* Reimbursement of administrative and case management costs incurred while staff work with the child, the child's family and the care provider
* Reimbursement for training agency staff and providers who work with the child or who administer the foster care program

All children entering foster care must be referred to Revenue Maximization for a IV-E eligibility determination regardless of length of stay in care.
Referral to Rev Max is via submission through the SHINES system.
To be eligible for IV-E Foster Care maintenance and administrative costs, all IV-E requirements must be met.

Children classified as Title IV-E eligible must have some relationship to the Aid to Families with Dependent Children (AFDC) program in addition to meeting other criteria.
IV-E is unrelated to Temporary Assistance to Needy Families (TANF).
In the Welfare Reform Act of 1996, Congress mandated that the state AFDC policy in effect on July 16, 1996 be used for determining the AFDC relationship for IV-E eligibility purposes.

=== Child Welfare Foster Care Funds (IV-B)

The Child Welfare Foster Care (IV-B) Program is a federal child welfare block grant that provides funds to states for foster care expenses.
A child who is eligible for IV-B is a child in placement for whom DFCS has partial or total responsibility and who has been determined ineligible for IV-E Foster Care.
The Title IV-B grant is capped.
Once these limited funds are spent, foster care expenses are paid primarily with state funds.
It is advantageous to pursue IV-E Foster Care for all children to conserve the use of state dollars.

=== Supplemental Security Income (SSI)

Supplemental Security Income (SSI) is a federal payment program for disabled individuals administered by the Social Security Administration.
Payments are made directly to the recipient from the federal government on a monthly basis.
However, when a child is in DFCS custody, the county department with custody becomes the payee for the child's SSI check.
SSI eligible children may be concurrently eligible for IV-E payments.

=== Medicaid Program

The Medicaid program is a joint federal/state program that is authorized under the Social Security Act.
Funds are available to states for providing medical services to eligible recipients and for reimbursing activities that support the administration of the Medicaid program.
DFCS accesses Medicaid funds through the Department of Community Health, Division of Medicaid, for case management and services for children in out-of-home care.
Children who are IV-E eligible and/or SSI eligible are automatically eligible for Medicaid.
However, children whose foster care is paid by state funds are not automatically eligible for Medicaid.
An eligibility determination must be completed on each child entering care.
