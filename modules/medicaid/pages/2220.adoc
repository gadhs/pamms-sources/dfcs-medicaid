= 2220 Enumeration
:chapter-number: 2200
:effective-date: July 2022
:mt: MT-65
:policy-number: 2220
:policy-title: Enumeration
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

Each Assistance Unit (AU) member must provide or apply for a Social Security Number (SSN) in order to receive Medicaid.

== Basic Considerations

Enumeration is the process by which an SSN is obtained and validated.

SSNs are used to secure information from other sources to achieve the following:

* complete reviews necessitated by federal benefit changes
* discover unreported income or resources
* prevent duplicate benefits
* verify reported information.

An attempt should be made to obtain the SSNs of non-AU individuals whose income and/or resources are considered in determining eligibility.
Clearly state that the provision of the SSN by non-AU members is voluntary and explain what the SSN will be used for, as defined above.

Eligibility is not adversely affected if the applicant fails to furnish the SSN of a non-AU individual unless questionable/conflicting income information can be resolved only with the SSN.
This should be a rare occurrence.
A case may not be closed solely for the failure to provide the SSN of a non-AU member.

NOTE: Benefits will not be denied during the application for, or the validation of, an SSN.

Refusal to be enumerated without Good Cause results in denial of Medicaid coverage for the non-enumerated AU member.

[caption=Exception]
NOTE: Enumeration is not required for Emergency Medical Assistance (EMA) and Newborn Medicaid COAs.

The A/R's verbal or written statement of the SSN, intent to apply or that s/he has applied for the SSN of an AU member meets the enumeration requirement.
If the A/R's statement of intent to apply or that s/he has applied for an SSN is accepted (not questionable), follow-up is required in the third month after the month of approval.
If the SSN or verification of an application for an SSN is not received within 30 days of the follow-up request, the individual is not eligible for Medicaid unless Good Cause is established.
Refer to Good Cause and <<chart-2220-2,Chart 2220.2>> in this Section.

=== TMA/4Mex

A Parent/Caretaker with Child(ren) Medicaid AU member who complied with the enumeration requirement by agreeing to apply for an SSN or by establishing Good Cause is no longer required to comply following approval for TMA or 4MEx.

=== Newborn Medicaid and EMA

Compliance with the enumeration process is not required for Newborn Medicaid, Family Medicaid EMA and ABD EMA.

=== Good Cause

Good Cause may be established for failure to meet the enumeration requirement.
Refusal to meet the requirement without Good Cause results in an automatic determination of non-compliance.

Good Cause is established when it is determined that the AU member has made every effort to obtain an SSN but has been unsuccessful.
The agency must make every effort to assist the AU in obtaining documents needed to complete the enumeration process.

Good Cause includes, but is not limited to the following:

* documentary evidence or collateral information that the AU member has applied for an SSN and has not yet been issued a number
* the inability of the AU to obtain the necessary documents required by the Social Security Administration (SSA).
Example: inability to obtain a birth certificate.

Good Cause does NOT include the following:

* lack of transportation
* temporary absence from the home
* age/illness/infirmity

NOTE: The eligibility worker must provide assistance to the A/R in obtaining information required to meet the enumeration requirement.

== Procedures

Use the following chart to determine the procedures for obtaining and validating an SSN in specific situations.

.Chart 2220.1 – Enumeration
[#chart-2220-1,cols="2,4"]
|===
^| IF the A/R ^| THEN

| is currently in the system with an SSN
| document the SSN.

Compare known SSN with the A/R's statement.

The SSN will be verified through the validation process.

| knows the SSN at the interview or provides the SSN on the application
| document the SSN.

Enter the SSN into the system.

Upload copies of available SSN cards to DIS.

| does not know the SSN at the interview or provide the SSN on the application
| request that the SSN be provided within 14 days.
Issue the verification checklist.

Document the SSN when it is provided.
Enter the SSN in the system.

*If SSN or proof of application for SSN is not provided, do not include non-enumerated individual in AU unless good cause is asserted.*

| has multiple SSNs
| refer the A/R to the Social Security Administration (SSA) to determine the correct SSN.

Document all known SSNs.

Inform the A/R of the responsibility to report the correct and primary

SSN to the county office upon resolution with SSA.

Refer to the chart entitled System Related Enumeration Problems if appropriate and/or xref:2005.adoc[].

| is enumerated at birth by a medical facility
| accept client statement, unless questionable.

Contact the A/R no later than the third month following the month of application for the SSN to determine if the SSN has been received.
If the SSN has not been received, contact the A/R monthly thereafter.

| never had an SSN

OR

had an SSN but the number is unknown or cannot be located
a| refer the AU member to SSA to apply for a new or replacement SSN.

Follow these steps:

. Inform the AU of the responsibility to submit original or certified copies of documents that verify age, identity, and citizenship (e.g., birth certificates, driver's licenses, etc.) to SSA with the application for an SSN.

. Provide the AU with a copy of Form SS-5 to complete and take to the Social Security Administration.
This form can be downloaded at: https://www.socialsecurity.gov/online/ss-5.html, or have the AU call Social Security at 1-800-772-1213 to obtain the form.

. Obtain the A/R's verbal or written statement that s/he has applied for or will apply for an SSN for the AU member.
Document the A/R's statement.

. Contact the A/R in the third month after the month of approval to obtain the SSN or verification of the application for an SSN.

. Allow 30 days from the date of request for the A/R to provide the SSN or verification of the application for an SSN.

. Document the SSN, upload verification of the application in DIS.
+
OR
+
determine if Good Cause exists if the requirement is not met.

. If Good Cause exists, contact the AU monthly to monitor Good Cause.

. If Good Cause does not exist, the non-enumerated individual is not included in the AU.

. Request the SSN at the next review if it has not been provided.
If the SSN has not been received by the next review, request the SSN at each review thereafter.
|===

=== Non-Compliance with Enumeration Process

AU members who fail or refuse, without Good Cause, to complete the enumeration process are not eligible to be included in a Family Medicaid or ABD AU.

=== Parent/Caretaker with Child(ren) Medicaid

A parent who does not meet the Medicaid enumeration requirement is penalized and his/her needs are not considered in determining Parent/Caretaker with Child(ren) Medicaid eligibility.
Refer to Chapter 2650, Family Medicaid Budgeting, for budgeting procedures for a penalized individual.

=== Child Under 19 Years of Age Medicaid and Family Medicaid Medically Needy

A pregnant woman who does not meet the enumeration requirement is ineligible.
If the pregnant woman has a child for whom Children Under 19 Years of Age Medicaid or FM-MN eligibility is being determined, she is included in the BG.

A child who does not meet the enumeration requirement is ineligible.
The child may be included in or excluded from the BG at the option of the A/R for any Non-MAGI Medicaid COA.

A parent who does not meet the enumeration requirement is included in the BG with his/her child(ren) for whom MAGI and Non-MAGI Family Medicaid eligibility is being considered.

A non-parent relative who does not meet the enumeration requirement is excluded from the BG.

=== CWFC Medicaid

A CWFC child who does not meet the enumeration requirement is ineligible.

=== Compliance

The individual is added to the AU the month the requirement is met, or Good Cause is established.

Use the following chart to determine required action when Good Cause is established, or the enumeration requirement is met following non-compliance.

.Chart 2220.2 - Good Cause in Enumeration
[#chart-2220-2,cols="2,4"]
|===
^| IF ^| THEN

| Good Cause is established at application processing

OR

Good Cause is established when an individual joins the AU
a| document the following:

* the AU's statement of the reason for non-compliance
* the reason for establishing Good Cause
* the offer of assistance in obtaining needed verification.

Include the individual in the AU and/or BG.

Monitor Good Cause assertion on a monthly basis by the following methods:

* contact the A/R in writing, in person, or by telephone
+
AND
* document the current status of the Good Cause determination.

| the non-enumerated individual meets the enumeration requirement

OR

establishes Good Cause
| document that the enumeration requirement has been met.

Add the individual to the AU/BG the month in which the requirement is met, or Good Cause is established.

| Good Cause assertion is denied at application processing
a| document the following:

* the AU's statement of the reason for non-compliance
* the reason for denial of the Good Cause claim
* the offer of assistance in applying for an SSN

Complete the application.
Do not include the non-enumerated individual in the AU.
Refer to <<chart-2220-1,Chart 2220.1>>.

Issue a notice to the AU.
Include the following information in the notice:

* the reason for the action
* the eligibility of the remaining AU members
* the action the individual must take to be added to the AU.

| Good Cause assertion is denied when an individual joins the AU
a| document the following:

* the AU's statement of the reason for non-compliance
* the reason for denial of the Good Cause claim
* the offer of assistance in applying for an SSN

Do not include the non-enumerated individual in the AU.

Issue a notice to the AU.
Include the following information in the notice:

* the reason for the action
* the eligibility and benefit level of the remaining AU members
* the action the individual must take to be added to the AU.

| AU member initially complies with enumeration but refuses to resolve discrepancies identified during the SSA validation process
a| document the following:

* the AU's statement of the reason for non-compliance
* the reason for denial of the Good Cause claim
* the offer of assistance in applying for an SSN

Do not include the non-enumerated individual in the AU.

Issue a notice to the AU.
Include the following information in the notice:

* the reason for the action
* the eligibility and benefit level of the remaining AU members
* the action the individual must take to be added to the AU.

| AU member is currently receiving benefits

OR

Good Cause for non-compliance no longer exists
a| document the reason Good Cause no longer exists.

Issue a timely notice to the AU.
The notice must include the following information:

* the reason for the action
* the eligibility of the remaining AU members
* the action the individual must take to be added to the AU.

Remove the non-enumerated individual from the AU effective the month following the month timely notice expires.
(Refer to <<chart-2220-1,Chart 2220.1>>.)
|===

Use the following chart to determine the procedures for various situations related to problems with SSNs.

.Chart 2220.3 – System Related Enumeration Problems
[#chart-2220-3,cols="2,4"]
|===
^| IF the A/R's SSN ^| THEN

| appears on the system generated SSN discrepancy listing
| research the case record to determine if the information regarding the A/R's full name, DOB and SSN matches information on the A/R's official documents.

Correct any information that is found to be in error.

Refer the client to SSA for corrective action if the SSA information is found to be the source of the error.

| matches with another SSN known to the system
a| screen and research both SSNs to determine which number on the system is correctly assigned.

NOTE: Contact with another DFCS office/county may be necessary.

Take action to have any erroneously entered SSNs corrected in the system.

OR

refer the A/R to SSA for corrective action if multiple individuals are verified to have been assigned the same SSN.

| is incorrect and is validated by the system
| contact the EMPI Help Desk at empi.helpdesk@dhs.ga.gov.
|===

== SSN Validation

=== Requirements

The system interfaces with the files at the Social Security Administration (SSA) to verify the accuracy of the SSN of an AU member.

=== Procedures

Follow the procedures in <<chart-2220-4>> to complete validation requirements.

.Chart 2220.4 - SSN Validation
[#chart-2220-4,cols="2,4"]
|===
^| IF an AU Member's SSN ^| THEN

| is valid
| The system will annotate the SSN with “Electronically verified by SVES”, “Electronically verified by SOLQ” or “Electronically verified by FDSH” (federally verified).

No further action is required.

| appears on the system generated enumeration or validation discrepancy lists
| determine if the AU member's full name, DOB and SSN matches information on the individual's official documents.

Correct any information that is in error.

Refer the A/R to SSA for corrective action if the SSA information is the source of the error.

| matches with another SSN known to the system
| determine which number on the system is correctly assigned.

Correct any SSNs erroneously entered in the system

OR

Refer the AU member to SSA for corrective action if multiple individuals are assigned the same SSN.

| is validated by the system but differs from verification (SSN card) obtained from the A/R
| follow the steps under How to Change a Validated SSN in this section.
|===

NOTE: Please note if the SSN is showing “Electronically verified by SOLQ” or “Electronically verified by FDSH”, generated by running the State Online Query or Federal Data Hub Services interfaces respectively, these are also considered federally verified SSNs.

=== How to Change a Validated SSN

[horizontal,labelwidth=10]
Step 1:: Gather the following case identifying information and report it in the order listed:

* worker's name
* worker's telephone number
* county, office, supervisor, user ID
* AU number
* AU name
* AU member's name
* AU member's ID number

Step 2:: Contact the EMPI Help Desk at empi.helpdesk@dhs.ga.gov.

Step 3:: Correct the SSN when the EMPI Helpdesk provides notification that the validation code has been removed.
