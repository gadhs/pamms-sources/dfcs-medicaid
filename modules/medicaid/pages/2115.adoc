= 2115 Disabled Adult Child (Public Law 99-643)
:chapter-number: 2100
:effective-date: February 2020
:mt: MT-58
:policy-number: 2115
:policy-title: Disabled Adult Child (Public Law 99-643)
:previous-policy-number: MT 15

include::partial$policy-header.adoc[]

== Requirements

Disabled Adult Child (PL 99-643) is a class of assistance (COA) that provides Medicaid for an individual age 18 or over who had his/her SSI terminated on or after 7/1/87 because of entitlement to or an increase in RSDI income received as a disabled adult child.

== Basic Considerations

To be eligible under the Disabled Adult Child COA, the A/R must meet the following conditions:

* The A/R is currently receiving RSDI as a disabled adult child.
* The A/R previously received SSI that was terminated on or after 7/1/87 because of an increase in or initial entitlement to RSDI as a disabled adult child.
The increase or initial entitlement must have been on RSDI (Title II) income, not RRR or other income
* The A/R is eligible for SSI if the initial entitlement to RSDI, any increase(s) in RSDI and/or RSDI COLAs received since the A/R last received SSI are disregarded.
* The A/R meets all basic and financial eligibility criteria.

NOTE: Length of Stay (LOS) and Level of Care (LOC) are not requirements for this COA.

NOTE: The RSDI claim number will end with a beneficiary identification code (BIC) that includes C if the A/R receives RSDI as a disabled adult child.

Approve Medicaid on the system using the Disabled Adult child COA if the A/R meets all the above eligibility criteria, including retroactive months if needed.

NOTE: Do not approve Medicaid using the Disabled Adult Child COA for any month for which the A/R was eligible for and received an SSI payment.

== Procedures

Follow the steps below to determine Medicaid eligibility under the Disabled Adult Child COA.

[horizontal,labelwidth=10]
Step 1:: Accept the A/R's Medicaid application.

Step 2:: Conduct an interview.

Step 3:: Obtain verification from SSA to verify the following:

* The date SSI benefits were terminated.
* The current amount of the A/R's RSDI disabled adult child benefit.
* The amounts of the RSDI initial entitlement, increase or COLA that caused SSI termination and all RSDI increases received since SSI was terminated.

Step 4:: Determine all basic eligibility criteria except LOS and LOC.
Refer to Chapter 2200, Basic Eligibility Criteria.

Step 5:: Determine financial eligibility using the current SSI income and resource limits.
Refer to Chapter 2500, ABD Financial Responsibility and Budgeting to determine the following:

* Whose income and resources to consider
* Which SSI income and resource limit (individual or couple) to use
* Which eligibility budget to complete.

Step 6:: Determine the A/R's countable income by disregarding the following amounts of RSDI income:

* The initial entitlement to or increase in RSDI as a disabled adult child or an increase in RSDI income that caused SSI termination
+
*OR*
* The RSDI disabled adult child COLA that caused SSI termination
+
*AND*
* All subsequent increases in RSDI.
This would include COLAs as well as RSDI increases due to a change in the parents' circumstances, such as retirement and/or death.
The only RSDI increase that would not be subject to disregard would be an increase due to the DAC's own work record.

Step 7::
+
--
NOTE: The RSDI claim number will end with a beneficiary identification code (BIC) that includes C if the A/R receives RSDI as a disabled adult child.

Approve Medicaid on the system using the Disabled Adult child COA if the A/R meets all the above eligibility criteria, including retroactive months if needed.

NOTE: Do not approve Medicaid using the Disabled Adult Child COA for any month for which the A/R was eligible for and received an SSI payment.
--