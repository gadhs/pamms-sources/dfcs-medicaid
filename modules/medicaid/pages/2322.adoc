= 2322 Life Estate and Remainder Interests
:chapter-number: 2300
:effective-date: July 2022
:mt: MT-65
:policy-number: 2322
:policy-title: Life Estate and Remainder Interests
:previous-policy-number: MT 64

include::partial$policy-header.adoc[]

== Requirements

The value of a life estate interest may or may not be a countable resource.
The value of a remainder interest is a countable resource for all ABD COAs.

For Family Medicaid COAs, life estate interest that an individual has a right to use, but not dispose of during his/her life, is excluded as a countable non-liquid resource.
However, consider any income received from the property.

== Basic Considerations

=== Life Estate

Under a life estate, an individual who owns property transfers ownership of that property to another individual while retaining, for the rest of his or her life (or the life of another person), certain rights to that property.

The owner of a life estate can sell the life estate but does not have full title to the property.
The life estate owner cannot sell the property or pass it on as an inheritance.

However, some states allow life estates with powers, wherein the owner of the property creates a life estate for himself or herself retaining the power to sell the property, with a remainder interest to someone else, such as a child.

=== NON-FBR and FBR COAs

If an A/R owns a life estate with powers, its resource value is its full equity value.

If an A/R owns a life estate with no powers, use the table on the following page to determine the resource value.

NOTE: If the property for which an A/R owns a life estate is the A/R's principal place of residence, apply the homeplace exclusion.

When the owner of the property gives it to one party in the form of a life estate and designates a second party to inherit it upon the death of the life estate holder, the second party has a remainder interest in the property.

If an A/R purchases a life estate interest in another individual's home the purchase price will be considered a transfer of resource, unless the purchaser resides in the home for at least one year (12 months) from the date of purchase (12 months must be consecutive).

When making the determination of whether the 12-month residency requirement was met look at factors like was the person's mail delivered there, did they pay property taxes etc.
Brief rehabilitation stays or vacations do not necessarily negate the residency, but this is a factual inquiry that must be conducted on the particular case.
If the A/R who purchased the life estate moves out prior to the end of the12 month period, a transfer of assets penalty must be imposed.
The uncompensated value is the full amount paid for the life estate as if the individual never moved into the home.

If purchase price was not for fair market value, then a transfer penalty must be applied.
If the purchaser's life expectancy is less than the value of the life estate or they make a gift of the life estate impose a transfer penalty.

=== Remainder Interest

The value of a remainder interest in non-homeplace property is a countable resource for all A/Rs.

*If an A/R transfers ownership of real property and retains life interest, he/she has transferred remainder interest.*
Consider a transfer of resources penalty on the value of the remainder interest.

=== Transfer of Assets Penalty

If an A/R transfers life estate interest, consider a transfer of assets penalty on the value of the life estate interest.
See xref:2342.adoc[].

== Procedures

[horizontal,labelwidth=10]
Step 1:: Obtain copies of legal documents which convey the life estate or remainder interest.

Step 2:: Determine if this has been the primary or only residence of the A/R and that it is not the purchase of a life estate in another person's property.
If the life estate meets this criteria, exclude the life estate interest value from countable resources.
If not, proceed to Step 3. Determine if a transfer of resource penalty should be applied for a remainder interest.

Step 3:: If the A/R has purchased a life estate interest in another's home, the following conditions must be met for the value of the life estate interest to be excluded from resources:
+
--
* The A/R must have resided in the home for a minimum of twelve months after the date of purchase.
If the A/R moves out of the home prior to the expiration of the twelve-month period, it is the same as if the A/R had never moved into the home.
* Determine if the purchase price of the life estate was for the fair market value.
* Determine if the person's life expectancy is equal to or greater than the value of the life estate purchased.
* The life estate must not be gifted to anyone.

If the life estate did not meet all of these criteria, impose a transfer of assets penalty.
Refer to xref:2342.adoc[].
--

Step 4:: Verify the CMV, minus any encumbrances, of any property in which an A/R owns a life estate interest, or any liquid asset in which an A/R owns a life estate interest, or in which an A/R owns a remainder interest.
Evaluate as a potential countable asset or transfer of asset.

Step 5:: If the life estate or remainder interest has a countable resource value that puts the A/R over the resource limit, close/deny the case.
If the life estate results in the imposition of a penalty, the penalty may result in denial/closure or in non-payment of the long-term care Medicaid services.
Refer to xref:2342.adoc[].

Use the following chart to determine the resource value of a life estate or remainder interest.
Multiply the CMV, minus any encumbrances, of the property by the life estate or remainder interest decimal that corresponds to the life estate interest holder's age.
Always use the life estater's age to determine the value of a life estate or remainder interest.

.Chart 2322.1 - Unisex Life Estate or Remainder Interest Table
[#chart-2322-1,width=50]
|===
| AGE | LIFE ESTATE | REMAINDER

| 0
| .97188
| .02812

| 1
| .98988
| .01012

| 2
| .99017
| .00983

| 3
| .99008
| .00992

| 4
| .98981
| .01019

| 5
| .98938
| .01062

| 6
| .98884
| .01116

| 7
| .98822
| .01178

| 8
| .98748
| .01252

| 9
| .98663
| .01337

| 10
| .98565
| .01435

| 11
| .98453
| .01547

| 12
| .98329
| .01671

| 13
| .98198.
| .01802

| 14
| .98066
| .01934

| 15
| .97937
| .02063

| 16
| .97815
| .02185

| 17
| .97700
| .02300

| 18
| .97590
| .02410

| 19
| .97480
| .02520

| 20
| ..97365
| .02635

| 21
| .97245
| .02755

| 22
| .97120
| .02880

| 23
| .96986
| .03014

| 24
| .96841
| .03159

| 25
| .96678
| .03322

| 26
| .96495
| .03505

| 27
| .96290
| .03710

| 28
| .96062
| .03938

| 29
| .95813
| .04187

| 30
| .95543
| .04457

| 31
| .95254
| .04746

| 32
| .94942
| .05058

| 33
| .94608
| .05392

| 34
| .94250
| .05750

| 35
| .93868
| .06132

| 36
| .93460
| .06540

| 37
| .93026
| .06974

| 38
| .92567
| .07433

| 39
| .92083
| .07917

| 40
| .91571
| .08429

| 41
| .91030
| .08970

| 42
| .90457
| .09543

| 43
| .89855
| .10145

| 44
| .89221
| .10779

| 45
| .88558
| .11442

| 46
| .87863
| .12137

| 47
| .87137
| .12863

| 48
| .86374
| .13626

| 49
| .85578
| .14422

| 50
| .84743
| .15257

| 51
| .83674
| .16126

| 52
| .82969
| .10731

| 53
| .82028
| .17972

| 54
| .81054
| .18946

| 55
| .80046
| .19954

| 56
| .79006
| .20994

| 57
| .77931
| .22069

| 58
| .76822
| .23178

| 59
| .75675
| .24325

| 60
| .74491
| .25509

| 61
| .73267
| .26733

| 62
| .72002
| .27998

| 63
| .70696
| .29304

| 64
| .69352
| .30648

| 65
| .67970
| .32030

| 66
| .66551
| .33449

| 67
| .65098
| .34902

| 68
| .63610
| .36390

| 69
| .62086
| .37914

| 70
| .60522
| .39478

| 71
| .58914
| .41086

| 72
| .57261
| .42739

| 73
| .55571
| .44429

| 74
| .53862
| .46138

| 75
| .52149
| .47851

| 76
| .50441
| .49559

| 77
| .48742
| .51258

| 78
| .47049
| .52951

| 79
| .45357
| .54643

| 80
| .43659
| .56341

| 81
| .41967
| .58033

| 82
| .40295
| .59705

| 83
| .38642
| .61358

| 84
| .36998
| .63002

| 85
| .35359
| .64641

| 86
| .33764
| .66236

| 87
| .32262
| .67738

| 88
| .30859
| .69141

| 89
| .29526
| .70474

| 90
| .28221
| .71779

| 91
| .26955
| .73045

| 92
| .25771
| .74229

| 93
| .24692
| .75308

| 94
| .23728
| .76272

| 95
| .22887
| .77113

| 96
| .22181
| .77819

| 97
| .21550
| .78450

| 98
| .21000
| .79000

| 99
| .20486
| .79514

| 100
| .19975
| .80025

| 101
| .19532
| .80468

| 102
| .19054
| .80946

| 103
| .18437
| .81563

| 104
| .17856
| .82144

| 105
| .16962
| .83038

| 106
| .15488
| .84512

| 107
| .13409
| .86591

| 108
| .10068
| .89932

| 109
| .04545
| .95455
|===
