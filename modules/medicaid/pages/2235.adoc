= 2235 Length of Stay for ABD Medicaid
:chapter-number: 2200
:effective-date: October 2022
:mt: MT-67
:policy-number: 2235
:policy-title: Length of Stay for ABD Medicaid
:previous-policy-number: MT 58

include::partial$policy-header.adoc[]

== Requirements

Length of Stay (LOS) is a basic eligibility requirement for the following ABD Medicaid CAP Classes of Assistance (COA):

* Elderly Disabled Waiver Program (EDWP) formerly known as Community Care Services Program (CCSP)
* Hospice Care
* Hospital
* Independent Care Waiver Program (ICWP)
* Nursing Home
* New Options Waiver (NOW)
* Comprehensive Supports Waiver Program (COMP)

An individual must remain in one of the above COAs for 30 continuous days to meet the LOS requirement.

== Basic Considerations

Once LOS is met, the Medicaid CAP is used to determine financial eligibility for each month the A/R resides in a Medicaid participating hospital or nursing home, or for each month the A/R receives either hospice services from an approved provider or a NOW/COMP, ICWP or EDWP waivered service.
If income exceeds the Medicaid CAP, refer to xref:2407.adoc[].

The LOS requirement is *not* applicable to individuals already receiving Medicaid at the time of admission to one of the institutions or programs listed above.

NOTE: Individuals receiving only Q Track COAs who enter one of these institutions or programs must meet the LOS requirement in order for the COA to be changed.
LOS may be assumed where permissible.
Refer to <<chart-2235-1,Chart 2235.1, Computing Length of Stay>>.

The LOS requirement is waived for individuals who die while residing in Living Arrangement D (LA-D).
Refer to PROCEDURES in this section for a list of LA-D situations.

The LOS requirement can be assumed to have been met before 30 continuous days of confinement has elapsed in certain situations.
Refer to <<chart-2235-1,Chart 2235.1, Computing Length of Stay>>.

== Procedures

Compute the LOS by adding the continuous days of confinement in LA-D, including days of confinement from a month in which an individual is ineligible for Medicaid.
The LOS requirement is met after 30 continuous days of LA-D confinement.

Consider the following to be LA-D confinement:

* Case management days in EDWP (CCSP) or ICWP
* Confinement in a Medicaid participating hospital or nursing home
* Enrollment in NOW/COMP
* Confinement in a non-Medicaid participating hospital or nursing home
* Confinement in an out-of-state medical institution
* Confinement in a state hospital
* Receipt of hospice services from an approved provider.

*Always* disregard the day of discharge when computing the LOS.

NOTE: If the LOS requirement is *not* met, complete a Continuing Medicaid Determination (CMD) to consider Medicaid eligibility under all COAs other than those using the Medicaid CAP.

Use the following chart to determine how to compute and verify the LOS requirement for specific situations:

[#chart-2235-1]
.Chart 2235.1 – Computing Length of Stay
|===
^| IF the A/R is in ^| THEN compute the LOS beginning with the day ^| AND the LOS requirement is

| EDWP (CCSP)
| of admission to care coordination
| assumed to have been met unless notification of discharge is received prior to approval of the case.

Verify by a Community Care Communicator from the EDWP (CCSP) Care Coordinator.

| Hospice care at home or in a nursing home
| of admission to hospice services
| assumed to have been met unless notification of discharge is received prior to approval of the case.

Verify by a Hospice Care Communicator (HCC), from the hospice care provider.

| a hospital
| of admission to a hospital
| met after 30 continuous days of hospital confinement.

Verify in writing or by a telephone contact with the hospital.

| ICWP
| of admission to case management services
| assumed to have been met unless notification of discharge is received prior to approval of the case.

Verify by an Independent Care Waiver Communicator from the ICWP case manager.

| NOW/COMP
| of admission to NOW/COMP (use enrollment date or date services begin)
| assumed to have been met unless notification of discharge is received prior to approval of the case.

Verify by an NOW/COMP Communicator from the CET.

| a nursing home or hospital swing bed
| of admission to a nursing home
| assumed to have been met unless notification of discharge is received prior to approval of the case.

Verify by Form DMA-59.

| any combination of the above situations
| of the first admission to LA-D, as long as the A/R goes directly from one LA-D to the other without interruption
| met using the requirement for the COA under which ongoing eligibility is approved.

Verify each of the admissions and discharges.
|===
