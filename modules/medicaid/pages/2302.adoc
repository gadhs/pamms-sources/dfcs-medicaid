= 2302 Ownership of Resources in ABD Medicaid
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2302
:policy-title: Ownership of Resources in ABD Medicaid
:previous-policy-number: MT 1

include::partial$policy-header.adoc[]

== Requirements

In order for the value of a resource to be applied to the resource limit, the A/R and/or deemor must have an ownership interest in the resource, and the A/R and/or deemor must have the legal right to the use and/or disposal of the resource.

== Basic Considerations

Ownership interests in property, whether real or personal, can occur in various types and forms.
Since the type and form of ownership may affect the value of property and even its status as a resource, they are significant in determining resource eligibility.

== Procedures

Use the following chart to determine the value of an A/R or deemor's ownership interest in a resource.

.Chart 2302.1 - Determining the Ownership of a Resource
[#chart-2302-1,cols="1,2,4"]
|===
^| Resource ^| Ownership Interest ^| Value

| Real Property
| Fee Simple
| Count the entire equity value to the A/R or deemor.

| Real Property
| Tenancy-In-Common
| Determine and count the A/R's or deemor's share of the equity value (each owner does not necessarily own equal shares).

| Real Property
| Joint Tenancy or Tenancy by the Entirety
| Divide the equity value by the number of joint owners (each owner owns an equal share).

| Real Property
| Life Estate
| Non-FBR A/R: Exclude total value.

FBR A/R: Use xref:2322.adoc#chart-2322-1[Chart 2322.1 - Unisex Life Estate or Remainder Interest Table] to determine value.
Refer to xref:2322.adoc[].

| Real Property
| Remainder Interest
| Divide the value of the remainder interest by the number of persons with a remainder interest.
Refer to xref:2322.adoc[].

| Unprobated Estate
| Heir Interest
| Will: Count the value of any resources left to the A/R or deemor until probated.

No Will: Use Georgia's Intestate Laws to determine the A/R's or deemor's share.
Refer to xref:2320.adoc[].

| Financial Instrument (savings or checking account, etc.)
| Joint
| Consider the financial instrument to be owned in equal shares by the Medicaid A/Rs whose names are listed as owners of the instrument.
Do not allow a share of the financial instrument to a Non-Medicaid owner.
If the A/R rebuts ownership of or unrestricted access to the financial instrument, refer to xref:2334.adoc[] for rebuttal procedures.
|===

NOTE: Any ownership interest in homeplace property is excluded.
