= 2121 Widow(er) 1984 (Public Law 99-272)
:chapter-number: 2100
:effective-date: February 2020
:mt: MT-58
:policy-number: 2121
:policy-title: Widow(er) 1984 (Public Law 99-272)
:previous-policy-number: MT 10

include::partial$policy-header.adoc[]

== Requirements

Widow(er) 1984 (PL 99-272) is a class of assistance (COA) for an individual who received RSDI and SSI concurrently and became ineligible for SSI due to an adjustment in his/her RSDI disabled widow(er)'s benefit effective 1/84.

== Basic Considerations

NOTE: Do NOT determine initial ABD Medicaid eligibility under the Widow(er) 1984 COA on any application filed on or after 7/1/88.
Complete annual reviews for continued eligibility on established cases as for any other COA.

To be eligible under the Widow(er) 1984 COA the A/R must meet the following conditions:

* The A/R received RSDI and SSI in 12/83.
* The A/R became entitled to and received an adjustment in his/her RSDI disabled widow(er)'s benefit effective 1/84 that caused SSI termination in the month the increased benefit was actually received.
* The A/R was continuously entitled to the increased RSDI benefit from 1/84 until the increase was actually received.
* The A/R is eligible for SSI if the 1/84 increase in the RSDI disabled widow(er) benefit and any subsequent COLAs are disregarded.
* The A/R meets all basic and financial eligibility criteria.

NOTE: Length of Stay (LOS) and Level of Care (LOC) are NOT requirements for this COA.

NOTE: Applications are no longer approved under this COA.

== Procedures

Complete an annual review of eligibility for individuals currently eligible under the Widower 1984 COA.
Refer to Section 2705, Reviews.

Use the following guidelines for completing the review:

[horizontal,labelwidth=10]
Step 1:: Obtain verification from the SSA to verify the following:

* The A/R's receipt of RSDI and SSI in 12/83.
* The A/R's receipt of an increased RSDI disabled widow(er)'s benefit effective 1/84.
* The A/R's ineligibility for SSI the month increased benefit was actually received.
* The A/R's continuous receipt of an RSDI disabled widow(er)'s benefit.
* The current amount of the RSDI widow(er)'s benefit and all subsequent COLAs.

Step 2:: Determine financial eligibility using the current SSI income and resource limits.
Refer to Chapter 2500, ABD Financial Responsibility and Budgeting, to determine the following

* Whose income and resources to consider.
* Which SSI income and resource limit (individual or couple) to use.
* Which eligibility budget to complete.

Step 3:: Determine the A/R's countable income by disregarding the following amounts of RSDI income:

* The 1/84 increase in the RSDI disabled widow(er)'s benefit.
* All subsequent COLAs.
+
NOTE: The RSDI claim number will end with a beneficiary identification code (BIC) that includes “W” if the A/R receives RSDI as a disabled widow(er)/disabled surviving divorced spouse.

NOTE: Do NOT approve Medicaid using the Widow(er) 1984 COA for any month for which the A/R was eligible for and received a SSI payment.
