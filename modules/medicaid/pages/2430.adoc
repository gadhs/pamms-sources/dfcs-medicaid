= 2430 Living Arrangement and In-Kind Support and Maintenance for ABD Medicaid
:chapter-number: 2400
:effective-date: February 2020
:mt: MT-58
:policy-number: 2430
:policy-title: Living Arrangement and In-Kind Support and Maintenance for ABD Medicaid
:previous-policy-number: MT 38

include::partial$policy-header.adoc[]

== Requirements

In-kind support and maintenance (ISM) is considered as unearned income when establishing financial eligibility for ABD Medicaid.
This policy does not apply to Family Medicaid.

== Basic Considerations

ISM is unearned income in the form of food or shelter provided to the Medicaid individual/couple or Medicaid child.

The value of ISM received by the Medicaid individual/couple is determined by subtracting the individual/couple's financial contribution toward household operating expenses from the value of food or shelter provided to the individual/couple by another individual.

The following two rules are used to value the ISM an individual/couple receives:

* the presumed maximum value (PMV) rule
* the value of the one-third reduction (VTR) rule.

=== Presumed Maximum Value (PMV) Rule

The PMV is one-third of the full FBR plus $20.00.

The PMV rule is used to value ISM received by a Medicaid individual / couple residing in Living Arrangement A or C (LA-A or C).

Under PMV, the value of ISM is determined as follows:

* The actual value (AV) of all food and shelter received by the household is determined.
* The ISM is included in the eligibility budget as unearned income using the AV or PMV, whichever is less.

=== Value of the One-Third Reduction (VTR) Rule

The VTR is one-third of the full SSI Federal Benefit Rate (FBR).

The VTR rule is used to value ISM received by a Medicaid individual/ couple residing in Living Arrangement B (LA-B).

The VTR is used if the Medicaid individual/couple meets both the following criteria:

* The individual lives throughout a month in another person's household.
* The individual receives both food and shelter from others living in that household.

Refer to Living Arrangement B (LA-B under PROCEDURES in this section) for more information on applying the VTR rule.

NOTE: The two rules for valuing ISM are mutually exclusive.
When the VTR rule is applied to ISM received in a month, the PMV rule cannot be applied to the same month.

=== Living Arrangement Codes

The Medicaid individual/couple's living arrangement is determined in order to select the appropriate means for placing a value on ISM (PMV or VTR).

There are four Living Arrangements: A, B, C and D.

The living arrangement is developed in the following order:

* D
* B
* A or C

[caption=Exception]
NOTE: Always consider Q Track and AMN individuals/couples to be residing in LA-A.
*DO NOT* develop ISM for Q Track and LA-D individuals/couples.
ISM must be developed for AMN A/Rs.

== Procedures

Follow the steps below to determine the amount of ISM to consider in the eligibility budget:

[horizontal,labelwidth=10]
Step 1:: Determine the Medicaid individual/couple's living arrangement (LA).

Step 2:: Value ISM based on the LA determined in Step 1.

* If the individual resides in LA-D, do not develop ISM.
* If the individual resides in LA-A or LA-C, develop the AV of ISM.
Include ISM not to exceed the PMV in the Medicaid eligibility budget.
* If the individual resides in LA-B, value ISM using the VTR.

=== Living Arrangement D (LA-D)

Consider a Medicaid individual whose income is under the Medicaid Cap and who resides in any of the following situations to be in LA-D:

* hospital confinement that meets LOS
* nursing home (NH) confinement
* receipt of CCSP, MRWP/CHSS or ICWP services at home
* receipt of hospice care
* receipt of GAPP services at home
* receipt of SSI or ABD Medicaid at home under a Deeming Waiver
* MRWP/CHSS

NOTE: DO NOT develop ISM for individuals residing in LA-D.

=== Living Arrangement A (LA-A)

Consider the Medicaid individual/couple to be residing in LA-A if any one of the following situations exists:

* The individual lives alone or with no adults other than his/her spouse.

* The individual has an ownership interest in his/her home.
* The individual has rental liability for his/her home.
* The individual lives in a Public Assistance (PA) household.
* The individual is a transient.
* The individual can show separate consumption of food.
* The individual can show separate purchase of food.
* The individual is sharing household expenses.
* The individual is earmarking his/her share of household expenses, known as earmarked sharing

=== Home Ownership

Consider the Medicaid individual/couple to have home ownership interest if s/he or his/her spouse has ownership interest of any of the following types:

* life estate interest
* partial ownership, such as a ½ undivided interest
* title or deed (full ownership)
* trust beneficiary
* unprobated estate interest
* warranty deed (the property is mortgaged).

==== Home Ownership Verification

Accept the individual's statement unless questionable.

=== Rental Liability

Consider the Medicaid individual/couple to have rental liability if the individual or his/her spouse has agreed to pay the landlord a specified amount periodically (monthly, weekly, etc.).

NOTE: Rental liability exists whether rent is actually being paid or if rent paid is less than the current market rental value as long as the agreement is still in effect.

==== Rental Liability Verification

Accept the individual's statement of rental liability if s/he lives alone or with his/her spouse and/or dependent child.
Verify rental liability for all other living situations.

=== Public Assistance Household

Consider the Medicaid individual/couple to live in a public assistance household (PA household) if each household member receives one of the following types of income:

* Temporary Assistance for Needy Families (TANF)
* Bureau of Indian Affairs general assistance programs
* Payments based on need which are provided under state or local government income maintenance programs
* Payments under the Disaster Relief Act of 1974
* Payments under the Refugee Assistance Act of 1980
* Supplemental Security Income (SSI)
* Veteran's Administration (VA) benefits that are based on need.
+
NOTE: Effective November 1, 1981, when a VA pension or compensation based on need includes an augmentation for a dependent, the dependent's portion of the VA payment is counted as income to him/her.
Such dependents are considered public assistance recipients.

==== Public Assistance Household Verification

Verify that all household members receive public assistance.

=== Transient

Consider a Medicaid individual to be a transient if s/he has no permanent living arrangement.

==== Transient Verification

Accept the individual's statement as verification.

=== Separate Consumption of Food

Consider separate consumption of food to exist when all the following conditions are met:

* The Medicaid individual/couple lives in a household with at least one other person other than a spouse, child, or person whose income is deemed to the individual.
* The individual does not have ownership interest or rental liability in the home.
* The individual does not live in a PA household.
* The individual, or at least one member of a couple, alleges eating no meals in the household during the month.

==== Separate Consumption of Food Verification

Obtain the individual's signed statement regarding separate consumption and verify the allegation with a knowledgeable adult member of the household other than the individual's spouse.

=== Separate Purchase of Food

Consider a separate purchase of food exists when all of the following conditions are met:

* The Medicaid individual/couple lives in a household with at least one person other than a spouse, child, or person whose income is deemed to the individual.
* The individual does not have ownership interest or rental liability in the home.
* The individual does not live in a PA household.
* The individual, or both members of a couple, eats meals in the household during a month.
* The individual, or at least one member of a couple, alleges buying his/her food apart from the food of other household members.

==== Separate Purchase of Food Verification

Obtain the individual's signed statement regarding separate purchase of food and verify the allegation with a knowledgeable adult member of the household other than the individual's spouse.

=== Sharing

Consider the Medicaid individual/couple to be sharing when all the following conditions are met:

* The individual lives in a household with at least one person other than a spouse, child, or person whose income is deemed to the individual.
* The individual does not have ownership interest or rental liability in the home.
* The individual does not live in a PA household.
* The individual, or both members of a couple, does not separately consume his/her food.
* The individual, or both members of a couple, does not separately purchase his/her food.
* The individual with ownership interest or rental liability makes a contribution toward the household operating expenses.
+
--
*OR*

the individual without ownership interest or rental liability makes a contribution toward any expense of the person with ownership interest or rental liability, such as household operating expenses, credit card payments, telephone bill or furniture bill.
--

Allowable household operating expenses include the following:

* food
* mortgage (including property insurance required by the mortgage holder)
* rent
* real property taxes
* heating fuel
* gas
* electricity
* water
* sewage
* garbage removal

NOTE: The use of land alone is not a shelter cost.
This means that an item such as a trailer space rental fee that does not include water, sewage, etc., is not a household operating expense for purposes of determining sharing.

Follow the steps below to perform a sharing computation:

[horizontal,labelwidth=10]
Step 1:: Determine the average household operating expenses.

Step 2:: Determine the household composition.

Step 3:: Determine the individual's pro rata share of household expenses by dividing the household operating expenses by the number of household members.
+
Assume all other members of the household share in the food expense unless information is obtained to the contrary.
If another member of the household does not share in the food, determine separate pro rata shares for food and shelter and add them together to determine the individual's pro rata share.

Step 4:: Determine the individual's average monthly contribution toward household expenses.

Step 5:: Compare the contribution to the pro rata share.
+
--
If the contribution is within $5 less than or greater than the pro rata share of expenses, consider the contribution and the pro rata share to be equal, and consider the individual to be sharing.

NOTE: When computing sharing for a Medicaid couple, subtract the couple's contribution from the pro rata share of household expenses multiplied by 2 to determine if sharing exists.
--

==== Sharing Verification

Obtain signed statement(s) of household expenses and contributions to establish that an individual is sharing.

=== Earmarked Sharing

Consider the Medicaid individual/couple to be earmarked sharing when all the following conditions are met:

* The individual lives in a household with at least one person other than his spouse, child, or a person whose income is deemed to the individual.
* The individual does not have ownership interest or rental liability in the home.
* The individual does not live in a PA household.
* The individual, or both members of an eligible couple, does not separately consume his/her food.
* The individual, or both members of a couple, does not separately purchase his/her food.
* The individual does not contribute within $5 of his/her pro rata share of household operating expenses for food and shelter.
* The individual, or at least one member of a couple, alleges earmarking part or all of his/her contribution toward the household food or shelter expense.

==== Earmarked Sharing Computation

Verify household expenses and compute earmarked sharing in the same manner as sharing, comparing the individual's contribution toward the earmarked expense to his/her pro rata share of the expense.
However, do not allow a $5 tolerance for earmarked sharing.

==== Earmarked Sharing Verification

Use the verification procedures for sharing.

=== Double Earmarking

When an individual earmarks a specific portion of his/her contribution for food and another specific portion for shelter, it is called double earmarking.

Compute the individual's pro rata share of food expenses and compare it to the portion of the contribution earmarked for food.

Compute the individual's pro rata share for shelter expenses and compare it to the portion of the contribution earmarked for shelter.

If either earmarked contribution equals or exceeds a pro rata share of the item for which it is earmarked, consider earmarked sharing to exist.
The individual is receiving ISM in the form of the item for which s/he is not earmarking.
Value this ISM under the PMV rule.

==== Double Earmarking Verification

If the individual makes a contribution, verify the contribution as follows:

* Obtain the individual's statement regarding earmarking.
* Obtain an additional signed statement from a knowledgeable adult member of the household other than the individual's spouse.
This statement should confirm the amount of the earmarked contribution and the household operating expenses for food or shelter, or both if these expenses were not obtained for a sharing determination.

NOTE: If evidence of household operating expenses and the earmarked contributions cannot be obtained, consider the individual to be residing in LA-B.

=== Living Arrangement C (LA-C)

Consider a Medicaid individual to be residing in LA-C if all of the following conditions exist:

* The individual is a disabled child under age 18.
* The individual lives with his/her parent(s).
* The individual's parents have ownership interest or rental liability in the home.

NOTE: A disabled child residing at home under the Deeming Waiver or Model Waiver class of ABD Medicaid is considered to be residing in LA-D.

=== ISM for an Individual Couple in LA-A or C

If a Medicaid individual or couple in LA-A or LA-C receives an item(s) of food or shelter during the month from an individual(s) other than:

* spouse or dependent children for LA-A
* parents or minor siblings for LA-C, place a value on this item(s) and include the value in the eligibility budget as ISM for the month of receipt.

=== PMV Rule

Use the PMV rule when an individual in LA-A or C receives ISM.

==== Rebuttal of the PMV Rule

Use of the PMV rule differs from use of the VTR in that an individual may rebut the value assigned to the PMV.
If the individual produces evidence that establishes the AV of the ISM is lower than the PMV, use the AV as the value of the ISM.

=== Eligible Expense for Computing ISM

To compute ISM for a Medicaid individual/couple in LA-A or C, use the household operating expenses listed under Sharing in this section.

NOTE: The use of land alone is not a shelter cost.
This means that an item such as a trailer space rental fee which does not include water, sewage, etc., is not a household operating expense for purpose of determining inside ISM, nor is it an item of outside ISM if someone outside the household pays the fee.
If the fee is not for use of land alone, that part of the fee for water, sewage, etc., is part of the household operating expenses.

==== Eligible Expense for Computing ISM Verification

Obtain a signed statement(s) of household expenses.

=== Types of ISM

Consider the following two types of ISM for a Medicaid individual/ couple in LA-A or LA-C:

* Inside ISM is ISM received from other members of the household in which the individual resides.
* Outside ISM is ISM received from someone outside of the household.

[caption=Exception]
NOTE: Do not develop Inside or Outside ISM for Q Track individuals/couples.

==== Inside ISM

Develop Inside ISM for a Medicaid individual/couple in LA-A only if the basis for residing in LA-A is one of the following:

* ownership
* rental liability
* separate consumption of food
* separate purchase of food
* earmarked sharing

Develop Inside ISM for a Medicaid individual in LA-C only when there are persons residing in the home other than the individual and his/her parents and other minor children.

Compute Inside ISM in the following manner:

* Determine the total household operating expenses.
* Divide the total household operating expenses by the number of household members to determine the individual's pro rata share of household expenses.
* Deduct the individual's or couple's contribution from the individual's or couple's (individual share multiplied by 2) pro rata share to determine AV of the Inside ISM.

==== Outside ISM

Develop Outside ISM for all Medicaid individuals/couples residing in LA-A or LA-C.

Compute Outside ISM in the following manner:

* Use the current market value (CMV) of the shelter or food paid by someone outside of the household.
* Deduct from the CMV any payment made by household members toward that item.
* Divide the balance by the number of household members to obtain the AV of the ISM to the individual.

==== Total Inside and Outside ISM

Total the AVs of the Inside and Outside ISM.
Use the total AV or the PMV, whichever is less, as the value of ISM to the Medicaid individual/ couple.

=== Living Arrangement B (LA-B)

Consider the Medicaid individual/couple to be residing in LA-B if both of the following conditions exist:

* The individual lives in the household of another.
* The individual is not residing in LA-A or C (because they are not paying their fair share of household expenses) or D.

=== VTR

If the individual is determined to be in LA-B, value ISM using the VTR.
The VTR is equal to one third of the individual or couple FBR for LA-A.

==== FBR for LA-B

Use the FBR for LA-B in order to account for the VTR.

==== Rebuttal of the VTR

Use of the FBR for valuing ISM cannot be rebutted by the A/R.

== Special Considerations

=== ISM to a Child in LA-C

When computing ISM for a child in LA-C, apply the parent(s)' contributions to their own pro rata share(s) of household operating expenses first.
Apply any amount of their contributions exceeding their pro rata share(s) of household operating expenses to their child's pro rata share of household operating expenses.

If the child lives with only his/her parent(s) and other minor children, develop Outside ISM only.

If the child lives with other adults in addition to his/her parent(s), develop Inside and Outside ISM.

NOTE: Do not deem ISM received by the parent(s) to the child in the Parent to Child Deeming budget.

=== ISM is a Result of a Third-Party Vendor Payment

When a third-party payment from outside the household is made directly to a vendor for an item of food or shelter, it results in ISM to the Medicaid individual/couple.

Include ISM received as the result of a third-party vendor payment as income for the month the food or shelter is available to the individual to use.

If a vendor extends credit to the individual and the third party pays for (or makes a payment on) food or shelter, include the value of the ISM as income for the month the payment is made.

NOTE: Include the ISM only in the month when the third party actually makes the payment, even though the individual may have received the food or shelter in a previous month.

=== Rent Free Shelter

Rent free shelter is a type of ISM in the form of shelter provided by someone outside the household to a Medicaid individual/couple residing in LA-A.

NOTE: Shelter that is income in return for services is not rent-free shelter.

Consider rent-free shelter to exist when no household member has ownership interest or rental liability for the dwelling in which the individual/couple lives.

NOTE: If there is an agreement to pay rent, consider rental liability to exist, even if the individual is not currently making rental payments.

The use of land alone is not rent-free shelter.

=== Rental Subsidy

A rental subsidy is a type of ISM in the form of subsidized shelter provided by someone outside the household to a Medicaid individual/ couple residing in LA-A.

Develop rental subsidy only when any household member has rental liability and a household member is the parent or child of the landlord.

Determine the amount of rental subsidy as follows:

* Compare the current market rental value (CMRV) of the dwelling to the actual amount of rent paid under the rental agreement.
* If the rent paid is less than the CMRV of the dwelling, consider all household members to be receiving a rental subsidy.
* Pro rate the value of the rental subsidy (CMRV minus rent paid) among all members of the household, including members who are ineligible or temporarily absent.
* Include the Medicaid individual's pro rata share of the rental subsidy as ISM not to exceed the PMV.

Do NOT consider the following as a rental subsidy:

* public housing assistance which is supplied by a state agency based on need
* public housing assistance excluded by federal statute, such as a HUD subsidy
* rental subsidies excluded under a plan for achieving self-sufficiency (PASS)
* rent/mortgage payments made under the terms of a credit life or credit disability policy.

=== No ISM Charged

Do not charge ISM if a Medicaid individual/couple receives food or shelter that meets any of the following criteria:

* It is specifically excluded by federal law, such as the Disaster Relief Act of 1974.
* It meets the criteria for exclusion of infrequent or irregular unearned income.
* It has no CMV.
* It is provided under a government (federal, state or local) medical or social service program.
* It is ABON from a state or one of its political subdivisions.
* It is food or shelter received at school by a child under age 22 who receives food or shelter only at school while temporarily absent from his/her parent's household.
* It is food or shelter received during a temporary absence.
* It is a replacement of a lost, damaged or stolen resource in the form of food or shelter, including temporary housing.
* It is provided by someone living in the same household whose income is subject to deeming to the individual.

NOTE: ISM received by a deemor is not deemed to the Medicaid individual.

=== Food, Clothing, Shelter which is Remuneration for Work

Refer to Section 2405, Treatment of Income, for a discussion of the treatment of food, clothing, or shelter which is remuneration for work.

=== Documentation

Document the ISM details in the system.
