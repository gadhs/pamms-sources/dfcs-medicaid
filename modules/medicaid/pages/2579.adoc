= 2579 SSI 1619 Individuals
:chapter-number: 2575
:effective-date: February 2020
:mt: MT-58
:policy-number: 2579
:policy-title: SSI 1619 Individuals
:previous-policy-number: MT 6

include::partial$policy-header.adoc[]

== Requirements

Section 1619 of Title XIX of the Social Security Act entitles certain blind and disabled individuals to receive SSI while employed.

== Basic Considerations

PL 99-643 created an exception to the rules regarding the treatment of SSI income for individuals eligible for SSI under Section 1619(a) or 1619(b) who entered a nursing home (NH) or CCSP or who remain in LA-A, B or C.

These NH or CCSP individuals will remain eligible for SSI for the first two months in a NH or CCSP.
The individual's SSI income is not considered in determining patient liability.
Individuals who are not in LA-D may be able to continue their Medicaid eligibility under 1619(a) or (b).

Individuals eligible for SSI under Section 1619(a) and (b) are those disabled or blind individuals who are employed and have earned income the month preceding the month of admission to a NH or CCSP or ongoing for those individuals who are not in LA-D.

=== 1619(a)

Individuals eligible under 1619(a) have earned income of less than the Substantial Gainful Activity (SGA) amount.

=== 1619(b)

Individuals eligible under 1619(b) have earned income of more than the SGA.
These individuals have too much income to receive an SSI payment but still receive Medicaid under an SSI ID number.

SSI recipients who are employed and have earnings over the SGA may continue to receive SSI and Medicaid under the authority of SSA Section 1619(a) and, if their earnings exceed the breakeven point, may continue to receive Medicaid (no SSI payment) under the authority of Section 1619(b).

There are also unearned income limits that are considered in calculating the break-even point.
Unearned income includes SSI, SSDI and Railroad Retirement Benefits.
Therefore, the exact break-even point varies depending on an individual's combination of earned and unearned income.

Refer to Appendix A for the current SGA and break-even point.

== Procedures

=== LA-A, B, or C Individuals

If a working disabled A/R reports that s/he lost SSI eligibility as a result of earnings and is not institutionalized, s/he may have been terminated in error.
In that situation, refer the A/R to SSA for eligibility determination under Section 1619(b).

If an individual's earned income will result in terminating medical assistance eligibility, determine if the individual has advised SSA and if SSA has made a decision about Section 1619 status.
If not, advise the individual to contact the SSA.
Individuals may find it helpful to obtain information about 1619 work incentives by first contacting the Benefits Planning, Assistance & Outreach Program at The Shepherd Center in Atlanta at:

[%hardbreaks]
Toll Free: 1-866-SSA-BPAO
1-866-772-2726
TTY: 404-367-1347

The SSA automatically puts an SSI recipient in Section 1619(a) status when their earnings are greater than the SGA amount but less than the break-even point (BEP).
The SSI check does not show that they are in Section 1619(a) status.
The only change will be a reduction in the amount of their SSI check.

The SSA puts an SSI recipient in Section 1619(b) status when their earnings are greater than the BEP but less than the state threshold amount.

The recipient should receive a notice from the SSA, and their SSI checks will stop.
The notice should include the term “1619(b)” and state that the individual will still get Medicaid even though their SSI checks have stopped.

When the SSA notifies DCH of an individual's 1619(b) eligibility, Medicaid continues automatically through the DCH system.
It is not necessary for DFCS to take any action when SSA has placed the person in 1619(b) status and DCH has been notified by the SSA.

=== LA-D Individuals

Verify the A/R's status under Section 1619 by requesting verification from SSA for any A/R who was employed the month prior to entering a NH or CCSP.

Do not include any of the SSI income in the patient liability budget.
Treat all other income under the usual rules for determining patient liability.

Authorize the vendor payment to the NH.
Refer to Section 2576, Vendor Payment Authorization.
