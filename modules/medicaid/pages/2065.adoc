= 2065 Family Medicaid Application Processing
:chapter-number: 2050
:effective-date: September 2024
:mt: MT-73
:policy-number: 2065
:policy-title: Family Medicaid Application Processing
:previous-policy-number: MT 70

include::partial$policy-header.adoc[]

== Requirements

The Family Medicaid application process begins with the request for health coverage and ends with notification to the Assistance Unit (AU) of its eligibility status.

== Basic Considerations

=== Order of Eligibility

Family Medicaid eligibility is determined in the following order:

* Newborn
* Pregnant Women Medicaid
* Parent/Caretaker with Child(ren) Medicaid
* other Family Medicaid COAs based on Parent/Caretaker with Child(ren) eligibility criteria, i.e., TMA, 4MEx
* Children Under 19 Years of Age Medicaid
* PeachCare for Kids®
* Family Medicaid Medically Needy
* Women's Health Medicaid (WHM)
* Pathways
* Planning for Healthy Babies (P4HB)
* Federally Facilitated Marketplace (FFM)

NOTE: Medicaid eligibility for a child in foster care is determined first under the IV-E FC program.
If ineligible under IV-E FC, Medicaid eligibility is determined under CWFC Medicaid.
Refer to Chapter 2800, Assistance to Children in Placement.

=== Application Requirements

An application for any Family Medicaid class of assistance may be made with any of the following forms:

* Form 297 Application for TANF, SNAP, or Medical Assistance
* Form 508 SNAP, TANF, Medical Assistance Renewal Form
* Form 632 Presumptive Eligibility (PE) for Pregnancy
* Form 632H Qualified Hospital Presumptive Eligibility Application
* Form 632W Presumptive Eligibility (PE) Women's Health Medicaid Application
* Form 700 Application for Medicaid & Medicare Savings for Qualified Beneficiaries
* Form 94 Medicaid Application
* Form 94A Medicaid Streamlined Application
* Federally Facilitated Marketplace (FFM) application
* Gateway Medical Assistance Online Application
* Gateway Medical Assistance Renewal
* Low Income Subsidy Application – SSA 1020B (LISA- application for Medicare Part D)
* PeachCare for Kids® Application *(Obsolete as of 09/2017)*
* Provider Portal Online Application
* SUCCESS Application for Assistance (AFA) *(Obsolete as of 09/05/2017)*

A completed application consists of a signed (either written or electronic such as on a  Gateway application) with information sufficient to contact the A/R or authorized representative (AREP).  The signature does not necessarily have to be that of the A/R.
Any information that is missing, incomplete or otherwise unclear may be obtained from the A/R or AREP after the signed application is received and registered in the system by the agency.

A new signed application is required in the following situations:

* When completing an add a program for a BG member who is now requesting Medicaid and had not requested coverage for him/herself on the last application filed.
* An application was previously correctly denied due to failure to provide required verification.
Applicant wants to reapply in a subsequent month for ongoing benefits.
Although the application date of the first application is protected, the applicant should sign another application unless there is good cause for not initially providing the verification.
* An application was previously correctly denied for not meeting a basic or financial eligibility criteria.
A/R now meets the criteria and wants to reapply for ongoing benefits.
Have the applicant complete and sign another application.
* An applicant applied for him/herself and children, but the case trickled to a lower COA.
A change then occurs that would make him/her eligible.

A new application is NOT required in the following situations:

* If the system denies the application because the worker has not acted timely on the case.
* If the applicant is already a Medicaid recipient and is changing to another COA, or a continuing Medicaid determination (CMD) is being completed.
* Adding a Newborn Medicaid case
* If a BG member is being added.
* When removing the Reasonable Opportunity Period (ROP) penalty, or adding a child back, to the active Medicaid AU effective the first day of the month that citizenship/immigration status/identity verification was provided.
* MAGI Medicaid cases (except for P4HB) that close for failing to return verification will be reinstated if all the verification is returned within 90 days of closure; an application will not be required.
Refer to xref:2706.adoc[Section 2706 - Medicaid Renewals].

NOTE: Homeless AUs are *NOT* required to provide a physical address but must provide sufficient information to establish Georgia residency.
The applicant's statement is acceptable unless conflicting information is known to the agency.

== Procedures

=== Application Screening

Screen the application in Gateway and Georgia Medicaid Management Information System (GAMMIS) to determine the following:

* current receipt of the benefits for which the AU is applying
* current receipt of other benefits.

=== Interview Requirements

A face-to-face (FTF) interview is *not* required for any Medicaid COA.
At the eligibility worker's (EW) discretion or the request of the applicant or PR, a FTF interview may be scheduled, however an application may *not* be denied for failure to appear for an interview.

The A/R is considered to be the primary source of information.
The A/R may authorize an Authorized Representative (AREP) to apply and provide information on his/her behalf. An elected AREP may have verbal or written designation. If the designation is written, the applicant's signature is required. However, because the A/R is considered the best source of information, s/he must be contacted to confirm that the information obtained is correct.
If information provided by a AREP is questionable or unclear, attempt to contact the A/R for clarification, unless contact with the A/R is precluded by physical or mental limitations. This can be accomplished either by telephone, mail, fax or in-person.

Information necessary to complete an eligibility determination may be obtained by any of the following methods:

* FTF interview
+
NOTE: A FTF interview may *not* be required of the applicant or AREP and an application may *not* be denied solely for failure to complete a FTF interview.
* telephone call
* mail
* home visit
* facsimile
* secure email
* Gateway

Orally or in writing, inform the A/R about the Medicaid program(s) for which s/he may be entitled.
Provide relevant information pamphlets or other printed material.
Explain the following information to the applicant/member or AREP:

* services provided by DFCS and how to obtain those services
* requirements of eligibility and the A/R's responsibility to provide correct information to establish eligibility
* HIPAA and confidentiality
* basic and financial eligibility requirements
* Clearinghouse requirements for any AU/BG member
* potential Medicaid COAs
* potential coverage for three months prior to the month of application
* periodic renewals (12-month renewals and renewals competed during an interim change)
* timely reporting of changes and how/where changes are to be reported
* assignment of Third-Party Liability (TPL)
* the role of the Division of Child Support Services (DCSS), assignment of medical support rights to the State, and Good Cause for non-compliance
* Early and Periodic Screening, Diagnostic, and Treatment (EPSDT) Referrals to anyone in the AU under the age of 21.
Information regarding EPSDT and contact information is included on the GA Gateway approval notice.
Refer to xref:2930.adoc[Section 2930 - Early and Periodic Screening, Diagnostic, and Treatment (EPSDT)].
* Woman Infant and Children (WIC) Referrals to anyone in the AU that is pregnant; breastfeeding; postpartum women; or has a child under the age of 5 years, xref:2985.adoc[Section 2985 - Women, Infant and Children (WIC) Services].
* the applicant's right to the following:
** a fair hearing (Refer to Appendix B for details),
** a decision within standard of promptness (SOP),
** confidentiality,
** non-discrimination in the processing of the application,
** their rights and responsibilities in the Medicaid program, included in the single streamlined application or xref:appendix-f/form-297a.adoc[Form 297A Rights and Responsibilities].

In addition, explain the following to an AU that includes a pregnant woman:

* the right to apply and how to apply for TANF 45 days prior to the expected date of delivery
* continuous financial eligibility for the pregnant woman
* presumptive eligibility (PE) Medicaid process and how to apply at a public health facility or other qualified provider if the Medicaid eligibility determination for the pregnant woman cannot be made the same day that the application is filed.
Accept the Medicaid application even if the applicant applies for PE Medicaid.

=== Mandatory Forms

Refer the A/R to other appropriate services such as family planning.

Complete the mandatory forms below when processing a Family Medicaid application:

* Application for Assistance

* Form 297-A (*only* if Form 297 is used to apply).

* xref:attachment$form-216.docx[Form 216 - Declaration of Citizenship] (conditionally mandatory, please see Note below)
+
NOTE: This form is not required if the single streamlined application, Form 94 (Rev. 5/10 or later), Form 94A, 297, 508 (Rev. 5/12 or later), 700 (Rev. 11/09 or later), Gateway Medicaid Online application, or FFM application are used as they contain the required language to meet the needs of the declaration.

* xref:attachment$form-5460.docx[Form 5460 - Notice of Privacy Practices (HIPAA)]
+
NOTE: Notice of Privacy Practices and Form 297-A may be mailed to the applicant.
The applicant is *NOT* required to sign and return either form, provided the case record is documented that the forms were sent.

* Form DMA 285, Third Party Liability Health Insurance Questionnaire, when the person has other health insurance coverage.
See xref:2230.adoc[Section 2230 - Third Party Liability] for when a DMA285 can be waived, and for other TPL requirements.
+
[caption=Exceptions]
NOTE: A DMA 285 is not required for children in placement.
For MAGI Family Medicaid COAs a separate DMA 285 is not required if the Form 94A Medicaid streamlined application, 297M, or pre-populated renewal form 508M has the TPL information included, and the form is signed.

* xref:attachment$form-138.docx[Form 138 - Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate in Child Support Services] (if a DCSS referral is required)
+
NOTE: Form 138 may be mailed to the applicant.
The applicant is *NOT* required to sign and return the form provided the case record is documented that the form was sent. If the application or renewal is completed using Gateway Medical Assistance Application, Gateway Medical Assistance Renewal, or Form 94A Medical Assistance Streamlined Application, then Form 138 is not required as the 138's language is included.

When the Form 94A Medicaid streamlined application is used it also incorporates the Declaration of Citizenship/Immigration Status and TPL; separate forms are not required.

Complete any other forms as necessary depending on the COA and the A/R's circumstances.

=== Other Required Actions

Determine if the A/R meets all points of eligibility.

Complete mandatory clearinghouse requirements.

Follow appropriate documentation standards for Family Medicaid.

Explore Medicaid eligibility for the three prior months.

Obtain required verification.

Refer to <<chart-2065-1,Chart 2065.1 - Family Medicaid Forms>> in this section.

=== Standard of Promptness

The eligibility determination for Family Medicaid COAs should be completed with real time determinations or as soon as all verification is received.
This should take no longer than the following time frames:

* 10 days from the date of application for pregnant women, regardless of COA
* 45 days for EMA-PgW
* 10 days from the date of report for newborns, regardless of COA

NOTE: This pertains to a child born to a woman who was eligible for and receiving Medicaid on the day the child was born who most commonly is approved for Newborn COA but could be approved/dual eligible for another COA, such as Parent/Caretaker.

* 45 days from the date of application for all other Family Medicaid COAs

Calculate the SOP beginning with the date of application.
Document the reason for any delays in the case record.

If the SOP date falls on a weekend or holiday, complete the application by the last workday *prior to* the weekend or holiday.

=== Application Processing Standards

Observe the following standards in processing Family Medicaid applications.

* Accept the signed application on the day the application is received by the agency.
* Register the application using the date the application was received by the agency.
The application must be registered within *24 hours* of receipt by the agency.
* If the applicant or AREP is not interviewed on the same day an application is filed and additional information is required, contact the applicant or AREP within a reasonable timeframe to obtain the information necessary to complete the application.
* If verification or additional information is required, complete a verification checklist and mail or give to the applicant or AREP.
Establish a reasonable deadline for returning requested verification.
Refer to xref:2051.adoc[Section 2051 - Verification].
* If incomplete verification is returned, send another checklist specifying what is required;
establish a new reasonable deadline for returning requested verification.
* If the applicant or AREP fails to meet the deadline for providing additional information, attempt to contact the applicant or AREP to assess the need for an extension of the deadline or the possibility of assisting in obtaining required verification.
+
NOTE: Do *not* deny an application for failure to provide verification if the verification can be obtained by the Case Worker.

* Deny an application at the first point ineligibility is established.
Do *not* leave a case pending in anticipation of the applicant becoming eligible at a future date beyond the ongoing benefit month.
* Do *not* deny an application solely because the SOP has been reached and eligibility cannot yet be determined.
* Deny an application before the SOP if the applicant or AREP fails to cooperate in the application process or fails to supply necessary information that s/he is capable of obtaining and DFCS has no direct means of obtaining.

=== Disposition of Application

Determine if the AU meets all points of eligibility.

Process applications in chronological order, with the exception of Medicaid coverage for pregnant women, based on the following:

* date of application
* whether all information is available to determine eligibility

If eligible, approve the application, within 45 days, for all eligible months including retroactive and ongoing months.
Process applications for pregnant women within 10 days to ensure early prenatal care.

=== Notification

Provide the applicant adequate written notification of the eligibility determination.
Adequate notification includes the reason(s) for any action taken.

A duplicate notice may be provided to the AREP upon request by the applicant.
The applicant, however, must receive all notices regarding his/her case(s).

Notification must explain the following:

* the basis for the approval/denial/termination
* the period of eligibility
* the reason for the action
* the AU's right to request a fair hearing (Refer to Appendix B for details)
* the telephone number of the DFCS Call Center
* the telephone number of legal services.

Generic denial reasons may be used as a secondary or tertiary denial/termination reason, but never as the sole reason for denial/termination.

=== Periods of Eligibility

Approve Medicaid and continue eligibility as long as the AU continues to meet the requirements of the COA under which eligibility was approved.
A Continuing Medicaid Determination (CMD) must be completed prior to denial or termination of any Medicaid COA.
Refer to xref:2052.adoc[Section 2052 - Continuing Medicaid Determination].

NOTE: Certain COAs are time limited.
Refer to Chapter 2100, Classes of Assistance.


[#chart-2065-1]
Chart 2065.1 - Family Medicaid Forms

image::2065.1 chart.jpg[]



[#declaration-note]
NOTE: *xref:attachment$form-216.docx[*Form 216 - Declaration of Citizenship/Immigration status] is not required if the A/R has declared their citizenship or immigrations status on the eligibility application (94, 94A, 297, 508, 700, or via Gateway Customer Portal application) and signed application under penalty of perjury.
Also, declaration of citizenship/immigration status/identity is not required if the A/R is determined under EMA procedures.
Refer to xref:2215.adoc[Section 2215 - Citizenship/Immigration/Identity].
*xref:appendix-f/form-138.adoc[*Form 138, Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate in Child Support Services] is not required if the application or renewal is via Gateway or 94A as the 138's language is included in both.  Refer to xref:2250.adoc[Section 2250 - Cooperation with Division of Child Support Services]*.
