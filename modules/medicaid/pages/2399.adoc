= 2399 Treatment of Resources by Resource Type Chart
:chapter-number: 2300
:effective-date: June 2024
:mt: MT-72
:policy-number: 2399
:policy-title: Treatment of Resources by Resource Type Chart
:previous-policy-number: MT 64

include::partial$policy-header.adoc[]

== Requirements

The chart below lists resources alphabetically and provides the following information

* description of the resource
* the value to consider - cash value (CV), equity value (EV) or fair market value (FMV)
* whether the resource is included (I) or excluded (E) in the eligibility determination.

.Chart 2399.1 - Types of Resources
[#chart-2399-1,cols="2,2,1,2"]
|===
^|SOURCE/TYPE ^|DESCRIPTION/VALUE TO CONSIDER ^|FAMILY MEDICAID ^|ABD MEDICAID FBR/NON-FBR

| ABLE Account (Achieving Better Life Experiences)
| Tax-advantaged savings account for individuals with disabilities.
^| E
|FBR - E

NON-FBR - E

Treatment of Distributions, refer to xref:2334.adoc[]

|ACTION / DOMESTIC VOLUNTEER PROGRAMS (Unspent)
| Refer to <<chart-2399-3,Chart 2399.3>> in this section.
^| E
|FBR - E

NON-FBR - E

| AGENT ORANGE PAYMENTS
| Payments made to Vietnam Veterans who were exposed to Agent Orange and to surviving spouses and children of deceased Vietnam Veterans who were exposed to Agent Orange.

Refer to <<chart-2399-2,Chart 2399.2>> in this section.
^| E
|FBR - E

NON-FBR - E

|ANNUITIES (Supplemental Retirement Plans)
a| An investment plan.
It can be established as a supplemental retirement plan through an insurance company or other investment source.

[caption=Exception]
NOTE: Exclude if termination of employment or retirement is required for access and employment continues.

NOTE: If recurring payments are made from the annuity, refer to xref:2499.adoc#chart-2499-1[Chart 2499.1, Types of Income in Medicaid], and xref:2339.adoc[Section 2339 - Annuities].
^| I
|FBR AND NON-FBR

Treat as Retirement Funds if actuarially sound.

Refer to Sections xref:2332.adoc[] and xref:2339.adoc[Section 2339 Annuities]

Treat as a Trust if not Actuarially sound.

Some annuities may be a resource regardless of being actuarially sound.

Refer to sections xref:2336.adoc[], xref:2337.adoc[], xref:2338.adoc[], and xref:2339.adoc[]

|AUSTRIAN SOCIAL INSURANCE (unspent)
| Refer to <<chart-2399-2,Chart 2399.2>> in this section.
^| E
| FBR - Exclude If Based on Wage Credits.
Refer to <<chart-2399-2,Chart 2399.2>>.

NON-FBR - SAME

| BONDS
| Government-issued interest-bearing Certificates redeemable on a specific date, such as U.S. savings bonds, municipal, corporate, government bonds, etc.

CMV is a countable resource.

Refer to  xref:2310.adoc[Section 2310 Bonds - U.S. Savings].
^| I
|FBR - I

NON-FBR -I

| BURIAL CONTRACTS / BURIAL SPACE ITEMS
a| Prepaid contracts to cover funeral expenses For ABD Medicaid, refer to xref:2311.adoc[Section 2311 Burial Contracts (Pre-Paid or Pre-Need) and Burial Space Items].

{asterisk}For Family Medicaid, exclude up to $1500 of the combined EV of all burial contracts and burial insurance for each AU or BG member

NOTE: Only one burial contract per A/R, spouse or deemor may be excluded for ABD Medicaid.
^| *
| FBR & Non-FBR

Exclude burial space items owned outright or if itemized in a paid up burial contract designated for the A/R or an immediate family member.

| BURIAL FUNDS
| Funds that have been set aside for burial.

For ABD refer to xref:2312.adoc[Section 2312 Burial Funds].
| There is no burial exclusion in Family Medicaid.
| FBR & Non-FBR

Exclude up to $1500 for FBR and $10,000 for Non-FBR each for A/R and their spouse minus CMV of irrevocable burial contracts and FV of excluded life (term and whole) and burial insurance policies.

Refer to xref:2312.adoc[Section 2312, Burial Funds]


| BURIAL PLOTS
| One burial plot per AU or BG member

Use EV for each additional plot and count toward the resource limit.
^| E
| FBR & NON-FBR

Exclude only the plots owned by A/R or deemor that are designated for Immediate Family Members.
Refer to Section 2311.

| CASH
| Money held by an AU or BG member that has not been considered as income for that month.

Use CV.
^| I
| FBR - I

NON-FBR - I

| CERTIFICATE OF DEPOSIT
| Certificate that states that the named person(s) has a specific sum on deposit which accrues interest over a set period of time less any penalties for early withdrawal.

Use CV.
^| I
| FBR - I

NON-FBR - I

| CHECKING ACCOUNTS
a| An account on which checks may be written against amounts on deposit.

Use CV less any money considered income in that month.

[caption=Exception]
NOTE: Refer to the section on Jointly Owned Bank Accounts if a resource is jointly owned or ownership is disputed.
^| I
| FBR - I

NON-FBR - I

| COIN COLLECTIONS
| A collection of coins, regardless of age.

Use the face value of the coin collection as the cash value.
^| I
| FBR - I

NON-FBR - I

| COMMINGLED FUNDS
a| Excluded resources commingled with countable resources

NOTE: The portion of the commingled funds that can be identified as excluded resources retain the exclusion.
Funds that cannot be identified as excluded resources must be counted in their entirety.
^| I
| FBR - I

NON-FBR - I

| CONTINUING CARE RETIREMENT COMMUNITY (CCRC) OR LIFE CARE COMMUNITY FACILITY ENTRANCE FEES
| Continuing Care Retirement Community (CCRC) / Life Care Community (LCC) – An organization which offers a contract to provide an individual of retirement status, other than an individual related by consanguinity or affinity to the provider furnishing the care, with board and lodging, licensed nursing facility care and medical or other health related services, or both upon payment of an entrance fee.
The CCRC/LCC provides multiple residential options in one location like independent living, assisted living, and skilled nursing care.
^| N/A
a| FBR - I

NON FBR - I

Include only if all three conditions are met.

. The Entrance fee can be used to pay for the care, under the terms of the entrance care contract, should other resources of the individual be insufficient: and
. The Entrance fee (or remaining portion) is refundable when the individual dies or terminates the contract & leaves the CCRC or LCC: and
. The Entrance fee does not confer an ownership interest in the community.

| CREDIT UNION ACCOUNTS
| Money on deposit with a Cooperative organization with the functions of a bank (loans, money, provides checking & savings account services, etc.).

Use CV less any money considered income in that month.
^| I
| FBR - I

NON-FBR - I

| DEFERRED COMPENSATION PLANS
| Tax deferred income in a fund available only upon termination of employment, hardship or retirement

Use CV of any withdrawals from plan.

Withdrawals would be counted in both ABD and Family Medicaid as income.
^| E
| FBR - E

NON-FBR - E

| DEATH BENEFITS (unspent)
| Money in excess of last illness and funeral expenses.
^| E
| FBR - May be able to exclude temporarily.
Refer to Chart 2399.2.

NON-FBR - SAME

| DISASTER RELIEF ACT OF 1974 AND EMERGENCY ASSISTANCE ACT OF 1988
| Any governmental (federal, state, local) payments, which are designated for the restoration of a home, damaged in a major disaster or natural catastrophe.
This includes governmental payments to save lives, protect property and public health and safety or to lessen or avert the threat of a catastrophe or major disaster.

Includes loans and grants from the Federal Emergency Management Assistance (FEMA).
Includes payments made by the Department of Housing and Urban Development, disaster loans, family grant programs and grants made by the Small Business Administration as a result of disasters.
^| E
| FBR - Exclude Permanently

NON-FBR - SAME

| DIVIDENDS LEFT TO ACCRUE ON:

INVESTMENTS

LIFE INSURANCE
| Accrued dividends earned on financial investments, such as stocks, are resources separate and apart from the investment source.
^| E

E
| FBR - I

NON-FBR - I

FBR - Countable resources on all policies, including term and excluded policies separate and apart from CSV.

NON-FBR – Exclude on excluded policies.
Count on non-excluded policies.

.2+.^| EARNED INCOME TAX CREDIT (EITC)
a| Tax credit that is received in one of the following ways:

* Advance payments - tax credits received as part of the regular pay check
^| E
| FBR - N/A

NON-FBR - SAME
a|* Non-recurring lump sum-tax credits received in the form of an income tax refund
^| E
|FBR - N/A

NON-FBR - SAME

| EDUCATION ASSISTANCE (unspent)
| Unspent portion of payments for education assistance which is excluded as a resource.
^| E
| FBR – Exclude portions for tuition fees and other necessary expenses

NON-FBR - SAME

.2+.^| ENERGY ASSISTANCE OTHER THAN LIHEAA
| Payments or allowances made under any federal, state, or local law for the purpose of energy assistance.
^| E
| FBR - E

|Federal or State one-time assistance for weatherization or emergency repair or replacement of heating or cooling devices.

Energy Assistance payments made under state law.
^| E
| NON-FBR - E

| EQUIPMENT
| Tools, machinery, stock, and inventory essential to the production of goods or services, even during temporary periods of unemployment or inactivity.

Annually produces income consistent with FMV, even if only used on a seasonal basis.

Contact local realtors, local tax assessors, small business administration, etc. to determine prevailing rate of return.
^| E
| FBR - E

NON-FBR - E

| ESCROW ACCOUNT
| Money set aside for a particular purpose that cannot be used otherwise, usually held by a third party.

*For ABD, treat escrow accounts other than those set up as part of a homeplace mortgage as a trust.
Any remaining funds will be subject to Estate Recovery.
^| E
| *

| GERMAN REPARATION (unspent)
| Unspent German Reparation payments are permanently resources.
^| E
| E

| HEALTH REIMBURSEMENT ACCOUNT
| An account through an employer which may only be used to reimburse individuals for certain medical services.

*Do not count as a resource in month of receipt or for one calendar month following month of receipt.
^| *
| *

| HOMEPLACE
| The home and surrounding land occupied by the AU or A/R, if not separated by intervening property owned by others.

*Refer to xref:2316.adoc[Section 2316 - Homeplace: ABD Medicaid] if the A/R is absent from a homeplace located in another state or LA-D A/R owns a home valued at over $500,000.
^a| E

[.text-left]
Refer to xref:2317.adoc[ Section 2317 - Family Medicaid Homeplace.]
|*FBR - E

*NON-FBR - E

| HOME REPLACEMENT FUNDS (unspent)
| Proceeds from the sale of a home.
^| E*

*Exclude for up to 6 months.
| FBR and NON-FBR - Exclude for up to 3 months if A/R signs a statement of intent to buy a new homeplace.
Refer to <<chart-2399-2,Chart 2399.2>>.

| HOUSEHOLD / PERSONAL GOODS
| Household and personal effects or other belongings such as furniture, appliances, clothing, personal items or items required because of a disability
^| E
| E

For investment property or items that do not meet the definition of “personal property of HH effects”, see xref:2319.adoc[ Section 2319 - Household Goods and Personal Effects].

| HOUSEHOLD ITEMS OF UNUSUAL VALUE
| Items such as expensive silver, jewelry, stamps, guns, or other such collections.
^| E
| See Household/Personal Goods and Refer to xref:2319.adoc[Section 2319 - Household Goods and Personal Effects]

| INCOME TAX REFUND
| Monetary refunds paid to taxpayers from the state or federal government Count the total amount of the refund if the refund is for a single individual.
If the refund is a joint check for a jointly filed tax return, see Jointly Owned Resources in xref:2301.adoc[Section 2301 - Family Medicaid Resources Overview] for Family Medicaid and xref:2300.adoc[2300 - ABD Medicaid Resources Overview] for ABD Medicaid.

If any portion of the refund includes EITC, refer to Earned Income Tax Credit (EITC) in this section.
^| I
|FBR - E

NON-FBR - E

| INDIAN / ALASKAN NATIVE PAYMENTS
a| Payments to Native Americans based on federal statutes.
Examples of these statutes include, but are not limited to, the following:

* Alaska Native Claims Settlement Act
* Sac and Fox Indian claims
* Indian Tribal Payments under PL 94-114, Section 6.
* Grand River Band of Ottawa Tribal payments under PL 94-540
* Public Law to the Confederated Tribes and Bands of the Yakima Indian and Apache Tribe of the Mescalero
* Payments made under the Maine Indian Claims Settlement Act of l980
* Navajo or Hopi Indian pursuant to PL 93-531
* Indian Child Welfare, Public Law 95- 608
^| E
| FBR - E

NON-FBR - E

| INDIAN GAMBLING ACT
| Tribal distribution to individuals on a per capita basis from tribally managed gaming revenues.

*Exclude only if held in trust by the Sec. of the Interior prior to distribution.
^| *
| *

.2+.^| INDIVIDUAL DEVELOPMENT ACCOUNT (IDA)
| An account established by or on behalf of a TANF A/R for post-secondary educational expenses, first purchase of a home or to start a new business.
Exclude funds up to $5000, including funds withdrawn and used for the stated purpose.
^| E
| FBR - E

NON-FBR - E
|At the point, the owner of the IDA is no longer a TANF recipient, the IDA becomes a countable resource in the FS and Medicaid programs.
^| I
|


| INHERITANCES AND UNPROBATED ESTATES AND WILLS
| An ownership interest in an unprobated estate may be a resource.
| Refer to Section 2320.
| FBR - Refer to Section 2320.

NON-FBR - SAME

| INSTALLMENT CONTRACTS / AGREEMENTS
(for sale of land or buildings)
a| A written agreement with specific stipulations for the sale of land or buildings and the contract/agreement produces income consistent with its FMV.

NOTE: The property sold under the contract or held as security in exchange for a purchase price consistent with the FMV of the property is also excluded.
^| E
| FBR - E

NON-FBR - E

| JAPANESE/ALEUTIAN RESTITUTION
| Restitution payments made by the U.S. Government to Japanese Americans and Aleutians or their survivors who were interned or relocated during WWII.
^| E
| FBR - Exclude Permanently

NON-FBR - SAME

| KEOGH PLAN (owned by individual)
a| A retirement plan

Consider the total CV of the funds in the retirement plan, minus the early withdrawal penalty.

NOTE: If the plan is owned by more than one person, refer to Jointly Owned Resources in xref:2301.adoc[] for Family Medicaid and xref:2302.adoc[].
^| I
| FBR - I

NON-FBR - I

| KEOGH PLAN (owned with others)
| If the plan contains a contractual agreement with an individual whose resources will not be considered in determining eligibility, the funds are considered inaccessible to the AU.
^| E
| Refer to Sections 2332 and 2334

| LIFE INSURANCE
| Insurance policy which pays a beneficiary on the death of an individual.

Refer to xref:2323.adoc[].
^| E
| FBR - Exclude CSV only if combined FV's of all policies is $1500 or less & is part of a burial exclusion.

Count accrued dividends on all policies.

NON-FBR - Exclude CSV only if combined FVs of all policies (whole & term) is $10,000 or less & is part of a burial exclusion.

Exclude accrued dividends on exempt policies as long as dividends are left to accrue.
Count as income dividends paid out.

| LIFE INTEREST / LIFE ESTATE AND REMAINDER INTEREST
| Property that an individual has a right to use but not dispose of during his/her life.

Consider any income received from the property.

* Refer to xref:2322.adoc[] for purchasing life estate in another's property.
^| E
| FBR - I

* NON-FBR - Exclude life estates, treat remainder interests as transfers.

.2+.^| LIVESTOCK/PETS
a| Animals owned for the following purposes:

* For purposes of feeding AU members
* Producing income at its FMV
* Used to assist a disabled individual.
^| E
| FBR - E

NON-FBR - E

a|
* Not producing income consistent with FMV
* Used for recreational purposes and no income is derived.

Count EV.
^| I
| FBR - I

NON-FBR - E

| LOANS FROM OTHERS - BORROWER
| Money received by the AU or A/R that the AU or A/R has an obligation to repay.

*Considered as a resource month after receipt, if retained; may be counted as income.
^| E
| * I

* Refer to xref:2347.adoc[]

| LOANS TO OTHERS - LENDER (NOTES RECEIVABLE)
| Monies loaned to persons outside the AU where a repayment agreement exists.

Count CV of any money owed to the AU.

*Refer to xref:2313.adoc[] for resource treatment. May be counted as income.
^| I
| *

| LOW INCOME HOME ENERGY ASSISTANCE ACT LIHEAA
| Payments for home energy provided to, or indirectly on behalf, of an AU.
^| E
| E

| LUMP SUMS
a| Money received in the form of a lump sum that is not expected to recur, i.e. rebates, retroactive or corrective payments for prior months, insurance settlements, federal or state tax refunds.

{asterisk}Count as income in the month of Receipt (refer to Income Chart 2499.1, Lump Sums).
Any remainder is counted as a resource beginning the month after receipt.

NOTE: Tax refunds do not count as income, at all.
Any remainder is counted as a resource beginning the month after receipt.
^| *
| *

| LUMP SUM / SSI BACK PAYMENTS
| Payments for previous SSI benefits owed and paid to an individual who is currently receiving SSI.

Payment for previous SSI benefits owed and paid to an individual who is no longer receiving SSI.

*Family Medicaid: Disregard as income/resource in the month of receipt and any remainder in the month after receipt.
Any remainder after these two months is counted as a resource.
^| *
| Refer to Section 2324 for ABD Medicaid.

| NON-HOMEPLACE, REAL PROPERTY
a| Buildings and lands which are owned by the AU and not considered part of the homeplace.

{asterisk}Count Equity value unless successfully rebutted or partially or totally excluded due to the following:

* Essential to self-support
* Undue hardship to co-owner
* Bona fide effort to sell
* Undue Hardship provision
^| I
| *FBR - I

*NON-FBR - I

| NON-LIQUID RESOURCES FOR WHICH A BONA FIDE EFFORT TO SELL IS BEING MADE
|
^| E
| FBR - E

NON-FBR - E

a|
[%hardbreaks]
PASS ACCOUNT
(Plan to Achieve Self- Sufficiency)
a|
Money deposited in a bank account to be used for an SSI individual in a plan for self-sufficiency approved by the SSA.

NOTE: The interest earned from a PASS account is disregarded as income.
^| E
| FBR - E

NON-FBR - E

| PATIENT FUND ACCOUNTS
| Funds held by a nursing home for their residents
^| N/A
| FBR - N/A

NON-FBR - I

.2+.^| PENSION PLAN (including 401K plans)
| A retirement plan provided by an employer

Exclude as inaccessible if still Employed under the plan.
For ABD Medicaid, exclude if termination of employment is required in order to receive benefits.
^| E
| FBR & NON-FBR - Exclude if receiving periodic payments.
Otherwise, count as a resource.
Exclude accounts owned by a ineligible spouse, parent or spouse of parent.
Refer to Section 2332.

|If funds are withdrawn, consider the cash value.
^| I
|N/A

.3+.^| PERSONAL PROPERTY - equipment, tools, machinery, stock and inventory essential to the production of goods or services, even during temporary periods of unemployment or inactivity
a|
If annually produces income consistent with FMV, even if only used on seasonal basis.

Contact local tax assessors, small business administration, etc. to determine prevailing rate of return.
^| E
| E

a|If essential to employment or self-employment of an AU member

NOTE: Value retains exclusion for one year from date the AU member terminates self-employment from farming.
^|E

|E

|Does not produce income consistent with FMV or is not essential to employment or self-employment.

Consider the equity value.
^|I
|I

| PREPAYMENTS AND DEPOSITS (NH)
| Made on behalf of a person who enters a nursing home and refunded when approved for Medicaid.
^| N/A
| FBR - N/A

NON-FBR - I

(unless refund is made to someone other than the A/R)

| PROPERTY ESSENTIAL TO SELF-SUPPORT
a|
* Business
* Goods/Service for Home Consumption
* Non-Business Income Producing
^| E
| FBR and Non-FBR - Exclude up to $6000 of EV if net earnings are at least 6% of the amount being excluded as a resource

| PUBLIC LAW 103-286
| Payments to individuals received as a result of their status as victims of Nazi persecution
^| E
| E

| Qualified Tuition Savings Programs (529 Plans)
| A savings plan for higher education.
Refer to xref:2344.adoc[].

* E - if A/R is beneficiary

* I - if A/R is donor
^| *
| *

| Qualified Income Trust (QIT)
| Income placed in a QIT is not considered in the income eligibility determination process for LA-D COAs.
See Section 2407 for any exceptions.
^| N/A
| LA-D - Exclude

Other COAs use OBRA '93 Trust provisions.

| RELOCATION ASSISTANCE (unspent)
| Refer to xref:2330.adoc[].
^| E
| FBR - Exclude up to 9 months (exclusion ends 5/94)

NON-FBR - SAME

| REPAIR/REPLACEMENT FUNDS
| Refer to xref:2331.adoc[].
^| E
| FBR - Exclude up to 18 months.
Refer to <<chart-2399-2,Chart 2399.2>> and <<chart-2399-3,2399.3>>.

NON-FBR - SAME

| RESOURCES OF AN SSI RECIPIENT
a| AN SSI recipient is a person who:

* Has been approved to receive benefits
* Receives benefits
* Is approved for/or receiving benefits but the benefits are suspended, being recouped because of an overpayment or not paid because the amount is less than the maximum issuance amount
^| E
a| N/A

[caption=Exception]
NOTE: Include resources of the SSI spouse in the couple eligibility budget when one member of the couple is an ABD Medicaid A/R and the other receives SSI.

.2+.^| RETAINED CASH AND INKIND PAYMENTS
| If used by the AU for vacation purposes during the year annually produces income consistent with FMV or produces income consistent with FMV.

Refer to xref:2410.adoc[], for treatment of income from rental property for ABD Medicaid.
^| E
| FBR - E

NON-FBR - E

| Does not annually produce income consistent with FMV.

Consider EV.
^| I
| FBR - I

NON-FBR - I


| RETIREMENT FUNDS
|*Refer to Pension Plan on page 2399-12 of this chart, xref:2332.adoc[] and xref:2334.adoc[].
^| *
| FBR - *

NON-FBR - *

| RETIREMENT ACCOUNTS (including IRAs/Keoghs)
| *Refer to Pension Plan on page 2399-12 of this chart and xref:2332.adoc[].
^| *
| FBR - *

NON-FBR - *

| REVERSE MORTGAGE
| Allows a homeowner to borrow, via a mortgage contract, some percentage of the appraised value of their home.
A periodic payment or line of credit is received and does not have to be repaid as long as the borrower resides in the home.

Often referred to as a Reverse Annuity Mortgage (RAM).
^| E
| FBR - Treat as a loan and count if retained in the month following the month of receipt.
Refer to Section 2405.

NON-FBR - SAME

| SAFE DEPOSIT BOX
| Secure storage in a bank or other institution where money and other valuables may be deposited.

Obtain a list of items that are in the box from the A/R.
Count cash value of the items unless otherwise excluded.
Refer to xref:2333.adoc[].
^| I
| FBR - Inventory required

NON-FBR - Inventory required only if statement of contents is questionable.

| SALE-LEASEBACK
| When a homeowner transfers title of the home to a buyer (e.g. an individual or financial institution) in exchange for an installment note satisfied by monthly payments.
The installment note may bear interest.
The buyer, in turn, allows the former homeowner to remain in the home for life (or until the arrangement is terminated) in exchange for rent.
Under this arrangement, the buyer is responsible for the payment of real estate taxes, major maintenance, and casualty insurance.
^| N/A
| FBR - Treat as the conversion of a resource and count if retained into the month following the month of receipt.
If the buyer pays taxes, insurance or for repairs on the home the value of these items is not ISM to the former homeowner.
Refer to Section 2405.

| SAVINGS ACCOUNTS
| Monies held in an interest-bearing account.

Count CV.
Refer to xref:2334.adoc[].
^| I
| FBR - I

NON-FBR - I

.2+.^| SECURITY DEPOSIT ON RENTAL PROPERTY OR UTILITIES
| Cash held by the provider and not accessible to the AU.
^| E
| FBR - E

NON-FBR - E

|Cash returned to the AU.
Resource in month received.
^|I
|FBR - I

NON-FBR - I

| SPENDING ACCOUNT
| Funds which are held in an account to pay certain expenses such as child care or medical expenses
^| E
| E

| SSA DIRECT EXPRESS ACCOUNT
| Direct Express is a debit card one can use to access their SSA benefits.

*Any balance carried over into the next month.

Refer to NOTE in xref:2334.adoc[].
^| I*
| FBR - I*

NON FBR - I*

| STABLE ACCOUNT (Georgia STABLE account, ABLE account)
| Refer to ABLE account
|
|

| STOCKS AND MUTUAL FUNDS
| A certificate which verifies ownership of shares in a company

Consider CV.

Verify the value of stock via the internet, the newspaper, or a broker.
Refer to xref:2335.adoc[].
^| I
| FBR - I (Refer to retirement funds in this chart for special instructions)

NON-FBR - I

| SUSAN WALKER V. BAYER CORPORATION SETTLEMENT PAYMENTS
| Cash settlement from a lawsuit
^| E
| FBR - E

NON FBR - SAME

.6+.^| TRUSTS
a|
Any funds in a trust or transferred to a trust and the income produced by that trust.

If the:

* Trust arrangement can be revoked by an AU member
^|I
.6+.^|Refer to xref:2335.adoc[], xref:2337.adoc[], xref:2338.adoc[], and xref:2339.adoc[].

a|* Beneficiary's name can be Changed during the POE
^|N/A
a|* Trust arrangement can cease during the POE
^|N/A
a|* Trustee administering the fund is a court, an institution, corporation or organization which is not under the direction or ownership of any AU member
^|E
a|* Trustee appointed by the court has court imposed limitations placed on the funds
^|E
a|* Trust investments made on behalf of the trust do not directly involve or assist any business or corporation under the control, direction, or influence of an AU member.
^|E

| TRUST PROPERTY MEDICAID QUALIFYING Prior to OBRA ‘93
| For description, refer to xref:2336.adoc[].
^| N/A
| FBR - Count as a resource any portion available to but not received by the A/R.
Develop a transfer of resources for any portion unavailable to the A/R.

Undue hardship may be considered.

NON-FBR - SAME

| TRUST PROPERTY NON-MEDICAID QUALIFYING
| For description, refer to xref:2337.adoc[].
^| N/A
| FBR - Exclude unless the A/R is legally empowered to revoke and use the funds.

NON-FBR - SAME

| UNIFORM GIFTS TO MINORS
| For description, refer to xref:2340.adoc[].
^| N/A
| FBR - Exclude until age 21.
If the A/R is a donor develop for a transfer of resources.

NON-FBR - SAME

| UNIFORM LOCATION ASSISTANCE AND REAL PROPERTY ACQUISITION
| Reimbursements received under PL 91-646, Section 210
^| E
| E

| UNITED STATES AUTOMOBILE ASSN. SUBSCRIBER'S SAVINGS ACCOUNT (USAA)
| Considered a reciprocal Interinsurance exchange.
May be in the form of a homeowner's insurance policy.
^| I
| I

| VEHICLES
| For description, refer to xref:2308.adoc[].
| Refer to xref:2308.adoc[].
| Refer to xref:2308.adoc[] for how to count in FBR and Non- FBR

| VICTIMS COMPENSATION (unspent)
| For description, refer to xref:2341.adoc[].
^| E
| FBR - Exclude up to 9 months.
Refer to chart 2.

NON-FBR - SAME

| WIC (Women Infants & Children Special Supplemental Food Program)
| Vouchers which are redeemable for food items received by certain women and children considered to be nutritionally high risk.
^| E
| E
|===

Use the following chart to determine the resource treatment of income retained after the month of receipt

.Chart 2399.2 - Resource Treatment of Income Retained After the Month of Receipt
[#chart-2399-2]
|===
| Type of Income | Treatment for Month of Receipt | Month Any Portion Retained Becomes a Resource | Treatment of Any Interest Earned

| Agent Orange
| Exclude as income.
| Never
| Count as income and resource.

| Austrian Social Insurance
| Exclude as income.
| Never
| Count as income and resource.

| Cash or In-Kind Payment to Replace or Repair Excluded Resources
| Exclude as income.
| 10^th^ month after month of receipt (exclude 9 calendar months).
Exclude an additional 9 months if reason for retention is beyond A/R's control.
| Exclude as income and resource during exclusion period.

| Death Benefits
| Count payments in excess of last illness and funeral expenses as income for the month of receipt.
| 2^nd^ month after month of receipt if last illness and burial expenses are unpaid at receipt.

1^st^ month after month of receipt if last illness and burial expenses paid before receipt.
| Count as income and resource.

| Disaster Assistance
| Exclude as income.
| Never
| Exclude as income and resource.

| Earned Income Tax Credit (EITC)
| Exclude as income.
| 10^th^ month after month of receipt (exclude 9 calendar months after month of receipt)
| Count as income and resource.

| German Reparation
| Exclude as income.
| Never
| Count as income and resource

| Home Replacement
| Exclude as income.
| ABD: 4^th^ month after sale of home if A/R signs a statement of intent to buy a new home within 3 months.

Family Medicaid: 7^th^ month after sale of home.
| Count as income and resource.

| Japanese or Aleutian Restitution
| Exclude as income.
| Never
| Count as income and resource.

| Relocation Assistance
| Exclude as income.
| 10^th^ month after month of receipt or until May 1994 (exclude 9 calendar months)
| Count as income and resource.

| SSI and RSDI Lump Sums
| Unearned Income.
| ABD Medicaid - 10^th^ month after month of receipt (exclude 9 calendar Months after month of receipt)

Family Medicaid - 7^th^ month after month of receipt (exclude 6 calendar months after month of receipt)
| Count as income and resource

| Victims Compensation
| Exclude as income.
| 10^th^ month after month of receipt or until May 1994 (exclude 9 calendar months)
| Count as income and resource.
|===

NOTE: Except for SSI and RSDI lump sums, the above unspent resources must have met the exclusion from income requirements in order to be excluded from resources.

NOTE: Refer to xref:2305.adoc[].

Use the following chart to determine if a benefit can be excluded from income and resources under Federal statutes other than Title XVI:

.Chart 2399-3 - BENEFITS EXCLUDED FROM BOTH INCOME AND RESOURCES BY A FEDERAL STATUTE OTHER THAN TITLE XVI
[#chart-2399-3]
|===
a|Unspent funds or assets from the following sources are excluded resources:

* Action programs and domestic volunteer services
 ** Volunteers in Service to America (VISTA)
 ** University Year for Action (UYA)
 ** special and demonstration programs
 ** retired senior volunteer programs
 ** foster grandparent programs
 ** senior companion program

* Low income energy assistance - Any assistance in any form, if provided under the federal Low-Income Home Energy Assistance Program (LIHEAP), such as cash, vouchers, in-kind.

* Federal housing assistance - Any assistance in which the Department of Housing and Urban Development (HUD) or the Farmers Home Administration (FHA) is involved, such as cash for utilities, rental subsidies, etc.

* Food programs with federal involvement
 ** USDA Food Stamps and USDA commodities
 ** School lunches and breakfasts
 ** WIC
 ** Nutrition and other programs for older Americans under Chapter 35 of Title 42 of the US code
 ** Meals furnished at senior citizens centers
 ** Meals on wheels
 ** Anything other than wages and salaries

* Educational assistance.
|===
