= 2398 Estate Recovery for ABD Medicaid
:chapter-number: 2300
:effective-date: July 2022
:mt: MT-65
:policy-number: 2398
:policy-title: Estate Recovery for ABD Medicaid
:previous-policy-number: MT 64

include::partial$policy-header.adoc[]

== Requirements

Estate Recovery is part of a federal program that was established with the Omnibus Budget Reconciliation Act of 1993, OBRA '93.
Beginning May 3, 2006, Georgia is implementing Estate Recovery (ER) to be in compliance with this mandate.

== Basic Considerations

Georgia will limit its ER to Medicaid monies that paid for an A/R's medical care beginning May 3, 2006, or the first month of Medicaid eligibility, whichever is later.

ER will be pursued under the following conditions:

* A/R's total estate is valued at $25,000 or more.
Effective July 1, 2018, the State will waive estate recovery of the first $25,000 of estates valued over $25,000 for any estate subject to an Estate Recovery claim for the deceased Medicaid Member with a date of death on or after July 1, 2018.
* Medicaid members who, at any age, are in a nursing facility, intermediate care facility for the mentally retarded, or other mental institution that have their medical care paid by Medicaid.
* Medicaid members who are 55 years of age or older and who receive home and community-based services or are enrolled in and receive services through a waiver program.
* Currently active A/Rs (including SSI recipients) who are not disenrolled from institutionalized Medicaid for NH, EDWP/Source, NOW/COMP, ICWP, Hospice, Institutionalized Hospice by May 3, 2006.
* An A/R who disenrolls to avoid ER and subsequently reapplies for Medicaid under one of the above COAs, will have ER pursued back to date of first Medicaid eligibility or May 03, 2006, whichever is later.

NOTE: Effective July 1, 2018, each member and/or authorized representative MUST receive notification of Estate Recovery at application and each renewal via first class mail or electronic means.

The A/R's assets that may be subject to ER are:

* All real estate property, including homeplace property
+
NOTE: A Life Estate does not exempt the estate or home from ER
* All personal property, whether held individually or jointly
* If a transfer penalty period was not completed due to the A/R's death, the value of the remaining penalty period may be recovered.

If the A/R or spouse transfers assets to avoid ER, a transfer penalty may be applied.
Refer to xref:2342.adoc[].

When the Medicaid A/R dies, the authorized representative, executor or heirs will be notified by DCH or their agent before any recovery is attempted on the A/R's estate.
Upon notification, the heirs of the estate will be given an opportunity to show if they meet one of the exceptions in the law that will delay ER, then they will be told by DCH how to request an undue hardship waiver.

Recovery may be delayed if the deceased A/R has:

* a surviving spouse,
* child(ren) under 21 years of age (for the delay to continue past age 21, the child must have become disabled prior to reaching age 21),
* child(ren) who are blind or permanently and totally disabled according to Social Security guidelines,
* a sibling of the A/R who was residing in the A/R's home for a least one year immediately prior to the A/R's date of institutionalization, and who was providing such care to the A/R that institutionalization was delayed or
* a child of the A/R who was residing in the A/R's home at least two years immediately prior to the A/R's date of institutionalization, and who was providing such care to the A/R that institutionalization was delayed

If ER is delayed under one of the above deferments, that delay is valid until one of the below things occur:

* the surviving spouse is deceased or divorced
* the child(ren) turns 21 years of age or decease, whichever occurs first
* the blind or totally disabled child(ren) decease
* the sibling/child deceases or no longer resides in the home

.Chart 2398.1 - ABD Medicaid Estate Recovery
[#chart-2398-1,cols=4*]
|===
2+^h| Who is Affected:
.2+.>h| Delay of Recovery Criteria:
.2+.>h| What will be Recovered:

^h| Age
^h| COAs

| Any age & institutional facility

a|
* L01(NH)
* L01(NH) with IC-MR LOC

.2+a|
* Surviving spouse
* Child(ren) under 21
* Child(ren): blind/disabled

.2+a|
* Homeplace
* Any other assets including joint tenancy & life estate

| Age 55 or older

a|
* W01(IH), W02(Hospice), W03(EDWP), W04(NOW), W05(ICWP), W07(COMP)
* L01(NH)
|===

== Procedures

Follow the steps below for Estate Recovery:

[horizontal,labelwidth=10]
Step 1:: Be prepared to answer questions regarding Medicaid eligibility/transfer penalty and make proper referrals, especially lien related questions, to Estate Recovery Office at 770-916-0328.

Step 2:: New applicants of L01(including Swing Bed),W01-W05, W07 Medicaid who have submitted their application via Form 700 (10-2021 or later) or via Provider Portal or Gateway Customer Portal (12/2021 or later) are considered to have met the ER acknowledgment requirement.
New applicants who have submitted an application using other means than what is listed above will require Form 315, Official Notice of Georgia Medicaid Estate Recovery Program, to be mailed via first class mail or via electronic means.
If requested, the DCH Estate Recovery informational brochure can be mailed to the client.
You can download the brochure at https://dch.georgia.gov or you can order by calling Health Management Systems (HMS) at 770-916-0328.
Refer to Appendix F (Forms), for a copy of xref:attachment$form-315-es.docx[DMA 315].
It is preferable to get the form returned and housed in the document imaging system.
However, if the A/R or representative does not return the document, document in Georgia Gateway that the 315 was sent and the date.
If Form 315 is not acknowledged at application, Form 315 will be system generated every 90 days until the form has been signed and returned by client/AREP.

Step 3:: A/Rs or AREPs who receive the DMA 315 and have notified the Eligibility Specialists of A/R's death should be closed due to death.
However, fax/email Form xref:attachment$form-327.docx[DMA 327], Estate Recovery Notification Form to Estate Recovery.
Refer to Appendix F, Forms for a copy of xref:attachment$form-327.docx[DMA 327].
Check that the closure is due to death.

Step 4:: Applicants who notify DCFS of their desire to withdraw from L01/W01-W05, W07 Medicaid, based on their intent to avoid Estate Recovery:

* Should have their case denied, but not earlier than 5/3/06.
* WAIVE Timely Notice Period.
* Close case in Georgia Gateway using Voluntary Closure.
* On Program Request Details, enter a termination date of the effective date of closure, but not earlier than 5/3/06.
Enter a discharge date on the appropriate ABD Medicaid screen if A/R physically leaves the facility or home and community-based waiver programs.
* Enter additional text on the closure notice stating, “AU closed at your request to avoid estate recovery.” Add this text to each case denied.
* Email/fax Form DMA 327, Estate Recovery Notification Form, only if the AU was approved for any months, within 5 days of receipt/notification, to Estate Recovery at the address/fax on the form.
Document in case notes date sent and why and reason for termination of vendor payment.
* Maintain a copy of the DMA 327 in the document imaging system.

Step 5:: When any recipient who meets or at one time did meet (on/after 5/3/06) the criteria for estate recovery dies or his/her Medicaid terminates, send in a Form xref:attachment$form-327.docx[DMA 327] to the Estate Recovery email address/fax.
Maintain a copy of the 327 in the document imaging system and retain for three years.

[NOTE]
====
When communicating upon death of AR, withdrawal, or termination of Medicaid, email or fax Form 327 to Gainwell Technologies.

[%hardbreaks]
Email address: gaestaterecoverydept@gainwelltechnologies.com
Phone 770-916-0328
Fax 678-569-0066
====
