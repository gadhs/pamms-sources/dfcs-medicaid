= 2925 The Food Stamp Program (DHS/DFCS)
:chapter-number: 2900
:effective-date: June 2020
:mt: MT-60
:policy-number: 2925
:policy-title: The Food Stamp Program (DHS/DFCS)
:previous-policy-number: MT 1

include::partial$policy-header.adoc[]

== Requirements

The purpose of the Food Stamp Program is to promote the well-being of the state's population by raising the level of nutrition among low-income assistance units.

The Food Stamp Program provides monthly benefits through Electronic Benefit Transfer (EBT) to low income families to help pay for the cost of food.

== Controlling Legislation

The Food Stamp Program is authorized by the Food Stamp Act of 1977.
The eligibility provisions of the Act are further developed in Title 7, Code of Federal Regulations, parts 210 through 299.
The Food Stamp Program is administered by the Food and Nutrition Service under the United States Department of Agriculture.
Title IV of the 2008 Farm Bill renames the Food Stamp Program as the Supplemental Nutrition Assistance Program (SNAP).

== History

The legal basis for the Food Stamp Program is the Food Stamp Act of 1977, the Omnibus Reconciliation Act of 1981, and the Food Security Act of 1964.
Benefits are funded 100 percent by the federal government, and administrative costs are shared by the state and federal governments on a 50-50 basis.

== Basic Considerations

Applications for Food Stamps are accepted at the local DFCS office and Social Security Administration.
Applications taken at the Social Security Administration are forwarded to DFCS for processing.
