= 2852 Medicaid Application Processing for Out of State Children Placed In Georgia
:chapter-number: 2800
:effective-date: January 2021
:mt: MT-63
:policy-number: 2852
:policy-title: Medicaid Application Processing for Out of State Children Placed In Georgia
:previous-policy-number: MT 45

include::partial$policy-header.adoc[]

== Requirements

Children from other states may be living in Georgia due to foster care placements or adoptive parents moving to Georgia with children receiving adoption assistance from other states.

== Basic Considerations

The Medicaid determinations require special considerations not necessary for children receiving Georgia foster care or adoption assistance.

The application for ICPC foster care Medical Assistance will come either from the SSCM or from the supervising provider, and the ICAMA application for adoption assistance MA may come from the State Adoptions Unit or the adoptive parent(s).

The adoptive family may apply for Medicaid through any County DFCS office or Gateway Customer Portal.

The SOP for these cases is the same as for Georgia children.

== Procedures

=== Authorizing Medicaid For Out-of-State IV-E FC Child Residing in Georgia

Follow the procedures below to authorize Medicaid for an out-of-state IV-E FC placed in Georgia when the ICPC worker assigned in the county initiates an FCC stage in SHINES.
SHINES generates a Medicaid application that is signed, saved and submitted and interfaces with the Gateway system.

The State Office ICPC unit establishes IV-E FC eligibility by verifying the following:

* The child receives IV-E FC per diem from the state of origin.

* The child is currently residing in Georgia in an approved foster care placement.
Verify the date of the move.

* The child is under age 18.
The child's DOB on the Medicaid card of the out- of-state origin is sufficient verification.

* Obtain the child's Social Security number.

* All ICPC documentation, including the child's IV-E determination, should be uploaded into SHINES under external documents.

* The Rev Max Medicaid Unit will register the MA application and assign to the appropriate regional Rev Max unit to process.

Some children are placed not under the supervision of GA DFCS, but under the supervision of a private agency.
In these cases, the private agency will apply for Medical Assistance using the Form 94 (application for Medical Assistance).

Private agency will email the following information to the Regional Rev Max Supervisor:

* Form 94 including child's name, DOB and SSN
* Copy of the Form 100A indicating the child's IV-E Eligibility from state of origin

Continue Medicaid until the child is no longer IV-E eligible;
has reached age 18; is no longer living in Georgia or until the SSCM/Private Agency requests case closure.

Complete reviews of the child's eligibility for IV-E FC Medicaid in Georgia annually based on information from the sending state.

NOTE: The IV-E determination and per diem payments remain the responsibility of the state with legal custody.

=== Authorizing Medicaid For an Out-of-State IV-B funded Child Placed in Georgia

A foster child in the custody of another state and funded through IV-B is ineligible for Georgia Medicaid under any COA.
The child is considered a legal resident of the state that retains custody and will not meet the residency criteria for Georgia Medicaid.
The SSCM will assist the other state in locating Georgia providers who will accept the other state's Medicaid coverage.

=== Authorizing Adoption Assistance Medicaid For Children from Another State

Under COBRA Reciprocity, Medicaid coverage for a IV-E or State Adoption Assistance child is available in any state that has signed the COBRA agreement.
The RMS must verify that the paying state has signed the COBRA agreement through the State Adoption Unit.

The IV-E or State AA payments remain the responsibility of the state of origin, but Medicaid coverage is the responsibility of the state of residence.
Medicaid covered services for the AA child are based on the coverage available in the state of residence, not the state of origin.

=== Request from Adoptive Family at a County DFCS Office

Follow the procedures below for an out-of-state AA child residing in Georgia when the request comes from the adoptive family at a county DFCS office or through the Gateway Customer Portal.
The MA application will be assigned to Rev Max for an eligibility determination.

Verify that the child is a recipient of IV-E or State Adoption Assistance.
Use the current certified copy of the approved adoption assistance agreement from the state of origin.
Make a copy of the agreement for the Georgia file.

NOTE: The adoptive parents must be able to provide this agreement and other documents showing IV-E or State AA eligibility in the other state.

Establish that the child is under age 18 for IV-E Adoption Assistance and under 21 for state adoption assistance.

NOTE: The child's birth date on his previous state's Medicaid card is sufficient or the written statement from the other state as to the child's date of birth is acceptable.

Establish that the child resides in Georgia and document the date the child moved to Georgia.
Obtain the child's Social Security number.

NOTE: The information may be obtained from the parents or the state of origin.

SSAU has responsibility for establishing the case in SHINES and submitting a Medicaid application to the appropriate Rev Max Regional Unit for any out of state Adoption Assistance child residing in Georgia.

Rev Max will determine Medicaid eligibility and the appropriate Adoption Assistance Class of Assistance.

OFI staff should not process the Adoption Assistance child for MA but may include the child for other benefits if the family applies for any assistance that would include the adoption assistance child.

The RMS will notify SAU of the Medicaid determination.

=== Request from the Office of Adoptions

Follow the procedures below to activate Medicaid for an out of state AA child residing in Georgia when the request comes from the State Adoption Unit (SAU).

SSAU will send a SHINES MA application to the appropriate Rev Max Regional Unit requesting a MA determination for an out of state AA Child residing in Georgia.
The SHINES Medicaid application will include whether the child is receiving IV-E or State funded AA, the name of the other State and identifying information on the child and adoptive parents.

Determine Medicaid eligibility under the appropriate Adoption Assistance Class of Assistance.

A Rev Max Specialist will complete the SHINES funding summary as notification of the Medicaid determination and complete the application in Gateway.

NOTE: Medicaid eligibility in Georgia begins the first month of Georgia residency, regardless of Medicaid status in the state of origin.

=== Adoption Assistance Medicaid For a GA Child Residing in Another State

When a child receiving IV-E or State Adoption Assistance from Georgia moves to another state, the child's Georgia Medicaid is terminated effective the month after the move.
Medicaid for the child should be applied for in the other state.

NOTE: A child receiving Adoption Assistance from Georgia but residing in another state may request to continue receiving Georgia MA in order to continue services from a GA.
provider under a care plan.

=== Guardianship Assistance Program (GAP)

A child receiving IV-E GAP assistance payments is categorically eligible for Medicaid in the child's State of residence regardless of whether the State of residence covers the Guardianship Assistance Program under its IV-E State plan.

The SHINES Medicaid application will be submitted by the State Adoption Unit with copies of all documentation forwarded to the appropriate RevMax Regional Unit.
Processing follows ICAMA IV-E Adoption Assistance processing in SHINES and Gateway and documented as Guardianship Assistance Program.
