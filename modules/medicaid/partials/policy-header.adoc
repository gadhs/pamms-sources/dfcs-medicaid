[.policy-header%noheader,cols=5*]
|===
.5+^.^a|image::ga-state-seal.svg[Georgia State Seal,680,684]

4+^s|Georgia Division of Family and Children Services +
Medicaid Policy Manual

s|Policy Title:
3+|{policy-title}

s|Effective Date:
3+|{effective-date}

s|Chapter:
|{chapter-number}
s|Policy Number:
|{policy-number}

s|Previous Policy Number(s):
.>|{previous-policy-number}
.>s|Updated or Reviewed in MT:
.>|{mt}
|===
